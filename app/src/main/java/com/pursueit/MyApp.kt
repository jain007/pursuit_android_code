package com.pursueit

import android.app.Application
import android.provider.Settings
import com.androidnetworking.AndroidNetworking
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.Tracker
import java.util.*


class MyApp: Application() {

    companion object {
        var IS_APP_IN_OPEN_STATE=false
        var ANDROID_ID = ""
        private var sTracker: Tracker? = null
        private var sAnalytics: GoogleAnalytics? = null

        @Synchronized
        fun getDefaultTracker(): Tracker? {
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            if (sTracker == null) {
                sTracker = sAnalytics?.newTracker(R.xml.global_tracker)
            }

            return sTracker
        }
    }

    override fun onCreate() {
        super.onCreate()

        AndroidNetworking.initialize(applicationContext)

        val locale = Locale("en")
        Locale.setDefault(locale)

        ANDROID_ID = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

        sAnalytics = GoogleAnalytics.getInstance(this)

    }



}
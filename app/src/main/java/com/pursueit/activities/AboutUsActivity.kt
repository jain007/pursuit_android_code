package com.pursueit.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.BuildConfig
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.model.UserModel
import com.pursueit.utils.MyAppConfig
import com.pursueit.utils.MyNavigations
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import java.util.concurrent.TimeUnit

class AboutUsActivity : AppCompatActivity() {

    lateinit var dialogMsg: DialogMsg
    var compositeDisposable=CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        compositeDisposable= CompositeDisposable()
        init()
        dialogMsg = DialogMsg()

        fastFetchValues()

        onClickListeners()
    }

    fun test(ok:()->Unit){

    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this,MyGoogleAnalytics.ABOUT_US_SCREEN,UserModel.getUserModel(this).userEmail,this::class.java)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun init() {
        toolbarFragment.txtToolbarHeader.text = "About Us"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

        Glide.with(applicationContext).load(R.drawable.ic_logo).into(imgLogo)
    }

    private fun fastFetchValues() {
        if (MyAppConfig.strAboutUsTitle.trim().isEmpty()) {
            MyAppConfig.fastFetchAppConfigValues(applicationContext, true, this, {
                runOnUiThread {
                    setValuesUi()
                }
            }, {
                dialogMsg.showErrorApiDialog(this, "Retry", View.OnClickListener {
                    dialogMsg.dismissDialog(this)
                    fastFetchValues()
                }, true)
            },compositeDisposable)
        } else {
            setValuesUi()
        }
    }

    private fun setValuesUi() {

        Glide.with(applicationContext).load(R.drawable.bg_about_us_header).into(imgHeaderBgAboutUs)
        txtVersion.text = "Version ${BuildConfig.VERSION_NAME}"

        txtTitleAboutUs.text = MyAppConfig.strAboutUsTitle

        if (MyAppConfig.strAboutUsDesc != null)
            txtDescAboutUs.text = MyAppConfig.strAboutUsDesc

        linFacebook.visibility=if(MyAppConfig.strLinkFacebook.trim().length>4) View.VISIBLE else View.GONE
        linTwitter.visibility=if(MyAppConfig.strLinkTwitter.trim().length>4) View.VISIBLE else View.GONE
        linInstagram.visibility=if(MyAppConfig.strLinkInstagram.trim().length>4) View.VISIBLE else View.GONE

        if(MyAppConfig.strLinkFacebook.trim().length<=4 && MyAppConfig.strLinkInstagram.trim().length<=4 && MyAppConfig.strLinkTwitter.trim().length<=4){
            linSocial.visibility=View.GONE
        }else{
            linSocial.visibility=View.VISIBLE
        }


    }

    private fun onClickListeners(){
        compositeDisposable.add(RxView.clicks(linFacebook).throttleFirst(1200,TimeUnit.MILLISECONDS).subscribe {
            goToSocialApp(MyAppConfig.strLinkFacebook,true)
        })

        compositeDisposable.add(RxView.clicks(linTwitter).throttleFirst(1200,TimeUnit.MILLISECONDS).subscribe {
            goToSocialApp(MyAppConfig.strLinkTwitter)
        })

        compositeDisposable.add(RxView.clicks(linInstagram).throttleFirst(1200,TimeUnit.MILLISECONDS).subscribe {
            goToSocialApp(MyAppConfig.strLinkInstagram)
        })

        compositeDisposable.add(RxView.clicks(linTermsOfUse).throttleFirst(1200,TimeUnit.MILLISECONDS).subscribe {
            MyNavigations.goToWebView(this,strTitle = getString(R.string.strTerms),strUrl = MyAppConfig.URL_TERMS)
        })

        compositeDisposable.add(RxView.clicks(linPrivacyPolicy).throttleFirst(1200,TimeUnit.MILLISECONDS).subscribe {
            MyNavigations.goToWebView(this,strTitle = getString(R.string.strPrivacyPolicy),strUrl = MyAppConfig.URL_PRIVACY_POLICY)
        })

        compositeDisposable.add(RxView.clicks(linFaqs).throttleFirst(1200,TimeUnit.MILLISECONDS).subscribe {
            MyNavigations.goToWebView(this,strTitle = getString(R.string.strFaqs),strUrl = MyAppConfig.URL_FAQ)
        })
    }

    private fun goToSocialApp(url: String, fbIntent: Boolean=false) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            if (fbIntent) {
                try {
                    val applicationInfo = packageManager.getApplicationInfo("com.facebook.katana", 0)
                    if (applicationInfo.enabled) {
                        // http://stackoverflow.com/a/24547437/1048340
                        val uri = Uri.parse("fb://facewebmodal/f?href=$url")
                        intent.data = uri
                        startActivity(intent)
                    } else {
                        intent.data = Uri.parse(url)
                        startActivity(intent)
                    }
                } catch (ignored: PackageManager.NameNotFoundException) {
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                }

            } else {
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        } catch (e: Exception) {
            Log.d("ExcSocial", "" + e)
        }

    }
}

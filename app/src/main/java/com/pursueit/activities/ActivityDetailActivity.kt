package com.pursueit.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.BuildConfig
import com.pursueit.R
import com.pursueit.adapters.*
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.custom.CustomFont
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.*
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_detail_activity.*
import kotlinx.android.synthetic.main.content_activity_detail_reviews.*
import kotlinx.android.synthetic.main.content_activity_detail_time_table.*
import kotlinx.android.synthetic.main.dialog_add_edit_review.*
import kotlinx.android.synthetic.main.dialog_enquire.*
import kotlinx.android.synthetic.main.dialog_make_payment.*
import kotlinx.android.synthetic.main.layout_apply_promotion.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@SuppressLint("SetTextI18n")
class ActivityDetailActivity : AppCompatActivity() {

    private var timeTableSlotsAdapter: TimeTableSlotsAdapter? = null
    var arrayTimeTable = ArrayList<ActivitySlotModel>()

    var reviewAdapter: ReviewsAdapter? = null
    var arrayReviewModel = ArrayList<ReviewModel>()

    //var similarActivityAdapter: SimilarActivityAdapter? = null
    //var arraySimilarActivities = ArrayList<ActivityModel>()

    var activityModel: ActivityModel? = null
    var selectedSlotModel: ActivitySlotModel? = null

    private lateinit var mySnackBar: MySnackBar

    val compositeDisposable = CompositeDisposable()

    private var dialogMsg: DialogMsg? = null

    lateinit var userModel: UserModel

    private var slotIndex = -1

    private var noOfSessionsToAttend = 0

    private var comingFromBooking = false
    private var bookingModel: MyBookingsModel? = null
    private var bookingIndexPos = 0

    private var wasFetchMembersHit = false

    private var vatAmount = 0.0
    private var totalAmount = 0.0

    private var dateBookingCancellationThreshold = ""

    private var orderId: String = ""

    var currentDate: Date? = null
    var strCurDateServer: String = ""

    val arrPromotionModel = ArrayList<PromotionModel>()

    var totalAfterPromotionDisApplied: Double = 0.0

    private var makePaymentDialog: Dialog? = null

    companion object {
        var goToCamp = false
        var goToMyBookings = false
        var goToExplore = false
        var goToPass = false
        var isBookingStatusCancelled = false
        var actDetail: WeakReference<ActivityDetailActivity>? = null

        //Used for saving state, when a guest user comes back after log in
        var activityId: String = ""
        var timeSlotId: String = ""
        var enrolmentId: String = ""
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_activity)
        LoginBasicsUi.setTransparentStatusBarAndBgImage(this)

        actDetail = WeakReference<ActivityDetailActivity>(this)

        userModel = UserModel.getUserModel(applicationContext)

        mySnackBar = MySnackBar(this, relParentActivityDetail)

        dialogMsg = DialogMsg()

        init()

        //reset state after redirection
        activityId = ""
        timeSlotId = ""
        enrolmentId = ""

        getSetActivityDetails(false)

        onClickListeners()


        //fillSimilarActivities()

        fastFetchActivityDetails()

        fastFetchReviews()


        //showPostReviewDialog()


        sparkFavoriteAct.visibility = View.GONE
        val markFavOrUnFav = MarkFavoriteOrUnFavorite(this)
        sparkFavoriteAct.setOnClickListener {

            val model = activityModel
            if (model != null) {
                markFavOrUnFav.fastMark(
                    activityId = model.activityId,
                    itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_ACTIVITY,
                    favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE
                )

                activityModel!!.isFavoriteMarked = !activityModel!!.isFavoriteMarked
                sparkFavoriteAct.playAnimation()
                sparkFavoriteAct.isChecked = activityModel!!.isFavoriteMarked

                val intent = Intent()
                intent.putExtra("actId", model.activityId)
                intent.putExtra("isFavorite", model.isFavoriteMarked)
                setResult(Activity.RESULT_OK, intent)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        if (ReviewsListingActivity.isReviewUpdated) {
            ReviewsListingActivity.isReviewUpdated = false
            fastFetchActivityDetails()
            fastFetchReviews()
        }

        MyGoogleAnalytics.sendSession(
            this,
            MyGoogleAnalytics.ACTIVITY_DETAIL_SCREEN + " - " + activityModel?.activityName,
            userModel.userEmail,
            this::class.java
        )
    }

    private fun init() {

        txtTableHeader.text = "Full Schedule"

        txtActivityDesc.typeface = CustomFont.setFontSemiBold(assets)
        txtProviderAboutUs.typeface = CustomFont.setFontSemiBold(assets)



        rvTimeTable.layoutManager = LinearLayoutManager(applicationContext)
        rvTimeTable.setHasFixedSize(true)

        rvReviews.layoutManager = LinearLayoutManager(applicationContext)
        rvReviews.setHasFixedSize(true)

        rvSimilarActivities.layoutManager =
            LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        rvSimilarActivities.addItemDecoration(
            PaddingItemDecoration(
                paddingEnd = resources.getDimension(
                    R.dimen.dim_10
                ).toInt()
            )
        )
        //     rvSimilarActivities.setHasFixedSize(true)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun onClickListeners() {

        compositeDisposable.add(
            RxView.clicks(linProvider).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                txtActivitySubTitle.performClick()
            })

        compositeDisposable.add(
            RxView.clicks(txtActivitySubTitle).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                //Do not redirect to service provider detail screen in case of pass activity
                if (activityModel?.isPassActivity == true) {
                    return@subscribe
                }

                MyNavigations.gotoProviderDetailActivity(this@ActivityDetailActivity, activityModel?.activityProviderId ?: "")
            })

        compositeDisposable.add(
            RxView.clicks(imgBack).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
                onBackPressed()
            })

        compositeDisposable.add(
            RxView.clicks(imgGetDirections).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                if (activityModel != null && selectedSlotModel != null) {
                    MyPlaceUtils.getDirections(
                        this,
                        selectedSlotModel!!.slotLat,
                        selectedSlotModel!!.slotLong
                    )
                }
            })

        compositeDisposable.add(
            RxView.clicks(btnShare).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                MyAppConfig.shareApp(this, "" + activityModel?.activityName)
            })

    }

    private fun getCancellationSpanMsg(strDateTime: String): Spanned {
        return if (!activityModel!!.isActivityBookingCancellable && !comingFromBooking) {
            Html.fromHtml("This is a non-cancellable booking")
        } else if (!activityModel!!.isActivityBookingCancellable && comingFromBooking) {
            Html.fromHtml("Cancellation deadline has passed. This booking cannot be cancelled.")
        } else {
            Html.fromHtml("Cancel before <font color=\"#2B2B2B\">$strDateTime</font> to get full refund")
        }
    }

    private fun fastFetchActivityDetails() {
        try {
            if (ConnectionDetector.isConnectingToInternet(this)) {
                DialogMsg.showPleaseWait(this)
                //cardProgress.visibility = View.VISIBLE
                val hashParams = HashMap<String, String>()
                var timeSlotId = ""
                var enrolmentId = ""
                try {
                    if (intent.hasExtra("notificationModel")) {
                        val notificationModel =
                            intent.getSerializableExtra("notificationModel") as NotificationModel
                        timeSlotId = notificationModel.timeSlotId
                        enrolmentId = notificationModel.enrollmentId
                    }
                } catch (e: Exception) {
                    Log.e("fastFetchAct", e.toString())
                }

                hashParams["activityId"] = "" + activityModel?.activityId
                hashParams.put("timeSlotId", selectedSlotModel?.slotId ?: timeSlotId)
                hashParams.put("enrolmentId", selectedSlotModel?.slotEnrolmentId ?: enrolmentId)
                hashParams.put("comingFrom", if (bookingModel != null) "1" else "2") //1->Booking 2->Normal
                FastNetworking.makeRxCallPost(this, Urls.SINGLE_ACTIVITY_DETAIL, true, hashParams, "PlaceDetail", object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                            //  cardProgress.visibility = View.GONE
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                                strCurDateServer = json.optString("current_date", "")

                                currentDate = ChangeDateFormat.getDateObjFromString(
                                    strCurDateServer, "yyyy-MM-dd HH:mm:ss"
                                )

                                val jsonData = json.getJSONObject("data")
                                activityModel = JsonParse.parseOneActivity(jsonData, json, bookingModel?.bookingThresholdDate)

                                getSetActivityDetails(true)


                                makeBookNowEnquireEnable()
                            } else {
                                showApiErrorRetry {
                                    fastFetchActivityDetails()
                                }
                            }
                        } catch (e: Exception) {
                            Log.d("ExcParse_", "" + e)
                            showApiErrorRetry {
                                fastFetchActivityDetails()
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        //showErrorMsg(ErrorMsgs.ERR_API_MSG)
                        try {
                            DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                            //   cardProgress.visibility = View.GONE
                            showApiErrorRetry {
                                fastFetchActivityDetails()
                            }
                        } catch (e: Exception) {

                        }
                    }

                },
                    compositeDisposable
                )
            } else {
                showConnectionError()
            }
        } catch (e: Exception) {

        }
    }

    private fun getFirstActiveEnrollmentDate(): String {
        selectedSlotModel!!.arrayEnrolModel.forEach {
            //Log.d("enrollModel", it.enrolDate + "  " + it.isDateAlreadyPassed)
            if (!it.isDateAlreadyPassed)
                return it.enrolDate
        }
        return ""
    }

    private fun makeBookNowEnquireEnable() {
        compositeDisposable.add(
            RxView.clicks(btnBookEnquire).throttleFirst(800, TimeUnit.MILLISECONDS).subscribe {

                /* if (btnBookEnquire.text.contains("cancel", true) || btnBookEnquire.text.contains("decline", true)) {
                     if (btnBookEnquire?.tag ?: "" == "cancelled" && dialogMsg != null) {
                         MyBookingsModel.showCancelReason(
                                 this@ActivityDetailActivity,
                                 bookingModel?.cancelReason ?: "",
                                 dialogMsg!!
                         )

                     }
                     return@subscribe
                 }*/

                if (btnBookEnquire.text.toString().contains("Book Now", true)) {
                    if (userModel.userId.trim().isEmpty()) {
                        if (activityModel != null)
                            activityId = activityModel!!.activityId

                        if (selectedSlotModel != null) {
                            timeSlotId = selectedSlotModel!!.slotId
                            enrolmentId = selectedSlotModel!!.slotEnrolmentId
                        }

                        MyProfileUtils.showSignUpLoginPopUp(this)
                    } else {
                        fastFetchFamilyMembersFirst(1)
                    }
                } else if (btnBookEnquire.text.toString().contains("Enquire", true)) {
                    showEnquireDialog()
                } else if (btnBookEnquire.text.toString().contains("Cancel Booking", true) && activityModel!!.isActivityBookingCancellable
                ) {
                    //cancel booking task
                    showSureCancelBooking()
                } else if (btnBookEnquire.text.toString().contains("Booking Confirmed", true) && !activityModel!!.isActivityBookingCancellable) {
                    showErrorDialog(
                        getCancellationSpanMsg(
                            JsonParse.getFormattedServerDateJavaReverse(
                                bookingModel!!.bookingThresholdDate
                            )
                        ).toString().trim()
                    )
                } else if(btnBookEnquire.text.toString().contains("Booking ${StaticValues.BOOKING_UNDER_PROCESS}")){
                    showErrorDialog(getString(R.string.booking_under_process_msg))
                }
            })
    }

    private fun getSetActivityDetails(comingFromDetailApi: Boolean) {
        try {

            if (activityModel == null) {
                activityModel = intent!!.extras!!.getSerializable("ActivityModel") as ActivityModel
                slotIndex = intent!!.extras!!.getInt("SlotIndex")

                comingFromBooking = intent!!.extras!!.getBoolean("ComingFromBooking")

                if (slotIndex < activityModel!!.arraySlotModel.size && activityModel!!.arraySlotModel.size > 0) {
                    Log.d(
                        "Slot_Selected_In",
                        "" + slotIndex + "\n" + activityModel!!.arraySlotModel[slotIndex] + "\nEnrolmentId: " + activityModel!!.arraySlotModel[slotIndex].slotEnrolmentId
                    )
                }

                if (intent?.extras?.getSerializable("BookingModel") != null) {
                    bookingModel =
                        intent?.extras?.getSerializable("BookingModel") as MyBookingsModel
                    bookingIndexPos = intent?.extras!!.getInt("BookingIndexPos")
                }
            }

            //fetch other similar activities on first activity information received
            if (!comingFromDetailApi)
                fastFetchSimilarActivities()

            //place all data related to activity
            if (comingFromDetailApi) {
                if (userModel.userId.trim().isEmpty()) {
                    sparkFavoriteAct.visibility = View.GONE
                } else {
                    sparkFavoriteAct.visibility = View.VISIBLE

                    if (MarkFavoriteOrUnFavorite.LAST_FAV_ACTIVITY_ID == activityModel?.activityId ?: "") {
                        sparkFavoriteAct.isChecked =
                            MarkFavoriteOrUnFavorite.LAST_FLAG_FOR_ACTIVITY_OR_CAMP == MarkFavoriteOrUnFavorite.FLAG_FAVORITE

                        MarkFavoriteOrUnFavorite.LAST_FAV_ACTIVITY_ID = ""
                        MarkFavoriteOrUnFavorite.LAST_FLAG_FOR_ACTIVITY_OR_CAMP = ""
                    } else {
                        sparkFavoriteAct.isChecked = activityModel?.isFavoriteMarked ?: false
                    }
                }
            }

            txtActivityTitle.text = activityModel?.activityName?.trim()
            txtActivitySubTitle.text = activityModel?.activityProviderName?.trim()
            txtActivityLocation.text = activityModel?.activityLocation

            txtAdditionalInfo.text = activityModel?.activityAdditionalInfo?.trim()
            txtOtherInfo.text = activityModel?.activityOtherInfo?.trim()
            txtCancellationPolicy.text = activityModel?.activityCancellationPolicy?.trim()

            txtStarCount.text = "(${activityModel?.activityStarRatingCount})"

            ratingBarDetail.rating = activityModel!!.activityStarRating

            Log.d("Rating_Avg", "" + ratingBarDetail.rating)

            linRatingBarView.visibility =
                if (activityModel!!.activityStarRatingCount > 0) View.VISIBLE else View.GONE

            txtActivityDesc.text = activityModel?.activityDesc

            txtProviderName.text = activityModel?.activityProviderName?.trim()
            txtProviderAboutUs.text = activityModel?.activityProviderAboutUs?.trim()

            //Provide logo image
            Glide.with(this)
                .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(false))
                .load(Urls.IMAGE_URL_PARTNER + activityModel?.activityProviderLogo)
                .into(imgProvider)

            //fill up view pager UI (Activity Images)
            val adapter = MyImagePagerAdapter(this, activityModel!!.arrayImages)
            viewPager.adapter = adapter
            worm_dots_indicator.setViewPager(viewPager)

            if (activityModel!!.arraySlotModel.size > 0 && slotIndex >= 0) {

                if (comingFromDetailApi)
                    slotIndex = 0

                if (slotIndex < activityModel!!.arraySlotModel.size) {
                    selectedSlotModel = activityModel!!.arraySlotModel[slotIndex]
                }

                //Earlier Code
                /*if (selectedSlotModel?.isSingleActivity == true) {
                    txtTableHeader.text = "Individual Session Schedule"
                }
                if (activityModel?.isPassActivity == true) {
                    txtTableHeader.text = "Schedule"
                }*/

                //New Code
                txtTableHeader.text = "Schedule"

                //Log.d("Slot_Selected_In_$comingFromDetailApi", "" + slotIndex + "\n" + selectedSlotModel.toString())

                if (comingFromDetailApi) {
                    getSessionsJson()
                    if (dateBookingCancellationThreshold.trim().isNotEmpty())
                        txtCancellationPolicy.text = getCancellationSpanMsg(
                            JsonParse.getFormattedDateCancellation(dateBookingCancellationThreshold)
                        )
                }

                if (comingFromDetailApi && comingFromBooking && bookingModel != null) {
                    if (bookingModel!!.bookingThresholdDate != null && !bookingModel!!.bookingStatus.equals(
                            StaticValues.BOOKING_STATUS_CANCELLED, true
                        )
                    )
                        txtCancellationPolicy.text =
                            getCancellationSpanMsg(
                                JsonParse.getFormattedDateCancellation(
                                    bookingModel!!.bookingThresholdDate!!
                                )
                            )
                }

                txtActivityLocation.text = selectedSlotModel?.slotLocation


                if (activityModel!!.activityLocation.equals("at your premise", true)) {
                    txtSlotLocation.text = "At Your Premise"
                    imgGetDirections.visibility = View.GONE
                } else {
                    imgGetDirections.visibility = View.VISIBLE
                    txtSlotLocation.text =
                        if (selectedSlotModel?.slotLocationFullAddress == "null") "---" else selectedSlotModel?.slotLocationFullAddress
                }

                //txtSlotLocation.text = selectedSlotModel?.slotLocationFullAddress
                txtSlotAge.text = selectedSlotModel?.slotAge?.replace("yr", "years")

                txtSlotType.text = selectedSlotModel?.slotType?.capitalize()
                if (txtSlotType.text.toString().trim().length > 2)
                    txtSlotType.text =
                        selectedSlotModel?.slotType?.trim()?.capitalize() + " Activity"

                //calculate price to pay
                //txtSlotPrice.text = if (selectedSlotModel?.slotPrice!!.trim().isEmpty()) "" else "AED ${selectedSlotModel?.slotPrice}"

                //Before rounding
                /*val totalPayable = String.format(
                        Locale.ENGLISH,
                        "%.2f",
                        (getTotalPricePayable().toDouble())
                ).toDouble()
                txtSlotPrice.text = "AED ${DecimalFormat("##.###").format(totalPayable)}"
                */

                val totalPayable = Math.round(getTotalPricePayable().toDouble())
                txtSlotPrice.text = "AED $totalPayable"


                Log.d("StatusAndAmount", "" + selectedSlotModel?.slotStatus + " - " + totalPayable)

                if (selectedSlotModel?.slotStatus.equals("Available", true)) {
                    txtSlotPrice.visibility = View.VISIBLE
                    txtSlotPrice.text = if (totalPayable > 0) "AED $totalPayable" else "Free"
                } else {
                    txtSlotPrice.visibility = if (totalPayable > 0) View.VISIBLE else View.GONE
                }

                try {
                    val strAmt = totalPayable.toString().trim()
                    Log.d("StrAmt", strAmt)
                    if (strAmt.endsWith(".0") && !txtSlotPrice.text.toString().trim().equals(
                            "Free",
                            true
                        )
                    ) {
                        txtSlotPrice.text = "AED ${strAmt.substring(0, strAmt.length - 2)}"
                    }
                } catch (e: Exception) {
                    Log.d("ExcStrAmt", "" + e)
                }

                txtSlotSessions.text = if (selectedSlotModel!!.noOfEligibleSessions > 0) selectedSlotModel!!.noOfEligibleSessions.toString() else "-"

                if (comingFromDetailApi)
                    btnBookEnquire.text = if (selectedSlotModel?.slotStatus.equals("Available", true)) "Book Now" else "Enquire"

                //if coming from booking and status is upcoming or ongoing, then show cancel button
                if (comingFromDetailApi && comingFromBooking) {
                    if (activityModel!!.isActivityBookingCancellable) {
                        btnBookEnquire.text = "Cancel Booking"
                    } else {
                        btnBookEnquire.text = "Booking Confirmed"
                    }

                    if (bookingModel != null) {
                        if (bookingModel!!.bookingStatus.equals(
                                StaticValues.BOOKING_STATUS_COMPLETED,
                                true
                            )
                        ) {
                            btnBookEnquire.text = "Sessions completed"
                        } else if (bookingModel!!.bookingStatus.equals(
                                StaticValues.BOOKING_STATUS_CANCELLED,
                                true
                            )
                        ) {
                            //btnBookEnquire.text = "Book Now"

                            btnBookEnquire.text = MyBookingsModel.getCancellationMessage(
                                bookingModel?.bookingCancelledBy
                                    ?: ""
                            )//"Booking Cancelled"
                            btnBookEnquire.tag = "cancelled"
                            btnBookEnquire.isEnabled = false
                        } else if (bookingModel!!.bookingStatus.equals(
                                StaticValues.BOOKING_STATUS_DECLINED,
                                true
                            )
                        ) {
                            btnBookEnquire.text = "Booking Declined"
                            btnBookEnquire.tag = "cancelled"
                            btnBookEnquire.isEnabled = false
                        } else if (bookingModel!!.bookingStatus.equals(
                                StaticValues.BOOKING_STATUS_AWAITING,
                                true
                            )
                        ) {
                            btnBookEnquire.text = "Booking ${StaticValues.BOOKING_UNDER_PROCESS}"
                            //btnBookEnquire.isEnabled = false
                        }
                    }

                }

                //in case when enrolments (time table is found but date has already passed then we need to show enquire)
                if (comingFromDetailApi && selectedSlotModel!!.arrayEnrolModel.any { !it.isDateAlreadyPassed } && !comingFromBooking) {
                    btnBookEnquire.text = "Book Now"
                } else if (comingFromDetailApi && selectedSlotModel!!.arrayEnrolModel.none { !it.isDateAlreadyPassed } && !comingFromBooking) {
                    btnBookEnquire.text = "Enquire"
                }

                //fill up time table (Date, Day and Time)


                timeTableSlotsAdapter =
                    TimeTableSlotsAdapter(this, selectedSlotModel!!.arrayEnrolModel)
                rvTimeTable.adapter = timeTableSlotsAdapter
                linTimeTable.visibility =
                    if (selectedSlotModel!!.arrayEnrolModel.size > 0) View.VISIBLE else View.GONE


                if (comingFromDetailApi && comingFromBooking && bookingModel?.bookingStatus.equals(
                        StaticValues.BOOKING_STATUS_COMPLETED,
                        true
                    ) && !activityModel!!.isReviewGiven
                )
                    showPostReviewDialog()

            } else {
                btnBookEnquire.text = "Enquire"
                linTimeTable.visibility = View.GONE
            }

            btnBookEnquire.visibility = if (comingFromDetailApi) View.VISIBLE else View.GONE
        } catch (e: Exception) {
            Log.d("ActivityGetExc", "" + e)
        }

        if (btnBookEnquire.text.toString().trim() == "Enquire" && comingFromDetailApi) {
            linSessions.visibility = View.GONE
            linType.visibility = View.GONE
            txtCancellationPolicyLabel.visibility = View.GONE
            txtCancellationPolicy.visibility = View.GONE
            txtOtherInfoLabel.visibility = View.GONE
            txtOtherInfo.visibility = View.GONE
        }

        if (bookingModel != null) {
            if (!bookingModel!!.bookingStatus.equals(
                    StaticValues.BOOKING_STATUS_CANCELLED,
                    true
                )
            ) { // not equals to cancel
                txtSlotPrice.text = "AED " + bookingModel!!.bookingAmtAfterTax
                if (bookingModel!!.sessionJson != "") {
                    try {

                        val jar = JSONArray(bookingModel!!.sessionJson)
                        txtSlotSessions.text = jar.length().toString()/*resources.getQuantityString(
                            R.plurals.session,
                            jar.length(),
                            jar.length()
                        )*/
                        val arrayModel = ArrayList<EnrolModel>()
                        repeat(jar.length()) {
                            jar.getJSONObject(it).run {
                                EnrolModel(getString("enrolment_slot_id")).also {
                                    it.enrolDay =
                                        optString("enrolment_slot_week", "").trim().capitalize()
                                    it.enrolDate = ChangeDateFormat.convertDate(
                                        getString("enrolment_slot_date"),
                                        "yyyy-MM-dd",
                                        "dd MMM yyyy"
                                    )
                                    it.enrolDuration = getString("enrolment_slot_duration")
                                    it.enrolTime = getString("enrolment_slot_time")

                                    Log.d(
                                        "Act:DatePassed",
                                        "endTime:" + it.enrolTime.substring(it.enrolTime.indexOf("-") + 1).trim()
                                    )

                                    it.enrolDateJava = ChangeDateFormat.getDateObjFromString(
                                        it.enrolDate + " " + it.enrolTime.substring(
                                            it.enrolTime.indexOf(
                                                "-"
                                            ) + 1
                                        ).trim(),
                                        "dd MMM yyyy HH:mm"
                                    )

                                    Log.d("Act:DatePassed", "enrolDate:" + it.enrolDate)
                                    Log.d("Act:DatePassed", "enrolDate:" + it.enrolDateJava)
                                    Log.d("Act:DatePassed", "CurrentDate:$currentDate")
                                    if (currentDate != null && it.enrolDateJava != null) {
                                        //Log.d("FormattedEnrolDate", "" + enrolModel.enrolDateJava)
                                        it.isDateAlreadyPassed =
                                            currentDate!!.after(it.enrolDateJava) //|| currentDate!!.equals(it)
                                        //Log.d("DateAlreadyPassed",""+enrolModel.isDateAlreadyPassed)

                                        Log.d("Act:DatePassed", it.isDateAlreadyPassed.toString())
                                        Log.d(
                                            "-----------------------",
                                            "-----------------------------------"
                                        )
                                    }
                                }.also {
                                    arrayModel.add(it)
                                }
                            }
                        }
                        timeTableSlotsAdapter = TimeTableSlotsAdapter(this, arrayModel)
                        rvTimeTable.adapter = timeTableSlotsAdapter

                    } catch (e: Exception) {

                    }
                }
            }

        }
        if (bookingModel != null) {
            /*if (bookingModel!!.bookingStatus.equals(
                    StaticValues.BOOKING_STATUS_COMPLETED, true) || bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_ONGOING, true)) {*/
            if (bookingModel?.bookingAmtAfterTax != "") {
                txtPaidAmount.visibility = View.VISIBLE
                txtSlotPrice.text =
                    "AED " + DecimalFormat("##.##").format(bookingModel?.bookingAmtAfterTax?.toDouble())
            }
            //}
        }


        //show promotion ribbon on detail screen, only when not coming from my booking
        if (!comingFromBooking)
            PromotionOperation.setPromotionText(linPromotion, activityModel?.maxValuePromotionModel)

    }

    private fun fillSimilarActivities(arrActivityModel: ArrayList<ActivityModel>) {
        if (arrActivityModel.size > 0) {
            linSimilarActivities.visibility = View.VISIBLE
            val adapter = ActivityForMapAdapter(
                this@ActivityDetailActivity,
                arrActivityModel,
                true,
                fromPass = activityModel?.isPassActivity ?: false
            )
            /*NearbyActivityAdapter(this,arrActivityModel){pos, progressShowMore ->

            }*///SimilarActivityAdapter(this, arraySimilarActivities)
            rvSimilarActivities.adapter = adapter
            /* rvSimilarActivities.setItemTransformer(ScaleTransformer.Builder().setMaxScale(1.05f).setMinScale(0.8f)
                 .setPivotX(Pivot.X.CENTER).setPivotY(Pivot.Y.BOTTOM).build())
             rvSimilarActivities.setSlideOnFling(true)*/


        } else {
            linSimilarActivities.visibility = View.GONE
        }

    }


    //1-> Show Make Payment,  2-> Show Enquire Dialog
    private fun fastFetchFamilyMembersFirst(flagMakePaymentOrEnquire: Int) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            DialogMsg.showPleaseWait(this)
            MyProfileUtils.fastFetchFamilyMembers(
                applicationContext,
                { success: Boolean, jsonObject: JSONObject?, anError: ANError? ->
                    DialogMsg.dismissPleaseWait(this)
                    wasFetchMembersHit = true
                    if (flagMakePaymentOrEnquire == 1) showMakePaymentDialog() else showEnquireDialog()

                },
                compositeDisposable
            )
        } else {
            mySnackBar.showSnackBarError(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }


    private fun showMakePaymentDialog() {
        if (activityModel != null) {

            val dialog = MyDialog(this).getMyDialog(R.layout.dialog_make_payment)
            makePaymentDialog = dialog


            if (activityModel?.isPassActivity == true) {
                dialog.linPass.visibility = View.VISIBLE
                dialog.cnsApplyPromoCode.visibility = View.GONE
            } else {
                dialog.linPass.visibility = View.GONE
                dialog.cnsApplyPromoCode.visibility = View.VISIBLE
            }

            arrPromotionModel.clear()
            activityModel?.arrPromotionModel?.forEach { it.isApplied = false }
            arrPromotionModel.addAll(activityModel?.arrPromotionModel ?: ArrayList())

            val walletPassBalance = JsonParse.getValidPassAmount(userModel.currentPassWallet)
            dialog.txtPassBalValue.text = "AED $walletPassBalance"
            if (walletPassBalance > 0.0) {
                dialog.txtPassValidTill.visibility = View.VISIBLE
                dialog.txtPassValidTill.text = "(Valid till ${userModel.passExpiryDate})"
            } else {
                dialog.txtPassValidTill.visibility = View.GONE
            }
            if (arrPromotionModel.size == 0) {
                dialog.cnsApplyPromoCode.visibility = View.GONE
            }

            dialog.imgAddAttendee.setOnClickListener {
                dialog.dismiss()
                MyProfileUtils.showAddMemberDialog(this, {
                    fastFetchFamilyMembersFirst(1)
                })
            }

            Glide.with(dialog.context)
                .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions())
                .load(Urls.IMAGE_FEATURE_ACTIVITY + activityModel!!.activityImageUrl)
                .into(dialog.imgActivityDialog)
            dialog.txtActivityTitleDialog.text = activityModel!!.activityName.trim()
            dialog.txtLocationDialog.text = activityModel!!.activityLocation.trim()
            dialog.txtActivitySubTitleDialog.text = activityModel!!.activityProviderName.trim()

            dialog.txtCancellationPolicyDialog.text = getCancellationSpanMsg(JsonParse.getFormattedDateCancellation(dateBookingCancellationThreshold))

            dialog.txtOtherInfoDialog.text = activityModel!!.activityOtherInfo.trim()

            /*val totalPayable =
                    String.format(
                            Locale.ENGLISH,
                            "%.2f",
                            (getTotalPricePayable().toDouble())
                    ).toDouble()*/
            val totalPayable = Math.round(getTotalPricePayable().toDouble())

            if (totalPayable == 0L) {
                dialog.txtFinalTotalLabel.text = "Final Price"
            }

            if (selectedSlotModel != null) {
                dialog.txtSlotAgeDialog.text = "Age ${selectedSlotModel!!.slotAge}".replace("yr", "years")
                val firstActiveDate = getFirstActiveEnrollmentDate()
                if (selectedSlotModel!!.slotEnrolStartDate == selectedSlotModel!!.slotEnrolEndDate)
                    dialog.txtSlotTimeDialog.text =
                        if (firstActiveDate != "") firstActiveDate else selectedSlotModel!!.slotEnrolStartDate
                else {
                    dialog.txtSlotTimeDialog.text =
                        "${if (firstActiveDate != "") firstActiveDate else selectedSlotModel!!.slotEnrolStartDate} - ${selectedSlotModel!!.slotEnrolEndDate}"
                    //"${selectedSlotModel!!.slotEnrolStartDate} - ${selectedSlotModel!!.slotEnrolEndDate}"

                }

                dialog.txtSessionsDialog.text =
                    "" + noOfSessionsToAttend + (if (noOfSessionsToAttend > 1) " Sessions" else " Session")
                try {

                    //Earlier
                    //"AED ${DecimalFormat("##.##").format(totalPayable.toDouble())}"
                    dialog.txtSlotPriceDialog.text = "AED $totalPayable"
                } catch (e: Exception) {
                    Log.e("txtSlotPriceDialog", e.toString())
                }


                //calculate VAT and then show total enteredAmount
                dialog.txtVatTitleDialog.text = "VAT(${MyAppConfig.vatRate}%)"

                //Commenting following statement because we will get VAT inclusive prices from now
                /*vatAmount = Math.round(
                    String.format(
                        Locale.ENGLISH,
                        "%.2f",
                        (totalPayable * MyAppConfig.vatRate) / 100.0
                    ).toDouble()
                ).toDouble()*/

                vatAmount = 0.0
                dialog.linVat.visibility = View.GONE

                dialog.txtVatValueDialog.text =
                    "AED ${ConvertToValidData.removeZeroAfterDecimal(vatAmount.toString())}"//.replace(".0", "").replace(".00", "")}"

                totalAmount = //Math.round(
                    String.format(
                        Locale.ENGLISH,
                        "%.2f",
                        (totalPayable + vatAmount)
                    ).toDouble()
                //)
                dialog.txtTotalPriceDialog.text =
                    "AED ${DecimalFormat("##.##").format(totalAmount)}"

                totalAfterPromotionDisApplied = totalAmount.toDouble()
            }
            dialog.rvAttendees.layoutManager = LinearLayoutManager(applicationContext)

            dialog.cnsApplyPromoCode.setOnClickListener {
                PromotionOperation.showApplyPromoCodeDialog(
                    arrPromotionModel, this@ActivityDetailActivity, totalAmount.toDouble()
                ) { totalDiscountPrice: Double, discountedPrice: Double ->
                    //   val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
                    //   calculateTotal(numberOfSelectedPerson) //,totalDiscountPrice,discountedPrice
                    totalAfterPromotionDisApplied = discountedPrice
                    PromotionOperation.setupUiForDiscountAndTotal(
                        this@ActivityDetailActivity,
                        totalDiscountPrice,
                        dialog.linDiscount,
                        dialog.txtApplyPromoCode,
                        dialog.imgPromotion,
                        dialog.txtDiscountValueDialog,
                        dialog.txtTotalPriceDialog,
                        discountedPrice,
                        totalAmount.toDouble(), dialog.btnMakePayment,
                        txtDiscountTitle = dialog.txtDiscount,
                        arrPromotionModel = arrPromotionModel
                    )
                }
            }

            val adapter = AttendeeAdapter(this, MyProfileUtils.arrayFamilyModel, { pos, isChecked, adapter ->
                MyProfileUtils.arrayFamilyModel.forEach {
                    it.isSelected = false
                }
                val model = MyProfileUtils.arrayFamilyModel[pos]
                model.isSelected = isChecked
                MyProfileUtils.arrayFamilyModel.set(pos, model)
                adapter.notifyDataSetChanged()
            }, {

            })

            dialog.rvAttendees.adapter = adapter

            dialog.imgClose.setOnClickListener { dialog.dismiss() }

            dialog.btnMakePayment.text = if (totalAmount > 0 && activityModel?.isPassActivity == false) "Make Payment" else if (totalAmount > 0 && activityModel?.isPassActivity == true) "Pay Via Wallet" else "Book Activity"


            dialog.txtAddMoney.setOnClickListener {
                MyNavigations.gotoAddMoneyActivity(this@ActivityDetailActivity, true)
            }

            dialog.btnMakePayment.setOnClickListener {

                //validation to check whether there is enough amount present in the pass or not
                if (activityModel?.isPassActivity == true && totalAfterPromotionDisApplied > 0) {

                    val curUserWallet = JsonParse.getValidPassAmount(userModel.currentPassWallet)

                    //validation: if activity slot date is greater than pass expiry date then show error
                    try {

                        val jsonSessions = JSONArray(getSessionsJson())
                        if (jsonSessions.length() > 0) {
                            val enrolDate = jsonSessions.getJSONObject(0).getString("enrolment_slot_date");
                            val dtEnrolStartDate = ChangeDateFormat.getDateFromString(enrolDate, "yyyy-MM-dd")
                            val dtExpiryDate = ChangeDateFormat.getDateFromString(userModel.passExpiryDate, "dd MMM yyyy")
                            if (dtEnrolStartDate != null && dtExpiryDate != null) {
                                Log.d("EnrolStartDate", "" + dtEnrolStartDate)
                                Log.d("ExpiryDate", "" + dtExpiryDate)
                                if (dtEnrolStartDate.after(dtExpiryDate)) {
                                    val strDateExpiry = ChangeDateFormat.convertDate(userModel.passExpiryDate,
                                        "dd MMM yyyy",
                                        "dd MMMM yyyy")
                                    showErrorDialog("You can only book activities till $strDateExpiry. Recharge your wallet to proceed with the booking.")
                                    return@setOnClickListener
                                }
                            }

                        }
                    } catch (e: Exception) {
                        Log.d("PassExpiry_", "" + e);
                    }

                    if (totalAfterPromotionDisApplied > curUserWallet) {

                        val errMsg = "You do not have sufficient balance in your wallet, please add funds to proceed"
                        dialogMsg?.showYesCancelDialog(act = this,
                            strMsg = errMsg,
                            onYesListener = View.OnClickListener {
                                dialogMsg?.dismissDialog(this)
                                MyNavigations.gotoAddMoneyActivity(this@ActivityDetailActivity, true)
                            },
                            isCancellable = true,
                            strBtnNoText = "Cancel",
                            strBtnYesText = "OK"
                        )

                        return@setOnClickListener
                    }


                    //validation to check if pass is already expired
                    try {
                        val dtExpiryDate = ChangeDateFormat.getDateFromString(userModel.passExpiryDate, "dd MMM yyyy")
                        val strCurDate = ChangeDateFormat.convertDate(strCurDateServer, "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy")
                        val dtCurDate = ChangeDateFormat.getDateFromString(strCurDate, "dd MMM yyyy")

                        if (dtCurDate != null && dtExpiryDate != null) {
                            Log.d("CurDate", "" + dtCurDate)
                            Log.d("ExpiryDate", "" + dtExpiryDate)
                            if (dtCurDate.after(dtExpiryDate)) {
                                showErrorDialog("Your pass is expired, kindly recharge your pass to continue")
                                return@setOnClickListener
                            }
                        }

                    } catch (e: Exception) {
                        Log.d("DateExpireExc", "" + e)
                    }


                }

                if (MyProfileUtils.arrayFamilyModel.size < 1) {
                    showErrorDialog("Please add at least one attendee to proceed")
                    return@setOnClickListener
                }

                val arraySelectedAttendee =
                    MyProfileUtils.arrayFamilyModel.filter { familyModel -> familyModel.isSelected }
                val countSelected = arraySelectedAttendee.size
                if (countSelected < 1) {
                    showErrorDialog("Please select one attendee to proceed")
                    return@setOnClickListener
                }

                if (selectedSlotModel != null && countSelected > 0) {
                    val memberAge = arraySelectedAttendee[0].familyMemberAgeDouble
                    Log.d("MemberAge_", "" + memberAge)
                    Log.d("MemberAgeFrom_", "" + selectedSlotModel!!.slotAgeFrom)
                    Log.d("MemberAgeTo_", "" + selectedSlotModel!!.slotAgeTo)
                    if (memberAge < selectedSlotModel!!.slotAgeFrom || memberAge > selectedSlotModel!!.slotAgeTo) {
                        showErrorDialog("You must be in the age group of (${selectedSlotModel?.slotAge?.replace(
                            "yr",
                            "years"
                        )}) to be eligible for this activity"
                        )
                        return@setOnClickListener
                    }
                }

                MyGoogleAnalytics.sendCustomEvent(
                    this,
                    MyGoogleAnalytics.CATEGORY_PAYMENT,
                    MyGoogleAnalytics.ACTION_MAKE_PAYMENT,
                    userModel.userEmail
                )

                dialog.dismiss()
                checkIfBookingIsClashing()
                //  fastRegisterBookingApi()
                //goToCcAvenue()
            }

            try {
                DialogMsg.animateViewDialog(dialog.relParentMakePayment)
                dialog.show()
                //clear promo selection array as soon as dialog shown
                //ApplyPromotionAdapter.clearSelectionArray()
            } catch (e: Exception) {

            }


        }
    }


    /*private fun showAddMemberDialog() {
        MyProfileUtils.showAddMemberDialog(this){
            fastFetchFamilyMembersFirstThenShowDialog()
        }
        *//*val dialog = MyDialog(this).getMyDialog(R.layout.dialog_add_member)
        var selectedGender = ""
        var selectedDob = ""

        dialog.imgMale.setOnClickListener {
            selectedGender = "male"
            dialog.imgMale.imageResource = R.drawable.ic_male_selected
            dialog.imgFemale.imageResource = R.drawable.ic_female_unselected
        }
        dialog.imgFemale.setOnClickListener {
            selectedGender = "female"
            dialog.imgMale.imageResource = R.drawable.ic_male_unselected
            dialog.imgFemale.imageResource = R.drawable.ic_female_selected
        }

        dialog.imgCloseDialogAdd.setOnClickListener {
            dialog.dismiss()
        }

        dialog.txtDob.setOnClickListener {
            val strDob = dialog.txtDob.text.toString().trim()
            Log.d("strDob", strDob)

            val cal = Calendar.getInstance()

            try {
                val arrDate = JsonParse.getFormattedDateTimeTableReverse(strDob).split("-")
                Log.d("ArrDate", arrDate.toString())
                if (arrDate.size > 1) {
                    cal.set(Calendar.YEAR, arrDate[0].toInt())
                    cal.set(Calendar.MONTH, arrDate[1].toInt() - 1)
                    cal.set(Calendar.DAY_OF_MONTH, arrDate[2].toInt())
                }
            } catch (e: Exception) {
                Log.d("CalSetExc", "" + e)
            }

            val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val strMonth = if ((month + 1) / 9 <= 1.0) "0${month + 1}" else "${month + 1}"
                val strDay = if ((dayOfMonth) / 9 <= 1.0) "0$dayOfMonth" else dayOfMonth.toString()
                selectedDob = "$year-$strMonth-$strDay"
                Log.d("SelectedDate", selectedDob)
                dialog.txtDob.text = JsonParse.getFormattedDateTimeTable(selectedDob)
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis

            datePickerDialog.show()
        }

        dialog.btnSave.setOnClickListener {
            val strName = dialog.etFullName.text.toString().trim()
            val strDob = dialog.txtDob.text.toString().trim()
            val strSchoolName = dialog.etSchoolName.text.toString().trim()
            val strInterests = dialog.etInterests.text.toString().trim()
            val strPrefLocation = dialog.etPrefLocation.text.toString().trim()

            val validations = MyValidations(this)
            if (strName.isEmpty()) {

                showErrorDialog("Please enter your full name")
                return@setOnClickListener
            }
            if (!validations.checkName(strName)) {
                showErrorDialog("Please enter a valid full name")
                return@setOnClickListener
            }

            if (strDob.isEmpty()) {
                showErrorDialog("Please enter your date of birth")
                return@setOnClickListener
            }

            if (selectedGender.isEmpty()) {
                showErrorDialog("Please enter your gender")
                return@setOnClickListener
            }

            if (strInterests.isEmpty()) {
                showErrorDialog("Please enter your interests")
                return@setOnClickListener
            }

            val hashParams = HashMap<String, String>()
            hashParams.put("name", strName)
            hashParams.put("gender", selectedGender)
            hashParams.put("interests", strInterests)
            hashParams.put("schoolName", strSchoolName)
            hashParams.put("preferredActivities", strPrefLocation)
            hashParams.put("dateOfBirth", JsonParse.getFormattedDateTimeTableReverse(strDob))

            if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                DialogMsg.showPleaseWait(this)
                MyProfileUtils.fastAddFamilyMember(this, hashParams, object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                dialog.dismiss()
                                fastFetchFamilyMembersFirstThenShowDialog()
                            } else {
                                toast(ErrorMsgs.ERR_API_MSG)
                            }
                        } catch (e: Exception) {
                            Log.d("ExcAddMember", "" + e)
                            toast(ErrorMsgs.ERR_PARSE_TITLE)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        toast(ErrorMsgs.ERR_API_MSG)
                    }

                })
            } else {
                toast(ErrorMsgs.ERR_CONNECTION_MSG)
            }


        }
        dialog.show()*//*
    }*/

    private fun checkIfBookingIsClashing() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            DialogMsg.showPleaseWait(this)
            val hashParams = HashMap<String, String>()
            hashParams["activityId"] = activityModel?.activityId ?: ""
            hashParams["sessionJson"] = getSessionsJson()
            hashParams["familyId"] =
                MyProfileUtils.arrayFamilyModel.filter { it.isSelected }.get(0).familyId
            hashParams["enrolmentId"] = activityModel!!.arraySlotModel[slotIndex].slotEnrolmentId

            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.CHECK_CLASH,
                true,
                hashParams,
                "Clash_API_",
                object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                fastRegisterBookingApi()
                            } else {
                                if (json.getString("clashstatus") == "0") {
                                    dialogMsg!!.showErrorRetryDialog(this@ActivityDetailActivity,
                                        "Oops",
                                        json.getString("message"),
                                        "Ok",
                                        View.OnClickListener {
                                            if (dialogMsg?.myDialogMsg?.isShowing == true)
                                                dialogMsg?.myDialogMsg?.dismiss()
                                        })
                                } else {
                                    dialogMsg!!.showYesCancelDialog(
                                        this@ActivityDetailActivity,
                                        json.getString("message"),
                                        View.OnClickListener {
                                            fastRegisterBookingApi()
                                            dialogMsg!!.dismissDialog(this@ActivityDetailActivity)
                                        },
                                        true,
                                        "Continue"
                                    )
                                }
                            }
                        } catch (e: Exception) {
                            Log.e("Clash_API: onApiSuccess", e.toString())
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        showApiError()
                    }

                },
                compositeDisposable
            )
        } else {
            showConnectionError()
        }
    }

    private fun showEnquireDialog() {
        if (MyProfileUtils.arrayFamilyModel.size < 1 && userModel.userId.trim().isNotEmpty() && !wasFetchMembersHit) {
            fastFetchFamilyMembersFirst(2)
            return
        }

        if (activityModel != null && selectedSlotModel != null) {
            val dialog = MyDialog(this).getMyDialog(R.layout.dialog_enquire)

            dialog.btnSubmitEnquire.text = "Send"

            dialog.imgCloseEnquire.setOnClickListener {
                dialog.dismiss()
            }

            if (userModel.userFNAme.trim().isNotEmpty())
                dialog.etFullNameEnquire.setText((userModel.userFNAme.trim() + " " + userModel.userLName).trim())

            if (userModel.userEmail.trim().isNotEmpty() && !userModel.userEmail.trim().equals(
                    "null",
                    true
                )
            )
                dialog.etEmailEnquire.setText(userModel.userEmail.trim())

            if (userModel.userPhone.trim().isNotEmpty() && !userModel.userPhone.trim().equals(
                    "null",
                    true
                )
            )
                dialog.etPhoneEnquire.setText(userModel.userPhone.trim())

            MyProfileUtils.setSelectionEnd(dialog.etFullNameEnquire)
            MyProfileUtils.setSelectionEnd(dialog.etEmailEnquire)
            MyProfileUtils.setSelectionEnd(dialog.etPhoneEnquire)

            if (userModel.userId.trim().isNotEmpty()) {
                dialog.linAttendeeEnquire.visibility = View.VISIBLE
                dialog.imgAddAttendeeEnquire.setOnClickListener {
                    dialog.dismiss()
                    MyProfileUtils.showAddMemberDialog(this, {
                        fastFetchFamilyMembersFirst(2)
                    })
                }

                dialog.rvAttendeesEnquire.layoutManager = LinearLayoutManager(this)

                val adapter = AttendeeAdapter(
                    this,
                    MyProfileUtils.arrayFamilyModel,
                    { pos, isChecked, adapter ->
                        MyProfileUtils.arrayFamilyModel.forEach {
                            it.isSelected = false
                        }
                        val model = MyProfileUtils.arrayFamilyModel[pos]
                        model.isSelected = isChecked
                        MyProfileUtils.arrayFamilyModel.set(pos, model)
                        adapter.notifyDataSetChanged()
                    },
                    {

                    })
                dialog.rvAttendeesEnquire.adapter = adapter


            } else {
                dialog.linAttendeeEnquire.visibility = View.GONE
            }

            dialog.btnSubmitEnquire.setOnClickListener {
                val strName = dialog.etFullNameEnquire.text.toString().trim()
                val strEmail = dialog.etEmailEnquire.text.toString().trim()
                val strPhone = dialog.etPhoneEnquire.text.toString().trim()
                val strMsg = dialog.etMessageDialog.text.toString().trim()

                val validations = MyValidations(this)
                if (strName.isEmpty()) {
                    showErrorDialog("Please enter your name")
                    return@setOnClickListener
                }
                if (!validations.checkName(strName)) {
                    showErrorDialog("Please enter a valid name")
                    return@setOnClickListener
                }

                if (strEmail.isEmpty()) {
                    showErrorDialog("Please enter your Email ID")
                    return@setOnClickListener
                }

                if (!validations.checkEmail(strEmail)) {
                    showErrorDialog("Please enter a valid Email ID")
                    return@setOnClickListener
                }

                if (strPhone.isEmpty()) {
                    showErrorDialog("Please enter your phone number")
                    return@setOnClickListener
                }

                if (!validations.checkMobile(strPhone)) {
                    showErrorDialog("Please enter a valid phone number")
                    return@setOnClickListener
                }

                if (strMsg.isEmpty()) {
                    showErrorDialog("Please enter your enquiry message")
                    return@setOnClickListener
                }


                var selectedAttendeeModel: FamilyModel? = null

                //If enquiry is being done by a logged in user, for his/her family member
                if (userModel.userId.trim().isNotEmpty()) {
                    if (MyProfileUtils.arrayFamilyModel.size < 1) {
                        showErrorDialog("Please add at least one attendee to send enquiry")
                        return@setOnClickListener
                    }

                    val countSelected =
                        MyProfileUtils.arrayFamilyModel.filter { familyModel -> familyModel.isSelected }
                            .size
                    if (countSelected < 1) {
                        showErrorDialog("Please select one attendee to send enquiry")
                        return@setOnClickListener
                    }

                    selectedAttendeeModel =
                        MyProfileUtils.arrayFamilyModel.singleOrNull { it.isSelected }

                    try {
                        if (selectedSlotModel != null && countSelected > 0) {
                            val memberAge = selectedAttendeeModel!!.familyMemberAgeDouble
                            Log.d("MemberAge_", "" + memberAge)
                            Log.d("MemberAgeFrom_", "" + selectedSlotModel!!.slotAgeFrom)
                            Log.d("MemberAgeTo_", "" + selectedSlotModel!!.slotAgeTo)
                            if (memberAge < selectedSlotModel!!.slotAgeFrom || memberAge > selectedSlotModel!!.slotAgeTo) {
                                showErrorDialog(
                                    "You must be in the age group of (${selectedSlotModel?.slotAge?.replace(
                                        "yr",
                                        "years"
                                    )}) to be eligible for this activity"
                                )
                                return@setOnClickListener
                            }
                        }

                    } catch (e: Exception) {
                        Log.e("ageCheck", e.toString())
                    }


                }


                val hashParams = HashMap<String, String>()
                hashParams.put("providerId", activityModel!!.activityProviderId)
                hashParams.put("activityId", activityModel!!.activityId)
                hashParams.put("timeSlotId", selectedSlotModel!!.slotId)
                hashParams.put("name", strName)
                hashParams.put("emailId", strEmail)
                hashParams.put("phoneNumber", strPhone)
                hashParams.put("message", strMsg)
                hashParams.put(
                    "attendeeId",
                    selectedAttendeeModel?.familyId ?: ""
                )

                fastSendEnquiry(hashParams, dialog)


            }

            try {
                DialogMsg.animateViewDialog(dialog.relParentEnquireDialog)
                dialog.show()
            } catch (e: Exception) {

            }
        }
    }

    private fun fastSendEnquiry(hashParams: HashMap<String, String>, dialog: Dialog) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            DialogMsg.showPleaseWait(this)
            FastNetworking.makeRxCallPost(
                applicationContext, Urls.SEND_USER_ENQUIRY,
                true,
                hashParams,
                "SendEnquiry",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                dialog.dismiss()
                                val strMsg = json.optString(
                                    "message",
                                    "Your enquiry has been successfully sent. We will get back to you as soon as possible."
                                )
                                dialogMsg?.showSuccessDialog(
                                    this@ActivityDetailActivity,
                                    "Success",
                                    strMsg,
                                    "OK",
                                    View.OnClickListener { dialogMsg?.dismissDialog(this@ActivityDetailActivity) })
                            } else {
                                showErrorDialog(ErrorMsgs.ERR_API_MSG)
                            }
                        } catch (e: Exception) {
                            Log.d("ExcSendEnquiry", "" + e)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        showErrorDialog(ErrorMsgs.ERR_API_MSG)

                    }

                },
                compositeDisposable
            )
        } else {
            toast(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }

    private fun fastFetchReviews() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            progressReviews.visibility = View.VISIBLE
            val hashParams = HashMap<String, String>()
            hashParams["activityId"] = "" + activityModel?.activityId
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.VIEW_REVIEWS,
                true,
                hashParams,
                "ViewReviews",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        doAsync {
                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    arrayReviewModel.clear()
                                    arrayReviewModel.addAll(JsonParse.parseReviews(json))

                                }

                            } catch (e: Exception) {
                                Log.d("ExcReviews", "" + e)
                            } finally {
                                runOnUiThread {
                                    progressReviews.visibility = View.GONE
                                    if (arrayReviewModel.size > 0) {
                                        linReviewsDetailPage.visibility = View.VISIBLE
                                        reviewAdapter = ReviewsAdapter(
                                            this@ActivityDetailActivity,
                                            arrayReviewModel,
                                            { posClicked ->
                                                val revModel = arrayReviewModel[posClicked]
                                                Log.d(
                                                    "RevModel",
                                                    "" + revModel + "\nRevUser:\n" + revModel.reviewUserId
                                                )
                                                Log.d("MyUserId", "" + userModel.userId)
                                                if (userModel.userId.trim().isNotEmpty() && revModel.reviewUserId.trim().isNotEmpty()) {
                                                    if (userModel.userId == revModel.reviewUserId) {
                                                        showPostReviewDialog(true, revModel)
                                                    }
                                                }
                                            },
                                            { _, _ ->

                                            })
                                        rvReviews.adapter = reviewAdapter

                                        txtViewAllReviews.visibility =
                                            if (arrayReviewModel.size > 2) View.VISIBLE else View.GONE
                                        compositeDisposable.add(
                                            RxView.clicks(txtViewAllReviews).throttleFirst(
                                                700,
                                                TimeUnit.MILLISECONDS
                                            ).subscribe {
                                                if (activityModel != null)
                                                    MyNavigations.goToAllReviews(
                                                        this@ActivityDetailActivity,
                                                        arrayReviewModel,
                                                        activityId = activityModel!!.activityId
                                                    )
                                            })

                                    } else {
                                        linReviewsDetailPage.visibility = View.GONE
                                    }

                                }
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        try {
                            progressReviews.visibility = View.GONE
                            linReviewsDetailPage.visibility = View.GONE
                        } catch (e: Exception) {

                        }
                    }

                })
        }
    }

    private fun showErrorDialog(strMsg: String, isSeatFull: Boolean = false) {
        dialogMsg?.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this@ActivityDetailActivity)

            if (isSeatFull) {
                //finish the intermediate ActivitySlots activity as well
                ActivitySlotsActivity.actSlots?.get()?.finish()

                //finish the intermediate ActivityDetail activity as well
                actDetail?.get()?.finish()

                //finish the intermediate CampDetail activity as well
                CampDetailActivity.campAct?.get()?.finish()

            }
        })
    }

    private fun showPostReviewDialog(
        isEditReview: Boolean = false,
        reviewModel: ReviewModel? = null
    ) {
        try {
            if (activityModel != null && selectedSlotModel != null) {
                if ((!activityModel!!.isReviewGiven || isEditReview) && userModel.userId.trim().isNotEmpty()) {
                    val dialog = MyDialog(this).getMyDialog(R.layout.dialog_add_edit_review)
                    if (!isEditReview) {
                        dialog.ratingBarPostReview.rating = 5f
                    }
                    dialog.ratingBarPostReview.setOnRatingChangeListener { baseRatingBar, flValue ->
                        Log.d("RatingValue", "" + flValue)
                    }

                    if (isEditReview && reviewModel != null) {
                        dialog.etReview.setText(reviewModel.reviewDesc.trim())
                        MyProfileUtils.setSelectionEnd(dialog.etReview)
                        dialog.ratingBarPostReview.rating = reviewModel.reviewRating
                        dialog.btnPostReview.text = "Update Review"
                    }

                    dialog.btnPostReview.setOnClickListener {
                        val strReview = dialog.etReview.text.toString().trim()
                        val flRatingValue = dialog.ratingBarPostReview.rating

                        /*if (strReview.isEmpty()) {
                            showErrorDialog("Please write some review and then proceed")
                            return@setOnClickListener
                        }*/

                        if (strReview.isNotEmpty() && strReview.length < 10) {
                            showErrorDialog("Your review should be at least of 10 characters")
                            return@setOnClickListener
                        }


                        dialog.dismiss()

                        val hashParams = HashMap<String, String>()
                        hashParams.put("activityId", "" + activityModel?.activityId)
                        hashParams.put("reviewRating", "" + flRatingValue)
                        hashParams.put("reviewDescription", strReview)

                        if (isEditReview && reviewModel != null)
                            hashParams.put("reviewId", "" + reviewModel.reviewId)

                        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                            MyReviewUtils.fastAddRatingReview(
                                this,
                                hashParams,
                                "AddReview",
                                { json ->
                                    try {
                                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                            val strMsg =
                                                json.optString(
                                                    "message",
                                                    "Your review has been successfully posted"
                                                )
                                            dialogMsg?.showSuccessDialog(
                                                this, "Success", strMsg, "OK",
                                                View.OnClickListener {
                                                    dialogMsg?.dismissDialog(this)
                                                    fastFetchActivityDetails()
                                                    fastFetchReviews()
                                                }, isCancellable = false
                                            )
                                        } else {
                                            showApiError()
                                        }
                                    } catch (e: Exception) {

                                    }
                                },
                                compositeDisposable
                            )
                        } else {
                            showConnectionError()
                        }

                    }

                    dialog.imgCloseDialogPostReview.setOnClickListener {
                        dialog.dismiss()
                    }

                    DialogMsg.animateViewDialog(dialog.relParentPostReview)

                    dialog.show()
                }
            }
        } catch (e: Exception) {
            Log.d("ReviewDialogExc", "" + e)
        }
    }

    //this enteredAmount is not including the VAT (Tax) enteredAmount
    private fun getTotalPricePayable(): String {
        var singlePrice = 0.0
        if (activityModel != null && selectedSlotModel != null) {
            try {
                singlePrice =
                    JsonParse.decimalFormat.format(selectedSlotModel!!.slotPrice.toDouble())
                        .toDouble()
            } catch (e: Exception) {
                singlePrice = 0.0
            }
            val arraySessionsToAttend =
                selectedSlotModel!!.arrayEnrolModel.filter { !it.isDateAlreadyPassed }
            noOfSessionsToAttend = arraySessionsToAttend.size

        }


        return "" + (singlePrice * noOfSessionsToAttend)
    }

    private fun getSessionsJson(): String {
        var strJson = "[]"
        try {
            val arrJson = JSONArray()
            for (model in selectedSlotModel!!.arrayEnrolModel.filter { !it.isDateAlreadyPassed }) {
                val objJson = JSONObject()
                objJson.put("enrolment_slot_id", model.enrolId)
                objJson.put(
                    "enrolment_slot_date",
                    JsonParse.getFormattedDateTimeTableReverse(model.enrolDate)
                )
                objJson.put("enrolment_slot_time", model.enrolTime)
                objJson.put("enrolment_slot_week", model.enrolDay)
                objJson.put("enrolment_slot_duration", model.enrolDuration)
                arrJson.put(objJson)

                //Log.d("JSON_",""+objJson)

                try {
                    if (arrJson.length() == 1) {
                        val strDate = objJson.getString("enrolment_slot_date").trim()
                        val strTime =
                            objJson.getString("enrolment_slot_time").split("-")[0].trim() + ":00"

                        Log.d("EnrolSlotDateTime", "$strDate $strTime")

                        val dtFirstSlot = JsonParse.getFormattedServerDateJava("$strDate $strTime")

                        Log.d("Cancellation_hours", "" + activityModel!!.activityCancellationHours)
                        val cal = Calendar.getInstance()
                        cal.time = dtFirstSlot
                        cal.add(Calendar.HOUR, -activityModel!!.activityCancellationHours)

                        Log.d("DateThreshold", "" + cal.time)
                        dateBookingCancellationThreshold = JsonParse.getFormattedServerDateJavaReverse(cal.time)
                        Log.d("DateThresholdConverted", dateBookingCancellationThreshold)

                        if (!comingFromBooking && activityModel != null && activityModel?.curServerDateJava != null) {
                            activityModel?.isActivityBookingCancellable =
                                !activityModel!!.curServerDateJava!!.after(cal.time)
                        }
                    }
                } catch (e: Exception) {
                    Log.d("NumberExc__", "" + e)
                }

            }
            strJson = arrJson.toString()
            Log.d("SessionsJson", strJson)
            return strJson
        } catch (e: Exception) {
            Log.d("Exc_JsonPrepare", "" + e)
        }
        return strJson
    }

    /*private fun goToCcAvenue() {

    }*/

    private fun fastRegisterBookingApi() {

        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {

            orderId = "" + System.currentTimeMillis()

            val hashParams = HashMap<String, String>()

            Log.d("IsPassActivity=>", "" + activityModel?.isPassActivity);
            hashParams["isPassAct"] = if (activityModel?.isPassActivity == true) "1" else ""

            hashParams["providerId"] = "" + activityModel?.activityProviderId
            hashParams["activityId"] = "" + activityModel?.activityId
            hashParams["enrolmentId"] = "" + selectedSlotModel?.slotEnrolmentId
            hashParams["familyId"] =
                "" + MyProfileUtils.arrayFamilyModel.filter { it.isSelected }.get(0).familyId

            //total enteredAmount without including tax
            hashParams["totalBeforeTax"] = "" + getTotalPricePayable().toDoubleOrNull()

            //tax enteredAmount
            hashParams["vatAmount"] = "" + vatAmount
            hashParams["vatRate"] = "" + MyAppConfig.vatRate

            //total enteredAmount after adding tax enteredAmount
            hashParams["pricePayable"] = "" + totalAfterPromotionDisApplied//+ totalAmount

            hashParams["sessionJson"] = getSessionsJson()
            hashParams["device"] = "android"

            hashParams["thresholdCancellationDate"] = dateBookingCancellationThreshold

            hashParams["orderId"] = orderId

            hashParams["promotionDetails"] =
                PromotionOperation.getPromotionJsonArray(arrPromotionModel)



            DialogMsg.showPleaseWait(this)
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.ADD_BOOKING,
                true,
                hashParams,
                "ConfirmBooking",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                val strSuccessMsg = json.optString(
                                    "message",
                                    "Your booking costing AED ${hashParams.get("pricePayable")} is successful"
                                )
                                try {
                                    if (activityModel?.isPassActivity == true) {
                                        userModel.currentPassWallet =
                                            DecimalFormat("##.##").format(userModel.currentPassWallet.toDouble() - totalAfterPromotionDisApplied.toDouble())
                                        UserModel.setUserModel(
                                            this@ActivityDetailActivity,
                                            userModel
                                        )
                                    }
                                } catch (e: Exception) {
                                    Log.d("currentPassWallet", e.toString())
                                }


                                MyNavigations.goToCcAvenue(
                                    this@ActivityDetailActivity,
                                    json.optString("booking_id", ""),
                                    "" + totalAfterPromotionDisApplied,
                                    "" + activityModel?.activityProviderId,
                                    strSuccessMsg,
                                    orderId
                                    , fromPassBooking = activityModel?.isPassActivity
                                        ?: false
                                )
                            } else {
                                val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)

                                if (json.has("seat_full") && json.optString("seat_full", "0") == "1") {
                                    showErrorDialog(strErrMsg, isSeatFull = true)
                                } else {
                                    val userStatus = json.optString("customer_status", "1")
                                    //show info pop up then log out user if user is deactivated
                                    if (userStatus.equals("0", true)) {
                                        dialogMsg?.showErrorRetryDialog(
                                            this@ActivityDetailActivity,
                                            ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,
                                            strErrMsg,
                                            "OK",
                                            View.OnClickListener {
                                                dialogMsg?.dismissDialog(this@ActivityDetailActivity)
                                                LoginBasicsUi.logOutUser(this@ActivityDetailActivity)
                                            },
                                            isCancellable = false
                                        )
                                    } else {
                                        showErrorDialog(strErrMsg)
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            Log.d("ParseError", "" + e)
                            showApiError()
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        showApiError()
                    }

                },
                compositeDisposable
            )

        } else {
            showConnectionError()
        }
    }

    private fun showApiError() {
        dialogMsg?.showErrorApiDialog(this, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        })
    }

    private fun showApiErrorRetry(onRetry: () -> Unit) {
        dialogMsg?.showErrorApiDialog(this, "Retry", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
            onRetry()
        }, isCancellable = true)
    }

    private fun showConnectionError() {
        dialogMsg?.showErrorConnectionDialog(this, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        })
    }


    private fun showSureCancelBooking() {
        val dialogConfirm = DialogMsg()
        val refundAmt = bookingModel?.bookingAmtAfterTax
        dialogConfirm.showYesCancelDialog(
            this,
            "Are you sure you want to cancel your booking?",
            View.OnClickListener {
                dialogConfirm.dismissDialog(this)
                fastCancelBooking()
            },
            isCancellable = true,
            strBtnYesText = "Yes",
            strBtnNoText = "No"
        )
    }

    private fun fastFetchSimilarActivities() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            val hashParams = HashMap<String, String>()
            hashParams["latitude"] = "" + MyPlaceUtils.placeModel.placeLat
            hashParams["longitude"] = "" + MyPlaceUtils.placeModel.placeLng
            hashParams["categoryId"] = activityModel?.categoryId ?: ""
            hashParams["activityId"] = activityModel?.activityId ?: ""
            hashParams["cityPlaceId"] = MyPlaceUtils.placeModel.placeCityId

            if (activityModel?.isPassActivity == true || bookingModel?.isPassAct == true)
                hashParams.put("showPassActivities", "1")
            //   DialogMsg.showPleaseWait(this)
            progressSimilar.visibility = View.VISIBLE
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.VIEW_SIMILAR_ACTIVITIES,
                true,
                hashParams,
                "SimilarActivities",
                object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        progressSimilar.visibility = View.GONE
                        // DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                            val (arrActivity, arrActivityForMap) = JsonParse.parseNearbyActivities(
                                json,
                                json)
                            fillSimilarActivities(arrActivityForMap)
                        } else {
                            linSimilarActivities.visibility = View.GONE
                        }
                    }

                    override fun onApiError(error: ANError) {
                        progressSimilar.visibility = View.GONE
                        //  DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                    }

                },
                compositeDisposable
            )
        }
    }

    private fun fastCancelBooking() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            val hashParams = HashMap<String, String>()
            hashParams["booking_id"] = "" + bookingModel?.bookingId
            hashParams["total_refund_amount"] = "" + bookingModel?.bookingAmtAfterTax
            hashParams["cancelled_device"] = "android"

            //If pass activity hit different API, if not hit different API
            val cancelUrl =
                if (activityModel?.isPassActivity == true) Urls.CANCEL_PASS_ACTIVITY_BOOKING else Urls.CANCEL_BOOKING

            DialogMsg.showPleaseWait(this)
            FastNetworking.makeRxCallPost(
                applicationContext,
                cancelUrl,
                true,
                hashParams,
                "CancelBooking",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                val strMsg = json.optString("message", "")
                                dialogMsg?.showSuccessDialog(
                                    this@ActivityDetailActivity,
                                    "Success",
                                    strMsg,
                                    "OK",
                                    View.OnClickListener {
                                        dialogMsg?.dismissDialog(this@ActivityDetailActivity)
                                        bookingModel?.bookingStatus =
                                            StaticValues.BOOKING_STATUS_CANCELLED
                                        isBookingStatusCancelled = true
                                        btnBookEnquire.text = "Book Now"
                                        finish()
                                    },
                                    isCancellable = false
                                )
                            } else {
                                val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)
                                showErrorDialog(strErrMsg)
                            }
                        } catch (e: Exception) {
                            showApiError()
                        }
                    }


                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        showApiError()
                    }

                })
        } else {
            showConnectionError()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MyNavigations.RQ_ADD_MONEY && resultCode == Activity.RESULT_OK) {
            userModel = UserModel.getUserModel(this@ActivityDetailActivity)
            makePaymentDialog?.run {
                val walletAmount = JsonParse.getValidPassAmount(userModel.currentPassWallet)
                txtPassBalValue.text = "AED $walletAmount"
                if (walletAmount > 0.0) {
                    txtPassValidTill.visibility = View.VISIBLE
                    txtPassValidTill.text = "(Valid till ${userModel.passExpiryDate})"
                }
            }
        }
    }


}

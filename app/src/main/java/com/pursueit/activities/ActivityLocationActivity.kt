package com.pursueit.activities

import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.pursueit.R
import com.pursueit.adapters.ActivityForMapAdapter
import com.pursueit.model.ActivityModel
import com.yarolegovich.discretescrollview.DiscreteScrollView
import kotlinx.android.synthetic.main.activity_location.*
import com.google.maps.android.ui.IconGenerator
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer

class ActivityLocationActivity : AppCompatActivity() {

    private val arrActivity: ArrayList<ActivityModel> by lazy {
        intent.getSerializableExtra("arrActivity") as ArrayList<ActivityModel>
    }
    private val iconFactory: IconGenerator by lazy {
        IconGenerator(this@ActivityLocationActivity)
    }
    private val arrMarkers = ArrayList<Marker>()
    private var oldMarker: Marker? = null
    private var olderMarkerText = ""
    private var googleMap: GoogleMap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        init()
        setAdapter()
        setListener()
    }

    private fun setAdapter() {
        dsvActivities.adapter = ActivityForMapAdapter(this@ActivityLocationActivity,arrActivity)
        dsvActivities.setItemTransformer(
            ScaleTransformer.Builder()
                .setMaxScale(1.00f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.CENTER) // CENTER is a default one
                .build())
        dsvActivities.setSlideOnFling(true)
    }

    fun init() {
        if (googleMap == null) {
            (supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment).getMapAsync { map ->
                googleMap = map

                if (googleMap != null) {
                    Log.d("actual array size", arrActivity.size.toString())
                    val arrUniqueLocations = arrActivity.distinctBy { Pair(it.lat, it.lng) }
                    Log.d("unique array size", arrUniqueLocations.size.toString())

                    arrUniqueLocations.forEach {
                        val latLng = LatLng(it.lat, it.lng)
                        addIcon(iconFactory, it.activityName, latLng)
                        //val marker = MarkerOptions().position(latLng)
                        //marker.title(it.activityName)
                        //googleMap!!.addMarker(marker)
                    }
                    if (arrUniqueLocations.isNotEmpty()) {
                        val model = arrUniqueLocations[0]
                        googleMap!!.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    model.lat,
                                    model.lng
                                ), 12f
                            ))
                    }

                    googleMap!!.setOnMarkerClickListener { it ->
                        val markerId = it.id
                        val marker = arrMarkers.single { it.id == markerId }
                        val pos =
                            arrActivity.indexOfFirst { it.lat == marker.position.latitude && it.lng == marker.position.longitude }
                        //val pos=arrActivity.indexOf(actModel)
                        //val pos=arrMarkers.indexOf(marker)
                        dsvActivities.smoothScrollToPosition(pos)
                        updateMarkerUI(arrActivity[pos], pos)
                        return@setOnMarkerClickListener false
                    }
                    if (ContextCompat.checkSelfPermission(
                            applicationContext,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        googleMap!!.isMyLocationEnabled = true
                    }
                }
            }
        }
    }

    private fun updateMarkerUI(model: ActivityModel, position: Int) {
        if (oldMarker != null) {
            iconFactory.setStyle(IconGenerator.STYLE_DEFAULT)
            oldMarker?.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(olderMarkerText))) //
        }
        val actModel = arrActivity[position]
        val marker = arrMarkers.single { it.position.latitude == actModel.lat && it.position.longitude == actModel.lng }
        //val marker=arrMarkers[position]
        iconFactory.setStyle(IconGenerator.STYLE_ORANGE)
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(model.activityName)))
        oldMarker = marker
        olderMarkerText = model.activityName
    }

    private fun setListener() {
        dsvActivities.addOnItemChangedListener { viewHolder, adapterPosition ->
            val model = arrActivity[adapterPosition]
            val cameraPos = CameraPosition.Builder().zoom(12f).target(LatLng(model.lat, model.lng)).build()
            val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPos)

            updateMarkerUI(model, adapterPosition)

            googleMap?.animateCamera(cameraUpdate)
        }



        dsvActivities.addScrollStateChangeListener(object :
            DiscreteScrollView.ScrollStateChangeListener<RecyclerView.ViewHolder> {
            override fun onScroll(
                scrollPosition: Float,
                currentPosition: Int,
                newPosition: Int,
                currentHolder: RecyclerView.ViewHolder?,
                newCurrent: RecyclerView.ViewHolder?
            ) {

            }

            override fun onScrollEnd(currentItemHolder: RecyclerView.ViewHolder, adapterPosition: Int) {

            }

            override fun onScrollStart(currentItemHolder: RecyclerView.ViewHolder, adapterPosition: Int) {

            }

        })
    }

    private fun addIcon(iconFactory: IconGenerator, text: CharSequence, position: LatLng) {
        val markerOptions =
            MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).position(position)
                .anchor(iconFactory.anchorU, iconFactory.anchorV)

        arrMarkers.add(googleMap?.addMarker(markerOptions)!!)
    }
}

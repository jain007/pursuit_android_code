package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.adapters.ActivitySlotsDetailAdapter
import com.pursueit.adapters.AgeGroupsAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.fragments.NearbyActivitiesListingFragment
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.*
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.action_layout_filter_option.view.*
import kotlinx.android.synthetic.main.activity_slots.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.dialog_filter_new.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.util.HashMap
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.any
import kotlin.collections.filter
import kotlin.collections.forEach
import kotlin.collections.single

class ActivitySlotsActivity : AppCompatActivity() {

    private var activityModel: ActivityModel? = null

    private var compositeDisposable = CompositeDisposable()

    private var arrayAgeGroups = ArrayList<AgeGroupModel>()
    private var arraySlotModel = ArrayList<ActivitySlotModel>()
    private var slotsAdapter: ActivitySlotsDetailAdapter? = null

    var arrayFlexiSessionModel = ArrayList<ActivityFlexiSessionModel>()

    lateinit var viewFilter: View

    var isFlexiActivity: Boolean = false

    //lateinit var arrActivityForMapScreen:ArrayList<ActivityModel>

    companion object {
        var actSlots: WeakReference<ActivitySlotsActivity>? = null
        //var isFilterApplied=false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slots)
        init()
        actSlots = WeakReference<ActivitySlotsActivity>(this)

        setActivityDetailsUi(false)

    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.MORE_SLOTS_SCREEN, UserModel.getUserModel(this).userEmail, this::class.java)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun init() {
        toolbarAndMenuFunctionality()



        rvActivitySlots.layoutManager = LinearLayoutManager(this)
        rvActivitySlots.setHasFixedSize(true)

        isFlexiActivity = intent.getBooleanExtra("isFlexiActivity", false)
    }

    private fun toolbarAndMenuFunctionality() {
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

        //menu on toolbar
        toolbarFragment.inflateMenu(R.menu.menu_nearby_activities)
        val itemSearch = toolbarFragment.menu.findItem(R.id.itemSearch)
        itemSearch.isVisible = false

        // decide bubble indicator for filter button inside option menu (menu_nearby_activities)
        try {

            //hide sort option
            toolbarFragment.menu.findItem(R.id.itemSort).isVisible = false

            val menuItem = toolbarFragment.menu.findItem(R.id.itemFilter)
            viewFilter = menuItem.actionView
            compositeDisposable.add(RxView.clicks(viewFilter.relFilterItem).throttleFirst(800, TimeUnit.MILLISECONDS).subscribe {
                showFilterDialogAgeGroups()
            })
            viewFilter.imgFilterIndicator.visibility = if (MyAppConfig.arrayAgeGroupModel.any { it.groupIsSelected }) View.VISIBLE else View.GONE
        } catch (e: Exception) {
        }
    }

    private fun setActivityDetailsUi(comingAfterApiCall: Boolean) {
        try {
            if (!comingAfterApiCall)
                activityModel = intent?.extras!!.getSerializable("ActivityModel") as ActivityModel

            if (isFlexiActivity)
                toolbarFragment.txtToolbarHeader.text = "Flexi-Activity Sessions"
            else
                toolbarFragment.txtToolbarHeader.text = "Activity Slots"
            toolbarFragment.txtToolbarHeader.setPadding(56, 0, 0, 0)

            PromotionOperation.setPromotionText(linPromotion, activityModel?.maxValuePromotionModel)

            //activity details
            txtActivityTitle.text = activityModel!!.activityName
            txtActivitySubTitle.text = activityModel!!.activityProviderName
            txtActivityLocation.text = (activityModel!!.activityLocation.trim() + "\n" + activityModel!!.activityMsgLocationSuffix.trim()).trim()

            txtStarCount.text = "(${activityModel?.activityStarRatingCount})"
            ratingBarAllSlots.rating = activityModel!!.activityStarRating
            linRatingBarView.visibility = if (activityModel!!.activityStarRatingCount > 0) View.VISIBLE else View.GONE


            Glide.with(applicationContext).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true)).load(Urls.IMAGE_FEATURE_ACTIVITY + activityModel?.activityImageUrl).into(imgActivity)

            if (isFlexiActivity) {
                arrayFlexiSessionModel.clear()
                arrayFlexiSessionModel.addAll(activityModel!!.arrFlexiSession)
            } else {
                arraySlotModel.clear()
                arraySlotModel.addAll(activityModel!!.arraySlotModel)
            }
            fillAdapter()


        } catch (e: Exception) {
            Log.d("Exc_ActSlots", "" + e)
        }
    }

    private fun fillAdapter() {
        if (slotsAdapter == null) {
            slotsAdapter = ActivitySlotsDetailAdapter(this, arraySlotModel, arrayFlexiSessionModel, isFlexiActivity) { pos ->

                if (activityModel!!.arrFlexiSession.size > 0) //flexi activity
                {
                    MyNavigations.goToFlexiActivityDetail(this@ActivitySlotsActivity, activityModel!!, pos)
                } else {
                    val selectedModel = arraySlotModel[pos]

                    //after filtering position tends to change, so calculate correct position and then move ahead
                    doAsync {
                        var actualPos = pos
                        for (i in 0 until activityModel!!.arraySlotModel.size) {
                            val actualModel = activityModel!!.arraySlotModel[i]
                            if (actualModel.slotEnrolmentId == selectedModel.slotEnrolmentId && actualModel.slotId == selectedModel.slotId) {
                                actualPos = i
                                break
                            }
                        }

                        runOnUiThread {
                            MyNavigations.goToActivityDetail(this@ActivitySlotsActivity, activityModel!!, actualPos)
                        }
                    }
                }

            }
            rvActivitySlots.adapter = slotsAdapter
        } else {
            slotsAdapter?.notifyDataSetChanged()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showFilterDialogAgeGroups() {
        try {
            if (arrayAgeGroups.isEmpty()) {
                arrayAgeGroups = MyAppConfig.arrayAgeGroupModel
                //arrayAgeGroups.forEach { it.groupIsSelected = false }
            }
            if (arrayAgeGroups.size > 0) {
                val dialog = MyDialog(this).getMyDialog(R.layout.dialog_filter_new)
                dialog.rvAgeGroups.layoutManager = LinearLayoutManager(this)
                dialog.rvAgeGroups.setHasFixedSize(true)


                val adapter = AgeGroupsAdapter(this, arrayAgeGroups) { pos, isChecked ->
                    arrayAgeGroups.forEach {
                        it.groupIsSelected = false
                    }
                    val model = arrayAgeGroups[pos]
                    model.groupIsSelected = isChecked
                    arrayAgeGroups.set(pos, model)

                    dialog.rvAgeGroups.adapter?.notifyDataSetChanged()
                }

                dialog.rvAgeGroups.adapter = adapter

                dialog.imgCloseDialogFilterNew.setOnClickListener {
                    dialog.dismiss()
                }

                dialog.txtClearFiltersNew.setOnClickListener {
                    dialog.dismiss()
                    /*arraySlotModel.clear()
                    arraySlotModel.addAll(activityModel!!.arraySlotModel)
                    fillAdapter()*/
                    viewFilter.imgFilterIndicator.visibility = View.GONE

                    //reset filters
                    DashboardActivity.ageFrom = ""
                    DashboardActivity.ageTo = ""
                    NearbyActivitiesListingFragment.resetMakeResponseEmptyAllCategories(this)

                    fastFilterActivityAndItsSlots(null)
                }


                dialog.txtApplyFilterNew.setOnClickListener {
                    val dialogErr = DialogMsg()
                    val ageGroups = arrayAgeGroups.filter { it.groupIsSelected }
                    if (ageGroups.isEmpty()) {
                        dialogErr.showErrorRetryDialog(this, "Alert", "Please select one age group to apply filter", "OK", View.OnClickListener { dialogErr.dismissDialog(this) })
                        return@setOnClickListener
                    }

                    val selectedAgeGroup = arrayAgeGroups.single { it.groupIsSelected }
                    /*arraySlotModel.clear()
                    activityModel!!.arraySlotModel.forEach {
                        try {
                            val selectedAgeFrom = selectedAgeGroup.groupAgeFrom.toInt()
                            val selectedAgeTo = selectedAgeGroup.groupAgeTo.toInt()
                            if ((it.slotAgeFrom >= selectedAgeFrom && it.slotAgeTo <= selectedAgeTo) || (it.slotAgeFrom <= selectedAgeTo && it.slotAgeTo >= selectedAgeFrom)) {
                                arraySlotModel.add(it)
                            }
                        } catch (e: Exception) {
                            Log.d("NumberFormatIssue", "" + e)
                        }
                    }*/

                    //fillAdapter()

                    //val countResults = arraySlotModel.size
                    //val strResults = if (countResults > 1) "results" else "result"
                    //toast("$countResults $strResults (Age group ${selectedAgeGroup.groupAgeFrom} - ${selectedAgeGroup.groupAgeTo} yr)")


                    dialog.dismiss()

                    viewFilter.imgFilterIndicator.visibility = View.VISIBLE

                    //also apply filter to other nearby activity listing
                    DashboardActivity.ageFrom = selectedAgeGroup.groupAgeFrom
                    DashboardActivity.ageTo = selectedAgeGroup.groupAgeTo
                    DashboardActivity.refreshNearbyPlaces = true
                    NearbyActivitiesListingFragment.resetMakeResponseEmptyAllCategories(this)

                    fastFilterActivityAndItsSlots(selectedAgeGroup)


                }


                DialogMsg.animateViewDialog(dialog.relParentAgeFilterNew)

                dialog.show()
            } else {
                toast("Please wait... we are fetching age groups")
            }
        } catch (e: Exception) {
            Log.d("ExcDialogFilterNew", "" + e)
        }
    }

    private fun fastFilterActivityAndItsSlots(ageGroupModel: AgeGroupModel?) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {

            DialogMsg.showPleaseWait(this)

            val hashParams = HashMap<String, String>()
            hashParams.put("latitude", "" + MyPlaceUtils.placeModel.placeLat)
            hashParams.put("longitude", "" + MyPlaceUtils.placeModel.placeLng)
            hashParams.put("ageFrom", DashboardActivity.ageFrom)
            hashParams.put("ageTo", DashboardActivity.ageTo)
            hashParams.put("activityId", "" + activityModel?.activityId)
            hashParams.put("cityPlaceId", "" + MyPlaceUtils.placeModel.placeCityId)
            hashParams.put("categoryId", "")
            hashParams.put("subCategoryId", "")
            hashParams.put("page", "1")

            FastNetworking.makeRxCallPost(this, Urls.NEARBY_ACTIVITIES, true, hashParams, "NearByActivity_" + activityModel?.activityId,
                object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@ActivitySlotsActivity)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            doAsync {
                                try {
                                    val arrayActivities = ArrayList<ActivityModel>()
                                    if (json.getBoolean(StaticValues.KEY_STATUS)) {
                                        arrayActivities.clear()
                                        val jsonData = json.getJSONObject("data")
                                        val (arrActivity, arrActivityForMapScreen) = JsonParse.parseNearbyActivities(jsonData, json)
                                        arrayActivities.addAll(arrActivity)
                                    }
                                    if (arrayActivities.size > 0) {
                                        activityModel = arrayActivities[0]
                                        runOnUiThread {
                                            setActivityDetailsUi(true)
                                        }
                                    } else {
                                        showError("No results found.\n(Age group ${ageGroupModel?.groupLabel})")
                                    }
                                } catch (e: Exception) {
                                    Log.d("ExcParse_${activityModel?.activityId}", "ActivityId: ${activityModel?.activityId}\n" + e)
                                }
                            }

                        } else {
                            showError("No results found\n(Age group ${ageGroupModel?.groupLabel})")
                        }

                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivitySlotsActivity)
                        showError()
                    }

                }, compositeDisposable
            )
        } else {
            showError(ErrorMsgs.ERR_CONNECTION_MSG)
        }

    }

    private fun showError(strMsg: String = ErrorMsgs.ERR_API_MSG) {
        val dialogMsg = DialogMsg()
        dialogMsg.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg.dismissDialog(this)
        }, true)
    }
}

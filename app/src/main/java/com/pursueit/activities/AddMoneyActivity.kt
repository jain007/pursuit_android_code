package com.pursueit.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.text.HtmlCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.androidnetworking.error.ANError
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.UserModel
import com.pursueit.utils.ChangeDateFormat
import com.pursueit.utils.ConnectionDetector
import com.pursueit.utils.MyNavigations
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_add_money.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

@SuppressLint("SetTextI18n")
class AddMoneyActivity : AppCompatActivity() {

    var compositeDisposable = CompositeDisposable()
    val dialogMsg:DialogMsg by lazy { DialogMsg()}
    var orderId=""
    var enteredAmount:String=""

    var amountAfterSuccess:String=""
    var expiryDate:String=""
    var expiryDays:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_money)

        orderId = "" + System.currentTimeMillis()

        init()
        setListeners()
    }

     fun init() {

        val userModel = UserModel.getUserModel(applicationContext)

        setData(userModel)

        toolbarFragment.txtToolbarHeader.text = "Wallet Summary and T&C's"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }
    }

    private fun setData(userModel: UserModel){
        if (userModel.currentPassWallet!="" && userModel.currentPassWallet!="null")
            txtPassBalanceValue.text="AED ${userModel.currentPassWallet}"
        else
            txtPassBalanceValue.text="AED 0"

        if (userModel.passExpiryDate!="" && userModel.passExpiryDate!="null")
            txtPassExpiryDate.text="* Valid till "+userModel.passExpiryDate
        else
            txtPassExpiryDate.visibility=View.GONE

        if (userModel.passExpiryDays.trim()!="") {
            if (userModel.universalPassExpiry == "1")
                txtExpiryMessage.text =
                    "* Please note that pass will expire within ${userModel.universalPassExpiry} day from the date of last purchase"
            else
                txtExpiryMessage.text =
                    "* Please note that pass will expire within ${userModel.universalPassExpiry} days from the date of last purchase"
        }
    }

    private fun setListeners()
    {
        compositeDisposable.add(RxView.clicks(etAddMoney).throttleFirst(300,TimeUnit.MILLISECONDS).subscribe {
            dialogMsg.showSelectAmountDialog(this@AddMoneyActivity,etAddMoney)
        })

        compositeDisposable.add(RxView.clicks(btnPurchase).throttleFirst(300,TimeUnit.MILLISECONDS).subscribe {
            enteredAmount=etAddMoney?.tag?.toString()?:""
            if (enteredAmount=="")
            {
                dialogMsg.showErrorRetryDialog(this, "Alert", "Please select the amount", "OK", View.OnClickListener {
                    dialogMsg.dismissDialog(this)
                })
                return@subscribe
            }
            fastAddMoney()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==MyNavigations.RQ_CODE_CCAVENUE){
            if (resultCode== Activity.RESULT_OK){
                when(data?.getStringExtra("status")?:""){
                    "PaymentSuccess"->{

                        val userModel=UserModel.getUserModel(this@AddMoneyActivity)

                        try {
                            userModel.currentPassWallet = DecimalFormat("##.##").format(amountAfterSuccess.toDouble())
                        }catch (e:Exception){
                            userModel.currentPassWallet = amountAfterSuccess
                        }

                        userModel.passExpiryDate=expiryDate
                        userModel.passExpiryDays=expiryDays

                        UserModel.setUserModel(this@AddMoneyActivity,userModel)

                        setData(userModel)

                        /*txtPassBalanceValue.text=userModel.currentPassWallet
                        txtPassExpiryDate.text="* Valid till "+userModel.passExpiryDate*/

                        val daysLabel=if (expiryDays=="1") "day" else "days"

                        val dialogMsgSuccess = DialogMsg()
                        dialogMsgSuccess.showSuccessDialog(
                            this@AddMoneyActivity,
                            "Money Added to Wallet",
                            spannedMsg = HtmlCompat.fromHtml("Your money successfully added to wallet."
                                    +"<br/><br/>"+"Current Wallet Amount: <font color=#ef6437>AED "+amountAfterSuccess+"</font>"
                                    +"<br/>"+"Expiry Date: <font color=#ef6437>"+expiryDate+"(after $expiryDays $daysLabel)</font>",HtmlCompat.FROM_HTML_MODE_LEGACY),
                            onRetryListener = View.OnClickListener {
                                dialogMsgSuccess.dismissDialog(this)
                                setResult(Activity.RESULT_OK)
                                onBackPressed()
                            },strMsg = "",isCancellable = false)
                       // fastAddMoney()
                    }

                    "PaymentFailed"->{
                        val userModel=UserModel.getUserModel(this@AddMoneyActivity)

                        val expiryDate = if(userModel.passExpiryDate.trim().isEmpty() || userModel.passExpiryDate.trim().equals("null",true)) "N/A" else userModel.passExpiryDate.trim()
                        val expiryDays = if(userModel.passExpiryDays.trim().isEmpty() || userModel.passExpiryDays.equals("null",true)) "N/A" else userModel.passExpiryDays.trim()


                        val strFailMsg = "Current wallet information is as follows.\n\nPrevious Balance : AED ${userModel.currentPassWallet}\nAdded Amount: AED 0\nNew Wallet Balance: AED ${userModel.currentPassWallet}\n\nExpiry Days: $expiryDays\nExpiry Date: $expiryDate"
                        dialogMsg.showErrorRetryDialog(
                            this@AddMoneyActivity,"Wallet Top up failed",strFailMsg,
                            strBtnText="Ok",
                            onRetryListener = View.OnClickListener { dialogMsg.dismissDialog(this@AddMoneyActivity) })
                    }
                }
            }
        }
    }

    private fun fastAddMoney(){
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            val hashParams = HashMap<String, String>()
            hashParams["amount"] = enteredAmount
            hashParams["orderId"] = orderId
            DialogMsg.showPleaseWait(this@AddMoneyActivity)
            FastNetworking.makeRxCallPost(applicationContext, Urls.ADD_MONEY, true, hashParams, "Add_Money",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@AddMoneyActivity)
                        if (json != null) {
                            if (json.getBoolean("status")) {
                                val jobData=json.getJSONObject("data")
                                amountAfterSuccess=jobData.getString("current_amount")
                                expiryDate= ChangeDateFormat.convertDate(jobData.getString("pass_expiry_date"),"yyyy-MM-dd","dd MMM yyyy")
                                expiryDays=jobData.getString("pass_expiry_days")

                                MyNavigations.goToCcAvenue(this@AddMoneyActivity,orderId,enteredAmount,"","success",orderId,false,fromAddMoney = true)

                              /* */
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@AddMoneyActivity)
                        dialogMsg.showErrorApiDialog(
                            this@AddMoneyActivity,
                            onRetryListener = View.OnClickListener { dialogMsg.dismissDialog(this@AddMoneyActivity) })
                    }

                })
        } else {
            dialogMsg.showErrorConnectionDialog(this@AddMoneyActivity, onRetryListener = View.OnClickListener { dialogMsg.dismissDialog(this@AddMoneyActivity) })
        }
    }

    override fun onDestroy() {
        FastNetworking.compositeDisposable.clear()
        compositeDisposable.clear()
        super.onDestroy()
    }
}

package com.pursueit.activities

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.pursueit.R
import com.pursueit.adapters.AppIntroPagerAdapter
import com.pursueit.model.AppIntroModel
import com.pursueit.utils.GetSetSharedPrefs
import com.pursueit.utils.LoginBasicsUi
import com.pursueit.utils.MyNavigations
import com.pursueit.utils.StaticValues
import kotlinx.android.synthetic.main.activity_app_intro.*

class AppIntroActivity : AppCompatActivity() {

    private var curPos = 0
    private val arrayModel = arrayListOf(
        AppIntroModel(
            R.drawable.img_app_intro_discover_it,
            "Find best suited classes and activities or\nExplore new hobbies and trends"
        ),
        AppIntroModel(
            R.drawable.img_app_intro_book_it,
            "Make bookings instantly and\nPay via credit card"
        ),
        AppIntroModel(R.drawable.img_app_intro_pursue_it, "Get ready to\nPursue your Passion")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_intro)
        LoginBasicsUi.setTransparentStatusBarAndBgImage(this)

        setupViewPager()

        onClickListeners()
    }

    private fun setupViewPager() {

        val adapter = AppIntroPagerAdapter(this, arrayModel)
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(pos: Int) {
                curPos = pos

                val isLastPage = curPos == arrayModel.size - 1
                txtNext.text = if (isLastPage) "Let's Go" else "Next"
                txtSkip.visibility = if (isLastPage) View.INVISIBLE else View.VISIBLE
            }

        })

        dotsIndicator.setViewPager(viewPager)
    }

    private fun onClickListeners() {
        txtSkip.setOnClickListener {
            goToMainActivity()
        }

        txtNext.setOnClickListener {
            if (curPos == arrayModel.size - 1) {
                goToMainActivity()
            } else {
                viewPager.setCurrentItem(curPos + 1, true)
            }
        }
    }

    private fun goToMainActivity() {

        //set the flag to true so that next time app intro screen is not shown, based on condition in splash
        fun setAppIntroShownInLocal(){
            val prefs=GetSetSharedPrefs(applicationContext)
            prefs.putDataBoolean(StaticValues.KEY_APP_INTRO_SHOWN,true)
        }

        setAppIntroShownInLocal()

        //proceed to main activity
        val bundle = intent.extras
        MyNavigations.goToMainActivity(this, bundle)
    }
}

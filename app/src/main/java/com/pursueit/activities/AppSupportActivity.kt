package com.pursueit.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.UserModel
import com.pursueit.utils.ConnectionDetector
import com.pursueit.utils.MyProfileUtils
import com.pursueit.utils.MyValidations
import kotlinx.android.synthetic.main.activity_app_support.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import org.json.JSONObject

class AppSupportActivity : AppCompatActivity() {

    private lateinit var userModel: UserModel
    lateinit var dialogMsg: DialogMsg

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_support)

        dialogMsg = DialogMsg()
        userModel = UserModel.getUserModel(applicationContext)
        init()
        setUserDetailsUi()

        btnSend.setOnClickListener {
            sendQuery()
        }
    }

    fun init() {
        toolbarFragment.txtToolbarHeader.text = "Contact Support"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

    }

    private fun setUserDetailsUi() {
        try {
            if (MyProfileUtils.isNotNull(userModel.userFNAme.trim()))
                etName.setText(userModel.userFNAme.trim())
            if (MyProfileUtils.isNotNull(userModel.userLName.trim()))
                etName.setText((etName.text.toString() + " " + userModel.userLName.trim()).trim())
            if (MyProfileUtils.isNotNull(userModel.userPhone.trim()))
                etPhone.setText(userModel.userPhone.trim())

            if (MyProfileUtils.isNotNull(userModel.userEmail.trim()))
                etEmail.setText(userModel.userEmail.trim())

            MyProfileUtils.setSelectionEnd(etName)
            MyProfileUtils.setSelectionEnd(etPhone)
            MyProfileUtils.setSelectionEnd(etEmail)

        } catch (e: Exception) {
            Log.d("UserDetailsUi", "" + e)
        }
    }

    private fun sendQuery() {
        val strName = etName.text.toString().trim()
        val strPhone = etPhone.text.toString().trim()
        val strEmail = etEmail.text.toString().trim()
        val strAddress = etAddress.text.toString().trim()
        val strMessage = etMessage.text.toString().trim()

        if (strName.isEmpty()) {
            showError(strMsg = "Please enter your name")
            return
        }

        if (strEmail.isEmpty()) {
            showError(strMsg = "Please enter email address")
            return
        } else {
            if (!MyValidations(this).checkEmail(strEmail)) {
                showError(strMsg = "Please enter a valid email address")
                return
            }
        }

        if (strPhone.isEmpty()) {
            showError(strMsg = "Please enter phone number")
            return
        } else {
            if (!MyValidations(this).checkMobile(strPhone)) {
                showError(strMsg = "Please enter a valid phone number")
                return
            }
        }

        if (strMessage.isEmpty()) {
            showError(strMsg = "Please enter message")
            return
        }

        fastSubmitQuery(strName, strPhone, strEmail, strAddress, strMessage)
    }

    private fun fastSubmitQuery(
        name: String,
        phone: String,
        email: String,
        address: String,
        message: String
    ) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            val hashParams = HashMap<String, String>()
            hashParams["name"] = name
            hashParams["email"] = email
            hashParams["phone"] = "+971$phone"
            hashParams["address"] = address
            hashParams["query"] = message
            DialogMsg.showPleaseWait(this@AppSupportActivity)
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.APP_SUPPORT,
                true,
                hashParams,
                "APP_SUPPORT",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@AppSupportActivity)
                        if (json != null) {
                            if (json.getBoolean("status")) {
                                DialogMsg().showSuccessDialog(
                                    this@AppSupportActivity,
                                    "Message sent",
                                    "Your query has been sent successfully",
                                    onRetryListener = object : View.OnClickListener {
                                        override fun onClick(p0: View?) {
                                            onBackPressed()
                                        }
                                    })
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@AppSupportActivity)
                        dialogMsg?.showErrorApiDialog(
                            this@AppSupportActivity,
                            onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@AppSupportActivity) })
                    }

                })
        } else {
            dialogMsg?.showErrorConnectionDialog(this@AppSupportActivity, onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@AppSupportActivity) })
        }
    }

    private fun showError(strTitle: String = "Alert", strMsg: String) {
        dialogMsg.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg.dismissDialog(this)
        })
    }

    override fun onDestroy() {
        super.onDestroy()

    }
}

package com.pursueit.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.pursueit.BuildConfig
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.activity_ccavenue.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import mumbai.dev.sdkdubai.*
import org.json.JSONObject
import kotlin.math.roundToInt

class CCAvenueActivity : AppCompatActivity(), CustomModel.OnCustomStateListener {

    private val MID = BuildConfig.MID
    private val ACCESS_CODE = BuildConfig.ACCESS_CODE
    //private val ENCRYPTION_KEY = "31FA6110173E8CD1915D633A6F0F27D0"
    //private val ENCRYPTION_KEY = "12FB0CECE24301000E6E357C75DC63E5"

    lateinit var userModel: UserModel

    /*private val RSA_URL = "https://v2rsolution.in/CCAvenue-php/GetRSA.php"
    private val CANCEL_URL = "https://v2rsolution.in/CCAvenue-php/ccavResponseHandler.php"
    private val REDIRECT_URL = "https://v2rsolution.in/CCAvenue-php/ccavResponseHandler.php"*/

    private var bookingId: String = ""
    private var bookingAmount: String = ""
    private var bookingSuccessMsg: String = ""
    private var providerId: String = ""
    private var orderId: String = ""
    private var fromAddMoney: Boolean = false

    var strPhone=""

    var fromPassBooking:Boolean=false
    var dialogPhoneNumber:Dialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ccavenue)

        userModel = UserModel.getUserModel(applicationContext)

        toolbarFragment.txtToolbarHeader.text = "Initiate Payment"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener {
            ActivityDetailActivity.goToMyBookings = true
            onBackPressed()
        }

        CustomModel.getInstance().setListener(this)

        bookingId = intent.extras.getString("BookingId")
        bookingAmount = intent.extras.getString("BookingAmount")
        bookingSuccessMsg = intent.extras.getString("BookingMsg")
        providerId = intent.extras.getString("ProviderId")
        orderId = intent.extras.getString("OrderId")
        fromAddMoney = intent.extras.getBoolean("fromAddMoney")

        try {
            when(BuildConfig.FLAVOR){
                "staging", "qa_testing", "dev" -> bookingAmount = bookingAmount.toDouble().roundToInt().toString()
            }
        }catch (e:Exception){
            Log.d("RoundOffBookingAmt",bookingAmount.toString())
        }


        Log.d("intent_data", "---------------start---------------------")
        Log.d("bookingId", bookingId)
        Log.d("bookingAmount", bookingAmount)
        Log.d("bookingSuccessMsg", bookingSuccessMsg)
        Log.d("providerId", providerId)
        Log.d("orderId", orderId)
        Log.d("fromAddMoney", fromAddMoney.toString())
        Log.d("intent_data", "------------------end------------------")

        //MyGoogleAnalytics.sendSession(this,MyGoogleAnalytics.INIT_PAYMENT_SCREEN,userModel.userEmail,this::class.java)

        try {
            if (intent.hasExtra("fromPassBooking") && intent.getBooleanExtra("fromPassBooking",false))
            {
                txtMessage.text="Your booking of amount AED ${bookingAmount} is confirmed. Your booking id is ${bookingId}"
                updateBookingSuccessUi("Booking Successful", null)
            }
            else if (bookingAmount.toDouble() > 0.0) {
                if (userModel.userPhone.trim().isEmpty()) {
                    UserProfileUtils.showEnterPhoneDialog(this){ dialog, phone ->
                        dialogPhoneNumber=dialog
                        strPhone=phone
                        SendPhoneOtp(this).sendOtp(strPhone) { otp->
                            MyNavigations.goToEnterOtpForPhone(this,otp,strPhone)
                        }
                    }
                } else {
                    initiateBookingCcAvenue()
                }
            } else {
                updateBookingSuccessUi("Booking Successful", null)
            }
        } catch (e: Exception) {

        }

        //updateBookingSuccessUi("Booking Successful",null)

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==MyNavigations.RQ_SEND_SMS){
            val isPhoneVerified=data?.getBooleanExtra("isPhoneVerified",false)?:false
            if (isPhoneVerified) {
                dialogPhoneNumber?.dismiss()
                userModel.userPhone = strPhone

                UserProfileUtils.fastUpdatePhoneProfileUser(applicationContext,isPhoneVerified)

                initiateBookingCcAvenue()
            }
            else{
                showError("Your phone number is not verified")
            }
        }
        else{
            showError("Your phone number is not verified")
        }
    }



    private fun initiateBookingCcAvenue() {

        MyGoogleAnalytics.sendCustomEvent(
            this,
            MyGoogleAnalytics.CATEGORY_PAYMENT,
            MyGoogleAnalytics.ACTION_PAYMENT_INIT + " - AED $bookingAmount",
            userModel.userEmail
        )

        //set up merchant details
        val mMerchant = MerchantDetails()
        mMerchant.access_code = ACCESS_CODE
        mMerchant.merchant_id = MID
        mMerchant.currency = "AED"
        mMerchant.amount = bookingAmount
        mMerchant.redirect_url = BuildConfig.CANCEL_REDIRECT_URL//"https://secure.ccavenue.ae/transaction/jsp/ccavResponseHandler_43551.jsp"
        mMerchant.cancel_url = BuildConfig.CANCEL_REDIRECT_URL//"https://secure.ccavenue.ae/transaction/jsp/ccavResponseHandler_43551.jsp"
        mMerchant.rsa_url = BuildConfig.GET_RSA_URL//"https://secure.ccavenue.ae/transaction/jsp/GetRSA.jsp"

        mMerchant.order_id = orderId
        try {
            mMerchant.customer_id = "${userModel.userId.toInt() + 100}"
        } catch (e: Exception) {
            mMerchant.customer_id = "test"
        }
        mMerchant.isShow_addr = false

        mMerchant.isCCAvenue_promo = true
        mMerchant.promo_code = ""

        val strAppName = getString(R.string.app_name) ?: ""
        mMerchant.add1 = strAppName
        mMerchant.add2 = strAppName
        mMerchant.add3 = strAppName
        mMerchant.add4 = strAppName
        mMerchant.add5 = strAppName

        //set up billing details
        Log.d("UserModel__", userModel.toString())
        val mBillingAddress = BillingAddress()
        mBillingAddress.name = userModel.userFNAme ?: "Anonymous"

        val strAddressForCCAvenue =
            if (MyPlaceUtils.placeModel.placeTitle.trim().isNotEmpty()) giveValidatedAddress(
                MyPlaceUtils.placeModel.placeTitle.trim()
            ) else "Dubai"
        mBillingAddress.address = strAddressForCCAvenue
        mBillingAddress.country = "United Arab Emirates"
        mBillingAddress.state = "Dubai"
        mBillingAddress.city = "Dubai"

        mBillingAddress.telephone = if (userModel.userPhone.trim().isEmpty()) "123456789" else userModel.userPhone.trim()
        mBillingAddress.email = userModel.userEmail



        //set up shipping details
        val mShippingAddress = ShippingAddress()
        mShippingAddress.name = mBillingAddress.name
        mShippingAddress.address = mBillingAddress.address
        mShippingAddress.country = mBillingAddress.country
        mShippingAddress.state = mBillingAddress.state
        mShippingAddress.city = mBillingAddress.city
        mShippingAddress.telephone = mBillingAddress.telephone

        val i = Intent(this@CCAvenueActivity, PaymentOptions::class.java)
        i.putExtra("merchant", mMerchant)
        i.putExtra("billing", mBillingAddress)
        i.putExtra("shipping", mShippingAddress)
        i.putExtra("standard instructions", StandardInstructions().also { it.si_type = "" })
        startActivity(i)

    }

    override fun stateChanged() {
        val responseCcAvenue = CustomModel.getInstance().state
        Log.d("TxnStatus_", responseCcAvenue)
        runOnUiThread {
            if (responseCcAvenue.equals("Transaction Closed", true)) {
                showPaymentFailed()
                MyGoogleAnalytics.sendCustomEvent(this, MyGoogleAnalytics.CATEGORY_PAYMENT, MyGoogleAnalytics.ACTION_PAYMENT_FAILED + " - AED $bookingAmount", userModel.userEmail)
            } else {
                if (fromAddMoney)
                    fastUpdateCustomerWallet()
                else
                    fastUpdateStatusOnOurServer(responseCcAvenue)
            }
        }
    }

    private fun fastUpdateCustomerWallet() {
        DialogMsg.showPleaseWait(this)
        val hashParams = HashMap<String, String>()
        hashParams["orderId"] = bookingId


        FastNetworking.makeRxCallPost(
            applicationContext,
            Urls.UPDATE_CUSTOMER_WALLET,
            true,
            hashParams,
            "",
            object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(this@CCAvenueActivity)
                    try {
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                            val intent = Intent()
                            intent.putExtra("status", "PaymentSuccess")
                            setResult(Activity.RESULT_OK, intent)
                            onBackPressed()
                            return
                        } else {
                            proceedWalletTopUpFailed()
                        }
                    } catch (e: Exception) {
                        Log.d("ParseError", "" + e)
                        showError(ErrorMsgs.ERR_PARSE_TITLE)
                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(this@CCAvenueActivity)
                    showApiError()
                }

            })
    }

    private fun proceedWalletTopUpFailed(){
        val intent = Intent()
        intent.putExtra("status", "PaymentFailed")
        setResult(Activity.RESULT_OK, intent)
        onBackPressed()
    }

    private fun fastUpdateStatusOnOurServer(strResponse: String) {

        DialogMsg.showPleaseWait(this)
        val hashParams = HashMap<String, String>()
        hashParams["bookingId"] = bookingId
        hashParams["enteredAmount"] = bookingAmount
        hashParams["providerId"] = providerId

        try {
            val jsonResponse = JSONObject(strResponse)
            /*hashParams["orderId"] = jsonResponse.optString("order_id", "0")
            hashParams["transactionStatus"] = jsonResponse.optString("order_status", "Failed")
            hashParams["trackingId"] = jsonResponse.optString("tracking_id", "0")
            hashParams["bankReferenceId"] = jsonResponse.optString("bank_ref_no", "0")*/

            hashParams["paymentStatus"] = jsonResponse.optString("payment_status", "Failure")

            if (hashParams["paymentStatus"]!!.equals("success", true)) {
                MyGoogleAnalytics.sendCustomEvent(this, MyGoogleAnalytics.CATEGORY_PAYMENT,
                    MyGoogleAnalytics.ACTION_PAYMENT_SUCCESS + " - AED $bookingAmount", userModel.userEmail)
            } else {
                MyGoogleAnalytics.sendCustomEvent(
                    this, MyGoogleAnalytics.CATEGORY_PAYMENT,
                    MyGoogleAnalytics.ACTION_PAYMENT_FAILED + " - AED $bookingAmount",
                    userModel.userEmail)
            }

        } catch (e: Exception) {

        }

        //hashParams["transactionResponse"] = strResponse

        FastNetworking.makeRxCallPost(
            applicationContext,
            Urls.UPDATE_TRANSACTION,
            true,
            hashParams,
            "UpdateTransaction",
            object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(this@CCAvenueActivity)
                    try {
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            if (hashParams["paymentStatus"]!!.equals("Success", true)) {

                                updateBookingSuccessUi("Payment Successful", json)
                            } else {

                                showPaymentFailed(json.optString("message", getString(R.string.transaction_failed_msg)))
                            }
                        } else {
                            val strMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)
                            if (hashParams["paymentStatus"]!!.equals("Success", true)) {

                                cardSuccess.visibility = View.VISIBLE
                                txtTitle.text = "Alert!"
                                toolbarFragment.txtToolbarHeader.text = "Payment Successful"
                                txtMessage.text = strMsg

                                Glide.with(this@CCAvenueActivity)
                                    .load(R.drawable.ic_booking_success)
                                    .into(imgPaymentStatus)
                            } else {

                                showPaymentFailed(json.optString("message", getString(R.string.transaction_failed_msg)))
                            }
                        }
                    } catch (e: Exception) {
                        Log.d("ParseError", "" + e)
                        showError(ErrorMsgs.ERR_PARSE_TITLE)
                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(this@CCAvenueActivity)
                    showApiError()
                }

            })
    }

    private fun updateBookingSuccessUi(strTitle: String, json: JSONObject? = null) {


        //finish the intermediate ActivitySlots activity as well
        ActivitySlotsActivity.actSlots?.get()?.finish()

        //finish the intermediate ActivityDetail activity as well
        ActivityDetailActivity.actDetail?.get()?.finish()

        //finish the intermediate CampDetail activity as well
        CampDetailActivity.campAct?.get()?.finish()

        FlexiActivityDetailActivity.actFlexiDetail?.get()?.finish()

        cardSuccess.visibility = View.VISIBLE
        txtTitle.text = "Thank You!"

        linGoTo.visibility = View.VISIBLE

        var fromCamp = false

        if (intent.hasExtra("fromCamp") && intent.getBooleanExtra("fromCamp", false)) {
                fromCamp = true
                btnExploreOrCamp.text = "Explore camps"
        }
        else if (intent.hasExtra("fromPassBooking") && intent.getBooleanExtra("fromPassBooking",false)){
                fromPassBooking=true
                btnExploreOrCamp.text = "Explore pass"

        }
        btnExploreOrCamp.setOnClickListener {
            if (fromCamp) {
                ActivityDetailActivity.goToCamp = true
            } else if (fromPassBooking)
                ActivityDetailActivity.goToPass = true
            else
                ActivityDetailActivity.goToExplore = true

            DashboardActivity.refreshNearbyPlaces=true

            MyNavigations.goToDashboard(this,withNewTask = true)

            //ProviderDetailActivity.actProviderDetail?.get()?.finish()
            //onBackPressed()
        }

        btnMyCorner.setOnClickListener {
            //navigate to my bookings automatically after booking successful
            ActivityDetailActivity.goToMyBookings = true
            //ProviderDetailActivity.actProviderDetail?.get()?.finish()
           // onBackPressed()

            DashboardActivity.refreshNearbyPlaces=true

            MyNavigations.goToDashboard(this,withNewTask = true)
        }


        toolbarFragment.txtToolbarHeader.text = strTitle.trim()

        try {
            txtMessage.text = if (json != null) json.optString("message", bookingSuccessMsg) else bookingSuccessMsg
        } catch (e: Exception) {
            Log.d("MsgExc__", "" + e)
        }
        Glide.with(this@CCAvenueActivity).load(R.drawable.ic_booking_success).into(imgPaymentStatus)
    }

    private fun showPaymentFailed(strMsg: String = "") {

        if(fromAddMoney){
            val userModel=UserModel.getUserModel(this)
            val strFailMsg = "Current wallet information is as follows.\n\nPrevious Balance : AED ${userModel.currentPassWallet}\nAdded Amount: AED 0\nNew Wallet Balance: AED${userModel.currentPassWallet}\n\nExpiry Days: ${userModel.passExpiryDays}\nExpiry Date: ${userModel.passExpiryDate}"

            cardSuccess.visibility = View.VISIBLE
            txtTitle.text = "Wallet Top Up Failed!"
            toolbarFragment.txtToolbarHeader.text = "Payment Failed!"
            txtMessage.text = if (strMsg.trim().isEmpty()) strFailMsg else strMsg


            Glide.with(this@CCAvenueActivity).load(R.drawable.ic_booking_failed).into(imgPaymentStatus)

            proceedWalletTopUpFailed()
        }else{
            cardSuccess.visibility = View.VISIBLE
            txtTitle.text = "Oops!"
            toolbarFragment.txtToolbarHeader.text = "Payment Failed"
            txtMessage.text = if (strMsg.trim().isEmpty()) getString(R.string.transaction_failed_msg) else strMsg


            Glide.with(this@CCAvenueActivity).load(R.drawable.ic_booking_failed)
                .into(imgPaymentStatus)
        }

    }

    private fun showApiError() {
        val dialogMsg = DialogMsg()
        dialogMsg.showErrorApiDialog(this, "OK", View.OnClickListener {
            dialogMsg.dismissDialog(this)
        })
    }

    private fun showError(strMsg: String) {
        val dialogMsg = DialogMsg()
        dialogMsg.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg.dismissDialog(this)
        })
    }

    private fun giveValidatedAddress(strAddress: String): String {
        var strPlace = ""
        for (i in 0 until strAddress.length) {
            val ch = strAddress.get(i)
            if ((ch in 'A'..'Z') || (ch in 'a'..'z') || (ch in '0'..'9') || ch == '-' || ch == ',' || ch == ' ') {
                //valid characters there
                strPlace += (ch.toString())
            } else {
                //invalid character found
                strPlace = ""
            }
        }
        if (strPlace.trim().startsWith("-") || strPlace.trim().startsWith(",") || strPlace.trim().endsWith(
                "-"
            ) || strPlace.trim().endsWith(",")
        ) {
            strPlace = strPlace.replace("-", "").replace(",", "")
        }

        Log.d("ValidatedAddress", "" + strPlace.trim())
        return strPlace.trim()
    }
}

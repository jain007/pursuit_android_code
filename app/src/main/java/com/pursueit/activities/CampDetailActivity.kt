package com.pursueit.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import com.pursueit.R
import com.pursueit.adapters.*
import com.pursueit.custom.CustomFont
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.*
import com.pursueit.utils.*
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_camp_detail.*
import kotlinx.android.synthetic.main.bottom_sheet_select_camp_schedule.view.*
import kotlinx.android.synthetic.main.bottom_sheet_select_week.view.*
import kotlinx.android.synthetic.main.content_activity_detail_reviews.*
import kotlinx.android.synthetic.main.content_activity_detail_time_table.*
import kotlinx.android.synthetic.main.content_circle_progress.*
import kotlinx.android.synthetic.main.dialog_add_edit_review.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.collections.filter
import kotlin.collections.forEach
import kotlin.collections.set

class CampDetailActivity : AppCompatActivity() {

    val compositeDisposable = CompositeDisposable()
    var bookingModel: MyBookingsModel? = null

    //  private val arrCampDiscountModel= ArrayList<CampDiscountModel>()
    // lateinit var bottomSheetBasicOperation:BottomSheetBasicOperation
    //   val arrDiscountSelectionModel=ArrayList<CampSelectionModel>()

    var bottomSheetDialogForScheduleSelection: DialogPlus? = null
    var bottomSheetDialogForWeekSelection: DialogPlus? = null

    lateinit var manageCampPaymentDialog: ManageCampPaymentDialog

    var selectedDayOrWeekPriceModel: DayOrWeekPriceModel? = null

    var campId = ""

    var campModel: CampModel? = null

    var dialogMsg: DialogMsg? = null

    var arrayReviewModel = ArrayList<ReviewModel>()
    var reviewAdapter: ReviewsAdapter? = null

    lateinit var userModel: UserModel

    private var isWeekSelectionDialogDismissedByUser = false

    var bottomWeekRootView: View? = null
    var bottomScheduleRootView: View? = null
    var dismissedViaScheduleSelection: Boolean = false

    var isComingFromBookingScreen: Boolean = false
    var bookingStatus: String = ""

    companion object {
        var onCampIdBeforeLogin = ""
        var campAct: WeakReference<CampDetailActivity>? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camp_detail)
        LoginBasicsUi.setTransparentStatusBarAndBgImage(this)

        campAct = WeakReference(this)
        if (intent.hasExtra("bookingModel")) {
            bookingModel = intent.getSerializableExtra("bookingModel") as MyBookingsModel?
        }

        //Hide promotion first up, load later after details are fetched
        linPromotion.visibility = View.GONE

        init()
        updateUIForCamp()
        setListener()

        fastFetchCampDetails()
        fastFetchReviews()

        val markFavOrUnFav = MarkFavoriteOrUnFavorite(this)
        sparkFavorite.visibility = View.GONE
        sparkFavorite.setOnClickListener {

            val model = campModel
            if (model != null) {
                markFavOrUnFav.fastMark(
                    campId = model.campId,
                    itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_CAMP,
                    favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE)

                campModel!!.isFavoriteMarked = !campModel!!.isFavoriteMarked
                sparkFavorite.playAnimation()
                sparkFavorite.isChecked = campModel!!.isFavoriteMarked

                val intent = Intent()
                intent.putExtra("isFavorite", campModel!!.isFavoriteMarked)
                intent.putExtra("campId", campModel!!.campId)
                setResult(Activity.RESULT_OK, intent)
            }
        }
    }

    fun setSelectedScheduleModelAndMakeChanges(selectedDayOrWeekPriceModel: DayOrWeekPriceModel) {
        this.selectedDayOrWeekPriceModel = selectedDayOrWeekPriceModel
    }


    fun init() {
        linSimilarActivities.visibility = View.GONE
        try {
            isComingFromBookingScreen = intent.getBooleanExtra("isComingFromBookingScreen", false)
            bookingStatus = intent.getStringExtra("bookingStatus")

            if (bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED, true)) {
                btnSelectCamp.text = "Camp Completed"
                btnSelectCamp.isEnabled = false
            } else if (bookingStatus.equals(StaticValues.CAMP_BOOKED, true) || bookingStatus.equals(StaticValues.BOOKING_STATUS_UPCOMING, true)) {
                btnSelectCamp.text = "Booking Confirmed"
                btnSelectCamp.isEnabled = false
            } else if (bookingStatus.equals(StaticValues.BOOKING_STATUS_CANCELLED, true)) {
                btnSelectCamp.text = MyBookingsModel.getCancellationMessage(bookingModel?.bookingCancelledBy ?: "")//"Booking Cancelled"
                btnSelectCamp.tag = "cancelled"
                btnSelectCamp.isEnabled = false
            } else if (bookingStatus.equals(StaticValues.BOOKING_STATUS_DECLINED, true)) {
                btnSelectCamp.text = "Booking Declined"
                btnSelectCamp.isEnabled = false
                btnSelectCamp.tag = "cancelled"
            } else if (bookingStatus.equals(StaticValues.BOOKING_STATUS_AWAITING, true)) {
                btnSelectCamp.text = "Booking ${StaticValues.BOOKING_UNDER_PROCESS}"
                //btnSelectCamp.isEnabled = false
            }
        } catch (e: Exception) {

        }

        btnSelectCamp.visibility = View.GONE

        rvReviews.layoutManager = LinearLayoutManager(applicationContext)

        userModel = UserModel.getUserModel(applicationContext)



        dialogMsg = DialogMsg()
        manageCampPaymentDialog = ManageCampPaymentDialog(this)

        campId = intent.getStringExtra("campId")

        //val view=layoutInflater.inflate(R.layout.bottom_sheet_select_camp_schedule,null)
        bottomSheetDialogForScheduleSelection = DialogPlus.newDialog(this)
            .setOnDismissListener {
                if (dismissedViaScheduleSelection) {
                    showBottomSheetDialogForWeekSelection()
                    dismissedViaScheduleSelection = false
                }
            }.setContentHolder(ViewHolder(R.layout.bottom_sheet_select_camp_schedule)).create()//BottomSheetDialog(this)
        //bottomSheetDialogForScheduleSelection!!.setContentView(view)
        bottomScheduleRootView = bottomSheetDialogForScheduleSelection!!.holderView

        bottomScheduleRootView!!.imgCloseBottomCamp.setOnClickListener {
            bottomSheetDialogForScheduleSelection!!.dismiss()
        }

        bottomSheetDialogForWeekSelection = DialogPlus.newDialog(this).setContentHolder(ViewHolder(R.layout.bottom_sheet_select_week)).create()

        bottomWeekRootView = bottomSheetDialogForWeekSelection!!.holderView

        bottomWeekRootView!!.imgCloseBottomSelectWeek.setOnClickListener {
            bottomSheetDialogForWeekSelection!!.dismiss()
        }

        //bottomWeekRootView!!.setContentView(R.layout.bottom_sheet_select_week)
        bottomWeekRootView!!.rvSelectWeek.layoutManager = LinearLayoutManager(applicationContext)

        txtCampDesc.typeface = CustomFont.setFontSemiBold(assets)
        txtProviderAboutUs.typeface = CustomFont.setFontSemiBold(assets)
        txtCampDesc.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorTextGreySubTitle))
        txtProviderAboutUs.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorTextGreySubTitle))
        //   bottomSheetBasicOperation= BottomSheetBasicOperation(this@CampDetailActivity,linBottomSheetSelection,blackTransparent)


    }

    private fun setListener() {
        //linProvider
        compositeDisposable.add(RxView.clicks(linProvider).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            txtCampProviderName.performClick()
        })

        compositeDisposable.add(RxView.clicks(txtCampProviderName).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            MyNavigations.gotoProviderDetailActivity(this@CampDetailActivity, campModel?.providerId ?: "")
        })

        compositeDisposable.add(RxView.clicks(bottomWeekRootView!!.btnDoneCampSelection)
            .throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
                if (bottomSheetDialogForWeekSelection!!.isShowing) {
                    isWeekSelectionDialogDismissedByUser = true
                    bottomSheetDialogForWeekSelection!!.dismiss()
                }
                showCampPaymentDialog()
            })

        compositeDisposable.add(RxView.clicks(imgBack).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            onBackPressed()
        })

        compositeDisposable.add(RxView.clicks(btnSelectCamp).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            /*if (btnSelectCamp.text.contains("cancel",true) || btnSelectCamp.text.contains("decline",true)) {
                if (btnSelectCamp?.tag ?: "" == "cancelled" && dialogMsg != null) {
                    MyBookingsModel.showCancelReason(
                        this@CampDetailActivity,
                        bookingModel?.cancelReason ?: "",
                        dialogMsg!!
                    )
                }
                return@subscribe
            }
*/
            if(btnSelectCamp.text.toString().trim().contains("Booking ${StaticValues.BOOKING_UNDER_PROCESS}")){
                showErrorDialog(getString(R.string.booking_under_process_msg))
            }else {
                if (userModel.userId.trim().isEmpty()) {
                    onCampIdBeforeLogin = campId
                    MyProfileUtils.showSignUpLoginPopUp(this)
                } else {
                    showBottomSheetDialogForScheduleSelection()
                }
            }
        })

        compositeDisposable.add(RxView.clicks(imgGetDirections).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            if (campModel != null) {
                try {
                    MyPlaceUtils.getDirections(this, campModel!!.venueLat.toDouble(), campModel!!.venueLng.toDouble())
                } catch (e: Exception) {
                    toast("Oops!! Location not available")
                }
            }
        })

        compositeDisposable.add(RxView.clicks(btnShare).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            MyAppConfig.shareApp(this, "" + campModel?.campName)
        })

    }

    private fun updateUIForCamp() // some common layout is used for activity detail and camp so need to updates UI according to camp
    {
        txtCol1.text = "WEEKS"
        txtCol2.text = "START DATE"
        txtCol3.text = "END DATE"

        txtAvailableDiscount.visibility = View.VISIBLE
        rvAvailableDiscounts.visibility = View.VISIBLE

        txtActivityDesc.visibility = View.GONE
        txtCampDesc.visibility = View.VISIBLE

        txtCancellationPolicyLabel.visibility = View.GONE
        txtCancellationPolicy.visibility = View.GONE
        txtOtherInfoLabel.visibility = View.GONE
        txtOtherInfo.visibility = View.GONE
    }

    fun showBottomSheetDialogForScheduleSelection() {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_select_camp_schedule, null)

        //  BottomSheetBehavior.from(view).state=BottomSheetBehavior.STATE_EXPANDED

        if (bottomSheetDialogForScheduleSelection == null) {
            bottomSheetDialogForScheduleSelection = DialogPlus.newDialog(this).setContentHolder(ViewHolder(R.layout.bottom_sheet_select_camp_schedule)).create()
            //bottomSheetDialogForScheduleSelection!!.setContentView(view)
        }
        //  bottomSheetDialogForScheduleSelection!!.lastDivider.requestFocus()
        if (campModel!!.arrDayOrWeekPriceModel.size == 1 && campModel!!.arrDiscountModel.size == 0) {
            bottomScheduleRootView!!.linBottomSheetSelection.setPadding(0, 0, 0, resources.getDimensionPixelOffset(R.dimen.dim_50))
        }

        bottomSheetDialogForScheduleSelection!!.show()
    }

    private fun showBottomSheetDialogForWeekSelection() {
        bottomWeekRootView!!.btnDoneCampSelection.alpha = 0.5f
        bottomWeekRootView!!.btnDoneCampSelection.isEnabled = false

        campModel!!.arrWeekModel.forEach {
            it.isSelected = false
        }

        bottomWeekRootView!!.rvSelectWeek.adapter = WeekSelectionAdapter(this, campModel!!.arrWeekModel, bottomWeekRootView!!.btnDoneCampSelection, selectedDayOrWeekPriceModel?.maxWeekSelection ?: 0)

        bottomSheetDialogForWeekSelection!!.show()

        /* bottomSheetDialogForWeekSelection!!.setOnDismissListener {
             if (!isWeekSelectionDialogDismissedByUser) {
                 bottomSheetDialogForScheduleSelection!!.show()
             }
             else{
                 isWeekSelectionDialogDismissedByUser=false
             }
         }*/
    }


    private fun showCampPaymentDialog() {
        if (manageCampPaymentDialog.dialogCampPayment != null) {
            manageCampPaymentDialog.setup()
        } else
            manageCampPaymentDialog.fastFetchFamilyMembersFirstThenShowDialog()
    }

    /* private fun showBottomSheetDialogFragment()
     {
         val fragment=ScheduleBottomSheetFragment()
         fragment.show(supportFragmentManager,"")
     }*/


    fun fastFetchCampDetails() {
        try {
            if (ConnectionDetector.isConnectingToInternet(this)) {
                cardProgress.visibility = View.VISIBLE
                var hashParams = HashMap<String, String>()
                hashParams["campId"] = campId
                hashParams["comingFrom"] = if (bookingModel != null) "1" else "2"

                hashParams = SearchAndFilterStaticData.addFilterData(hashParams)

                FastNetworking.makeRxCallPost(applicationContext, Urls.VIEW_CAMP_DETAIL, true, hashParams, "CampDetail"
                    , object : FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {
                            try {
                                cardProgress.visibility = View.GONE
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonData = json.getJSONObject("data")
                                    campModel = JsonParse.parseCampDetailResponse(jsonData, json)
                                    setAllDetails()

                                } else {
                                    showApiErrorRetry {
                                        fastFetchCampDetails()
                                    }
                                }

                            } catch (e: Exception) {
                                Log.e("CampDetail", e.toString())
                                showApiErrorRetry {
                                    fastFetchCampDetails()
                                }
                            }
                        }

                        override fun onApiError(error: ANError) {
                            cardProgress.visibility = View.GONE
                            showApiErrorRetry {
                                fastFetchCampDetails()
                            }
                        }

                    }, compositeDisposable)
            } else {
                showConnectionError()
            }
        } catch (e: Exception) {

        }
    }

    fun setAllDetails() {
        val adapter = MyImagePagerAdapter(this, campModel!!.arrayImages, true)
        viewPager.adapter = adapter
        worm_dots_indicator.setViewPager(viewPager)

        if (userModel.userId.trim().isEmpty()) {
            sparkFavorite.visibility = View.GONE
        } else {
            sparkFavorite.visibility = View.VISIBLE
            if (MarkFavoriteOrUnFavorite.LAST_CAMP_ID == campId) {
                sparkFavorite.isChecked = MarkFavoriteOrUnFavorite.LAST_FLAG_FOR_ACTIVITY_OR_CAMP == MarkFavoriteOrUnFavorite.FLAG_FAVORITE

                MarkFavoriteOrUnFavorite.LAST_CAMP_ID = ""
                MarkFavoriteOrUnFavorite.LAST_FLAG_FOR_ACTIVITY_OR_CAMP = ""
            } else {

                sparkFavorite.isChecked = campModel!!.isFavoriteMarked
            }
        }

        txtCampTitle.text = campModel?.campName?.trim()
        txtCampProviderName.text = campModel?.serviceProviderName?.trim()
        txtActivityDesc.text = campModel?.providerDescription?.trim()

        val flowLayoutManager = FlowLayoutManager()
        flowLayoutManager.isAutoMeasureEnabled = true
        rvTags.layoutManager = flowLayoutManager
        rvTags.adapter = RecyclerTagAdapter(campModel!!.arrTagList)

        txtAge.text = campModel?.ageRageData
        txtTiming.text = campModel?.timeRangeData
        if (campModel!!.isTransportAvailable)
            txtTransportation.text = "Yes (Fees Applies)"
        else
            txtTransportation.text = "No"
        txtCampLocation.text = campModel?.venueFullAddress


        //   txtStartingPrice.text="AED "+campModel!!.startingPrice.replace(".0","")
        try {
            txtStartingPrice.text = "AED " + DecimalFormat("##.##").format(campModel!!.startingPrice.toDouble())
        } catch (e: Exception) {
        }




        if (intent.hasExtra("bookingModel")) {
            bookingModel = intent.getSerializableExtra("bookingModel") as MyBookingsModel?
            if (bookingModel != null) {
                /*if (bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED,true) || bookingStatus.equals(StaticValues.CAMP_BOOKED,true))
                {*/
                if (bookingModel?.bookingAmtAfterTax != "") {
                    txtStartingFromLabel.text = "Paid Amount"
                    //txtStartingPrice.text="AED "+bookingModel.bookingAmtAfterTax.replace(".0","")
                    try {
                        txtStartingPrice.text =
                            "AED " + DecimalFormat("##.##").format(bookingModel?.bookingAmtAfterTax?.toDouble())
                    } catch (e: Exception) {
                    }
                }
                //}
            }

        }
        txtCampDesc.text = campModel?.campAbout

        rvAvailableDiscounts.layoutManager = LinearLayoutManager(applicationContext)
        rvAvailableDiscounts.adapter = CampDiscountAdapter(campModel!!.arrDiscountModel)

        if (campModel!!.arrDiscountModel.size == 0) {
            txtAvailableDiscount.visibility = View.GONE
        }

        rvTimeTable.layoutManager = LinearLayoutManager(applicationContext)
        if (bookingModel != null) {
            val arrWeekId = bookingModel!!.campWeekId.split(",")
            val filteredArray = campModel!!.arrWeekModel.filter { arrWeekId.contains(it.id) }
            rvTimeTable.adapter = WeekAdapterForCampDetail(ArrayList(filteredArray), this)
        } else {
            rvTimeTable.adapter = WeekAdapterForCampDetail(campModel!!.arrWeekModel, this)
        }

        txtAdditionalInfo.text = campModel?.additionalInfo ?: ""

        Glide.with(this).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(false)).load(Urls.IMAGE_URL_PARTNER + campModel?.providerLogo).into(imgProvider)
        txtProviderName.text = campModel?.serviceProviderName
        txtProviderAboutUs.text = campModel?.providerDescription



        bottomScheduleRootView!!.rvCampSchedule.layoutManager = LinearLayoutManager(applicationContext)
        bottomScheduleRootView!!.rvCampSchedule.adapter = CampSelectionAdapter(this, campModel!!.arrDayOrWeekPriceModel)


        bottomScheduleRootView!!.rvDiscountSchedule.layoutManager = LinearLayoutManager(applicationContext)
        bottomScheduleRootView!!.rvDiscountSchedule.adapter = CampDiscountAdapter(campModel!!.arrDiscountModel, true)
        if (campModel!!.arrDiscountModel.size == 0) {
            bottomScheduleRootView!!.dividerCampSchedule.visibility = View.GONE
            bottomScheduleRootView!!.txtCampDiscountDialog.visibility = View.GONE
            bottomScheduleRootView!!.rvDiscountSchedule.visibility = View.GONE
        } else {
            bottomScheduleRootView!!.rvDiscountSchedule.visibility = View.VISIBLE
            bottomScheduleRootView!!.dividerCampSchedule.visibility = View.VISIBLE
            bottomScheduleRootView!!.txtCampDiscountDialog.visibility = View.VISIBLE
        }

        btnSelectCamp.visibility = View.VISIBLE

        if (isComingFromBookingScreen && bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED, true) && !campModel!!.isReviewGiven)
            showPostReviewDialog()


        //show promotion ribbon on detail screen, only when not coming from my booking
        if (!isComingFromBookingScreen) {
            //Log.d("CampPromotion",""+campModel?.maxValuePromotionModel)
            PromotionOperation.setPromotionText(linPromotion, campModel?.maxValuePromotionModel)
        }
    }

    private fun showApiErrorRetry(onRetry: () -> Unit) {
        dialogMsg?.showErrorApiDialog(this, "Retry", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
            onRetry()
        }, isCancellable = true)
    }

    fun showConnectionError() {
        dialogMsg?.showErrorConnectionDialog(this, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        })
    }

    private fun fastFetchReviews() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            progressReviews.visibility = View.VISIBLE
            val hashParams = java.util.HashMap<String, String>()
            hashParams["campId"] = campId
            FastNetworking.makeRxCallPost(applicationContext, Urls.VIEW_CAMP_REVIEWS, true, hashParams, "ViewReviews", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    doAsync {
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                arrayReviewModel.clear()
                                arrayReviewModel.addAll(JsonParse.parseReviews(json))

                            }

                        } catch (e: Exception) {
                            Log.d("ExcReviews", "" + e)
                        } finally {
                            runOnUiThread {
                                progressReviews.visibility = View.GONE
                                if (arrayReviewModel.size > 0) {
                                    linReviewsDetailPage.visibility = View.VISIBLE
                                    reviewAdapter = ReviewsAdapter(this@CampDetailActivity, arrayReviewModel, { posClicked ->
                                        val revModel = arrayReviewModel[posClicked]
                                        Log.d("RevModel", "" + revModel + "\nRevUser:\n" + revModel.reviewUserId)
                                        Log.d("MyUserId", "" + userModel.userId)
                                        if (userModel.userId.trim().isNotEmpty() && revModel.reviewUserId.trim().isNotEmpty()) {
                                            if (userModel.userId == revModel.reviewUserId) {
                                                showPostReviewDialog(true, revModel)
                                            }
                                        }
                                    }, { _, _ ->

                                    })
                                    rvReviews.adapter = reviewAdapter

                                    txtViewAllReviews.visibility = if (arrayReviewModel.size > 2) View.VISIBLE else View.GONE
                                    compositeDisposable.add(RxView.clicks(txtViewAllReviews).throttleFirst(700, TimeUnit.MILLISECONDS).subscribe {
                                        if (campModel != null)
                                            MyNavigations.goToAllReviews(this@CampDetailActivity, arrayReviewModel, activityId = campModel!!.campId)
                                    })

                                } else {
                                    linReviewsDetailPage.visibility = View.GONE
                                }

                            }
                        }
                    }
                }

                override fun onApiError(error: ANError) {
                    try {
                        progressReviews.visibility = View.GONE
                        linReviewsDetailPage.visibility = View.GONE
                    } catch (e: Exception) {

                    }
                }
            })
        }
    }

    private fun showPostReviewDialog(isEditReview: Boolean = false,
                                     reviewModel: ReviewModel? = null) {
        try {
            if (campModel != null) {
                if ((!campModel!!.isReviewGiven || isEditReview) && userModel.userId.trim().isNotEmpty()) {
                    val dialog = MyDialog(this).getMyDialog(R.layout.dialog_add_edit_review)

                    if (!isEditReview) {
                        dialog.ratingBarPostReview.rating = 5f
                    }

                    dialog.ratingBarPostReview.setOnRatingChangeListener { baseRatingBar, flValue ->
                        Log.d("RatingValue", "" + flValue)
                    }



                    if (isEditReview && reviewModel != null) {
                        dialog.etReview.setText(reviewModel.reviewDesc.trim())
                        MyProfileUtils.setSelectionEnd(dialog.etReview)
                        dialog.ratingBarPostReview.rating = reviewModel.reviewRating
                        dialog.btnPostReview.text = "Update Review"
                    }

                    dialog.btnPostReview.setOnClickListener {
                        val strReview = dialog.etReview.text.toString().trim()
                        val flRatingValue = dialog.ratingBarPostReview.rating

                        /*if (strReview.isEmpty()) {
                            showErrorDialog("Please write some review and then proceed")
                            return@setOnClickListener
                        }*/

                        if (strReview.isNotEmpty() && strReview.length < 10) {
                            showErrorDialog("Your review should be at least of 10 characters")
                            return@setOnClickListener
                        }

                        dialog.dismiss()

                        val hashParams = java.util.HashMap<String, String>()
                        hashParams.put("campId", "" + campId)
                        hashParams.put("reviewRating", "" + flRatingValue)
                        hashParams.put("reviewDescription", strReview)

                        if (isEditReview && reviewModel != null)
                            hashParams.put("reviewId", "" + reviewModel.reviewId)

                        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                            MyReviewUtils.fastAddRatingReview(this, hashParams, "AddReview", { json ->
                                try {
                                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                        val strMsg = json.optString("message", "Your review has been successfully posted")
                                        dialogMsg?.showSuccessDialog(
                                            this, "Success", strMsg, "OK",
                                            View.OnClickListener {
                                                dialogMsg?.dismissDialog(this)
                                                fastFetchCampDetails()
                                                fastFetchReviews()
                                            }, isCancellable = false
                                        )
                                    } else {
                                        showApiError()
                                    }
                                } catch (e: Exception) {

                                }
                            }, compositeDisposable, fromCamp = true)
                        } else {
                            showConnectionError()
                        }

                    }

                    dialog.imgCloseDialogPostReview.setOnClickListener {
                        dialog.dismiss()
                    }

                    DialogMsg.animateViewDialog(dialog.relParentPostReview)

                    dialog.show()
                }
            }
        } catch (e: Exception) {
            Log.d("ReviewDialogExc", "" + e)
        }
    }

    fun showErrorDialog(strMsg: String) {
        dialogMsg?.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this@CampDetailActivity)
        })
    }

    fun showApiError() {
        dialogMsg?.showErrorApiDialog(this, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }


}

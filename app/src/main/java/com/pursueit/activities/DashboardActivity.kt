package com.pursueit.activities

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessaging
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.BuildConfig
import com.pursueit.MyApp.Companion.IS_APP_IN_OPEN_STATE
import com.pursueit.R
import com.pursueit.adapters.*
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.CategorySubCategoryActivitiesSearchFragment
import com.pursueit.dialogs.CategorySubCategorySearchFragment
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.fcm.MyFirebaseMessagingService
import com.pursueit.fragments.MoreFragment
import com.pursueit.fragments.MyCornerFragment
import com.pursueit.fragments.NearbyFragment
import com.pursueit.fragments.PromotionListingFragment
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.*
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.content_dashboard_header.*
import kotlinx.android.synthetic.main.content_dashboard_main_listing.*
import kotlinx.android.synthetic.main.content_dashboard_toolbar.*
import kotlinx.android.synthetic.main.content_dashboard_toolbar.view.*
import kotlinx.android.synthetic.main.dialog_filter_new.*
import kotlinx.android.synthetic.main.dialog_search_category.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.browse
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.schedule

class DashboardActivity : AppCompatActivity() {

    lateinit var categoryAdapter: DashboardActivityAdapter

    lateinit var campAdapter: DashboardCampAdapter

    var arrayPromotionModel = ArrayList<PromotionModel>()
    lateinit var promotionAdapter: DashboardPromotionAdapter

    var arrayPassModel = ArrayList<PassModel>()
    private lateinit var passAdapter: DashboardPassAdapter

    var arrayFeaturedPartnerModel = ArrayList<FeaturedPartnerModel>()
    lateinit var partnerAdapter: DashboardFeaturedAdapter

    var compositeDisposable = CompositeDisposable()

    var dialogMsg: DialogMsg? = null
    var dialogMsgAppUpdate: DialogMsg? = null

    var myPlaceUtils: MyPlaceUtils? = null

    lateinit var userModel: UserModel

    private var isNearbyExploreSelected: Boolean = false

    //var exploreAllTab = true //if true, on click of bottom option nearby(explore), default All tab will be selected
    var retainSelectedTab = true

    var isProfileRefreshRunning = false

    var timer: CountDownTimer? = null

    val arrDashCampModel = ArrayList<CampModel>()

    lateinit var initialLoader: DialogMsg

    var nearByFragment: NearbyFragment? = null

    var myCornerFragment: MyCornerFragment? = null

    companion object {
        var selectedPromotionModel: PromotionModel? = null
        var refreshNearbyPlaces = false

        //var selectedCatModel: CategoryModel? = null
        //var activityNameSearch: String = ""
        var selectedSearchModel: SearchModel? = null
        var ageFrom = ""
        var ageTo = ""

        var showPassActivities = false
        //var actDashboard:DashboardActivity?=null

        fun stopScroll(collapsingToolbar: CollapsingToolbarLayout, appBar: AppBarLayout) {
            val toolbarLayoutParams = collapsingToolbar.layoutParams as AppBarLayout.LayoutParams
            toolbarLayoutParams.scrollFlags = 0
            collapsingToolbar.layoutParams = toolbarLayoutParams

            val appBarLayoutParams = appBar.layoutParams as CoordinatorLayout.LayoutParams
            appBarLayoutParams.behavior = null
            appBar.layoutParams = appBarLayoutParams
        }

        fun startScroll(collapsingToolbar: CollapsingToolbarLayout, appBar: AppBarLayout) {
            val toolbarLayoutParams = collapsingToolbar.layoutParams as AppBarLayout.LayoutParams
            toolbarLayoutParams.scrollFlags =
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            collapsingToolbar.layoutParams = toolbarLayoutParams

            val appBarLayoutParams = appBar.layoutParams as CoordinatorLayout.LayoutParams
            appBarLayoutParams.behavior = AppBarLayout.Behavior()
            appBar.layoutParams = appBarLayoutParams
        }

        fun showSearchCatAndActivityPopUp(act: AppCompatActivity) {
            val weakAct = WeakReference<AppCompatActivity>(act)
            try {
                val dialog = MyDialog(weakAct.get()!!).getMyDialog(R.layout.dialog_search_category)

                dialog.txtSearchCatSubCat.setOnClickListener {
                    dialog.dismiss()
                    InitFragment.initDialogFragment(
                        weakAct.get()!!.supportFragmentManager,
                        CategorySubCategorySearchFragment()
                    )
                }

                dialog.imgCloseDialogSearch.setOnClickListener {
                    dialog.dismiss()
                }


                dialog.show()
            } catch (e: Exception) {
                Log.e("ShowPopUpExc", "" + e)
            }
        }

        fun filterActivitiesOnSearchClick(
            act: AppCompatActivity,
            searchModel: SearchModel,
            fromPass: Boolean = false
        ) {
            if (act is DashboardActivity) {
                selectedSearchModel = searchModel
                Log.d("SearchModelClicked", "" + selectedSearchModel?.toString())
                NearbyFragment.selectedPos = -1
                MyPlaceUtils.resetAllCategoryLocalResponses(context = act.applicationContext)

                for (i in 0 until StaticValues.arrayDashboardCategories.size) {
                    if (StaticValues.arrayDashboardCategories[i].catId == selectedSearchModel!!.catId) {
                        NearbyFragment.selectedPos = i
                        break
                    }
                }
                act.retainSelectedTab = true

                act.handleExploreNearbyClick(false, fromPass)

            }
        }

        fun filterActivitiesOnPromotion(act: AppCompatActivity, promotionModel: PromotionModel) {
            if (act is DashboardActivity) {
                selectedPromotionModel = promotionModel
                Log.d("SearchModelClicked", "" + selectedSearchModel?.toString())
                NearbyFragment.selectedPos = -1
                MyPlaceUtils.resetAllCategoryLocalResponses(context = act.applicationContext)

                for (i in 0 until StaticValues.arrayDashboardCategories.size) {
                    if (StaticValues.arrayDashboardCategories[i].catId == promotionModel.catId) {
                        NearbyFragment.selectedPos = i
                        break
                    }
                }
                act.retainSelectedTab = true

                act.handleExploreNearbyClick(false)
            }
        }


        fun filterCampOnSearchClick(act: AppCompatActivity, searchModel: SearchModel) {
            if (act is DashboardActivity) {
                CampListingActivity.searchViaCampNameOrTagName = true
                if (searchModel.flag == 5) {
                    CampListingActivity.campId = searchModel.catId
                    CampListingActivity.tagId = ""
                } else if (searchModel.flag == 6) {
                    CampListingActivity.tagId = searchModel.catId
                    CampListingActivity.campId = ""
                }
                act.handleExploreCamp(true)
            }
        }

        fun filterCampOnPromotion(act: AppCompatActivity, promotionModel: PromotionModel) {
            if (act is DashboardActivity) {
                CampListingActivity.searchViaCampNameOrTagName = true
                CampListingActivity.campId = promotionModel.campId
                CampListingActivity.tagId = ""
                act.handleExploreCamp(true)
            }
        }

        fun showAllAct(act: DashboardActivity) {
            selectedPromotionModel?.activityId = ""
            selectedPromotionModel?.catId = ""
            selectedPromotionModel?.subCateId = ""
            act.retainSelectedTab = false
            MyPlaceUtils.resetAllCategoryLocalResponses(act.applicationContext)
            act.handleExploreNearbyClick(false)
        }

        fun showFilterDialogNew(
            act: Activity,
            isComingFromNearbyFragment: Boolean = false,
            isComingFromCamp: Boolean = false//,
            //   fragment: Fragment? = null
            , fromNewFilterDialog: Boolean = false
            , ageSelectedListener: (ageGroupModel: AgeGroupModel) -> Unit = {}
            , onClearFilter: () -> Unit = {},
            selectedAgeGroupModel: AgeGroupModel? = null
        ) {

            fun resetSelectionsAgeGroup(forceClear: Boolean = false) {

                Log.d("SelectedAgeGroup_", "-->" + selectedAgeGroupModel)

                //If the main age group selected model is null, then set unselected for all age groups
                if (selectedAgeGroupModel == null || forceClear) {
                    MyAppConfig.arrayAgeGroupModel.forEach { it.groupIsSelected = false }
                }

                //regain the last state of selected age model
                if (selectedAgeGroupModel != null) {
                    MyAppConfig.arrayAgeGroupModel.filter { it.groupAgeFrom == selectedAgeGroupModel.groupAgeFrom && it.groupAgeTo == selectedAgeGroupModel.groupAgeTo }
                        .forEach { it.groupIsSelected = true }
                }
            }

            try {
                if (MyAppConfig.arrayAgeGroupModel.size > 0) {

                    resetSelectionsAgeGroup()

                    StaticValues.isExploreAlreadyClickedFirstTime = true

                    val dialog = MyDialog(act).getMyDialog(R.layout.dialog_filter_new)
                    dialog.rvAgeGroups.layoutManager = LinearLayoutManager(act)
                    dialog.rvAgeGroups.setHasFixedSize(true)

                    Log.d("AgeGroup_Array", "" + MyAppConfig.arrayAgeGroupModel)
                    val adapter =
                        AgeGroupsAdapter(act, MyAppConfig.arrayAgeGroupModel) { pos, isChecked ->
                            resetSelectionsAgeGroup(true)
                            val model = MyAppConfig.arrayAgeGroupModel[pos]
                            model.groupIsSelected = isChecked
                            MyAppConfig.arrayAgeGroupModel[pos] = model

                            dialog.rvAgeGroups.adapter?.notifyDataSetChanged()
                        }

                    dialog.rvAgeGroups.adapter = adapter

                    dialog.imgCloseDialogFilterNew.setOnClickListener {
                        dialog.dismiss()
                    }

                    dialog.txtClearFiltersNew.setOnClickListener {
                        dialog.dismiss()

                        if (act is DashboardActivity)
                            act.toolbar.imgFilterIndicatorDashboard.visibility = View.GONE

                        //reload only when some filter is already applied
                        if (ageFrom.trim().isNotEmpty() || ageTo.trim().isNotEmpty()) {
                            ageFrom = ""
                            ageTo = ""

                            selectedSearchModel = null
                            selectedPromotionModel = null

                            MyPlaceUtils.resetAllCategoryLocalResponses(context = act.applicationContext)

                            SearchAndFilterStaticData.isAgeFilterApplied = false

                            resetSelectionsAgeGroup(true)

                            if (!fromNewFilterDialog) {
                                if (isComingFromCamp && act is CampListingActivity) {
                                    // need to handle filer for camp
                                    CampListingActivity.needToRefreshNearByCamps = true
                                    // if (fragment is CampListingFragment)
                                    act.clearListAndMakeApiCall()
                                } else if (act is DashboardActivity) {
                                    act.retainSelectedTab = isComingFromNearbyFragment
                                    Log.d("RetainSelected__", "" + act.retainSelectedTab)
                                    Log.d("RetainSelected__", "" + showPassActivities)
                                    act.handleExploreNearbyClick(
                                        isActualMenuItemClick = false,
                                        passAct = showPassActivities
                                    )
                                }
                            } else {
                                onClearFilter()
                            }
                        }
                    }


                    dialog.txtApplyFilterNew.setOnClickListener {
                        val dialogErr = DialogMsg()
                        /*if (ageFrom.trim().isEmpty() || ageTo.trim().isEmpty()) {
                            return@setOnClickListener
                        }*/

                        selectedSearchModel = null
                        selectedPromotionModel = null

                        val ageGroups = MyAppConfig.arrayAgeGroupModel.filter { it.groupIsSelected }
                        if (ageGroups.isNotEmpty()) {
                            val selectedAgeGroup =
                                MyAppConfig.arrayAgeGroupModel.single { it.groupIsSelected }
                            ageFrom = selectedAgeGroup.groupAgeFrom
                            ageTo = selectedAgeGroup.groupAgeTo

                            ageSelectedListener(selectedAgeGroup)

                            if (!fromNewFilterDialog) {
                                SearchAndFilterStaticData.selectedAgeGroupModel = selectedAgeGroup
                                SearchAndFilterStaticData.isAgeFilterApplied = true

                                if (act is DashboardActivity)
                                    act.toolbar.imgFilterIndicatorDashboard.visibility =
                                        View.VISIBLE
                            }
                        } else {
                            dialogErr.showErrorRetryDialog(
                                act,
                                "Alert",
                                "Please select one age group to apply filter",
                                "OK",
                                View.OnClickListener { dialogErr.dismissDialog(act) })
                            return@setOnClickListener
                        }




                        dialog.dismiss()

                        MyPlaceUtils.resetAllCategoryLocalResponses(context = act.applicationContext)

                        if (act is DashboardActivity)
                            act.toolbar.imgFilterIndicatorDashboard.visibility = View.VISIBLE

                        if (!fromNewFilterDialog) {
                            if (isComingFromCamp) {
                                // need to handle filer for camp
                                CampListingActivity.needToRefreshNearByCamps = true
                                if (act is CampListingActivity)
                                    act.clearListAndMakeApiCall()
                            } else if (act is DashboardActivity) {
                                act.retainSelectedTab = isComingFromNearbyFragment
                                Log.d("RetainSelected__", "" + act.retainSelectedTab)
                                Log.d("RetainSelected__", "" + showPassActivities)
                                act.handleExploreNearbyClick(false, showPassActivities)
                            }
                        }
                    }


                    DialogMsg.animateViewDialog(dialog.relParentAgeFilterNew)

                    dialog.show()
                } else {
                    act.toast("Please wait... we are fetching age groups")
                }
            } catch (e: Exception) {
                Log.e("ExcDialogFilterNew", "" + e)
            }
        }

    }


    /*fun setExploreAllBool(act: AppCompatActivity, exploreAll: Boolean) {
        if (act is DashboardActivity)
            act.exploreAllTab = exploreAll

        if (exploreAll) {
            NearbyFragment.lastSelectedPos = -1
        }

        if(!retainSelectedTab){
            selectedTabPos = -1
        }
    }*/

    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    private fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    private fun setCampAdapter() {
        if (arrDashCampModel.size > 0) {
            //  setVisibilityOfHomeScreenData(true)
            setUIBackgrounds(linActivity, linCamp, linPromotion, linPass, linFeaturePartners)
            /*linCamp.visibility = View.VISIBLE
            dividerLast.visibility=View.VISIBLE*/

            rvCamp.layoutManager =
                LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)

            campAdapter = DashboardCampAdapter(this@DashboardActivity, arrDashCampModel) {
            }
            val scaleInAdapter = ScaleInAnimationAdapter(campAdapter)
            rvCamp.adapter = scaleInAdapter
            rvCamp.addItemDecoration(
                PaddingItemDecoration(
                    paddingEnd = resources.getDimension(R.dimen.dim_11).toInt()
                )
            )
        } else {
            setUIBackgrounds(linActivity, linCamp, linPromotion, linPass, linFeaturePartners)
            //setVisibilityOfHomeScreenData(false)
            /*linCamp.visibility = View.GONE
            dividerLast.visibility=View.GONE*/
        }
    }

    fun setPromotionAdapter() {
        runOnUiThread {
            rvPromotions.layoutManager =
                LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
            val adapter = DashboardPromotionAdapter(this, arrayPromotionModel)
            rvPromotions.adapter = ScaleInAnimationAdapter(adapter)

            txtSeeAllPromotion.visibility =
                if (arrayPromotionModel.size < 3) View.GONE else View.VISIBLE
        }
    }

    /* private fun setVisibilityOfHomeScreenData(showData:Boolean)
     {
         if (showData)
         {
             linCamp.visibility = View.VISIBLE
             dividerLast.visibility=View.VISIBLE
             linFeaturePartners.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.white))
             //coloBgGrey
         }
         else{
             linCamp.visibility = View.GONE
             dividerLast.visibility=View.GONE
             linFeaturePartners.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.coloBgGrey))
         }
     }*/

    private fun setUIBackgrounds(vararg views: View) {

        try {
            if (arrDashCampModel.size == 0)
                views[1].visibility = View.GONE
            else
                views[1].visibility = View.VISIBLE

            if (arrayPromotionModel.size == 0)
                views[2].visibility = View.GONE
            else
                views[2].visibility = View.VISIBLE

            if (arrayPassModel.size == 0)
                views[3].visibility = View.GONE
            else
                views[3].visibility = View.VISIBLE

            if (arrayFeaturedPartnerModel.size == 0)
                views[4].visibility = View.GONE
            else
                views[4].visibility = View.VISIBLE
        } catch (e: Exception) {
            Log.e("setUIBackgrounds", e.toString())
        }

        var setBgGrey = false
        views.forEach {
            if (it.visibility == View.VISIBLE) {
                if (setBgGrey) {
                    it.setBackgroundColor(
                        ContextCompat.getColor(applicationContext, R.color.coloBgGrey)
                    )
                } else {
                    it.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.white))
                }
                setBgGrey = !setBgGrey
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        Log.d("notificationKeys", "onCreate")


        IS_APP_IN_OPEN_STATE = true

        Log.d("notificationKeys", "from here:")
        intent?.extras?.keySet()?.forEach {
            Log.d("notificationKeys$it", "" + intent?.extras?.get(it).toString())
        }

        if (intent.hasExtra("notificationModel")) {
            Log.d("notificationKeys", "hasExtra: true")
            MyPlaceUtils.placeModel.placeId = ""
            refreshNearbyPlaces = false
            selectedSearchModel = null
            selectedPromotionModel = null
        }


        setUIBackgrounds(linActivity, linCamp, linPromotion, linPass, linFeaturePartners)
        //setVisibilityOfHomeScreenData(false)
        /*linCamp.visibility = View.GONE
        dividerLast.visibility=View.GONE*/

        MyAppConfig.noOfAttempts = 0

        NearbyFragment.selectedPos = -1

        /* val screenHeight = getScreenHeight()
         Log.d("ScreenWidthHeight", "" + getScreenWidth() + " - " + screenHeight)
         if (screenHeight >= 1600)
             stopScroll(collapsingToolbar, appBarDashboard)
         else
             startScroll(collapsingToolbar, appBarDashboard)*/

        setCollapsingToolbarBehaviorListener()

        FirebaseMessaging.getInstance().isAutoInitEnabled = true

        userModel = UserModel.getUserModel(applicationContext)

        init()

        compositeDisposable = CompositeDisposable()

        dialogMsg = DialogMsg()

        onClickListeners()

        //fillFeaturedPartners()

        //fastCheckAuthToken()

        MyFirebaseMessagingService.subscribeTopicAll()
        //MyFirebaseMessagingService.unsubscribeTopicAllOtherFlavours()

        setCampAdapter()

        /*  linCamp.visibility = View.VISIBLE
          dividerLast.visibility = View.VISIBLE
          linFeaturePartners.setBackgroundColor(
              ContextCompat.getColor(
                  applicationContext,
                  R.color.white
              )
          )*/

        DialogMsg.showPleaseWait(this)

        try {
            if (intent.hasExtra("notificationModel")) {
                var finishDashboardAct: Boolean = false
                if (intent.hasExtra("finishDashboardAct"))
                    finishDashboardAct = intent.getBooleanExtra("finishDashboardAct", false)
                val notificationModel = intent.getStringExtra("notificationModel")?.let {
                    GetSetSharedPrefs.convertToModel(
                        it,
                        NotificationModel::class.java
                    )
                }
                //intent.getSerializableExtra("notificationModel") as NotificationModel
                NotificationHandler(this).navigateNotification(notificationModel!!, !isTaskRoot)

            }
        } catch (e: Exception) {
            Log.e("NotificationException", e.toString())
        }




        if (MyAppConfig.defaultPlaceModel != null) {
            beginLocationFetch()
        } else {
            checkDefaultLocationAndFetchIfRequired()
        }
    }

    private fun checkDefaultLocationAndFetchIfRequired() {
        if (MyAppConfig.defaultPlaceModel == null) {
            MyAppConfig.fastFetchAppConfigValues(applicationContext, false, null, {
                runOnUiThread {
                    if (DialogMsg.myDialogPleaseWait?.isShowing != true)
                        DialogMsg.showPleaseWait(this)
                    beginLocationFetch()
                    //    fastFetchActivityCatAndCamps(showUiLoader)
                }

                handleAppUpdateAvailablePopUp()
            }, {
            }, compositeDisposable)
        }
    }

    private fun setCollapsingToolbarBehaviorListener() {
        appBarDashboard.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (appBarLayout != null) {
                if (Math.abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                    //  Collapsed
                    linHowItWorks.visibility = View.GONE
                    relHeaderContainer.setPadding(0, 0, 0, 40)
                } else {
                    //Expanded
                    linHowItWorks.visibility = View.VISIBLE
                    relHeaderContainer.setPadding(0, 0, 0, 0)
                }
            }
        })

    }

    override fun onResume() {
        super.onResume()

        Log.d("notificationKeys", "onResume")

        checkForNotificationBadge()

        //after coming back from search city
        try {
            MyGoogleAnalytics.sendSession(
                this, MyGoogleAnalytics.DASHBOARD,
                userModel.userEmail,
                this::class.java
            )

            if (MyPlaceUtils.placeModel.placeTitle.trim().isNotEmpty()) {
                txtPlaceHeader.text =
                    MyPlaceUtils.replaceUAECountry(MyPlaceUtils.placeModel.placeAddress)

                if (refreshNearbyPlaces) {
                    refreshNearbyPlaces = false
                    MyPlaceUtils.resetAllCategoryLocalResponses(applicationContext)

                    //fastFetchActivityCatAndCamps(true)
                    fastCheckAuthToken(true)

                    //redirect to explore in case search place was clicked from nearby explore fragment page
                    Log.d("NearbySelected", "" + isNearbyExploreSelected)
                    if (isNearbyExploreSelected) {
                        retainSelectedTab = true
                        handleExploreNearbyClick(false, showPassActivities)
                    }
                }
            }
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }

        //navigate to my bookings automatically after booking successful
        if (ActivityDetailActivity.goToMyBookings) {
            ActivityDetailActivity.goToMyBookings = false
            bottomNavView.selectedItemId = R.id.itemMyCorner
        } else if (ActivityDetailActivity.goToExplore) {
            ActivityDetailActivity.goToExplore = false
            bottomNavView.selectedItemId = R.id.itemNearby
        } else if (ActivityDetailActivity.goToPass) {
            showPassActivities = true
            bottomNavView.selectedItemId = R.id.itemNearby
            Handler().postDelayed({
                ActivityDetailActivity.goToPass = false
            }, 800)
        } else if (ActivityDetailActivity.goToCamp) {
            ActivityDetailActivity.goToCamp = false
            txtSeeAllCamp.performClick()
        }

        try {
            if (ageFrom.trim().isNotEmpty() && ageTo.trim().isNotEmpty()) {
                toolbar.imgFilterIndicatorDashboard.visibility = View.VISIBLE
            } else {
                toolbar.imgFilterIndicatorDashboard.visibility = View.GONE
            }
        } catch (e: Exception) {

        }

        try {
            if (MyAppConfig.serverAppVersionCode.trim().isNotEmpty()) {
                handleAppUpdateAvailablePopUp()
            }

        } catch (e: Exception) {

        }
    }

    private fun updateFcmToken() {
        doAsync {
            Timer().schedule(3000) {
                if (userModel.userId.trim().isEmpty()) {
                    /*uiThread {
                        MyProfileUtils.showSignUpLoginPopUp(this@DashboardActivity)
                    }*/
                } else {
                    MyProfileUtils.fastUpdateFcmToken(this@DashboardActivity)
                }
            }
        }
    }

    private fun keepCheckingDefaultLocation() {
        if (timer == null) {
            timer = object : CountDownTimer(60000, 1000) {
                override fun onFinish() {

                }

                override fun onTick(millisUntilFinished: Long) {
                    Log.d("ComingHereTimer", "" + millisUntilFinished)
                    if (MyAppConfig.defaultPlaceModel != null) {
                        MyPlaceUtils.placeModel = MyAppConfig.defaultPlaceModel!!
                        txtPlaceHeader.text =
                            MyPlaceUtils.replaceUAECountry(MyPlaceUtils.placeModel.placeAddress)
                        cancel()
                    }
                }

            }
        }
        timer?.start()
    }

    private fun beginLocationFetch() {
        //fetch current place only when we have empty place model
        if (MyPlaceUtils.placeModel.placeId.trim().isEmpty()) {
            MyPlaceUtils.initPlaceClient(applicationContext)
            if (myPlaceUtils == null) {
                myPlaceUtils = MyPlaceUtils(this, object : MyPlaceUtils.OnCityFetchedListener {
                    override fun onCityFetched(strCityPlaceId: String) {
                        MyPlaceUtils.resetAllCategoryLocalResponses(applicationContext)
                        Log.d("MyPlaceModel", "" + MyPlaceUtils.placeModel)
                        txtPlaceHeader.text =
                            MyPlaceUtils.replaceUAECountry(MyPlaceUtils.placeModel.placeAddress)

                        //fastFetchActivityCatAndCamps(true)

                        fastCheckAuthToken(false)
                    }

                    override fun onCityError() {
                        //toast(ErrorMsgs.ERR_API_MSG)
                        decideDefaultLocation()
                    }
                })
            }
            myPlaceUtils?.setAsCurrent = true
            myPlaceUtils?.requestLocationPermission()
        } else {
            DialogMsg.dismissPleaseWait(this@DashboardActivity)
        }
    }

    private fun decideDefaultLocation() {
        if (MyAppConfig.defaultPlaceModel != null) {
            MyPlaceUtils.placeModel = MyAppConfig.defaultPlaceModel!!
            txtPlaceHeader.text =
                MyPlaceUtils.replaceUAECountry(MyPlaceUtils.placeModel.placeAddress)

            Handler().postDelayed({
                fastCheckAuthToken(true)
            }, 1000)

        } else {
            keepCheckingDefaultLocation()
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        FastNetworking.compositeDisposable.clear()
        super.onDestroy()
    }

    private fun init() {
        Glide.with(this).load(R.drawable.bg_dashboard_header).into(imgHeaderDashboard)
        Glide.with(this).load(R.drawable.ic_logo).into(imgLogo)
        SetupToolbar.setToolbar(this, "", false)
        toolbar.imgFilterIndicatorDashboard.visibility = View.GONE

        setupBottomNavigationView()

        rvActivities.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvActivities.addItemDecoration(
            PaddingItemDecoration(
                resources.getDimension(R.dimen.dim_12).toInt()
            )
        )
        rvActivities.setHasFixedSize(true)

        rvPromotions.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvPromotions.addItemDecoration(
            PaddingItemDecoration(
                resources.getDimension(R.dimen.dim_12).toInt()
            )
        )
        rvPromotions.setHasFixedSize(true)

        rvPass.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvPass.addItemDecoration(
            PaddingItemDecoration(
                paddingEnd = resources.getDimension(R.dimen.dim_11).toInt()
            )
        )
        rvPass.setHasFixedSize(true)

        rvFeaturedPartners.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvFeaturedPartners.addItemDecoration(
            PaddingItemDecoration(
                resources.getDimension(R.dimen.dim_12).toInt()
            )
        )
        rvFeaturedPartners.setHasFixedSize(true)
    }

    private fun setupBottomNavigationView() {
        bottomNavView.setOnNavigationItemSelectedListener { item ->

            clearAllBackStack()
            isNearbyExploreSelected = false

            when (item.itemId) {
                R.id.itemHome -> {
                    DashboardActivity.selectedSearchModel = null
                    selectedPromotionModel = null

                    return@setOnNavigationItemSelectedListener true
                }

                R.id.itemNearby -> {
                    retainSelectedTab = false

                    val isActualUserClick =
                        if (showPassActivities) !ActivityDetailActivity.goToPass else true
                    handleExploreNearbyClick(isActualUserClick, passAct = showPassActivities)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.itemMyCorner -> {
                    //Log.d("UserModel_id", "" + userModel.userId)
                    if (userModel.userId.trim().isNotEmpty()) {
                        myCornerFragment = MyCornerFragment()
                        InitFragment(this).initFragment(
                            fragment = myCornerFragment,//MyBookingsFragment(),
                            manager = supportFragmentManager
                        )
                    } else
                        MyProfileUtils.showSignUpLoginPopUp(this)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.itemMore -> {
                    //Log.d("UserModel_id", "" + userModel.userId)
                    if (userModel.userId.trim().isNotEmpty())
                        InitFragment(this).initFragment(
                            fragment = MoreFragment(),
                            manager = supportFragmentManager
                        )
                    else
                        MyProfileUtils.showSignUpLoginPopUp(this)
                    return@setOnNavigationItemSelectedListener true
                }
            }
            false
        }
    }

    private fun handleExploreCamp(fromHomeSearch: Boolean) {
        //  clearAllBackStack()
        //  InitFragment(this).initFragment(fragment = CampListingFragment(), manager = supportFragmentManager)
        startActivity<CampListingActivity>("fromHomeSearch" to fromHomeSearch)
    }

    //isActualMenuItemClick-> true means user actually clicked the item, false means navigation on this item forcefully due to search or filters
    fun handleExploreNearbyClick(isActualMenuItemClick: Boolean, passAct: Boolean = false) {
        Log.d("ShowPass=>", "" + passAct)
        showPassActivities = passAct && !isActualMenuItemClick
        Log.d("ShowPassActivities=>", "" + passAct)

        //isNearbyExploreSelected = false

        /* selectedPromotionModel?.catId=""
         selectedPromotionModel?.activityId=""
         selectedPromotionModel?.subCateId=""*/

        //Mandatory Age Pop Up not needed any more
        /*if (!StaticValues.isExploreAlreadyClickedFirstTime) {
            StaticValues.isExploreAlreadyClickedFirstTime = true
            runOnUiThread {
                showFilterDialogNew(this@DashboardActivity, true, onClearFilter = {SearchAndFilterStaticData.selectedAgeGroupModel=null})
            }

        }*/

        isNearbyExploreSelected = true

        goToNearbyActivitiesFragment()

        if (!isActualMenuItemClick) {
            if (bottomNavView.selectedItemId != R.id.itemNearby)
                bottomNavView.menu.findItem(R.id.itemNearby).isChecked = true
        }

    }


    override fun onBackPressed() {
        super.onBackPressed()
        if (supportFragmentManager.backStackEntryCount < 1) {
            bottomNavView.selectedItemId = R.id.itemHome
        }
    }

    private fun goToNearbyActivitiesFragment() {
        //Log.d("SelectedTabPos", "" + selectedTabPos)

        GlobalScope.launch(Dispatchers.Default) {
            delay(250)
            with(Dispatchers.Main) {
                clearAllBackStack()

                if (!retainSelectedTab) {
                    NearbyFragment.selectedPos = -1
                }
                Log.d("SelectedTabPos", "" + NearbyFragment.selectedPos)
                nearByFragment = NearbyFragment.newInstance()
                InitFragment(this@DashboardActivity).initFragment(
                    fragment = nearByFragment,
                    manager = supportFragmentManager
                )
            }

        }

    }

    private fun fillPasses() {
        runOnUiThread {
            passAdapter = DashboardPassAdapter(this, arrayPassModel)
            rvPass.adapter = passAdapter
        }
    }

    /*private fun fillActivities() {
        arrayActivityModel.clear()
        arrayActivityModel.add(ActivityModel(activityName = "Sports", activityImage = R.drawable.ic_dummy_sports_))
        arrayActivityModel.add(ActivityModel(activityName = "Dance", activityImage = R.drawable.ic_dummy_dance_))
        arrayActivityModel.add(ActivityModel(activityName = "Music", activityImage = R.drawable.ic_dummy_music_))
        arrayActivityModel.add(ActivityModel(activityName = "Cooking", activityImage = R.drawable.ic_dummy_cooking_))

        activityAdapter = DashboardActivityAdapter(this, arrayActivityModel)
        rvActivities.adapter = activityAdapter
    }*/

    /*private fun fillPromotions() {
        arrayPromotionModel.clear()
        arrayPromotionModel.add(PromotionModel(promotionImage = R.drawable.ic_dummy_promo_1_1))
        arrayPromotionModel.add(PromotionModel(promotionImage = R.drawable.ic_dummy_promo_2_2))

        promotionAdapter = DashboardPromotionAdapter(this, arrayPromotionModel)
        rvPromotions.adapter = promotionAdapter
    }

    private fun fillPasses() {
        arrayPassModel.clear()
        arrayPassModel.add(PassModel(passImage = R.drawable.ic_dummy_pass_1))
        arrayPassModel.add(PassModel(passImage = R.drawable.ic_dummy_pass_2))
        arrayPassModel.add(PassModel(passImage = R.drawable.ic_dummy_pass_3))

        passAdapter = DashboardPassAdapter(this, arrayPassModel)
        rvPass.adapter = passAdapter
    }*/

    /*private fun fillFeaturedPartners() {
        arrayFeaturedPartnerModel.clear()
        arrayFeaturedPartnerModel.add(FeaturedPartnerModel(partnerImage = R.drawable.ic_dummy_featured_partner_1))
        arrayFeaturedPartnerModel.add(FeaturedPartnerModel(partnerImage = R.drawable.ic_dummy_featured_partner_2))
        arrayFeaturedPartnerModel.add(FeaturedPartnerModel(partnerImage = R.drawable.ic_dummy_featured_partner_3))
        arrayFeaturedPartnerModel.add(FeaturedPartnerModel(partnerImage = R.drawable.ic_dummy_featured_partner_1))
        arrayFeaturedPartnerModel.add(FeaturedPartnerModel(partnerImage = R.drawable.ic_dummy_featured_partner_2))

        partnerAdapter = DashboardFeaturedAdapter(this, arrayFeaturedPartnerModel)
        rvFeaturedPartners.adapter = partnerAdapter
    }*/


    private fun onClickListeners() {

        fun playYoutubeVideo(videoId: String) {
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$videoId"))
            appIntent.putExtra("force_fullscreen", true)
            try {
                startActivity(appIntent)
            } catch (e: ActivityNotFoundException) {
                val webIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$videoId"))

                startActivity(webIntent)
            } catch (e: ActivityNotFoundException) {
                toast("Didn't find any app which can play youtube video")
            }
        }

        compositeDisposable.add(
            RxView.clicks(txtPassSeeAll).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                retainSelectedTab = false
                MyPlaceUtils.resetAllCategoryLocalResponses(applicationContext)
                handleExploreNearbyClick(false, true)
            })

        compositeDisposable.add(
            RxView.clicks(linHowItWorks).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                try {
                    if (!MyAppConfig.howItWorksUrl.contains("http")) {
                        playYoutubeVideo(MyAppConfig.howItWorksUrl)
                    } else if (MyAppConfig.howItWorksUrl.contains("youtube.com", false)) {
                        val key =
                            MyAppConfig.howItWorksUrl.substring(MyAppConfig.howItWorksUrl.indexOf("v=") + 2)
                        Log.d("youtubeKey", key)
                        playYoutubeVideo(key)
                    } else {
                        val intent = Intent()
                        intent.action = Intent.ACTION_VIEW
                        intent.data = Uri.parse(MyAppConfig.howItWorksUrl)
                        startActivity(intent)
                    }
                } catch (e: Exception) {
                    toast("Unfortunately, video is not available")
                }
            })


        compositeDisposable.add(
            RxView.clicks(relNotification).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                if (userModel.userId.trim().isEmpty()) {
                    MyProfileUtils.showSignUpLoginPopUp(this)
                } else {
                    MyNavigations.gotoActivityListing(this@DashboardActivity)
                }
            })

        compositeDisposable.add(
            RxView.clicks(txtSeeAllPromotion).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                InitFragment(this).initFragment(
                    PromotionListingFragment(),
                    true,
                    supportFragmentManager
                )
                //  MyNavigations.gotoPromotionListingActivity(this@DashboardActivity)
            })

        compositeDisposable.add(
            RxView.clicks(txtSeeAllActivities).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                //setExploreAllBool(this, true)
                retainSelectedTab = false
                MyPlaceUtils.resetAllCategoryLocalResponses(applicationContext)
                handleExploreNearbyClick(false)

            })

        compositeDisposable.add(
            RxView.clicks(txtSeeAllCamp).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                handleExploreCamp(false)
            })

        compositeDisposable.add(
            RxView.clicks(relPlaceHeader).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                MyNavigations.goToSearchPlace(this)
            })

        compositeDisposable.add(
            RxView.clicks(txtSearchCatDashboard).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                //showSearchCatAndActivityPopUp(this)
                InitFragment.initDialogFragment(
                    supportFragmentManager,
                    CategorySubCategoryActivitiesSearchFragment.newInstanceToSearchActivityAndCamp()
                )
            })

        compositeDisposable.add(
            RxView.clicks(imgFilter).throttleFirst(
                800,
                TimeUnit.MILLISECONDS
            ).subscribe {
                //showing just age filter
                //showFilterDialogNew(this, false)

                //Complete filter dialog
                dialogMsg?.showNewFilterDialog(this, false, filterApplied = {})
            })
    }


    private fun clearAllBackStack() {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
    }


    private fun fastCheckAuthToken(showUiLoader: Boolean = true) {
        Log.d("Rx_UserModel_UserId", "-->" + userModel.userId)
        Log.d("Rx_AUTH_TOKEN", "-->" + StaticValues.AUTH_TOKEN)
        if (userModel.userId.trim().isNotEmpty()) {
            Log.d("User_id_", "_" + userModel.userId)
            StaticValues.reviveHeaders(applicationContext)
            if (StaticValues.AUTH_TOKEN.trim().isNotEmpty()) {
                Log.d("User_id_Auth_Token", "_" + StaticValues.AUTH_TOKEN)
                fastFetchCategoriesAndPartners(showUiLoader)
                return
            }
        }

        if (StaticValues.AUTH_TOKEN.trim().isEmpty()) {

            //if guest user token needs to be generated then just reset the user model, as guest

            doAsync {
                UserModel.setUserModel(applicationContext, UserModel(""))
                userModel = UserModel.getUserModel(applicationContext)
            }

            if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                if (showUiLoader)
                    DialogMsg.showPleaseWait(this)

                FastNetworking.makeRxCallPost(
                    applicationContext,
                    Urls.REGISTER_GUEST_USER,
                    false,
                    tag = "Guest_User_Auth",
                    onApiResult = object : FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {
                            /*  if (showUiLoader)
                                  DialogMsg.dismissPleaseWait(this@DashboardActivity)*/
                            fastFetchCategoriesAndPartners(showUiLoader)
                        }

                        override fun onApiError(error: ANError) {
                            if (showUiLoader) {
                                // DialogMsg.dismissPleaseWait(this@DashboardActivity)
                                dialogMsg?.showErrorApiDialog(
                                    this@DashboardActivity,
                                    onRetryListener = View.OnClickListener {
                                        dialogMsg?.dismissDialog(this@DashboardActivity)
                                        fastCheckAuthToken(showUiLoader)
                                    },
                                    isCancellable = false
                                )
                            }
                        }

                    })
            } else {
                dialogMsg?.showErrorConnectionDialog(
                    this,
                    onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@DashboardActivity) },
                    isCancellable = false
                )
            }
        } else {
            fastFetchCategoriesAndPartners(showUiLoader)
        }
    }

    fun fastFetchActivityCatAndCamps(showUiLoader: Boolean) {
        //if (DialogMsg.myDialogPleaseWait?.isShowing?:false)
        //   DialogMsg.dismissPleaseWait(this@DashboardActivity)

        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            if (showUiLoader) {
                if (DialogMsg.myDialogPleaseWait?.isShowing != true)
                    DialogMsg.showPleaseWait(this)
            }

            val hashParams = HashMap<String, String>()
            hashParams.put("latitude", "" + MyPlaceUtils.placeModel.placeLat)
            hashParams.put("longitude", "" + MyPlaceUtils.placeModel.placeLng)
            hashParams.put("cityPlaceId", "" + MyPlaceUtils.placeModel.placeCityId)

            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.HOME_SCREEN_CATEGORIES_PARTNERS,
                true,
                tag = "HomeData",
                hashParams = hashParams,
                onApiResult = object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            //if (showUiLoader)
                            DialogMsg.dismissPleaseWait(this@DashboardActivity)

                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                StaticValues.arrayDashboardCategories.clear()
                                arrDashCampModel.clear()
                                arrayFeaturedPartnerModel.clear()

                                rvCamp.adapter?.notifyDataSetChanged()
                                rvActivities.adapter?.notifyDataSetChanged()
                                rvFeaturedPartners?.adapter?.notifyDataSetChanged()

                                doAsync {
                                    val jobData = json.getJSONObject("data")
                                    if (jobData?.has("general_promotion") == true) {
                                        StaticValues.arrGlobalPromotion =
                                            JsonParse.parsePromotion(jobData.getJSONArray("general_promotion"))
                                    }

                                    val jsonCatArray = jobData.getJSONArray("categories")
                                    val onlineCatModel = CategoryModel(
                                        catName = "Online Classes",
                                        catImageDrawable = ContextCompat.getDrawable(
                                            this@DashboardActivity,
                                            R.drawable.ic_online
                                        )
                                    )
                                    StaticValues.arrayDashboardCategories.add(onlineCatModel)
                                    repeat(jsonCatArray.length()) {
                                        val objCat = jsonCatArray.getJSONObject(it)
                                        val catModel = CategoryModel(
                                            catId = objCat.optString("cat_id", ""),
                                            catName = objCat.optString("cat_name", ""),
                                            catImageUrl = objCat.optString("cat_image", ""),
                                            activityId = objCat.optString("activity_id", "")
                                        )
                                        val jsonSubCatArray =
                                            objCat.getJSONArray("get_sub_category")
                                        repeat(jsonSubCatArray.length()) {
                                            val subCat = jsonSubCatArray.getJSONObject(it)
                                            val subCatModel = SubCategoryModel(
                                                subCatId = subCat.optString("sub_cat_id"),
                                                subCatName = subCat.optString("sub_cat_name")
                                            )
                                            catModel.subCategories.add(subCatModel)
                                        }
                                        StaticValues.arrayDashboardCategories.add(catModel)
                                    }

                                    val jsonCampArray = jobData.getJSONArray("camp")
                                    repeat(jsonCampArray.length()) {
                                        val jobCamp = jsonCampArray.getJSONObject(it)
                                        jobCamp.run {
                                            CampModel(
                                                getString("camp_id"),
                                                getString("camp_name"),
                                                getString("camp_feature_img")
                                            )
                                        }.apply {
                                            PromotionOperation.setPromotionDataInCampModel(
                                                jobCamp,
                                                this,
                                                "",
                                                jobCamp.optString("max_camp_price", "0.0"),
                                                json
                                            )
                                        }.also {
                                            arrDashCampModel.add(it)
                                        }
                                    }

                                    val jsonPromotionArray = jobData.getJSONArray("promotions")
                                    Log.d("promotionJar", jsonPromotionArray.toString())

                                    arrayPromotionModel.clear()
                                    if (StaticValues.arrGlobalPromotion.isNotEmpty()) {
                                        arrayPromotionModel.addAll(StaticValues.arrGlobalPromotion)
                                    }

                                    arrayPromotionModel.addAll(
                                        JsonParse.parsePromotion(
                                            jsonPromotionArray,
                                            fromHome = true
                                        )
                                    )

                                    setPromotionAdapter()


                                    arrayPassModel.clear()
                                    val jsonPassArray = jobData.getJSONArray("pass_activity")
                                    repeat(jsonPassArray.length()) {
                                        val job = jsonPassArray.getJSONObject(it)
                                        job.run {
                                            PassModel(
                                                getString("activity_id"),
                                                getString("activity_name"),
                                                getString("activity_feature_img"),
                                                getString("cat_id"),
                                                getString("sub_cat_id")
                                            )
                                        }.apply {
                                            PromotionOperation.setPromotionDataInPassModel(
                                                job,
                                                this,
                                                job.getString("provider_id")
                                            )
                                        }.also {
                                            arrayPassModel.add(it)
                                        }

                                    }
                                    fillPasses()


                                    MyPlaceUtils.resetAllCategoryLocalResponses(applicationContext)

                                    runOnUiThread {
                                        try {
                                            if (!isFinishing) {

                                                setCampAdapter()

                                                categoryAdapter = DashboardActivityAdapter(
                                                    this@DashboardActivity,
                                                    StaticValues.arrayDashboardCategories
                                                ) { pos ->
                                                    NearbyFragment.selectedPos = pos
                                                    retainSelectedTab = true
                                                    MyPlaceUtils.resetAllCategoryLocalResponses(
                                                        applicationContext
                                                    )
                                                    handleExploreNearbyClick(false)
                                                }
                                                val animAdapter =
                                                    ScaleInAnimationAdapter(categoryAdapter)
                                                rvActivities.adapter = animAdapter
                                            }
                                        } catch (e: Exception) {
                                            Log.d("ExcCatUi", "" + e)
                                        }
                                    }
                                }


                                doAsync {
                                    val jsonPartnerArray =
                                        json.getJSONObject("data").getJSONArray("partners")
                                    arrayFeaturedPartnerModel.clear()
                                    repeat(jsonPartnerArray.length()) {
                                        val objPartner = jsonPartnerArray.getJSONObject(it)
                                        val partnerModel = FeaturedPartnerModel(
                                            objPartner.optString("provider_id", ""),
                                            objPartner.optString("provider_name", ""),
                                            partnerImageUrl = objPartner.optString(
                                                "provider_logo",
                                                ""
                                            )
                                        )
                                        arrayFeaturedPartnerModel.add(partnerModel)
                                    }

                                    runOnUiThread {
                                        try {
                                            if (!isFinishing) {
                                                partnerAdapter = DashboardFeaturedAdapter(
                                                    this@DashboardActivity,
                                                    arrayFeaturedPartnerModel
                                                )
                                                val animAdapter =
                                                    ScaleInAnimationAdapter(partnerAdapter)
                                                rvFeaturedPartners.adapter = animAdapter

                                                decideNavigationToActivityDetail()
                                                decideNavigationToCampDetail()
                                                decideNavigationToFlexiActivity()
                                            }
                                        } catch (e: Exception) {
                                            Log.d("ExcCatUi", "" + e)
                                        }
                                    }
                                }


                            }
                        } catch (e: Exception) {
                            Log.d("ExcParse", "" + e)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@DashboardActivity)

                        if (showUiLoader) {

                            dialogMsg?.showErrorApiDialog(
                                this@DashboardActivity,
                                onRetryListener = View.OnClickListener {
                                    dialogMsg?.dismissDialog(this@DashboardActivity)
                                    fastFetchCategoriesAndPartners()
                                },
                                isCancellable = true
                            )

                            decideNavigationToActivityDetail()
                            decideNavigationToCampDetail()
                            decideNavigationToFlexiActivity()
                        }
                    }

                })
        } else {
            dialogMsg?.showErrorConnectionDialog(
                this,
                onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@DashboardActivity) },
                isCancellable = true
            )
        }
    }

    private fun fastFetchCategoriesAndPartners(showUiLoader: Boolean = true) {

        if (showUiLoader) {
            updateFcmToken()

            fastRefreshViewMyProfile()

            checkDefaultLocationAndFetchIfRequired()


        }


        Handler().postDelayed({
            SaveSchoolAndInterest(this, compositeDisposable).fetchAndSaveIntoSharedPref()
        }, 2000)


        fastFetchActivityCatAndCamps(showUiLoader)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (nearByFragment != null) {
            nearByFragment?.onActivityResult(requestCode, resultCode, data)
        }
        if (myCornerFragment != null) {
            myCornerFragment?.onActivityResult(requestCode, resultCode, data)
        }
        if (requestCode == MyPlaceUtils.REQUEST_CHECK_SETTINGS) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    if (myPlaceUtils != null)
                        myPlaceUtils?.initMyCurrentPlace()
                }
                Activity.RESULT_CANCELED -> {
                    toast("Please enable location services")
                    decideDefaultLocation()
                }
            }
        }

    }


    private fun fastRefreshViewMyProfile() {
        doAsync {
            isProfileRefreshRunning = true
            MyProfileUtils.fastRefreshProfile(applicationContext, { model ->
                isProfileRefreshRunning = false
                if (model != null) {
                    userModel = model
                    try {
                        if (!userModel.isUserActive) {
                            runOnUiThread {
                                dialogMsg?.showErrorRetryDialog(
                                    this@DashboardActivity,
                                    "Oops!",
                                    ErrorMsgs.ERR_MSG_USER_DE_ACTIVE,
                                    "Log in",
                                    View.OnClickListener {
                                        dialogMsg?.dismissDialog(this@DashboardActivity)
                                        LoginBasicsUi.logOutUser(this@DashboardActivity)
                                    }, false
                                )
                            }

                        }
                    } catch (e: Exception) {
                        Log.e("ExcUserUi", "" + e)
                    }
                } else {
                    Log.e("UserModelNull", "Issue")
                }
            }, compositeDisposable)
        }


    }


    private fun handleAppUpdateAvailablePopUp() {
        try {
            val curAppVersionCode = BuildConfig.VERSION_CODE
            val serverAppVersionCode = MyAppConfig.serverAppVersionCode.toInt()
            if (curAppVersionCode < serverAppVersionCode) {
                runOnUiThread {
                    if (dialogMsgAppUpdate == null)
                        dialogMsgAppUpdate = DialogMsg()

                    var isAppUpdateAlreadyShowing = false
                    if (dialogMsgAppUpdate != null) {
                        if (dialogMsgAppUpdate!!.myDialogMsg != null) {
                            isAppUpdateAlreadyShowing = dialogMsgAppUpdate!!.myDialogMsg!!.isShowing
                        }
                    }
                    if (!isAppUpdateAlreadyShowing) {
                        dialogMsgAppUpdate?.showYesCancelAppUpdateAvailable(
                            this,
                            View.OnClickListener {
                                dialogMsgAppUpdate?.dismissDialog(this)
                                try {
                                    browse(MyAppConfig.PLAY_STORE_URL + BuildConfig.APPLICATION_ID)
                                } catch (e: Exception) {
                                    Log.e("ExcPlayStoreNav", "" + e)
                                }
                            },
                            MyAppConfig.isServerAppUpdateMandatory
                        )
                    }
                }

            }
        } catch (e: Exception) {
            Log.e("ExcShowUpdate", "" + e)
        }
    }

    private fun decideNavigationToActivityDetail() {
        if (ActivityDetailActivity.activityId.trim()
                .isNotEmpty() || ActivityDetailActivity.timeSlotId.trim()
                .isNotEmpty() || ActivityDetailActivity.enrolmentId.trim().isNotEmpty()
        ) {
            val actModel = ActivityModel(activityId = ActivityDetailActivity.activityId)
            actModel.arraySlotModel.add(ActivitySlotModel(slotId = ActivityDetailActivity.timeSlotId).also {
                it.slotEnrolmentId = ActivityDetailActivity.enrolmentId
            })
            MyNavigations.goToActivityDetail(this, actModel, 0, false, null)
        }
    }

    private fun decideNavigationToCampDetail() {
        if (CampDetailActivity.onCampIdBeforeLogin.trim().isNotEmpty()) {
            MyNavigations.goToCampDetail(this, CampDetailActivity.onCampIdBeforeLogin, false, "")
            CampDetailActivity.onCampIdBeforeLogin = ""
        }
    }

    fun decideNavigationToFlexiActivity() {
        if (FlexiActivityDetailActivity.onFlexiActivityId.trim().isNotEmpty()) {
            val activityModel =
                ActivityModel(activityId = FlexiActivityDetailActivity.onFlexiActivityId)
            MyNavigations.goToFlexiActivityDetail(this, activityModel, 0, false)
            FlexiActivityDetailActivity.onFlexiActivityId = ""
        }
    }

    private fun checkForNotificationBadge() {
        /*  if (UserModel.getLastNotificationId(this@DashboardActivity) == "") {
              imgCircleNotifier.visibility = View.VISIBLE
          } else {
              imgCircleNotifier.visibility = View.GONE
          }*/

        if (userModel.userId.trim().isEmpty()) {
            return
        }

        if (ConnectionDetector.isConnectingToInternet(this)) {
            doAsync {

                FastNetworking.makeRxCallPost(this@DashboardActivity,
                    Urls.VIEW_NOTIFICATION, true, tag = "View_Notification",
                    onApiResult = object : FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {

                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonData = json.getJSONObject("data")

                                    val jsonNotificationArray = jsonData.getJSONArray("data")
                                    val array =
                                        JsonParse.getNotificationModel(jsonNotificationArray)
                                    if (array.size > 0) {
                                        val newNotificationId = array[0].notificationId
                                        val oldNotificationId =
                                            UserModel.getLastNotificationId(this@DashboardActivity)
                                        Log.d("newNotificationId", newNotificationId)
                                        Log.d("oldNotificationId", oldNotificationId)
                                        if (newNotificationId.toLong() > oldNotificationId.toLong()) {
                                            // show notification badge
                                            imgCircleNotifier.visibility = View.VISIBLE
                                        } else {
                                            imgCircleNotifier.visibility = View.GONE
                                        }
                                    }
                                } else {

                                }
                            } catch (e: Exception) {
                                Log.d("ExcParse_CampList", "" + e)

                            }


                        }

                        override fun onApiError(error: ANError) {


                        }

                    })
            }
        } else {

        }
    }

}

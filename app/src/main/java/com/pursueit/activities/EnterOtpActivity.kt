package com.pursueit.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.androidnetworking.error.ANError
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_enter_otp.*
import kotlinx.android.synthetic.main.dialog_change_pass.*
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class EnterOtpActivity : AppCompatActivity() {

    private var compositeDisposable = CompositeDisposable()
    //private lateinit var snackBar:MySnackBar

    lateinit var dialogMsg: DialogMsg
    var strActualOtp: String? = ""
    var strEmail: String? = ""


    var isComingFromRegistration: Boolean = false
    var userJson = "{}"

    var isResendTimerRunning = false
    private val TOTAL_SECONDS_TO_WAIT = 60
    private var seconds = TOTAL_SECONDS_TO_WAIT + 1
    private var timer: CountDownTimer? = null

    var fromPhoneVerification:Boolean=false
    var phoneNumber:String=""

    lateinit var sendPhoneOtp:SendPhoneOtp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_otp)
        SetupToolbar.setToolbar(this, "", true, R.drawable.ic_dialog_close)
        compositeDisposable = CompositeDisposable()

        sendPhoneOtp= SendPhoneOtp(this)
        dialogMsg = DialogMsg()

        try {
            fromPhoneVerification=intent?.getBooleanExtra("fromPhoneVerification",false) == true
            if (fromPhoneVerification){
                phoneNumber=intent.getStringExtra("phoneNumber")
                strActualOtp = intent?.extras?.getString("ActualOtp")

                txtMsg.text = "Please type the verification code sent to +971$phoneNumber"
            }
            else {
                //get actual otp
                strActualOtp = intent?.extras?.getString("ActualOtp")
                strEmail = intent?.extras?.getString("Email")
                isComingFromRegistration = intent?.extras?.getBoolean("IsComingFromRegistration")!!
                userJson = intent?.extras?.getString("UserJson")!!

                txtMsg.text = "Please type the verification code sent to $strEmail"

                pinView.requestFocus()
            }
        } catch (e: Exception) {
            Log.d("Exc__", "" + e)
        }

        onClickListeners()

        startResendTimer()

    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this,MyGoogleAnalytics.VERIFY_CODE_SCREEN,if(strEmail!=null) strEmail!! else "",this::class.java)
    }

    private fun onClickListeners() {
        compositeDisposable.add(RxView.clicks(btnVerifyNow).throttleFirst(800, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                val strPinInput = pinView.text?.toString()?.trim()
                if (strPinInput != null) {
                    if (strPinInput.isEmpty() || strPinInput.length < 6) {
                        showValidationError(strErrMsg = "Please enter the verification code to proceed")
                        return@subscribe
                    }

                    if (fromPhoneVerification)
                    {
                        if (strActualOtp==strPinInput)
                        {
                            val curUser = UserModel.getUserModel(applicationContext)
                            UserModel.setUserModel(applicationContext,curUser.also {
                                it.isPhoneVerified = true
                                it.userPhone = phoneNumber
                            })
                            val intent=Intent()
                            intent.putExtra("isPhoneVerified",true)
                            setResult(Activity.RESULT_OK,intent)
                            onBackPressed()
                        }
                        else{
                            showValidationError(
                                strHeading = ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,
                                strErrMsg = "You've entered an incorrect verification code."
                            )
                        }
                    }
                    else {
                        if (strActualOtp != null && strEmail != null) {
                            if (strActualOtp == strPinInput) {
                                if (!isComingFromRegistration)
                                    showResetPasswordPopUp()
                                else {
                                    timer?.cancel()
                                    enableResendButton()
                                    parseUserAndProceed(JSONObject(userJson))
                                }
                            } else {
                                showValidationError(strHeading = ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,
                                    strErrMsg = "You've entered an incorrect verification code.")
                            }
                        }
                    }

                }
            })

        compositeDisposable.add(RxView.clicks(btnResend).throttleFirst(8000, TimeUnit.MILLISECONDS).subscribe {
            if (!isResendTimerRunning) {
                //call resend code API
                if (fromPhoneVerification)
                {
                    sendPhoneOtp.sendOtp(phoneNumber){otp->
                        strActualOtp=otp
                        startResendTimer()
                    }
                } else
                    fastResendOtp()
            }
        })
    }

    private fun fastResendOtp() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            DialogMsg.showPleaseWait(this)
            val hashParams = HashMap<String, String>()
            hashParams.put("emailId", "" + strEmail)
            FastNetworking.makeRxCallPost(applicationContext, if(isComingFromRegistration) Urls.RESEND_OTP else Urls.FORGOT_PASSWORD, true, hashParams, if(isComingFromRegistration) "ResendVerificationCode" else "ForgotPass", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(this@EnterOtpActivity)
                    try {
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            if(isComingFromRegistration) {
                                strActualOtp = json.optString("otp","")
                            }else{
                                strActualOtp=json.getJSONObject("data").optString("otp","")
                            }
                            startResendTimer()
                        } else {
                            val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)
                            showApiError(strErrMsg = strErrMsg)
                        }
                    } catch (e: Exception) {

                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(this@EnterOtpActivity)
                    showApiError(strErrMsg = ErrorMsgs.ERR_API_MSG)
                }

            }, compositeDisposable)
        } else {
            showApiError(strErrMsg = ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }



    private fun startResendTimer() {
        btnResend.isEnabled = false
        isResendTimerRunning = true
        btnResend.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorTextGreySubTitle))
        seconds = TOTAL_SECONDS_TO_WAIT + 1
        if (timer == null) {
            timer = object : CountDownTimer(1000L * (TOTAL_SECONDS_TO_WAIT + 1), 1000) {
                override fun onFinish() {
                    enableResendButton()
                }

                override fun onTick(millisUntilFinished: Long) {
                    seconds -= 1
                    btnResend.text = "Resend ($seconds)"
                }

            }
        }
        timer?.start()
    }

    private fun enableResendButton() {
        btnResend.isEnabled = true
        isResendTimerRunning = false
        btnResend.text = "Resend Code"
        btnResend.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorTextBlack))
    }

    private fun showResetPasswordPopUp() {
        val dialog = MyDialog(this).getMyDialog(R.layout.dialog_change_pass)

        dialog.txtHeaderChangePass.text = "Reset Password"

        dialog.relOldPass.visibility = View.GONE

        dialog.etOldPass.transformationMethod = PasswordTransformationMethod()
        dialog.etNewPass.transformationMethod = PasswordTransformationMethod()
        dialog.etConfPass.transformationMethod = PasswordTransformationMethod()

        dialog.imgCloseDialogChangePass.setOnClickListener {
            dialog.dismiss()
        }

        var isPassShowingOld = false
        var isPassShowingNew = false
        var isPassShowingConf = false

        dialog.imgOldPass.setOnClickListener {
            isPassShowingOld = !isPassShowingOld
            MyProfileUtils.setPasswordToggle(dialog.imgOldPass, dialog.etOldPass, isPassShowingOld)
        }

        dialog.imgNewPass.setOnClickListener {
            isPassShowingNew = !isPassShowingNew
            MyProfileUtils.setPasswordToggle(dialog.imgNewPass, dialog.etNewPass, isPassShowingNew)
        }

        dialog.imgConfPass.setOnClickListener {
            isPassShowingConf = !isPassShowingConf
            MyProfileUtils.setPasswordToggle(dialog.imgConfPass, dialog.etConfPass, isPassShowingConf)
        }

        dialog.btnUpdatePass.setOnClickListener {
            val strNewPass = dialog.etNewPass.text.toString().trim()
            val strConfPass = dialog.etConfPass.text.toString().trim()

            if (!ConnectionDetector.isConnectingToInternet(applicationContext)) {
                dialogMsg.showErrorConnectionDialog(this, "OK", View.OnClickListener {
                    dialogMsg.dismissDialog(this)
                })
                return@setOnClickListener
            }


            if (strNewPass.isEmpty()) {
                showValidationError(strErrMsg = "Please enter your new password")
                return@setOnClickListener
            }
            if (strNewPass.length < 6) {
                showValidationError(strErrMsg = "New Password must be at least 6 characters long")
                return@setOnClickListener
            }

            if (strConfPass.isEmpty()) {
                showValidationError(strErrMsg = "Please re-type your new password")
                return@setOnClickListener
            }

            if (strNewPass != strConfPass) {
                showValidationError(strErrMsg = "Both passwords do not match")
                return@setOnClickListener
            }

            dialog.dismiss()

            val hashParams = HashMap<String, String>()
            hashParams.put("emailId", "" + strEmail)
            hashParams.put("newPassword", strNewPass)

            dialog.dismiss()

            DialogMsg.showPleaseWait(this)
            FastNetworking.makeRxCallPost(applicationContext, Urls.RESET_PASSWORD, false, hashParams, "ResetPass", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(this@EnterOtpActivity)
                    try {
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            parseUserAndProceed(json = json)
                        } else {
                            showApiError()
                        }
                    } catch (e: Exception) {
                        showApiError(strHeading = ErrorMsgs.ERR_PARSE_TITLE, strErrMsg = ErrorMsgs.ERR_API_MSG)
                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(this@EnterOtpActivity)
                    showApiError()
                }

            }, compositeDisposable)

        }

        dialog.show()
    }

    private fun parseUserAndProceed(json: JSONObject) {
        try {
            val strMsg = json.optString("message", "Your password has been successfully updated")

            UserModel.parseUserJson(applicationContext, json, { userModel ->

                if (userModel.isUserActive) {

                    //when forgot password/reset work is to be done
                    if (!isComingFromRegistration) {
                        showSuccessAndProceed(strMsg)
                    } else {
                        //when right after entering the correct verification code, set email verified true and move further
                        DialogMsg.showPleaseWait(this)
                        FastNetworking.makeRxCallPost(this, Urls.UPDATE_USER_EMAIL_VERIFY_STATUS, true, HashMap<String, String>(), "UpdateEmailStatus", object :
                            FastNetworking.OnApiResult {
                            override fun onApiSuccess(json: JSONObject?) {
                                try {
                                    DialogMsg.dismissPleaseWait(this@EnterOtpActivity)
                                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                        val strSuccessMsg = json.optString("message", "Your email address has been successfully verified")
                                        showSuccessAndProceed(strSuccessMsg)
                                    } else {
                                        showApiError()
                                    }
                                } catch (e: Exception) {
                                    showApiError(strHeading = ErrorMsgs.ERR_PARSE_TITLE, strErrMsg = ErrorMsgs.ERR_API_MSG)
                                }

                            }

                            override fun onApiError(error: ANError) {
                                DialogMsg.dismissPleaseWait(this@EnterOtpActivity)
                                showApiError()
                            }

                        })

                    }

                } else {
                    dialogMsg.showErrorRetryDialog(this@EnterOtpActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, "Your account is de-activated, please contact admin", "OK", View.OnClickListener { dialogMsg.dismissDialog(this@EnterOtpActivity) })
                }
            }, { exc ->
                Log.d("ExceptionUserParse", "" + exc)
                showApiError(strHeading = ErrorMsgs.ERR_PARSE_TITLE, strErrMsg = ErrorMsgs.ERR_API_MSG)
            })
        } catch (e: Exception) {

        }
    }

    private fun showSuccessAndProceed(strMsg: String) {
        dialogMsg.showSuccessDialog(
            this@EnterOtpActivity, "Success", strMsg, "OK",
            View.OnClickListener {
                dialogMsg.dismissDialog(this@EnterOtpActivity)
                MyNavigations.goToDashboard(this@EnterOtpActivity)
                finishAffinity()
            }, false
        )
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun showValidationError(strHeading: String = "Alert", strErrMsg: String = "") {
        dialogMsg.showErrorRetryDialog(this@EnterOtpActivity, strHeading, strErrMsg, "OK", View.OnClickListener { dialogMsg.dismissDialog(this@EnterOtpActivity) })
    }

    private fun showApiError(strHeading: String = "Oops!", strErrMsg: String = ErrorMsgs.ERR_API_MSG) {
        dialogMsg.showErrorRetryDialog(this@EnterOtpActivity, strHeading, strErrMsg, "OK", View.OnClickListener { dialogMsg.dismissDialog(this@EnterOtpActivity) })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}

package com.pursueit.activities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.orhanobut.dialogplus.DialogPlus
import com.orhanobut.dialogplus.ViewHolder
import com.pursueit.R
import com.pursueit.adapters.*
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.custom.CustomFont
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.*
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_flexi_detail.*
import kotlinx.android.synthetic.main.bottom_sheet_select_camp_schedule.view.*
import kotlinx.android.synthetic.main.content_activity_detail_reviews.*
import kotlinx.android.synthetic.main.content_activity_detail_time_table.*
import kotlinx.android.synthetic.main.dialog_add_edit_review.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.*
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FlexiActivityDetailActivity : AppCompatActivity() {

    //var timeTableSlotsAdapter: TimeTableSlotsAdapter? = null
    var arrayTimeTable = ArrayList<ActivitySlotModel>()

    var reviewAdapter: ReviewsAdapter? = null
    var arrayReviewModel = ArrayList<ReviewModel>()

    //var similarActivityAdapter: SimilarActivityAdapter? = null
    //var arraySimilarActivities = ArrayList<ActivityModel>()

    var activityModel: ActivityModel? = null
    var selectedSlotModel: ActivityFlexiSessionModel? = null

    private lateinit var mySnackBar: MySnackBar

    val compositeDisposable = CompositeDisposable()

    var dialogMsg: DialogMsg? = null

    lateinit var userModel: UserModel

    private var sessionIndex = -1

    //private var noOfSessionsToAttend = 0

    private var comingFromBooking = false
    private var bookingModel: MyBookingsModel? = null
    private var bookingIndexPos = 0

    /*private var wasFetchMembersHit = false

    private var vatAmount = 0.0
    private var totalAmount = 0L*/

    private var dateBookingCancellationThreshold = ""

    private var orderId: String = ""

    var dialogSessionSelection: DialogPlus? = null

    var bottomSessionRootView: View? = null

    var dismissedViaSessionSelection: Boolean = false

    var selectedFlexiSessionModel: ActivityFlexiSessionModel? = null

    lateinit var manageFlexiActivityPaymentDialog: ManageFlexiActivityPaymentDialog

    var isComingFromBookingScreen: Boolean = false
    //var bookingStatus:String=""

    var bookingStatus: String = ""

    companion object {
        var goToMyBookings = false
        var isBookingStatusCancelled = false
        var actFlexiDetail: WeakReference<FlexiActivityDetailActivity>? = null

        //Used for saving state, when a guest user comes back after log in
        var activityId: String = ""
        var timeSlotId: String = ""
        var enrolmentId: String = ""

        var onFlexiActivityId = ""
    }

    fun setSelectedSessionModelAndMakeChanges(flexiSessionModel: ActivityFlexiSessionModel) {
        this.selectedFlexiSessionModel = flexiSessionModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flexi_detail)

        LoginBasicsUi.setTransparentStatusBarAndBgImage(this)

        actFlexiDetail = WeakReference<FlexiActivityDetailActivity>(this)

        userModel = UserModel.getUserModel(applicationContext)

        mySnackBar = MySnackBar(this, relParentActivityDetail)

        dialogMsg = DialogMsg()

        init()

        //reset state after redirection
        activityId = ""
        timeSlotId = ""
        enrolmentId = ""

        getSetActivityDetails(false)

        onClickListeners()


        //fillSimilarActivities()

        fastFetchActivityDetails()

        fastFetchReviews()

        //showPostReviewDialog()

        setListener()


        sparkFavoriteAct.visibility = View.GONE
        val markFavOrUnFav = MarkFavoriteOrUnFavorite(this)
        sparkFavoriteAct.setOnClickListener {

            val model = activityModel
            if (model != null) {
                markFavOrUnFav.fastMark(
                    activityId = model.activityId,
                    itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_ACTIVITY,
                    favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE
                )

                activityModel!!.isFavoriteMarked = !activityModel!!.isFavoriteMarked
                sparkFavoriteAct.playAnimation()
                sparkFavoriteAct.isChecked = activityModel!!.isFavoriteMarked

                val intent = Intent()
                intent.putExtra("actId", model.activityId)
                intent.putExtra("isFavorite", model.isFavoriteMarked)
                setResult(Activity.RESULT_OK, intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (ReviewsListingActivity.isReviewUpdated) {
            ReviewsListingActivity.isReviewUpdated = false
            fastFetchActivityDetails()
            fastFetchReviews()
        }

        MyGoogleAnalytics.sendSession(
            this,
            MyGoogleAnalytics.ACTIVITY_DETAIL_SCREEN + " - " + activityModel?.activityName,
            userModel.userEmail,
            this::class.java
        )
    }

    private fun setListener() {
        compositeDisposable.add(
            RxView.clicks(btnBookEnquire).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {

               /* if (btnBookEnquire.text.contains("cancel",true) || btnBookEnquire.text.contains("decline",true)) {
                    if (btnBookEnquire?.tag ?: "" == "cancelled" && dialogMsg != null) {
                        MyBookingsModel.showCancelReason(
                            this@FlexiActivityDetailActivity,
                            bookingModel?.cancelReason ?: "",
                            dialogMsg!!
                        )

                    }
                    return@subscribe
                }*/

                if(btnBookEnquire.text.toString().contains("Booking ${StaticValues.BOOKING_UNDER_PROCESS}")){
                    showErrorDialog(getString(R.string.booking_under_process_msg))
                }else{
                    if (userModel.userId.trim().isEmpty()) {
                        onFlexiActivityId = activityModel?.activityId ?: ""
                        MyProfileUtils.showSignUpLoginPopUp(this)
                    } else {
                        showDialogForSessionSelection()
                    }
                }

            })
    }

    fun showDialogForSessionSelection() {
        if (activityModel!!.arrFlexiSession.size == 1) {
            bottomSessionRootView?.linBottomSheetSelection?.setPadding(
                0,
                0,
                0,
                resources.getDimensionPixelOffset(R.dimen.dim_50)
            )
        }
        dialogSessionSelection!!.show()
    }

    fun showFlexiActivityPaymentDialog() {
        if (manageFlexiActivityPaymentDialog.dialogFlexiActivityPayment != null) {
            manageFlexiActivityPaymentDialog.setup()
        } else
            manageFlexiActivityPaymentDialog.fastFetchFamilyMembersFirstThenShowDialog()
    }

    private fun init() {
        /*try {
            isComingFromBookingScreen=intent.getBooleanExtra("isComingFromBookingScreen",false)
            //bookingStatus = intent.getStringExtra("bookingStatus")
        }catch (e:Exception)
        {

        }*/


       // linValidityInfo.visibility = View.VISIBLE

        hideShowCancellation(true)


        txtTableHeader.text = "Booking Options"
        linLegacyTimeTable.visibility = View.GONE
        linPricing.visibility = View.VISIBLE
        rvFlexiTimeTable.visibility = View.VISIBLE
        txtFlexiTimeTable.visibility = View.VISIBLE

        dialogSessionSelection = DialogPlus.newDialog(this)
            .setOnDismissListener {

            }.setContentHolder(ViewHolder(R.layout.bottom_sheet_select_camp_schedule))
            .create()//BottomSheetDialog(this)

        bottomSessionRootView = dialogSessionSelection!!.holderView

        bottomSessionRootView!!.txtCampTitleDialog.text = "Select Session"
        bottomSessionRootView!!.imgCloseBottomCamp.setOnClickListener {
            dialogSessionSelection!!.dismiss()
        }

        manageFlexiActivityPaymentDialog = ManageFlexiActivityPaymentDialog(this)

        txtActivityDesc.typeface = CustomFont.setFontSemiBold(assets)
        txtProviderAboutUs.typeface = CustomFont.setFontSemiBold(assets)

        rvFlexiTimeTable.layoutManager = LinearLayoutManager(applicationContext)

        rvTimeTable.layoutManager = LinearLayoutManager(applicationContext)
        rvTimeTable.setHasFixedSize(true)

        rvReviews.layoutManager = LinearLayoutManager(applicationContext)
        rvReviews.setHasFixedSize(true)

        rvSimilarActivities.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        rvSimilarActivities.addItemDecoration(PaddingItemDecoration(paddingEnd = resources.getDimension(
                    R.dimen.dim_10
                ).toInt()))
        //     rvSimilarActivities.setHasFixedSize(true)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun onClickListeners() {

        compositeDisposable.add(RxView.clicks(linProvider).throttleFirst(1000,TimeUnit.MILLISECONDS).subscribe {
            txtActivitySubTitle.performClick()
        })

        compositeDisposable.add(RxView.clicks(txtActivitySubTitle).throttleFirst(1000,TimeUnit.MILLISECONDS).subscribe {
            MyNavigations.gotoProviderDetailActivity(this@FlexiActivityDetailActivity,activityModel?.activityProviderId?:"")
        })

        compositeDisposable.add(
            RxView.clicks(imgBack).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                onBackPressed()
            })

        compositeDisposable.add(
            RxView.clicks(imgGetDirections).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                try {
                    if (activityModel != null && selectedSlotModel != null) {
                        MyPlaceUtils.getDirections(
                            this,
                            selectedSlotModel!!.latitude.toDouble(),
                            selectedSlotModel!!.longitude.toDouble()
                        )
                    }
                } catch (e: Exception) {
                    Log.e("location_error", e.toString())
                }

            })

        compositeDisposable.add(
            RxView.clicks(btnShare).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                MyAppConfig.shareApp(this, "" + activityModel?.activityName)
            })

    }

    private fun getCancellationSpanMsg(strDateTime: String): Spanned {
        return if (!activityModel!!.isActivityBookingCancellable && !comingFromBooking) {
            Html.fromHtml("This is a non-cancellable booking")
        } else if (!activityModel!!.isActivityBookingCancellable && comingFromBooking) {
            Html.fromHtml("Cancellation deadline has passed. This booking cannot be cancelled.")
        } else {
            Html.fromHtml("Cancel before <font color=\"#2B2B2B\">$strDateTime</font> to get full refund")
        }
    }

    private fun fastFetchActivityDetails() {
        try {
            if (ConnectionDetector.isConnectingToInternet(this)) {
                DialogMsg.showPleaseWait(this@FlexiActivityDetailActivity)
                //  cardProgress.visibility = View.VISIBLE
                val hashParams = HashMap<String, String>()
                hashParams["activityId"] = "" + activityModel?.activityId
                //    hashParams.put("timeSlotId", "" + selectedSlotModel?.slotId)
                //    hashParams.put("enrolmentId", "" + selectedSlotModel?.slotEnrolmentId)
                hashParams.put(
                    "comingFrom",
                    if (bookingModel != null) "1" else "2"
                ) //1->Booking 2->Normal
                FastNetworking.makeRxCallPost(
                    this,
                    Urls.SINGLE_ACTIVITY_DETAIL,
                    true,
                    hashParams,
                    "PlaceDetail",
                    object :
                        FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {
                            try {
                                DialogMsg.dismissPleaseWait(this@FlexiActivityDetailActivity)
                                // cardProgress.visibility = View.GONE
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonData = json.getJSONObject("data")
                                    activityModel =
                                        JsonParse.parseOneActivity(
                                            jsonData,
                                            json,
                                            bookingModel?.bookingThresholdDate
                                        )

                                    getSetActivityDetails(true)

                                    fastFetchSimilarActivities()
                                    //  makeBookNowEnquireEnable()
                                } else {
                                    showApiErrorRetry {
                                        fastFetchActivityDetails()
                                    }
                                }
                            } catch (e: Exception) {
                                Log.d("ExcParse_", "" + e)
                                showApiErrorRetry {
                                    fastFetchActivityDetails()
                                }
                            }
                        }

                        override fun onApiError(error: ANError) {
                            //showErrorMsg(ErrorMsgs.ERR_API_MSG)
                            try {
                                DialogMsg.dismissPleaseWait(this@FlexiActivityDetailActivity)
                                //  cardProgress.visibility = View.GONE
                                showApiErrorRetry {
                                    fastFetchActivityDetails()
                                }
                            } catch (e: Exception) {

                            }
                        }
                    },
                    compositeDisposable
                )
            } else {
                showConnectionError()
            }
        } catch (e: Exception) {

        }
    }

    private fun getSetActivityDetails(comingFromDetailApi: Boolean) {
        try {

            bottomSessionRootView!!.dividerCampSchedule.visibility = View.GONE
            bottomSessionRootView!!.txtCampDiscountDialog.visibility = View.GONE
            bottomSessionRootView!!.rvDiscountSchedule.visibility = View.GONE


            btnBookEnquire.text = "Book Now"
            if (activityModel == null) {
                activityModel = intent!!.extras!!.getSerializable("ActivityModel") as ActivityModel
                sessionIndex = intent!!.extras!!.getInt("sessionIndex")

                comingFromBooking = intent!!.extras!!.getBoolean("ComingFromBooking")

                if (activityModel!!.arrFlexiSession.size > sessionIndex) {
                    Log.d(
                        "Slot_Selected_In",
                        "" + sessionIndex + "\n" + activityModel!!.arrFlexiSession[sessionIndex]
                    )

                }

                if (intent?.extras?.getSerializable("BookingModel") != null) {
                    bookingModel =
                        intent?.extras?.getSerializable("BookingModel") as MyBookingsModel
                    bookingIndexPos = intent?.extras!!.getInt("BookingIndexPos")
                }
            }

            try {
                txtSlotPrice.text = "AED " + DecimalFormat("##.##").format(activityModel!!.flexiStartingPrice.toDouble())
            }catch (e:Exception){
                Log.e("txtSlotPriceFORMAT",e.toString())
            }

            txtSlotSessions.text = activityModel!!.flexiStartingSessionNumber

            bottomSessionRootView!!.rvCampSchedule.layoutManager =
                LinearLayoutManager(applicationContext)
            bottomSessionRootView!!.rvCampSchedule.adapter =
                FlexiActivitySessionAdapter(this, activityModel!!.arrFlexiSession)

            //place all data related to activity
            if (comingFromDetailApi) {
                if (userModel.userId.trim().isEmpty()) {
                    sparkFavoriteAct.visibility = View.GONE
                } else {
                    sparkFavoriteAct.visibility = View.VISIBLE
                    if (MarkFavoriteOrUnFavorite.LAST_FAV_ACTIVITY_ID == activityModel?.activityId ?: "") {
                        sparkFavoriteAct.isChecked =
                            MarkFavoriteOrUnFavorite.LAST_FLAG_FOR_ACTIVITY_OR_CAMP == MarkFavoriteOrUnFavorite.FLAG_FAVORITE

                        MarkFavoriteOrUnFavorite.LAST_FAV_ACTIVITY_ID = ""
                        MarkFavoriteOrUnFavorite.LAST_FLAG_FOR_ACTIVITY_OR_CAMP = ""
                    } else {
                        sparkFavoriteAct.isChecked = activityModel?.isFavoriteMarked ?: false
                    }
                }
            }

            txtActivityTitle.text = activityModel?.activityName?.trim()
            txtActivitySubTitle.text = activityModel?.activityProviderName?.trim()
            txtActivityLocation.text = activityModel?.activityLocation

            txtAdditionalInfo.text = activityModel?.activityAdditionalInfo?.trim()
            txtOtherInfo.text = activityModel?.activityOtherInfo?.trim()
            txtCancellationPolicy.text = activityModel?.activityCancellationPolicy?.trim()



            txtStarCount.text = "(${activityModel?.activityStarRatingCount})"

            ratingBarDetail.rating = activityModel!!.activityStarRating

            Log.d("Rating_Avg", "" + ratingBarDetail.rating)

            linRatingBarView.visibility =
                if (activityModel!!.activityStarRatingCount > 0) View.VISIBLE else View.GONE

            txtActivityDesc.text = activityModel?.activityDesc

            txtProviderName.text = activityModel?.activityProviderName?.trim()
            txtProviderAboutUs.text = activityModel?.activityProviderAboutUs?.trim()

            //Provide logo image
            Glide.with(this)
                .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(false))
                .load(Urls.IMAGE_URL_PARTNER + activityModel?.activityProviderLogo)
                .into(imgProvider)

            //fill up view pager UI (Activity Images)
            val adapter = MyImagePagerAdapter(this, activityModel!!.arrayImages)
            viewPager.adapter = adapter
            worm_dots_indicator.setViewPager(viewPager)

            if (activityModel!!.arrFlexiSession.size > 0 && sessionIndex >= 0) {

                if (comingFromDetailApi)
                    sessionIndex = 0

                selectedSlotModel = activityModel!!.arrFlexiSession[sessionIndex]
                //Log.d("Slot_Selected_In_$comingFromDetailApi", "" + sessionIndex + "\n" + selectedSlotModel.toString())

                if (comingFromDetailApi) {
                    //    getSessionsJson()
                    if (dateBookingCancellationThreshold.trim().isNotEmpty())
                        txtCancellationPolicy.text = getCancellationSpanMsg(
                            JsonParse.getFormattedDateCancellation(dateBookingCancellationThreshold)
                        )
                }

                if (comingFromDetailApi && comingFromBooking && bookingModel != null) {
                    hideShowCancellation(true)
                    txtCancellationPolicy.text="This is a non-cancellable booking"
                    /*if (bookingModel!!.bookingThresholdDate != null && !bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_CANCELLED, true))
                    txtCancellationPolicy.text = getCancellationSpanMsg(JsonParse.getFormattedDateCancellation(bookingModel!!.bookingThresholdDate!!))*/
                }else{
                    hideShowCancellation(false)
                }

                txtActivityLocation.text = selectedSlotModel?.location
               // Log.d("FullAddressSlot", selectedSlotModel?.fullAddress)
                if (activityModel!!.activityLocation.equals("at your premise",true)){
                    txtSlotLocation.text ="At Your Premise"
                    imgGetDirections.visibility=View.GONE
                }
                else {
                    imgGetDirections.visibility=View.VISIBLE
                    txtSlotLocation.text = if (selectedSlotModel?.fullAddress == "null") "---" else selectedSlotModel?.fullAddress
                }
                txtSlotAge.text = selectedSlotModel?.ageRange?.replace("yr", "year")

                txtSlotType.text = selectedSlotModel?.sessionType?.capitalize()
                if (txtSlotType.text.toString().trim().length > 2)
                    txtSlotType.text =
                        selectedSlotModel?.sessionType?.trim()?.capitalize() + " Activity"

                //calculate price to pay
                //txtSlotPrice.text = if (selectedSlotModel?.slotPrice!!.trim().isEmpty()) "" else "AED ${selectedSlotModel?.slotPrice}"
                //     val totalPayable = Math.round(getTotalPricePayable().toDouble())
                //   txtSlotPrice.text = "AED $totalPayable"

                //       Log.d("StatusAndAmount", "" + selectedSlotModel?.slotStatus + " - " + totalPayable)
/*
                if (selectedSlotModel?.slotStatus.equals("Available", true)) {
                    txtSlotPrice.visibility = View.VISIBLE
                    txtSlotPrice.text = if (totalPayable > 0) "AED $totalPayable" else "Free"
                } else {
                    txtSlotPrice.visibility = if (totalPayable > 0) View.VISIBLE else View.GONE
                }

                try {
                    val strAmt = totalPayable.toString().trim()
                    Log.d("StrAmt", strAmt)
                    if (strAmt.endsWith(".0") && !txtSlotPrice.text.toString().trim().equals("Free", true)) {
                        txtSlotPrice.text = "AED ${strAmt.substring(0, strAmt.length - 2)}"
                    }
                } catch (e: Exception) {
                    Log.d("ExcStrAmt", "" + e)
                }

               *//* txtSlotSessions.text =
                    if (selectedSlotModel!!.noOfEligibleSessions > 0) selectedSlotModel!!.noOfEligibleSessions.toString() else "-"*//*

                if (comingFromDetailApi)
                    btnBookEnquire.text =
                        if (selectedSlotModel?.slotStatus.equals("Available", true)) "Book Now" else "Enquire"*/

                //if coming from booking and status is upcoming or ongoing, then show cancel button
                /*  if (comingFromDetailApi && comingFromBooking) {

                      if (bookingModel!= null && bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED,true))
                      {
                          btnBookEnquire.text="Activity Completed"
                          btnBookEnquire.isEnabled=false
                      }

                     *//* if (activityModel!!.isActivityBookingCancellable) {
                        btnBookEnquire.text = "Cancel Booking"
                    } else {
                        btnBookEnquire.text = "Booking Confirmed"
                    }*//*

                    *//*if (bookingModel != null) {
                        if (bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED, true)) {
                            btnBookEnquire.text = "Sessions completed"
                        } else //if (bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_CANCELLED, true))
                        {
                            btnBookEnquire.text = "Book Now"
                        }
                    }*//*

                }*/


                //in case when enrolments (time table is found but date has already passed then we need to show enquire)
                /*   if (comingFromDetailApi && selectedSlotModel!!.arrayEnrolModel.any { !it.isDateAlreadyPassed } && !comingFromBooking) {
                       btnBookEnquire.text = "Book Now"
                   } else if (comingFromDetailApi && selectedSlotModel!!.arrayEnrolModel.none { !it.isDateAlreadyPassed } && !comingFromBooking) {
                       btnBookEnquire.text = "Enquire"
                   }*/

                //fill up time table (Date, Day and Time)
                //     timeTableSlotsAdapter = TimeTableSlotsAdapter(this, selectedSlotModel!!.arrayEnrolModel)

                rvFlexiTimeTable.adapter = FlexiTimeTableAdapter(activityModel!!.arrFlexiTimeTable)


                if (activityModel!!.arrFlexiSession.size > 0) {
                    if (bookingModel != null) {
                        val arrBookedFlexiSession = ArrayList<ActivityFlexiSessionModel>()
                        try {
                            val jarSession = JSONArray(bookingModel!!.sessionJson)
                            repeat(jarSession.length()) {
                                val job = jarSession.getJSONObject(it)
                                job.run {

                                    Log.d("SesDuration",getString("duration"))

                                    var duration=0
                                    try {
                                        duration=getInt("duration")
                                    }catch (e:Exception){Log.e("durationConvert",e.toString())}



                                    ActivityFlexiSessionModel(
                                        getString("flexi_activity_id"),
                                        getString("no_of_sessions"),
                                        ""
                                        ,
                                        getString("price"),JsonParse.convertMinutesToHours(duration)
                                        /*getString("duration")*/,
                                        "",
                                        "",
                                        "",
                                        "",
                                        "",
                                        getString("valid_days"),
                                        "",
                                        ""
                                    )
                                        .also {
                                            arrBookedFlexiSession.add(it)
                                        }
                                }
                            }
                        } catch (e: Exception) {
                            Log.e("bookedFlexi", e.toString())
                        }

                        rvTimeTable.adapter =
                            FlexiSessionTableForDetailAdapter(arrBookedFlexiSession)
                        linTimeTable.visibility =
                            if (arrBookedFlexiSession.size > 0) View.VISIBLE else View.GONE

                        /*try {
                            val strBookingDate=ChangeDateFormat.convertDate(bookingModel!!.bookingDate,"yyyy-MM-dd HH:mm:ss","dd MMM yyyy")
                            val dateObjOfBooking=ChangeDateFormat.getDateObjFromString(strBookingDate,"dd MMM yyyy")
                            val cal=Calendar.getInstance()
                            cal.time=dateObjOfBooking
                            cal.add(Calendar.DAY_OF_MONTH,arrBookedFlexiSession[0].validDays.toInt())
                            val expiryDate=cal.time
                            val strExpiryDate=ChangeDateFormat.convertDateIntoString(expiryDate,"dd MMM yyyy")

                            linValidityInfo.visibility=View.VISIBLE
                            txtValidityInfo.text="Session will expire on $strExpiryDate"
                        }catch (e:Exception)
                        {
                            Log.e("parseExcep",e.toString())
                        }*/

                        try {
                            val strExpiryDate = ChangeDateFormat.getDateAfterGivenDays(
                                bookingModel!!.bookingDate,
                                "yyyy-MM-dd HH:mm:ss",
                                arrBookedFlexiSession[0].validDays.toInt(),
                                "dd MMM yyyy"
                            )
                            txtValidityInfo.text = "Validity will expire on $strExpiryDate"
                            linValidityInfo.visibility = View.VISIBLE

                        } catch (e: Exception) {
                            Log.e("parseExcep", e.toString())
                        }

                    } else {
                        rvTimeTable.adapter =
                            FlexiSessionTableForDetailAdapter(activityModel!!.arrFlexiSession)
                        linTimeTable.visibility =
                            if (activityModel!!.arrFlexiSession.size > 0) View.VISIBLE else View.GONE
                    }
                }

                if (comingFromDetailApi && comingFromBooking && bookingModel?.bookingStatus.equals(
                        StaticValues.BOOKING_STATUS_COMPLETED,
                        true
                    ) && !activityModel!!.isReviewGiven
                )
                    showPostReviewDialog()

            } else {
                btnBookEnquire.text = "Enquire"
                linTimeTable.visibility = View.GONE
            }

            btnBookEnquire.visibility = if (comingFromDetailApi) View.VISIBLE else View.GONE
        } catch (e: Exception) {
            Log.d("ActivityGetExc", "" + e)
        }




        try {
            isComingFromBookingScreen = intent.getBooleanExtra("ComingFromBooking", false)
            if (intent?.extras?.getSerializable("BookingModel") != null) {
                bookingModel = intent?.extras?.getSerializable("BookingModel") as MyBookingsModel
                bookingStatus = bookingModel?.bookingStatus ?: ""
            }

            if (bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED, true)) {
                btnBookEnquire.text = "Flexi Completed"
                btnBookEnquire.isEnabled = false
            } else if (bookingStatus.equals(StaticValues.BOOKING_STATUS_ONGOING, true) || bookingStatus.equals(StaticValues.BOOKING_STATUS_UPCOMING,true)) {
                btnBookEnquire.text = "Booking Confirmed"
                btnBookEnquire.isEnabled = false
            }
            else if (bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_DECLINED, true))
            {
                btnBookEnquire.text = "Booking Declined"
                btnBookEnquire.isEnabled=false
                btnBookEnquire.tag="cancelled"
            }
            else if (bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_AWAITING, true))
            {
                btnBookEnquire.text = "Booking ${StaticValues.BOOKING_UNDER_PROCESS}"
                //btnBookEnquire.isEnabled=false
            }

            if (isComingFromBookingScreen &&  bookingModel!!.bookingStatus.equals(StaticValues.BOOKING_STATUS_CANCELLED, true)) {
                //btnBookEnquire.text = "Book Now"
                btnBookEnquire.text = MyBookingsModel.getCancellationMessage(bookingModel?.bookingCancelledBy?:"")//"Booking Cancelled"
                btnBookEnquire.tag="cancelled"
                btnBookEnquire.isEnabled=false
            }
        } catch (e: Exception) {

        }

        if (bookingModel != null) {
          /*  if (bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED, true)
                || bookingStatus.equals(StaticValues.BOOKING_STATUS_ONGOING, true)
            ) {*/
                if (bookingModel?.bookingAmtAfterTax != "") {
                    txtStartingFromLabel.text = "Paid Amount"
                    txtSlotPrice.text = "AED " + DecimalFormat("##.##").format(bookingModel?.bookingAmtAfterTax?.toDouble())
                    //txtSlotPrice.text="AED "+bookingModel?.bookingAmtAfterTax?.replace(".0","")
                }
            //}
        }


        if (btnBookEnquire.text.toString().trim() == "Enquire" && comingFromDetailApi) {
            linSessions.visibility = View.GONE
            linType.visibility = View.GONE
            txtOtherInfoLabel.visibility = View.GONE
            txtOtherInfo.visibility = View.GONE
            hideShowCancellation(false)
        }

        //show promotion ribbon on detail screen, only when not coming from my booking
        if (!comingFromBooking)
            PromotionOperation.setPromotionText(linPromotion, activityModel?.maxValuePromotionModel)
    }

    private fun hideShowCancellation(showCancellationPolicy: Boolean){
        if(showCancellationPolicy){
            txtCancellationPolicy.visibility=View.VISIBLE
            txtCancellationPolicyLabel.visibility = View.VISIBLE
        }else{
            txtCancellationPolicy.visibility=View.GONE
            txtCancellationPolicyLabel.visibility = View.GONE
        }
    }

    private fun fillSimilarActivities(arrActivityModel: ArrayList<ActivityModel>) {
        if (arrActivityModel.size > 0) {
            linSimilarActivities.visibility = View.VISIBLE
            val adapter = ActivityForMapAdapter(this@FlexiActivityDetailActivity, arrActivityModel)
            /*NearbyActivityAdapter(this,arrActivityModel){pos, progressShowMore ->

            }*///SimilarActivityAdapter(this, arraySimilarActivities)
            rvSimilarActivities.adapter = adapter
            /* rvSimilarActivities.setItemTransformer(ScaleTransformer.Builder().setMaxScale(1.05f).setMinScale(0.8f)
                 .setPivotX(Pivot.X.CENTER).setPivotY(Pivot.Y.BOTTOM).build())
             rvSimilarActivities.setSlideOnFling(true)*/
        } else {
            linSimilarActivities.visibility = View.GONE
        }

    }


    /*private fun showAddMemberDialog() {
        MyProfileUtils.showAddMemberDialog(this){
            fastFetchFamilyMembersFirstThenShowDialog()
        }
        *//*val dialog = MyDialog(this).getMyDialog(R.layout.dialog_add_member)
        var selectedGender = ""
        var selectedDob = ""

        dialog.imgMale.setOnClickListener {
            selectedGender = "male"
            dialog.imgMale.imageResource = R.drawable.ic_male_selected
            dialog.imgFemale.imageResource = R.drawable.ic_female_unselected
        }
        dialog.imgFemale.setOnClickListener {
            selectedGender = "female"
            dialog.imgMale.imageResource = R.drawable.ic_male_unselected
            dialog.imgFemale.imageResource = R.drawable.ic_female_selected
        }

        dialog.imgCloseDialogAdd.setOnClickListener {
            dialog.dismiss()
        }

        dialog.txtDob.setOnClickListener {
            val strDob = dialog.txtDob.text.toString().trim()
            Log.d("strDob", strDob)

            val cal = Calendar.getInstance()

            try {
                val arrDate = JsonParse.getFormattedDateTimeTableReverse(strDob).split("-")
                Log.d("ArrDate", arrDate.toString())
                if (arrDate.size > 1) {
                    cal.set(Calendar.YEAR, arrDate[0].toInt())
                    cal.set(Calendar.MONTH, arrDate[1].toInt() - 1)
                    cal.set(Calendar.DAY_OF_MONTH, arrDate[2].toInt())
                }
            } catch (e: Exception) {
                Log.d("CalSetExc", "" + e)
            }

            val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val strMonth = if ((month + 1) / 9 <= 1.0) "0${month + 1}" else "${month + 1}"
                val strDay = if ((dayOfMonth) / 9 <= 1.0) "0$dayOfMonth" else dayOfMonth.toString()
                selectedDob = "$year-$strMonth-$strDay"
                Log.d("SelectedDate", selectedDob)
                dialog.txtDob.text = JsonParse.getFormattedDateTimeTable(selectedDob)
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis

            datePickerDialog.show()
        }

        dialog.btnSave.setOnClickListener {
            val strName = dialog.etFullName.text.toString().trim()
            val strDob = dialog.txtDob.text.toString().trim()
            val strSchoolName = dialog.etSchoolName.text.toString().trim()
            val strInterests = dialog.etInterests.text.toString().trim()
            val strPrefLocation = dialog.etPrefLocation.text.toString().trim()

            val validations = MyValidations(this)
            if (strName.isEmpty()) {

                showErrorDialog("Please enter your full name")
                return@setOnClickListener
            }
            if (!validations.checkName(strName)) {
                showErrorDialog("Please enter a valid full name")
                return@setOnClickListener
            }

            if (strDob.isEmpty()) {
                showErrorDialog("Please enter your date of birth")
                return@setOnClickListener
            }

            if (selectedGender.isEmpty()) {
                showErrorDialog("Please enter your gender")
                return@setOnClickListener
            }

            if (strInterests.isEmpty()) {
                showErrorDialog("Please enter your interests")
                return@setOnClickListener
            }

            val hashParams = HashMap<String, String>()
            hashParams.put("name", strName)
            hashParams.put("gender", selectedGender)
            hashParams.put("interests", strInterests)
            hashParams.put("schoolName", strSchoolName)
            hashParams.put("preferredActivities", strPrefLocation)
            hashParams.put("dateOfBirth", JsonParse.getFormattedDateTimeTableReverse(strDob))

            if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                DialogMsg.showPleaseWait(this)
                MyProfileUtils.fastAddFamilyMember(this, hashParams, object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                dialog.dismiss()
                                fastFetchFamilyMembersFirstThenShowDialog()
                            } else {
                                toast(ErrorMsgs.ERR_API_MSG)
                            }
                        } catch (e: Exception) {
                            Log.d("ExcAddMember", "" + e)
                            toast(ErrorMsgs.ERR_PARSE_TITLE)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        toast(ErrorMsgs.ERR_API_MSG)
                    }

                })
            } else {
                toast(ErrorMsgs.ERR_CONNECTION_MSG)
            }


        }
        dialog.show()*//*
    }*/


    private fun fastFetchReviews() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            progressReviews.visibility = View.VISIBLE
            val hashParams = HashMap<String, String>()
            hashParams["activityId"] = "" + activityModel?.activityId
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.VIEW_REVIEWS,
                true,
                hashParams,
                "ViewReviews",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        doAsync {
                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    arrayReviewModel.clear()
                                    arrayReviewModel.addAll(JsonParse.parseReviews(json))

                                }

                            } catch (e: Exception) {
                                Log.d("ExcReviews", "" + e)
                            } finally {
                                runOnUiThread {
                                    progressReviews.visibility = View.GONE
                                    if (arrayReviewModel.size > 0) {
                                        linReviewsDetailPage.visibility = View.VISIBLE
                                        reviewAdapter = ReviewsAdapter(
                                            this@FlexiActivityDetailActivity,
                                            arrayReviewModel,
                                            { posClicked ->
                                                val revModel = arrayReviewModel[posClicked]
                                                Log.d(
                                                    "RevModel",
                                                    "" + revModel + "\nRevUser:\n" + revModel.reviewUserId
                                                )
                                                Log.d("MyUserId", "" + userModel.userId)
                                                if (userModel.userId.trim().isNotEmpty() && revModel.reviewUserId.trim().isNotEmpty()) {
                                                    if (userModel.userId == revModel.reviewUserId) {
                                                        showPostReviewDialog(true, revModel)
                                                    }
                                                }
                                            },
                                            { _, _ ->

                                            })
                                        rvReviews.adapter = reviewAdapter

                                        txtViewAllReviews.visibility =
                                            if (arrayReviewModel.size > 2) View.VISIBLE else View.GONE
                                        compositeDisposable.add(
                                            RxView.clicks(txtViewAllReviews).throttleFirst(
                                                700,
                                                TimeUnit.MILLISECONDS
                                            ).subscribe {
                                                if (activityModel != null)
                                                    MyNavigations.goToAllReviews(
                                                        this@FlexiActivityDetailActivity,
                                                        arrayReviewModel,
                                                        activityId = activityModel!!.activityId
                                                    )
                                            })

                                    } else {
                                        linReviewsDetailPage.visibility = View.GONE
                                    }

                                }
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        try {
                            progressReviews.visibility = View.GONE
                            linReviewsDetailPage.visibility = View.GONE
                        } catch (e: Exception) {

                        }
                    }

                })
        }
    }

    fun showErrorDialog(strMsg: String, isSeatFull:Boolean=false) {
        dialogMsg?.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this@FlexiActivityDetailActivity)

            if(isSeatFull){
                //finish the intermediate ActivitySlots activity as well
                ActivitySlotsActivity.actSlots?.get()?.finish()

                //finish the intermediate ActivityDetail activity as well
                ActivityDetailActivity.actDetail?.get()?.finish()

                //finish the intermediate CampDetail activity as well
                CampDetailActivity.campAct?.get()?.finish()

                this.finish()
            }
        })
    }

    private fun showPostReviewDialog(
        isEditReview: Boolean = false,
        reviewModel: ReviewModel? = null
    ) {
        try {
            if (activityModel != null && selectedSlotModel != null) {
                if ((!activityModel!!.isReviewGiven || isEditReview) && userModel.userId.trim().isNotEmpty()) {
                    val dialog = MyDialog(this).getMyDialog(R.layout.dialog_add_edit_review)
                    if (!isEditReview) {
                        dialog.ratingBarPostReview.rating = 5f
                    }
                    dialog.ratingBarPostReview.setOnRatingChangeListener { baseRatingBar, flValue ->
                        Log.d("RatingValue", "" + flValue)
                    }

                    if (isEditReview && reviewModel != null) {
                        dialog.etReview.setText(reviewModel.reviewDesc.trim())
                        MyProfileUtils.setSelectionEnd(dialog.etReview)
                        dialog.ratingBarPostReview.rating = reviewModel.reviewRating
                        dialog.btnPostReview.text = "Update Review"
                    }

                    dialog.btnPostReview.setOnClickListener {
                        val strReview = dialog.etReview.text.toString().trim()
                        val flRatingValue = dialog.ratingBarPostReview.rating

                        /*if (strReview.isEmpty()) {
                            showErrorDialog("Please write some review and then proceed")
                            return@setOnClickListener
                        }*/

                        if (strReview.isNotEmpty() && strReview.length < 10) {
                            showErrorDialog("Your review should be at least of 10 characters")
                            return@setOnClickListener
                        }

                        dialog.dismiss()

                        val hashParams = HashMap<String, String>()
                        hashParams.put("activityId", "" + activityModel?.activityId)
                        hashParams.put("reviewRating", "" + flRatingValue)
                        hashParams.put("reviewDescription", strReview)

                        if (isEditReview && reviewModel != null)
                            hashParams.put("reviewId", "" + reviewModel.reviewId)

                        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                            MyReviewUtils.fastAddRatingReview(
                                this,
                                hashParams,
                                "AddReview",
                                { json ->
                                    try {
                                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                            val strMsg =
                                                json.optString(
                                                    "message",
                                                    "Your review has been successfully posted"
                                                )
                                            dialogMsg?.showSuccessDialog(
                                                this, "Success", strMsg, "OK",
                                                View.OnClickListener {
                                                    dialogMsg?.dismissDialog(this)
                                                    fastFetchActivityDetails()
                                                    fastFetchReviews()
                                                }, isCancellable = false
                                            )
                                        } else {
                                            showApiError()
                                        }
                                    } catch (e: Exception) {

                                    }
                                },
                                compositeDisposable
                            )
                        } else {
                            showConnectionError()
                        }

                    }

                    dialog.imgCloseDialogPostReview.setOnClickListener {
                        dialog.dismiss()
                    }

                    DialogMsg.animateViewDialog(dialog.relParentPostReview)

                    dialog.show()
                }
            }
        } catch (e: Exception) {
            Log.d("ReviewDialogExc", "" + e)
        }
    }

    //this enteredAmount is not including the VAT (Tax) enteredAmount
    /* private fun getTotalPricePayable(): String {
         var singlePrice = 0.0
         if (activityModel != null && selectedSlotModel != null) {
             try {
                 singlePrice = JsonParse.decimalFormat.format(selectedSlotModel!!.slotPrice.toDouble()).toDouble()
             } catch (e: Exception) {
                 singlePrice = 0.0
             }
             val arraySessionsToAttend = selectedSlotModel!!.arrayEnrolModel.filter { !it.isDateAlreadyPassed }
             noOfSessionsToAttend = arraySessionsToAttend.size

         }


         return "" + (singlePrice * noOfSessionsToAttend)
     }*/

    /*   private fun getSessionsJson(): String {
           var strJson = "[]"
           try {
               val arrJson = JSONArray()
               for (model in selectedSlotModel!!.arrayEnrolModel.filter { !it.isDateAlreadyPassed }) {
                   val objJson = JSONObject()
                   objJson.put("enrolment_slot_id", model.enrolId)
                   objJson.put("enrolment_slot_date", JsonParse.getFormattedDateTimeTableReverse(model.enrolDate))
                   objJson.put("enrolment_slot_time", model.enrolTime)
                   objJson.put("enrolment_slot_week", model.enrolDay)
                   objJson.put("enrolment_slot_duration", model.enrolDuration)
                   arrJson.put(objJson)

                   //Log.d("JSON_",""+objJson)

                   try {
                       if (arrJson.length() == 1) {
                           val strDate = objJson.getString("enrolment_slot_date").trim()
                           val strTime = objJson.getString("enrolment_slot_time").split("-")[0].trim() + ":00"

                           Log.d("EnrolSlotDateTime", "$strDate $strTime")

                           val dtFirstSlot = JsonParse.getFormattedServerDateJava("$strDate $strTime")

                           Log.d("Cancellation_hours", "" + activityModel!!.activityCancellationHours)
                           val cal = Calendar.getInstance()
                           cal.time = dtFirstSlot
                           cal.add(Calendar.HOUR, -activityModel!!.activityCancellationHours)

                           Log.d("DateThreshold", "" + cal.time)
                           dateBookingCancellationThreshold = JsonParse.getFormattedServerDateJavaReverse(cal.time)
                           Log.d("DateThresholdConverted", dateBookingCancellationThreshold)

                           if (!comingFromBooking && activityModel != null && activityModel?.curServerDateJava != null) {
                               activityModel?.isActivityBookingCancellable =
                                   !activityModel!!.curServerDateJava!!.after(cal.time)
                           }
                       }
                   } catch (e: Exception) {
                       Log.d("NumberExc__", "" + e)
                   }

               }
               strJson = arrJson.toString()
               return strJson
           } catch (e: Exception) {
               Log.d("Exc_JsonPrepare", "" + e)
           }
           return strJson
       }*/

    /*private fun goToCcAvenue() {

    }*/


    fun showApiError() {
        dialogMsg?.showErrorApiDialog(this, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        })
    }

    private fun showApiErrorRetry(onRetry: () -> Unit) {
        dialogMsg?.showErrorApiDialog(this, "Retry", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
            onRetry()
        }, isCancellable = true)
    }

    fun showConnectionError() {
        dialogMsg?.showErrorConnectionDialog(this, "OK", View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        })
    }


    private fun fastFetchSimilarActivities() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            val hashParams = HashMap<String, String>()
            hashParams["latitude"] = "" + MyPlaceUtils.placeModel.placeLat
            hashParams["longitude"] = "" + MyPlaceUtils.placeModel.placeLng
            hashParams["categoryId"] = activityModel?.categoryId ?: ""
            hashParams["activityId"] = activityModel?.activityId ?: ""
            hashParams["cityPlaceId"] = MyPlaceUtils.placeModel.placeCityId
            //   DialogMsg.showPleaseWait(this)
            progressSimilar.visibility = View.VISIBLE
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.VIEW_SIMILAR_ACTIVITIES,
                true,
                hashParams,
                "SimilarActivities",
                object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        progressSimilar.visibility = View.GONE
                        //DialogMsg.dismissPleaseWait(this@FlexiActivityDetailActivity)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                            val (arrActivity, arrActivityForMap) = JsonParse.parseNearbyActivities(json, json)
                            fillSimilarActivities(arrActivityForMap)
                        }
                        else{
                            linSimilarActivities.visibility = View.GONE
                        }
                    }

                    override fun onApiError(error: ANError) {
                        //    DialogMsg.dismissPleaseWait(this@FlexiActivityDetailActivity)
                        progressSimilar.visibility = View.GONE
                    }

                },
                compositeDisposable
            )
        }
    }


}

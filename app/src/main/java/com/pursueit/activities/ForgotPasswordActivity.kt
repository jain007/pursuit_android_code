package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.androidnetworking.error.ANError
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.content_forgot_password.*
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class ForgotPasswordActivity : AppCompatActivity() {

    private val compositeDisposable=CompositeDisposable()
    //private lateinit var snackBar:MySnackBar

    var dialogMsg: DialogMsg? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        LoginBasicsUi.setTransparentStatusBarAndBgImage(this, imgBg)
        SetupToolbar.setToolbar(this, "", true)

        init()
        onClickListeners()

        etForgotPassEmailId.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.FORGOT_PASSWORD_SCREEN,"",this::class.java)
    }

    private fun init(){
        dialogMsg = DialogMsg()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onClickListeners(){
        compositeDisposable.add(
            RxView.clicks(btnResetPass)
                .throttleFirst(500,TimeUnit.MILLISECONDS)
                .subscribe{
                    val strEmail=etForgotPassEmailId.text.toString().trim()

                    if(strEmail.isEmpty()){
                        showValidationError("Please enter your registered email address")
                        //snackBar.showSnackBarError("Please enter your registered email address")
                        return@subscribe
                    }

                    val validations=MyValidations(this)

                    if(!validations.checkEmail(strEmail)){
                        showValidationError("Please enter a valid email address")
                        //snackBar.showSnackBarError("Please enter a valid email address")
                        return@subscribe
                    }

                    if(ConnectionDetector.isConnectingToInternet(applicationContext)){
                        DialogMsg.showPleaseWait(this)
                        val hashParams=HashMap<String,String>()
                        hashParams.put("emailId",strEmail)
                        FastNetworking.makeRxCallPost(applicationContext,Urls.FORGOT_PASSWORD,false,hashParams,"ForgotPass",object : FastNetworking.OnApiResult{
                            override fun onApiSuccess(json: JSONObject?) {
                                try{
                                    DialogMsg.dismissPleaseWait(this@ForgotPasswordActivity)

                                    if(json!!.getBoolean(StaticValues.KEY_STATUS)){
                                        val jsonDataObj=json.getJSONObject("data")
                                        if(jsonDataObj.has("otp")) {
                                            MyNavigations.goToEnterOtp(this@ForgotPasswordActivity,jsonDataObj.getString("otp"),strEmail)
                                        }else{
                                            showApiError(strErrMsg = "Couldn't send OTP")
                                        }
                                    }else{
                                        val strErrMsg=json.optString("message","You've entered an invalid email address, please enter your registered email address to reset your password")
                                        showApiError(ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,strErrMsg)
                                    }
                                }catch (e:Exception){
                                    Log.d("ParseExc",""+e)
                                    showApiError(ErrorMsgs.ERR_PARSE_TITLE,ErrorMsgs.ERR_API_MSG)
                                }
                            }

                            override fun onApiError(error: ANError) {
                                DialogMsg.dismissPleaseWait(this@ForgotPasswordActivity)
                                showApiError()
                            }

                        },compositeDisposable)
                    }else{
                        dialogMsg?.showErrorConnectionDialog(this,"OK",View.OnClickListener { dialogMsg?.dismissDialog(this) })
                    }

                })
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun showValidationError(strErrMsg:String="") {
        dialogMsg?.showErrorRetryDialog(this@ForgotPasswordActivity, "Alert", strErrMsg, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@ForgotPasswordActivity) })
    }

    private fun showApiError(strHeading:String="Oops!",strErrMsg:String=ErrorMsgs.ERR_API_MSG) {
        dialogMsg?.showErrorRetryDialog(this@ForgotPasswordActivity, strHeading, strErrMsg, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@ForgotPasswordActivity) })
    }
}

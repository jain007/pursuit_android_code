package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatEditText
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.androidnetworking.error.ANError
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_login_register.*
import kotlinx.android.synthetic.main.content_sign_in.*
import kotlinx.android.synthetic.main.content_sign_up.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class LoginRegisterActivity : AppCompatActivity() {

    var flagLoginRegister = 1 //1->Login  2->Register

    var isPasswordShowing = false

    //var mySnackBar: MySnackBar? = null

    var compositeDisposable = CompositeDisposable()

    var dialogMsg: DialogMsg? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_register)

        compositeDisposable = CompositeDisposable()

        LoginBasicsUi.setTransparentStatusBarAndBgImage(this, imgBg)

        SetupToolbar.setToolbar(this, "", true)

        dialogMsg = DialogMsg()

        init()

        onClickListeners()

        MainActivity.customTextViewLinks(this,txtTermsPrivacyPolicyRegister)
    }

    override fun onResume() {
        super.onResume()
        when (flagLoginRegister) {
            1 -> MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.LOGIN_SCREEN, "", this::class.java)
            2 -> MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.REGISTER_SCREEN, "", this::class.java)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun init() {
        //mySnackBar = MySnackBar(this, relParent)

        flagLoginRegister = intent?.extras!!.getInt(StaticValues.LOGIN_REGISTER_FLAG)

        //visibility of login or register screens
        if (flagLoginRegister == 1) {
            linSignIn.visibility = View.VISIBLE
            linSignUp.visibility = View.GONE
        } else {
            linSignIn.visibility = View.GONE
            linSignUp.visibility = View.VISIBLE
        }

        etLoginPassword.transformationMethod = PasswordTransformationMethod()
        etPassword.transformationMethod = PasswordTransformationMethod()
    }

    private fun onClickListeners() {

        compositeDisposable.add(
            RxView.clicks(imgPassword).throttleFirst(300, TimeUnit.MILLISECONDS).subscribe {
                setPasswordToggle(imgPassword, etPassword)
            })

        compositeDisposable.add(
            RxView.clicks(imgLoginPassword).throttleFirst(300, TimeUnit.MILLISECONDS).subscribe {
                setPasswordToggle(imgLoginPassword, etLoginPassword)
            })

        compositeDisposable.add(
            RxView.clicks(txtForgotPass).throttleFirst(300, TimeUnit.MILLISECONDS).subscribe {
                startActivity<ForgotPasswordActivity>()
            })

        // for login
        compositeDisposable.add(
            RxView.clicks(btnLogIn).throttleFirst(300, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                val strEmail = etLoginEmailId.text.toString().trim()
                val strPass = etLoginPassword.text.toString().trim()

                val validations = MyValidations(this)

                if (strEmail.isEmpty()) {
                    showValidationError("Please enter your Email ID")
                    //mySnackBar?.showSnackBarError("Please enter your email address")
                    return@subscribe
                }

                if (!validations.checkEmail(strEmail)) {
                    showValidationError("Please enter a valid Email ID")
                    //mySnackBar?.showSnackBarError("Please enter a valid email address")
                    return@subscribe
                }

                if (strPass.isEmpty()) {
                    showValidationError("Please enter your password")
                    //mySnackBar?.showSnackBarError("Please enter your password")
                    return@subscribe
                }

                if (strPass.length < 6) {
                    showValidationError("Please enter a valid password\n(Must be at least 6 characters in length)")
                    //mySnackBar?.showSnackBarError("Please enter a valid password\n(Should be at least 6 characters in length)")
                    return@subscribe
                }

                //API Params filling
                val hashParams = HashMap<String, String>()
                hashParams.put("emailId", strEmail.trim())
                hashParams.put("password", strPass.trim())

                fastRegisterOrLoginUser(Urls.LOGIN_USER, hashParams)

            })


        //for registration
        compositeDisposable.add(
            RxView.clicks(btnSignUp).throttleFirst(300, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                val strFName = etFirstName.text.toString().trim()
                val strLName = etLastName.text.toString().trim()
                val strEmail = etEmailId.text.toString().trim()
                val strPass = etPassword.text.toString().trim()

                val validations = MyValidations(this)

                if (strFName.isEmpty()) {
                    showValidationError("Please enter your first name")
                    //mySnackBar?.showSnackBarError("Please enter your first name")
                    return@subscribe
                }

                if (!validations.checkName(strFName)) {
                    showValidationError("Please enter a valid first name\n(Only alphabets are allowed)")
                    //mySnackBar?.showSnackBarError("Please enter a valid first name\n(Only alphabets are allowed)")
                    return@subscribe
                }

                if (strLName.isEmpty()) {
                    showValidationError("Please enter your last name")
                    //mySnackBar?.showSnackBarError("Please enter your last name")
                    return@subscribe
                }

                if (!validations.checkName(strLName)) {
                    showValidationError("Please enter a valid last name\n(Only alphabets are allowed)")
                    //mySnackBar?.showSnackBarError("Please enter a valid last name\n(Only alphabets are allowed)")
                    return@subscribe
                }

                if (strEmail.isEmpty()) {
                    showValidationError("Please enter your Email ID")
                    //mySnackBar?.showSnackBarError("Please enter your email address")
                    return@subscribe
                }

                if (!validations.checkEmail(strEmail)) {
                    showValidationError("Please enter a valid Email ID")
                    //mySnackBar?.showSnackBarError("Please enter a valid email address")
                    return@subscribe
                }

                if (strPass.isEmpty()) {
                    showValidationError("Please enter your password")
                    //mySnackBar?.showSnackBarError("Please enter your password")
                    return@subscribe
                }

                if (strPass.length < 6) {
                    showValidationError("Please enter a valid password\n(Must be at least 6 characters in length)")
                    //mySnackBar?.showSnackBarError("Please enter a valid password\n(Should be at least 6 characters in length)")
                    return@subscribe
                }

                //API Params filling
                val hashParams = HashMap<String, String>()
                hashParams.put("firstName", strFName.trim())
                hashParams.put("lastName", strLName.trim())
                hashParams.put("emailId", strEmail.trim())
                hashParams.put("password", strPass.trim())


                fastRegisterOrLoginUser(Urls.REGISTER_USER, hashParams)
                //saveUserAndProceed()


            })

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        FastNetworking.compositeDisposable.clear()
        super.onDestroy()
    }

    private fun setPasswordToggle(imgPass: ImageView, etPass: AppCompatEditText) {
        isPasswordShowing = !isPasswordShowing

        if (isPasswordShowing) {
            imgPass.imageResource = R.drawable.ic_password_not_showing
            etPass.transformationMethod = DoNothingTransformationMethod()
        } else {
            imgPass.imageResource = R.drawable.ic_password_showing
            etPass.transformationMethod = PasswordTransformationMethod()
        }

        if (etPass.text.toString().isNotEmpty()) {
            etPass.setSelection(etPass.text.toString().length)
        }
    }


    //pass respective url and hashparams in the function for register or login
    private fun fastRegisterOrLoginUser(url: String, hashParams: HashMap<String, String>) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            DialogMsg.showPleaseWait(this@LoginRegisterActivity)
            FastNetworking.makeRxCallPost(applicationContext, url, false, hashParams, if (url == Urls.LOGIN_USER) "Login" else "Registration", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(this@LoginRegisterActivity)
                    parseUserResponse(json, hashParams)
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(this@LoginRegisterActivity)
                    dialogMsg?.showErrorApiDialog(this@LoginRegisterActivity, onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@LoginRegisterActivity) })
                }

            })
        } else {
            dialogMsg?.showErrorConnectionDialog(this@LoginRegisterActivity, onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@LoginRegisterActivity) })
        }
    }


    //for both login and registration
    private fun parseUserResponse(json: JSONObject?, hashParams: HashMap<String, String>) {
        try {
            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                MyGoogleAnalytics.sendCustomEvent(this, MyGoogleAnalytics.CATEGORY_LOGIN_METHOD, if (flagLoginRegister == 1) MyGoogleAnalytics.ACTION_NORMAL_LOGIN else MyGoogleAnalytics.ACTION_NORMAL_REGISTER, hashParams["emailId"]!!)

                if (flagLoginRegister == 1) {
                    val jsonData = json.getJSONObject("data")
                    val isEmailVerified = jsonData.optString("is_email_verified", "0") == "1"
                    if (isEmailVerified) {
                        UserModel.parseUserJson(applicationContext, json, { userModel ->
                            if (userModel.isUserActive) {
                                MyNavigations.goToDashboard(this)
                                finishAffinity()
                            } else {
                                dialogMsg?.showErrorRetryDialog(this@LoginRegisterActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, "Your account is de-activated, please contact admin", "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@LoginRegisterActivity) })
                            }
                        }, { exc ->
                            Log.d("ExceptionUserParse", "" + exc)
                            showParseError()
                        })
                    } else {
                        //if user email is not verified, then go to verification screen
                        goToVerifyCodeScreen(json, hashParams)
                    }
                } else {
                    // if registration successful, go to to verification code screen
                    if (json.has("otp")) {
                        goToVerifyCodeScreen(json, hashParams)
                    } else {
                        toast("Verification Code Error")
                    }
                }
            } else {
                //error
                var strErrMsg = ErrorMsgs.ERR_API_MSG
                if (json.has("message"))
                    strErrMsg = json.getString("message")

                dialogMsg?.showErrorRetryDialog(this@LoginRegisterActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, strErrMsg, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@LoginRegisterActivity) })
            }
        } catch (e: Exception) {
            Log.d("ParseExc", "" + e)
            showParseError()
        }
    }

    private fun goToVerifyCodeScreen(json: JSONObject, hashParams: HashMap<String, String>) {
        try {
            if (json.has("otp")) {
                val strOtp = json.getString("otp")
                MyNavigations.goToEnterOtp(this, strOtp, hashParams["emailId"]!!, true, json = json.toString())
            } else {
                toast("Verification Code Error")
            }
        } catch (e: Exception) {
            Log.d("ExcGoToVerifyScreen", "" + e)
            toast("Parse Error")
        }
    }

    private fun showParseError() {
        dialogMsg?.showErrorRetryDialog(this@LoginRegisterActivity, ErrorMsgs.ERR_PARSE_TITLE, ErrorMsgs.ERR_API_MSG, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@LoginRegisterActivity) })
    }

    private fun showValidationError(strErrMsg: String = "") {
        dialogMsg?.showErrorRetryDialog(this@LoginRegisterActivity, "Alert", strErrMsg, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@LoginRegisterActivity) })
    }


}

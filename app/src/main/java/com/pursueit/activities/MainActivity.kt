package com.pursueit.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.BuildConfig
import com.pursueit.MyApp
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PlaceModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_circle_progress.*
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {


    var compositeDisposable = CompositeDisposable()

    private lateinit var mGoogleApiClient: GoogleApiClient
    private val RC_GOOGLE_SIGN_IN = 7
    private val TAG = "GoogleSignIn"


    private lateinit var fbCallbackManager: CallbackManager

    var dialogMsg: DialogMsg? = null

    companion object {

        //fo main page and register page, terms & conditions and privacy policy redirections
        fun customTextViewLinks(act:Activity,view: TextView) {
            val strTerms="Terms and Conditions"
            val strPrivacyPolicy="Privacy Policy"
            val spanTxt = SpannableStringBuilder(
                "By signing up you agree to ")
            spanTxt.append(strTerms)
            spanTxt.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    MyNavigations.goToWebView(act,strTerms,MyAppConfig.URL_TERMS)
                }
            }, spanTxt.length - strTerms.length, spanTxt.length, 0)
            spanTxt.append(" & $strPrivacyPolicy")
            spanTxt.setSpan(ForegroundColorSpan(Color.BLACK), spanTxt.indexOf(strPrivacyPolicy), spanTxt.length, 0)
            spanTxt.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    MyNavigations.goToWebView(act,strPrivacyPolicy,MyAppConfig.URL_PRIVACY_POLICY)
                }
            }, spanTxt.length - (" $strPrivacyPolicy").length, spanTxt.length, 0)

            spanTxt.append(" of Pursueit.")

            view.setLinkTextColor(Color.WHITE)
            view.movementMethod = LinkMovementMethod.getInstance()
            view.setText(spanTxt, TextView.BufferType.SPANNABLE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

        fbCallbackManager = CallbackManager.Factory.create()

        compositeDisposable = CompositeDisposable()

        AndroidNetworking.initialize(applicationContext)

        //reset things
        MyPlaceUtils.placeModel= PlaceModel()
        DashboardActivity.ageFrom=""
        DashboardActivity.ageTo=""
        DashboardActivity.selectedSearchModel=null

        init()

        dialogMsg = DialogMsg()


        googleSignInPreWork()
        fbLoginPreWork()

        onClickListeners()

        decideIfUserAlreadyLoggedIn()

        customTextViewLinks(this,txtTermsPrivacyPolicy)

    }

    private fun init() {
        Glide.with(applicationContext).load(R.drawable.bg_main).into(imgBg)
        Glide.with(applicationContext).load(R.drawable.ic_logo).into(imgLogo)
    }

    private fun fbLoginPreWork() {
        LoginManager.getInstance().registerCallback(fbCallbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    // App code
                    Log.d("AccessTokenRec", "" + loginResult)
                    loginFacebookNow(loginResult.accessToken)

                }

                override fun onCancel() {
                    // App code
                    Log.d("Cancelled", "User Cancelled")
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Log.d("FbException", "" + exception)
                }
            })
    }

    private fun loginFacebookNow(accessToken: AccessToken?) {
        val context = this
        if (accessToken != null) {
            val request = GraphRequest.newMeRequest(
                accessToken
            ) { `object`, response ->
                Log.v("LoginActivity", response.toString())

                // Application code
                try {


                    Log.d("FbJsonResponse", "" + `object`)
                    val strId = `object`.getString("id")
                    val strName = `object`.getString("name")

                    var strEmail = ""
                    if (`object`.has("email")) {
                        if (`object`.getString("email").trim().isNotEmpty())
                            strEmail = `object`.getString("email")
                    }

                    val photoUrl = "http://graph.facebook.com/$strId/picture?type=large"
                    /*val userModel = getUserModel(strName, strEmail, "", 3, strId, photoUrl)
                    fastLogin(userModel)*/

                    if(strEmail.isEmpty()){
                        dialogMsg?.showErrorRetryDialog(this,"Alert","Please allow access to your Email-Id.\nPursueit cannot proceed further without your Email-Id","OK",View.OnClickListener {
                            dialogMsg?.dismissDialog(this)
                        },false)

                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut()
                        }

                        return@newMeRequest
                    }

                    val hashParams = HashMap<String, String>()
                    hashParams["customerRegType"] = "facebook"
                    hashParams["uuidToken"] = MyApp.ANDROID_ID
                    hashParams["socialId"] = strId
                    hashParams["emailId"] = "" + strEmail
                    hashParams["imageUrl"] = photoUrl

                    val arrName = strName.split(" ")
                    if (arrName.isNotEmpty())
                        hashParams["firstName"] = arrName[0]

                    hashParams["lastName"] = if (arrName.size > 1) arrName[1] else ""

                    fastLoginUser(hashParams)

                } catch (e: Exception) {
                    Log.d("Exception_Graph_Fb", "" + e)
                    Toast.makeText(context, "Some error occurred", Toast.LENGTH_SHORT).show()
                }

                cardProgress.visibility = View.GONE
            }
            val parameters = Bundle()
            parameters.putString("fields", "id,name,email")
            request.parameters = parameters
            request.executeAsync()
        }
    }

    private fun googleSignInPreWork() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(BuildConfig.GOOGLE_OAUTH_SERVER_TOKEN)
            .requestEmail()
            .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()

    }

    override fun onConnectionFailed(conResult: ConnectionResult) {
        Log.d("ConnectionFailed", "" + conResult)
    }

    private fun revokeAccess() {
        try {
            Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback {
                //updateUI(false);
            }
        } catch (e: Exception) {
            Log.d("RevokeAccessExc", "" + e)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fbCallbackManager.onActivityResult(requestCode, resultCode, data)

        cardProgress.visibility = View.GONE
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            if (data != null) {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                handleGoogleSignInResult(result)
            } else {
                toast("Please try again with google sign in")
            }
        }
    }

    private fun handleGoogleSignInResult(result: GoogleSignInResult?) {
        try {
            Log.d(TAG, "GoogleSignInResult:" + result?.isSuccess + "\n" + result?.status + "\n" + result?.signInAccount)
            if (result != null) {
                if (result.isSuccess) {
                    // Signed in successfully, show authenticated UI.
                    val acct = result.signInAccount

                    Log.v(TAG, "display name: " + acct!!.displayName!!)

                    val personName = acct.displayName
                    val email = acct.email
                    val id = "" + acct.id

                    var fName = ""
                    var lName = ""

                    try {
                        if (personName!!.contains(" ")) {
                            fName = personName.split(" ")[0]
                            lName = personName.split(" ")[1]
                        }
                    } catch (e: Exception) {

                    }


                    Log.v(TAG, "Name: $personName, email: $email")

                    var personPhotoUrl = ""

                    try {
                        personPhotoUrl = acct.photoUrl!!.toString()

                        Log.e(TAG, "Image: $personPhotoUrl")
                    } catch (e: Exception) {
                        Log.v("NullPointerImage", "" + e)
                    }


                    /* val userModel = getUserModel(personName!!, email!!, "", 2, userSocialId = id, userPhotoUrl = personPhotoUrl)

                     fastLogin(userModel)*/

                    val hashParams = HashMap<String, String>()
                    hashParams.put("customerRegType", "google")
                    hashParams.put("uuidToken", MyApp.ANDROID_ID)
                    hashParams.put("socialId", id)
                    hashParams.put("emailId", "" + email)
                    hashParams.put("firstName", fName)
                    hashParams.put("lastName", lName)
                    hashParams.put("imageUrl", personPhotoUrl)

                    fastLoginUser(hashParams)

                    revokeAccess()


                } else {
                    // Signed out, show unauthenticated UI.
                    //updateUI(false);
                }

            }
        } catch (e: Exception) {
            Log.d("ExcHandleSignIn", "" + e)
        }
    }

    private fun onClickListeners() {

        compositeDisposable.add(RxView.clicks(btnRegister).throttleFirst(500, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
            MyNavigations.goToRegistration(this)
        })

        compositeDisposable.add(RxView.clicks(btnLogin).throttleFirst(500, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
            MyNavigations.goToNormalLogin(this)
        })

        compositeDisposable.add(RxView.clicks(linSkip).throttleFirst(500, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
            val bundle=intent.extras
            MyNavigations.goToDashboard(this,bundle)
            finish()
        })

        compositeDisposable.add(RxView.clicks(linGoogle).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
            if (!ConnectionDetector.isConnectingToInternet(this)) {
                showConnectionError()
                return@subscribe
            }

            cardProgress.visibility = View.VISIBLE
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
        })

        compositeDisposable.add(RxView.clicks(linFacebook).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
            if (!ConnectionDetector.isConnectingToInternet(this)) {
                showConnectionError()
                return@subscribe
            }

            try {
                cardProgress.visibility = View.GONE
                val accessToken = AccessToken.getCurrentAccessToken()
                val isLoggedIn = accessToken != null && !accessToken.isExpired
                if (isLoggedIn) {
                    loginFacebookNow(accessToken)
                } else {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email,public_profile"))
                }
            } catch (e: Exception) {
                dialogMsg?.showErrorRetryDialog(this,"Oops!",ErrorMsgs.ERR_API_MSG,"OK",View.OnClickListener {
                    dialogMsg?.dismissDialog(this)
                },true)
            }
        })
    }

    private fun showConnectionError(){
        dialogMsg?.showErrorConnectionDialog(this,"OK",View.OnClickListener {
            dialogMsg?.dismissDialog(this)
        },true)
    }

    private fun decideIfUserAlreadyLoggedIn() {
        try {
            val userModel = UserModel.getUserModel(applicationContext)
            Log.d("UserModel_", "" + userModel.toString())
            if (userModel.userId.trim().isNotEmpty()) {
                MyNavigations.goToDashboard(this, intent.extras)
                finish()
            }
        } catch (e: Exception) {
            Log.d("LoggedInUserExc", "" + e)
        }

    }

    override fun onResume() {
        super.onResume()
        val userModel = UserModel.getUserModel(applicationContext)
        Log.d("UserModel_", "" + userModel.toString())
        if (userModel.userId.trim().isEmpty()) {
            MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.MAIN_SCREEN,"",this::class.java)
        }
    }

    //pass hashparams in the function for register or login
    private fun fastLoginUser(hashParams: HashMap<String, String>) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            DialogMsg.showPleaseWait(this@MainActivity)
            FastNetworking.makeRxCallPost(applicationContext, Urls.LOGIN_USER_SOCIAL, false, hashParams, "LoginSocial", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(this@MainActivity)
                    try {
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            val strLoginType = json.getString("login_type")
                            val strMsg = json.optString("message", "Please login with your email and password, as you have already registered with ${getString(R.string.app_name)}.")
                            if (strLoginType.equals("normal", true)) {
                                dialogMsg?.showErrorRetryDialog(this@MainActivity, "Alert", strMsg, "OK", View.OnClickListener {
                                    dialogMsg?.dismissDialog(this@MainActivity)
                                    MyNavigations.goToNormalLogin(this@MainActivity)
                                }, isCancellable = false)
                            } else {
                                if (hashParams["customerRegType"].toString().trim().equals(strLoginType.trim(),true)) {
                                    parseUserResponse(json)
                                    MyGoogleAnalytics.sendCustomEvent(this@MainActivity,MyGoogleAnalytics.CATEGORY_LOGIN_METHOD,hashParams["customerRegType"].toString().trim(),if(hashParams["emailId"]!=null) hashParams["emailId"]!! else "")
                                }else{
                                    dialogMsg?.showErrorRetryDialog(this@MainActivity, "Alert", strMsg, "OK", View.OnClickListener {
                                        dialogMsg?.dismissDialog(this@MainActivity)
                                    }, isCancellable = false)
                                }
                            }

                        } else {
                            dialogMsg?.showErrorApiDialog(this@MainActivity, onRetryListener = View.OnClickListener {
                                dialogMsg?.dismissDialog(this@MainActivity)
                            })
                        }
                    } catch (e: Exception) {
                        Log.d("Exc_SocialLogin", "" + e)
                    }

                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(this@MainActivity)
                    dialogMsg?.showErrorApiDialog(this@MainActivity, onRetryListener = View.OnClickListener {
                        dialogMsg?.dismissDialog(this@MainActivity)
                    })
                }

            })
        } else {
            dialogMsg?.showErrorConnectionDialog(this@MainActivity, onRetryListener = View.OnClickListener {
                dialogMsg?.dismissDialog(this@MainActivity)
            })
        }
    }

    private fun parseUserResponse(json: JSONObject?) {
        try {
            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                UserModel.parseUserJson(applicationContext, json, { userModel ->
                    if (userModel.isUserActive) {
                        MyNavigations.goToDashboard(this)
                        finishAffinity()
                    } else {
                        dialogMsg?.showErrorRetryDialog(this@MainActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, "Your account is de-activated, please contact admin", "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@MainActivity) })
                    }
                }, { exc ->
                    Log.d("ExceptionUserParse", "" + exc)
                    showParseError()
                })
            } else {
                //error
                var strErrMsg = ErrorMsgs.ERR_API_MSG
                if (json.has("message"))
                    strErrMsg = json.getString("message")

                dialogMsg?.showErrorRetryDialog(this@MainActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, strErrMsg, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@MainActivity) })
            }
        } catch (e: Exception) {
            Log.d("ParseExc", "" + e)
            showParseError()
        }
    }

    private fun showParseError() {
        dialogMsg?.showErrorRetryDialog(this@MainActivity, ErrorMsgs.ERR_PARSE_TITLE, ErrorMsgs.ERR_API_MSG, "OK", View.OnClickListener { dialogMsg?.dismissDialog(this@MainActivity) })
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }


}

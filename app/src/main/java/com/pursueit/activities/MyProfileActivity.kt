package com.pursueit.activities

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.jakewharton.rxbinding2.view.RxView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pursueit.R
import com.pursueit.adapters.FamilyMemberAdapter
import com.pursueit.adapters.MyAddressSwipeAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PlaceModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import id.zelory.compressor.Compressor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_my_profile.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.dialog_camera_gallery.*
import kotlinx.android.synthetic.main.dialog_change_pass.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.util.concurrent.TimeUnit

class MyProfileActivity : AppCompatActivity() {

    private lateinit var userModel: UserModel

    private lateinit var compositeDisposable: CompositeDisposable

    private lateinit var mySnackBar: MySnackBar

    private var familyAdapter: FamilyMemberAdapter? = null
    private var addressAdapter: MyAddressSwipeAdapter? = null

    private val AUTO_COMPLETE_REQ_CODE = 50

    lateinit var dialogMsg: DialogMsg

    private var profileImagePicked: File? = null

    val FOLDER_IMAGES = "pursueit"
    val CAMERA_CODE = 9068
    val GALLERY_CODE = 4972

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)

        dialogMsg = DialogMsg()

        mySnackBar = MySnackBar(this, relParentProfile)

        compositeDisposable = CompositeDisposable()

        userModel = UserModel.getUserModel(applicationContext)

        init()

        setUserDetailsUi()

        onClickListeners()

        fastFetchFamilyMembers()

        fastFetchMyAddresses()

        fastRefreshViewMyProfile()

       // fetchSchoolAndInterest()
    }



    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this,MyGoogleAnalytics.MY_PROFILE,userModel.userEmail,this::class.java)
    }

    override fun onDestroy() {
        compositeDisposable.clear()

        super.onDestroy()
    }


    private fun init() {
        toolbarFragment.txtToolbarHeader.text = "Profile Settings"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

        toolbarFragment.txtSaveProfile.visibility = View.VISIBLE

        Glide.with(this).load(R.drawable.ic_bg_header_profile_more).into(imgBgProfileHeader)

        rvFamilyMembers.layoutManager = LinearLayoutManager(this)
        rvFamilyMembers.setHasFixedSize(true)

        rvAddresses.layoutManager = LinearLayoutManager(this)
        rvAddresses.setHasFixedSize(true)
    }

    private fun setUserDetailsUi() {
        try {
            if (MyProfileUtils.isNotNull(userModel.userFNAme.trim()))
                etFirstName.setText(userModel.userFNAme.trim())
            if (MyProfileUtils.isNotNull(userModel.userLName.trim()))
                etLastName.setText(userModel.userLName.trim())
            if (MyProfileUtils.isNotNull(userModel.userPhone.trim()))
                etPhoneNumber.setText(userModel.userPhone.trim())

            if (MyProfileUtils.isNotNull(userModel.userEmail.trim()))
                etEmail.setText(userModel.userEmail.trim())

            MyProfileUtils.setSelectionEnd(etFirstName)
            MyProfileUtils.setSelectionEnd(etLastName)
            MyProfileUtils.setSelectionEnd(etPhoneNumber)


            if (userModel.userImage.trim().length > 5) {
                Glide.with(applicationContext).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true)).load(Urls.IMAGE_USER + userModel.userImage).into(imgUserProfile)
            } else if (userModel.userSocialImageUrl.trim().length > 5) {
                Glide.with(applicationContext).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true)).load(userModel.userSocialImageUrl).into(imgUserProfile)
            } else {
                Glide.with(applicationContext).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true)).load(R.drawable.ic_placeholder_square).into(imgUserProfile)
            }

            txtUserName.text = (etFirstName.text.toString().trim() + " " + etLastName.text.toString().trim()).trim()

            Log.d("WalletBal",""+userModel.currentPassWallet)

            if (userModel.currentPassWallet.trim()!="" && userModel.currentPassWallet.trim()!="null")
                txtPassBalance.text="${StaticValues.PASS_WALLET_BALANCE}: AED ${userModel.currentPassWallet}"+StaticValues.getSuffixValidTill(userModel)
            else
                txtPassBalance.text="${StaticValues.PASS_WALLET_BALANCE}: AED 0"

            relChangePass.visibility = if (userModel.userHasPassword) View.VISIBLE else View.GONE

        } catch (e: Exception) {
            Log.d("UserDetailsUi", "" + e)
        }
    }



    private fun onClickListeners() {

        compositeDisposable.add(RxView.clicks(toolbarFragment.txtSaveProfile).throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
            val phone=etPhoneNumber.text.toString().trim()

            if ((!userModel.userPhoneVerified || userModel.userPhone!=phone) && phone!="")
            {
                val strFName = etFirstName.text.toString().trim()
                val strLName = etLastName.text.toString().trim()
                val strPhone = etPhoneNumber.text.toString().trim()

                if (strFName.isEmpty()) {
                    showError(strMsg = "Please enter your first name")
                    return@subscribe
                }

                if (strLName.isEmpty()) {
                    showError(strMsg = "Please enter your last name")
                    return@subscribe
                }

                if(strPhone.isEmpty()){
                    showError(strMsg = "Please enter your phone number")
                    return@subscribe
                }

                if (strPhone.isNotEmpty()) {
                    if (!MyValidations(this).checkMobile(strPhone)) {
                        showError(strMsg = "Please enter a valid phone number")
                        return@subscribe
                    }
                    else{
                        SendPhoneOtp(this).sendOtp(strPhone) { otp->
                            MyNavigations.goToEnterOtpForPhone(this,otp,strPhone) }
                        return@subscribe
                    }
                }

                fastUpdateProfile()
            }
            else {
                fastUpdateProfile()
            }
        })

        compositeDisposable.add(RxView.clicks(relProfileUserImage).throttleFirst(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                            if (report != null) {
                                if (report.areAllPermissionsGranted()) {
                                    pickImage()
                                } else {
                                    toast("Please allow camera and storage permissions to upload picture")
                                }
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                            token?.continuePermissionRequest()
                        }

                    })
                    .check()
            })

        compositeDisposable.add(RxView.clicks(imgAddMember).throttleFirst(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                MyProfileUtils.showAddMemberDialog(this, this@MyProfileActivity::fastFetchFamilyMembers)
            })

        compositeDisposable.add(RxView.clicks(relChangePass).throttleFirst(1000, TimeUnit.MILLISECONDS)
            .subscribe {
                val dialog = MyDialog(this).getMyDialog(R.layout.dialog_change_pass)

                dialog.relOldPass.visibility = if (userModel.userHasPassword) View.VISIBLE else View.GONE

                dialog.etOldPass.transformationMethod = PasswordTransformationMethod()
                dialog.etNewPass.transformationMethod = PasswordTransformationMethod()
                dialog.etConfPass.transformationMethod = PasswordTransformationMethod()

                dialog.imgCloseDialogChangePass.setOnClickListener {
                    dialog.dismiss()
                }

                var isPassShowingOld = false
                var isPassShowingNew = false
                var isPassShowingConf = false

                dialog.imgOldPass.setOnClickListener {
                    isPassShowingOld = !isPassShowingOld
                    MyProfileUtils.setPasswordToggle(dialog.imgOldPass, dialog.etOldPass, isPassShowingOld)
                }

                dialog.imgNewPass.setOnClickListener {
                    isPassShowingNew = !isPassShowingNew
                    MyProfileUtils.setPasswordToggle(dialog.imgNewPass, dialog.etNewPass, isPassShowingNew)
                }

                dialog.imgConfPass.setOnClickListener {
                    isPassShowingConf = !isPassShowingConf
                    MyProfileUtils.setPasswordToggle(dialog.imgConfPass, dialog.etConfPass, isPassShowingConf)
                }

                dialog.btnUpdatePass.setOnClickListener {
                    val strOldPass = dialog.etOldPass.text.toString().trim()
                    val strNewPass = dialog.etNewPass.text.toString().trim()
                    val strConfPass = dialog.etConfPass.text.toString().trim()

                    if (!ConnectionDetector.isConnectingToInternet(applicationContext)) {
                        dialogMsg.showErrorConnectionDialog(this, "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                        return@setOnClickListener
                    }

                    if (userModel.userHasPassword) {
                        if (strOldPass.isEmpty()) {
                            dialogMsg.showErrorRetryDialog(this, "Alert", "Please enter your old password", "OK", View.OnClickListener {
                                dialogMsg.dismissDialog(this)
                            })
                            return@setOnClickListener
                        }
                        if (strOldPass.length < 6) {
                            dialogMsg.showErrorRetryDialog(this, "Alert", "Password must be at least 6 characters long", "OK", View.OnClickListener {
                                dialogMsg.dismissDialog(this)
                            })
                            return@setOnClickListener
                        }
                    }

                    if (strNewPass.isEmpty()) {
                        dialogMsg.showErrorRetryDialog(this, "Alert", "Please enter your new password", "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                        return@setOnClickListener
                    }
                    if (strNewPass.length < 6) {
                        dialogMsg.showErrorRetryDialog(this, "Alert", "New Password must be at least 6 characters long", "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                        return@setOnClickListener
                    }

                    if (strOldPass == strNewPass && strOldPass.isNotEmpty()) {
                        dialogMsg.showErrorRetryDialog(this, "Alert", "New Password must be different from the old one", "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                        return@setOnClickListener
                    }

                    if (strConfPass.isEmpty()) {
                        dialogMsg.showErrorRetryDialog(this, "Alert", "Please re-type your new password", "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                        return@setOnClickListener
                    }

                    if (strNewPass != strConfPass) {
                        dialogMsg.showErrorRetryDialog(this, "Alert", "Both passwords do not match", "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                        return@setOnClickListener
                    }



                    fastChangePassword(if (userModel.userHasPassword) strOldPass else "", strNewPass, dialog)

                }

                dialog.show()
            })

    }

    private fun fastFetchFamilyMembers() {
        try {
            if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                progressFamilyMembers.visibility = View.VISIBLE
                MyProfileUtils.fastFetchFamilyMembers(applicationContext, { success: Boolean, jsonObject: JSONObject?, anError: ANError? ->
                    progressFamilyMembers.visibility = View.GONE
                    fillFamilyMembersUi()
                }, compositeDisposable)
            } else {
                mySnackBar.showSnackBarError(ErrorMsgs.ERR_CONNECTION_MSG)
            }
        } catch (e: Exception) {
            Log.d("Exc_FetchFamily", "" + e)
        }
    }

    private fun fillFamilyMembersUi() {
        if (familyAdapter == null) {
            familyAdapter = FamilyMemberAdapter(this, MyProfileUtils.arrayFamilyModel, { pos ->
                MyProfileUtils.showAddMemberDialog(this, this@MyProfileActivity::fastFetchFamilyMembers, MyProfileUtils.arrayFamilyModel[pos],pos = pos,recyclerView = rvFamilyMembers)
            }, { pos ->
                dialogMsg.showYesCancelDialog(this, "Are you sure you want to remove this family member?", View.OnClickListener {
                    dialogMsg.dismissDialog(this)
                    if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                        MyProfileUtils.fastDeleteAddressOrFamilyMember(this, MyProfileUtils.arrayFamilyModel[pos].familyId, 2, {
                            try {
                                MyProfileUtils.arrayFamilyModel.removeAt(pos)
                                rvFamilyMembers.adapter?.notifyItemRemoved(pos)
                                rvFamilyMembers.adapter?.notifyItemRangeChanged(pos, MyProfileUtils.arrayFamilyModel.size)
                            } catch (e: Exception) {

                            }
                        }, {
                            dialogMsg.showErrorApiDialog(this, "OK", View.OnClickListener {
                                dialogMsg.dismissDialog(this)
                            })
                        })
                    } else {
                        dialogMsg.showErrorRetryDialog(this, "Oops!", ErrorMsgs.ERR_CONNECTION_MSG, "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                        })
                    }
                })
            })
            rvFamilyMembers.adapter = familyAdapter
        } else {
            familyAdapter?.notifyDataSetChanged()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == AUTO_COMPLETE_REQ_CODE) {
                when (resultCode) {
                    AutocompleteActivity.RESULT_OK -> {

                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.d("PlaceId", "" + place.id)
                        Log.d("PlaceName", "" + place.name)
                        Log.d("PlaceAddress", "" + place.address)
                        Log.d("PlaceLatLng", "" + place.latLng?.latitude + " - " + place.latLng?.longitude)

                        startFetchingCityIdNow(place)
                    }

                    AutocompleteActivity.RESULT_ERROR -> {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("PlaceError", "" + status)
                        dialogMsg.showErrorRetryDialog(this, "Oops!", ErrorMsgs.ERR_API_MSG, "OK", View.OnClickListener { dialogMsg.dismissDialog(this@MyProfileActivity) })
                    }
                }
            }
            else if(requestCode==MyNavigations.RQ_SEND_SMS){
                val isPhoneVerified=data.getBooleanExtra("isPhoneVerified",false)

                fastUpdateProfile(isPhoneVerified)
            }
        }

        val dialogErr = DialogMsg()

        when (requestCode) {

            CAMERA_CODE, GALLERY_CODE -> {
                EasyImage.handleActivityResult(requestCode, resultCode, data, this, object :
                    DefaultCallback() {
                    /*fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                        Log.d("ImageFile", "" + imageFile + "\n" + imageFile?.absolutePath)

                    }*/

                    override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                        //Some error handling
                        dialogErr.showErrorRetryDialog(this@MyProfileActivity, "Unknown Error", "Some error occurred, please make sure you have sufficient storage and permission", "OK", View.OnClickListener {
                            dialogErr.dismissDialog(this@MyProfileActivity)
                        })
                    }


                    override fun onImagesPicked(imagesFiles: List<File>, source: EasyImage.ImageSource, type: Int) {
                        if (imagesFiles.isNotEmpty()) {
                            Log.d("AbsolutePathImage", imagesFiles[0].absolutePath)
                            compressImageAndUpload(imagesFiles[0])
                        }
                    }

                    override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                        super.onCanceled(source, type)
                        // Cancel handling, you might wanna remove taken photo if it was canceled
                        try {
                            if (source == EasyImage.ImageSource.CAMERA_IMAGE) {
                                val photoFile = EasyImage.lastlyTakenButCanceledPhoto(this@MyProfileActivity)
                                photoFile?.delete()
                            }
                        } catch (e: Exception) {
                            Log.d("Exc_Delete_Camera", "" + e)
                        }
                    }

                })
            }

        }
    }

    private fun startFetchingCityIdNow(place: Place) {
        DialogMsg.showPleaseWait(this)

        val myPlace = MyPlaceUtils(this, object : MyPlaceUtils.OnCityFetchedListener {
            override fun onCityFetched(strCityPlaceId: String) {
                DialogMsg.dismissPleaseWait(this@MyProfileActivity)
                Log.d("AddressPlaceToAdd", place.name + " - " + place.address + " - " + place.id + "\nCityId: " + strCityPlaceId)
                Log.d("MyPlaceAlreadySelected", MyPlaceUtils.placeModel.toString())


                val placeModel = PlaceModel(place.id ?: "", place.name ?: "", place.address ?: "", place.latLng?.latitude ?: 0.0, place.latLng?.longitude ?: 0.0).also {
                    it.placeCityId = strCityPlaceId
                }

                MyProfileUtils.fastAddAddressNow(this@MyProfileActivity, placeModel, {
                    fastFetchMyAddresses()
                }, compositeDisposable)
            }

            override fun onCityError() {
                DialogMsg.dismissPleaseWait(this@MyProfileActivity)
                toast(ErrorMsgs.ERR_API_MSG)
            }

        })

        myPlace.setAsCurrent = false

        try {
            if (place.latLng != null) {
                myPlace.initCurrentCityCallGeocodeApi(place.latLng!!.latitude, place.latLng!!.longitude,overwritePlaceAddress = false)
            }
        } catch (e: Exception) {
            Log.d("PlaceException", "" + e)
        }
    }


    private fun fastFetchMyAddresses() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            progressAddress.visibility = View.VISIBLE
            MyProfileUtils.fastFetchMyAddresses(this, {
                progressAddress.visibility = View.GONE
                if (addressAdapter == null) {
                    addressAdapter = MyAddressSwipeAdapter(this, MyProfileUtils.arrayMyAddressModel) { pos ->

                        dialogMsg.showYesCancelDialog(this, "Are you sure you want to remove this address from your address book?", View.OnClickListener {
                            dialogMsg.dismissDialog(this)
                            /*toast("AddressId -> ${MyProfileUtils.arrayMyAddressModel[pos].placeAddressIdDb}")*/
                            if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                                MyProfileUtils.fastDeleteAddressOrFamilyMember(this, MyProfileUtils.arrayMyAddressModel[pos].placeAddressIdDb, 1, {

                                    try {
                                        MyProfileUtils.arrayMyAddressModel.removeAt(pos)
                                        rvAddresses.adapter?.notifyItemRemoved(pos)
                                        rvAddresses.adapter?.notifyItemRangeChanged(pos, MyProfileUtils.arrayMyAddressModel.size)
                                    } catch (e: Exception) {

                                    }
                                }, {
                                    dialogMsg.showErrorApiDialog(this, "OK", View.OnClickListener {
                                        dialogMsg.dismissDialog(this)
                                    })
                                })
                            } else {
                                dialogMsg.showErrorRetryDialog(this, "Oops!", ErrorMsgs.ERR_CONNECTION_MSG, "OK", View.OnClickListener {
                                    dialogMsg.dismissDialog(this)
                                })
                            }
                        })
                    }
                    rvAddresses.adapter = addressAdapter
                } else {
                    addressAdapter?.notifyDataSetChanged()
                }

                makeAddAddressClickEnable()
            }, {
                progressAddress.visibility = View.GONE
                makeAddAddressClickEnable()
            }, compositeDisposable)
        }
    }

    private fun makeAddAddressClickEnable() {
        compositeDisposable.add(RxView.clicks(imgAddAddress).throttleFirst(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                if (MyProfileUtils.arrayMyAddressModel.size < 5) {
                    val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, MyPlaceUtils.placeFields).setCountry("AE").build(this)
                    startActivityForResult(intent, AUTO_COMPLETE_REQ_CODE)
                } else {
                    dialogMsg.showErrorRetryDialog(this, "Alert", "You cannot add more than 5 addresses in your address book", "OK", View.OnClickListener { dialogMsg.dismissDialog(this) })
                }
            })
    }


    private fun fastUpdateProfile(isPhoneVerified:Boolean=userModel.isPhoneVerified) {
        val strFName = etFirstName.text.toString().trim()
        val strLName = etLastName.text.toString().trim()
        val strPhone = etPhoneNumber.text.toString().trim()

        if (strFName.isEmpty()) {
            showError(strMsg = "Please enter your first name")
            return
        }

        if (strLName.isEmpty()) {
            showError(strMsg = "Please enter your last name")
            return
        }

        if(strPhone.isEmpty()){
            showError(strMsg = "Please enter your phone number")
            return
        }

        if (strPhone.isNotEmpty()) {
            if (!MyValidations(this).checkMobile(strPhone)) {
                showError(strMsg = "Please enter a valid phone number")
                return
            }
        }

        var isProfileChanged = false
        if (userModel.userFNAme.trim() != strFName && strFName.isNotEmpty()) {
            isProfileChanged = true
            userModel.userFNAme = strFName
        }

        if (userModel.userLName.trim() != strLName && strLName.isNotEmpty()) {
            isProfileChanged = true
            userModel.userLName = strLName
        }

        if (userModel.userPhone.trim() != strPhone) {
            isProfileChanged = true
            userModel.userPhone = strPhone
        }

        if (profileImagePicked != null) {
            isProfileChanged = true
        }


        if (isProfileChanged && ConnectionDetector.isConnectingToInternet(applicationContext)) {
            MyProfileUtils.fastEditProfile(this@MyProfileActivity, userModel, profileImagePicked,compositeDisposable,isPhoneVerified)
        } else if (isProfileChanged) {
            dialogMsg.showErrorRetryDialog(this@MyProfileActivity, "Alert", ErrorMsgs.ERR_CONNECTION_MSG, "Retry", View.OnClickListener {
                dialogMsg.dismissDialog(this@MyProfileActivity)
            })
        } else {
            dialogMsg.showSuccessDialog(this, "Success", "Your profile is already updated.", "OK", View.OnClickListener {
                dialogMsg.dismissDialog(this)
            }, isCancellable = true)
        }
    }

    private fun showError(strTitle: String = "Alert", strMsg: String) {
        dialogMsg.showErrorRetryDialog(this, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg.dismissDialog(this)
        })
    }

    private fun fastRefreshViewMyProfile() {
        progressUserProfile.visibility = View.VISIBLE
        MyProfileUtils.fastRefreshProfile(applicationContext, { model ->
            progressUserProfile.visibility = View.GONE
            if (model != null) {
                userModel = model
                if (!isFinishing) {
                    if (userModel.isUserActive) {
                        setUserDetailsUi()
                    }else{
                        dialogMsg.showErrorRetryDialog(this@MyProfileActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, ErrorMsgs.ERR_MSG_USER_DE_ACTIVE, "Log in", View.OnClickListener {
                            dialogMsg.dismissDialog(this@MyProfileActivity)
                            LoginBasicsUi.logOutUser(this@MyProfileActivity)
                        }, false)
                    }
                }
            } else {
                Log.d("UserModelNull", "Issue")
            }
        }, compositeDisposable)
    }

    private fun fastChangePassword(strOldPass: String, strNewPass: String, dialog: Dialog) {
        DialogMsg.showPleaseWait(this)
        val hashParams = HashMap<String, String>()
        hashParams.put("oldPassword", strOldPass)
        hashParams.put("newPassword", strNewPass)
        FastNetworking.makeRxCallPost(applicationContext, Urls.CHANGE_USER_PASS, true, hashParams, "ChangePassword", object :
            FastNetworking.OnApiResult {
            override fun onApiSuccess(json: JSONObject?) {
                try {
                    DialogMsg.dismissPleaseWait(this@MyProfileActivity)
                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                        dialog.dismiss()
                        val strSuccessMsg = json.optString("message", "Your password has been successfully updated")
                        dialogMsg.showSuccessDialog(this@MyProfileActivity, "Success", strSuccessMsg, "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this@MyProfileActivity)
                        })
                        userModel.userHasPassword = true
                    } else {
                        val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)
                        dialogMsg.showErrorRetryDialog(this@MyProfileActivity, ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE, strErrMsg, "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(this@MyProfileActivity)
                        })
                    }
                } catch (e: Exception) {

                }
            }

            override fun onApiError(error: ANError) {
                DialogMsg.dismissPleaseWait(this@MyProfileActivity)
                dialogMsg.showErrorRetryDialog(this@MyProfileActivity, "Oops!", ErrorMsgs.ERR_API_MSG, "OK", View.OnClickListener {
                    dialogMsg.dismissDialog(this@MyProfileActivity)
                })
            }

        })
    }


    private fun pickImage() {
        EasyImage.configuration(this)
            .setImagesFolderName(getString(R.string.app_name)) // images folder name, default is "EasyImage"
            .setAllowMultiplePickInGallery(false)


        val dialog = MyDialog(this).getMyDialog(R.layout.dialog_camera_gallery)
        dialog.linCamera.setOnClickListener {
            dialog.dismiss()
            EasyImage.openCameraForImage(this, 0)
        }

        dialog.linGallery.setOnClickListener {
            dialog.dismiss()
            EasyImage.openGallery(this, 0)
        }
        dialog.show()
    }

    private fun compressImageAndUpload(imageFile: File) {
        DialogMsg.showPleaseWait(this)
        doAsync {
            Log.d("ImageFile", imageFile.absolutePath)

            val DIR_APP = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath + "/" + FOLDER_IMAGES
            val fileDir = File(DIR_APP)
            if (!fileDir.isDirectory) {
                fileDir.mkdirs()
            }

            var compressedImage = Compressor(this@MyProfileActivity)
                .setDestinationDirectoryPath(DIR_APP)
                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                .setMaxHeight(320)
                .setMaxWidth(320)
                .compressToFile(imageFile)

            val imageSizeKB = compressedImage.length() / 1024

            try {
                val isSuccessDelete = imageFile.delete()
                Log.d("TempFileDeleted", "File Deleted: $isSuccessDelete")
            } catch (e: Exception) {
                Log.d("DeleteExc", "" + e)
            }

            Log.d("CompressedImageSize", "" + imageSizeKB)

            if (imageSizeKB > 800) {
                Log.d("Compressing2ndTime", "ImageSize: " + imageSizeKB)
                compressedImage = Compressor(this@MyProfileActivity)
                    .setDestinationDirectoryPath(DIR_APP)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(compressedImage)
            }

            Log.d("CompressedImage", "" + compressedImage.absolutePath)
            profileImagePicked = compressedImage

            runOnUiThread {
                DialogMsg.dismissPleaseWait(this@MyProfileActivity)
                Glide.with(this@MyProfileActivity).load(profileImagePicked).into(imgUserProfile)
            }

        }
    }


}

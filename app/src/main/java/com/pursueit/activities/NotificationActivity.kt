package com.pursueit.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.NotificationAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.NotificationModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.HashMap

class NotificationActivity : AppCompatActivity() {

    val arrNotificationModel=ArrayList<NotificationModel>()
    var curPage=1
    var totalPages = 1
    var isLoadingAlready = false

    lateinit var noActivityFound: NoRecordFound

    var adapter: NotificationAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        init()

        imgBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun init() {

        txtPlaceHeader.text="Notifications"

        imgBack.visibility = View.VISIBLE

        rvNotificationListing.itemAnimator = null
        rvNotificationListing.isEnabled = false
        rvNotificationListing.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
      //  rvNotificationListing.addItemDecoration(ItemOffsetDecorationForLinearVertical(resources.getDimension(R.dimen.dim_10).toInt()))
        rvNotificationListing.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))

        swipeRefresh.isEnabled = false

        noActivityFound = NoRecordFound(this, relListingParent)
        noActivityFound.init()
        noActivityFound.hideEmptyUi()

        swipeRefresh.isEnabled = false
        swipeRefreshFunctionality()

        initiateWork()
    }

    private fun swipeRefreshFunctionality() {
        swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        swipeRefresh.setOnRefreshListener {
            clearListAndMakeApiCall()
        }
    }

    private fun clearListAndMakeApiCall() {

        arrNotificationModel.clear()
        rvNotificationListing?.adapter?.notifyDataSetChanged()

        curPage = 1
        noActivityFound.hideEmptyUi()


        /*for (i in 1..totalPages)
            saveResponseJson(act.applicationContext, "" + strCatId, i, "")*/

        //initFetchingResponse(loadMoreOrSwipeRefresh = true)

        fastFetchNotificationListing()
    }

    private fun initiateWork() {

        noActivityFound.hideEmptyUi()
        fastFetchNotificationListing()
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                rvNotificationListing.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }


    private fun fastFetchNotificationListing(progressShowMore: ProgressWheel?=null)
    {
        if (ConnectionDetector.isConnectingToInternet(this))
        {
            if (curPage==1)
            {
                arrNotificationModel.clear()

                try {
                    if (curPage<=1)
                    {
                        rvNotificationListing.showShimmerAdapter()
                    }
                    else{
                        hideShimmer()
                    }
                }catch (e:Exception){}
            }

            doAsync {

                val hashParams = HashMap<String, String>()
                hashParams.put("page", "" + curPage)

                FastNetworking.makeRxCallPost(this@NotificationActivity,
                    Urls.VIEW_NOTIFICATION,true,tag = "View_Notification",hashParams = hashParams,
                    onApiResult = object : FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {

                            try {
                                isLoadingAlready = false

                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonData=json.getJSONObject("data")
                                    totalPages=jsonData.optInt("last_page")
                                    val jsonNotificationArray=jsonData.getJSONArray("data")
                                    val array= JsonParse.getNotificationModel(jsonNotificationArray)
                                    arrNotificationModel.addAll(array)

                                    this@NotificationActivity.runOnUiThread {
                                        fillListingAdapter()
                                    }
                                }
                                else{
                                    showErrorUi("No notification available")
                                }
                            }catch (e:Exception)
                            {
                                Log.d("ExcParse_CampList", "" + e)
                                showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                            }


                            if (arrNotificationModel.size==0)
                                UserModel.setLastNotificationId(this@NotificationActivity,"0")

                        }

                        override fun onApiError(error: ANError) {
                            showErrorUi(ErrorMsgs.ERR_API_MSG)

                            runOnUiThread {  if (progressShowMore != null) {
                                progressShowMore.visibility = View.GONE
                            }}

                        }

                    })
            }
        }
        else{
            showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }

    private fun showErrorUi(strMsg: String) {
        try {
            if (!this@NotificationActivity.isFinishing) {
                hideShimmer()
                rvNotificationListing.visibility = View.GONE
                noActivityFound.showNoActivities(strMsg, View.OnClickListener {
                    noActivityFound.hideEmptyUi()
                    fastFetchNotificationListing()
                },fromNotification = true)
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun showErrorMsg(strMsg: String) {
        try {
            if (!this@NotificationActivity.isFinishing) {
                rvNotificationListing.hideShimmerAdapter()
            }
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")
        noActivityFound.hideEmptyUi()
        rvNotificationListing.visibility = View.VISIBLE

        if (rvNotificationListing.adapter == null || adapter == null) {
            rvNotificationListing.isEnabled = true
            adapter = NotificationAdapter(this,arrNotificationModel) { pos, progressShowMore ->
                Log.d("progressPosition",pos.toString())
                Log.d("Cur_", "" + curPage)
                Log.d("last_", "" + totalPages)
                progressShowMore.visibility = View.GONE
                if (curPage < totalPages && !isLoadingAlready) {
                    isLoadingAlready = true
                    curPage += 1
                    progressShowMore.visibility = View.VISIBLE
                    fastFetchNotificationListing(progressShowMore)
                } else {
                    progressShowMore.visibility = View.GONE
                }
            }
            rvNotificationListing.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }


}

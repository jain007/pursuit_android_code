package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.PaymentTransactionsAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PaymentTransactionModel
import com.pursueit.model.UserModel
import com.pursueit.utils.ConnectionDetector
import com.pursueit.utils.ErrorMsgs
import com.pursueit.utils.JsonParse
import com.pursueit.utils.NoRecordFound
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_payment_transactions.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class PaymentTransactionsActivity : AppCompatActivity() {

    lateinit var userModel:UserModel

    var curPage = 1
    var totalPages = 1

    var arrayPaymentTransactionModel = ArrayList<PaymentTransactionModel>()

    var adapter: PaymentTransactionsAdapter? = null

    var compositeDisposable = CompositeDisposable()

    private var isLoadingAlready = false

    lateinit var noRecordUi: NoRecordFound

    var fromPassTransaction:Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_transactions)

        fromPassTransaction=intent.getBooleanExtra("fromPassTransaction",false)

        userModel= UserModel.getUserModel(this)

        init()

        fastViewPaymentTransactions()
    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.PAYMENTS_HISTORY_SCREEN,userModel.userEmail,this::class.java)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun init() {
        compositeDisposable = CompositeDisposable()
        if (fromPassTransaction)
            toolbarFragment.txtToolbarHeader.text = "Pass Transaction"
        else
             toolbarFragment.txtToolbarHeader.text = "Payment History"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

        noRecordUi = NoRecordFound(this)
        noRecordUi.init()

        rvPaymentTransactions.layoutManager = LinearLayoutManager(this)
        rvPaymentTransactions.setHasFixedSize(true)
    }

    private fun fastViewPaymentTransactions(progressLoadMore: ProgressWheel? = null) {
        if (ConnectionDetector.isConnectingToInternet(this)) {
            noRecordUi.hideEmptyUi()

            if (curPage == 1)
                progressMain.visibility = View.VISIBLE

            progressLoadMore?.visibility = View.VISIBLE

            val hashParams = HashMap<String, String>()
            hashParams["page"] = "" + curPage



            FastNetworking.makeRxCallPost(applicationContext, if (fromPassTransaction) Urls.PASS_TRANSACTION else Urls.VIEW_PAYMENT_TRANSACTIONS, true, hashParams, "View_Transactions", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    doAsync {
                        try {
                            if (curPage == 1)
                                arrayPaymentTransactionModel.clear()

                            arrayPaymentTransactionModel.addAll(JsonParse.parsePaymentHistory(json,fromPassTransaction))


                            totalPages = json!!.getJSONObject("data").optInt("last_page", 1)
                        } catch (e: Exception) {
                            Log.d("Exc_PayTrans", "" + e)
                        } finally {
                            isLoadingAlready = false
                            runOnUiThread {
                                progressLoadMore?.visibility = View.GONE
                                fillAdapter()
                            }
                        }
                    }

                }

                override fun onApiError(error: ANError) {
                    isLoadingAlready = false
                    progressMain.visibility = View.GONE
                    progressLoadMore?.visibility = View.GONE
                    if (curPage == 1) {
                        showApiError {
                            fastViewPaymentTransactions()
                        }
                    }
                }

            }, compositeDisposable)
        }
    }

    private fun fillAdapter() {
        if (adapter == null) {
            adapter = PaymentTransactionsAdapter(this, arrayPaymentTransactionModel, this::onLastItemReached,fromPassTransaction)
            rvPaymentTransactions.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }

        progressMain.visibility = View.GONE

        if (arrayPaymentTransactionModel.size > 0) noRecordUi.hideEmptyUi() else noRecordUi.showNoPaymentTransactions()
    }

    private fun onLastItemReached(pos: Int, progressLoadMore: ProgressWheel) {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            if (curPage < totalPages && !isLoadingAlready) {
                progressLoadMore.visibility = View.VISIBLE
                isLoadingAlready = true
                curPage += 1
                fastViewPaymentTransactions()
            }
        }
    }

    private fun showApiError(onRetry: () -> Unit) {
        noRecordUi.showMsgError("Oops!", ErrorMsgs.ERR_API_MSG, View.OnClickListener {
            onRetry()
        })
    }


}

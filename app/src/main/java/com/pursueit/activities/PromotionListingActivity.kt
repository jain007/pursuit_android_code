package com.pursueit.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.PromotionListingAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PromotionModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.activity_promotion_listing.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.HashMap

class PromotionListingActivity : AppCompatActivity() {

    val arrPromotionModel=ArrayList<PromotionModel>()
    var curPage=1
    var totalPages = 1
    var isLoadingAlready = false

    lateinit var noActivityFound: NoRecordFound

    var adapter: PromotionListingAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promotion_listing)

        init()

        imgBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun init() {

        txtPlaceHeader.text="Promotions"

        imgBack.visibility = View.VISIBLE

        rvPromotionListing.itemAnimator = null
        rvPromotionListing.isEnabled = false
        rvPromotionListing.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvPromotionListing.addItemDecoration(ItemOffsetDecorationForLinearVertical(resources.getDimension(R.dimen.dim_10).toInt()))

        swipeRefresh.isEnabled = false

        noActivityFound = NoRecordFound(this, relListingParent)
        noActivityFound.init()
        noActivityFound.hideEmptyUi()

        swipeRefresh.isEnabled = false
        swipeRefreshFunctionality()

        initiateWork()
    }

    private fun swipeRefreshFunctionality() {
        swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        swipeRefresh.setOnRefreshListener {
            clearListAndMakeApiCall()
        }
    }

    private fun clearListAndMakeApiCall() {


        arrPromotionModel.clear()
        rvPromotionListing?.adapter?.notifyDataSetChanged()

        curPage = 1
        noActivityFound.hideEmptyUi()


        /*for (i in 1..totalPages)
            saveResponseJson(act.applicationContext, "" + strCatId, i, "")*/

        //initFetchingResponse(loadMoreOrSwipeRefresh = true)

        fastFetchPromotionListing()
    }

    private fun initiateWork() {

        noActivityFound.hideEmptyUi()
        fastFetchPromotionListing()
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                rvPromotionListing.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }


    private fun fastFetchPromotionListing(progressShowMore: ProgressWheel?=null)
    {
        if (ConnectionDetector.isConnectingToInternet(this))
        {
            if (curPage==1)
            {
                arrPromotionModel.clear()

                try {
                    if (curPage<=1)
                    {
                        rvPromotionListing.showShimmerAdapter()
                    }
                    else{
                        hideShimmer()
                    }
                }catch (e:Exception){}
            }

            doAsync {

                val hashParams = HashMap<String, String>()
                hashParams["page"] = "" + curPage

                FastNetworking.makeRxCallPost(this@PromotionListingActivity,Urls.VIEW_PROMOTION,true,tag = "View_Promotion",hashParams = hashParams,
                    onApiResult = object : FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {

                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonData=json.getJSONObject("data")
                                    totalPages=jsonData.optInt("last_page")
                                    val jsonPromotionArray=jsonData.getJSONArray("data")

                                    val array=JsonParse.parsePromotion(jsonPromotionArray,fromHome = true)

                                    arrPromotionModel.addAll(array)

                                    this@PromotionListingActivity.runOnUiThread {
                                        fillListingAdapter()
                                    }
                                }
                                else{
                                    showErrorUi(getString(R.string.error_message))
                                }
                            }catch (e:Exception)
                            {
                                Log.d("ExcParse_CampList", "" + e)
                                showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                            }


                        }

                        override fun onApiError(error: ANError) {
                            showErrorUi(ErrorMsgs.ERR_API_MSG)

                            runOnUiThread {  if (progressShowMore != null) {
                                progressShowMore.visibility = View.GONE
                            }}

                        }

                    })
            }
        }
        else{
            showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }

    private fun showErrorUi(strMsg: String) {
        try {
            if (!this@PromotionListingActivity.isFinishing) {
                hideShimmer()
                rvPromotionListing.visibility = View.GONE
                noActivityFound.showNoActivities(strMsg, View.OnClickListener {
                    noActivityFound.hideEmptyUi()
                    fastFetchPromotionListing()
                })
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun showErrorMsg(strMsg: String) {
        try {
            if (!this@PromotionListingActivity.isFinishing) {
                rvPromotionListing.hideShimmerAdapter()
            }
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")
        noActivityFound.hideEmptyUi()
        rvPromotionListing.visibility = View.VISIBLE

        if (rvPromotionListing.adapter == null || adapter == null) {
            rvPromotionListing.isEnabled = true
            adapter = PromotionListingAdapter(this,arrPromotionModel) { pos, progressShowMore ->
                Log.d("Cur_", "" + curPage)
                Log.d("last_", "" + totalPages)
                progressShowMore.visibility = View.GONE
                if (curPage < totalPages && !isLoadingAlready) {
                    isLoadingAlready = true
                    curPage += 1
                    progressShowMore.visibility = View.VISIBLE
                    fastFetchPromotionListing(progressShowMore)
                } else {
                    progressShowMore.visibility = View.GONE
                }
            }
            rvPromotionListing.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

}

package com.pursueit.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding2.view.RxView
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.MyImagePagerAdapter
import com.pursueit.adapters.NearbyActivityAdapter
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_provider_detail.*
import kotlinx.android.synthetic.main.content_circle_progress.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import kotlin.Exception


class ProviderDetailActivity : AppCompatActivity() {

    var arrayImages=ArrayList<String>()
    val compositeDisposable = CompositeDisposable()

    var lat=0.0
    var lng=0.0

    var curPos = 0
    var curPage = 1
    var totalPages = 1
    var isLoadingAlready = false

    var adapter: NearbyActivityAdapter? = null

    var dialogMsg: DialogMsg? = null

    var providerId=""

    companion object {
        var actProviderDetail: WeakReference<ProviderDetailActivity>? = null
    }

    private var arrayNearbyActivities = ArrayList<ActivityModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_provider_detail)

        dialogMsg = DialogMsg()

        actProviderDetail=WeakReference(this)


        linMain.visibility=View.GONE
        providerId=intent.getStringExtra("providerId")
        fastFetchProviderDetail()
        fastFetchNearbyActivities("","")

       /* compositeDisposable.add(
            RxView.clicks(imgGetDirections).throttleFirst(
                1000,
                TimeUnit.MILLISECONDS
            ).subscribe {
                try {
                    MyPlaceUtils.getDirections(this@ProviderDetailActivity, lat, lng)
                }catch (e:Exception){

                }
            })*/

        imgBack.setOnClickListener {
            onBackPressed()
        }
        linRatingBarView.setOnClickListener {
            MyNavigations.goToAllReviews(this@ProviderDetailActivity,ArrayList(),"",providerId)
        }

        txtProviderTitle.setOnClickListener {
            linRatingBarView.performClick()
        }
    }

    private fun fastFetchProviderDetail() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            val hashParams = HashMap<String, String>()
            hashParams["providerId"] = providerId

            //   DialogMsg.showPleaseWait(this)
            cardProgress.visibility = View.VISIBLE
            FastNetworking.makeRxCallPost(
                applicationContext,
                Urls.VIEW_SERVICE_PROVIDER,
                true,
                hashParams,
                "ProviderDetail",
                object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {

                        cardProgress.visibility = View.GONE
                        // DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                            try {
                                val jobData=json.getJSONObject("data")
                                jobData.run {
                                    txtProviderTitle.text=getValidString("provider_name")
                                    txtLocation.text=getValidString("provider_location")
                                    txtAboutProviderTitle.text="About "+getValidString("provider_name")
                                    txtActivityDesc.text=getValidString("provider_about_us")

                                    Glide.with(applicationContext)
                                        .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(isCenterCrop = false))
                                        .load(Urls.IMAGE_URL_PARTNER + getValidString("provider_logo")).into(imgFeaturedPartner)

                                    val avgRating=getValidString("provider_rating")
                                    val totalCount=getValidString("provider_total_rating_count")

                                    try {
                                        ratingBarDetail.rating=avgRating.toFloat()
                                    }catch (e:Exception){
                                        linRatingBarView.visibility=View.GONE
                                        Log.d("Invalid Rating",e.toString())
                                    }

                                    txtStarCount.text="($totalCount)"

                                    val jarImageGallery=getJSONArray("get_image_gallery")
                                    repeat(jarImageGallery.length()){
                                        val jobImage=jarImageGallery.getJSONObject(it)
                                        arrayImages.add(jobImage.getString("photo_gallery"))
                                    }

                                    val jarVideoGallery=getJSONArray("get_video_gallery")
                                    repeat(jarVideoGallery.length()){
                                        val jobVideo=jarVideoGallery.getJSONObject(it)
                                        arrayImages.add("http://img.youtube.com/vi/" + jobVideo.getString("video_gallery") + "/hqdefault.jpg")
                                    }

                                    if (arrayImages.size==0){
                                        arrayImages.add(Urls.IMAGE_URL_PARTNER + getValidString("provider_logo"))
                                        setViewPagerAdapter(true)
                                    }
                                    else{
                                        setViewPagerAdapter()
                                    }

                                    try {
                                        lat=getDouble("provider_lat")
                                        lng=getDouble("provider_long")
                                    }catch (e:Exception){
                                        Log.e("error occurred",e.toString())
                                    }


                                }

                            }catch (e:Exception){
                                Log.e("fastFetchProviderDetail",e.toString())
                            }
                        }

                        linMain.visibility=View.VISIBLE
                    }

                    override fun onApiError(error: ANError)  {
                        cardProgress.visibility = View.GONE
                        //  DialogMsg.dismissPleaseWait(this@ActivityDetailActivity)
                    }
                }, compositeDisposable)
        }
    }

    private fun fastFetchNearbyActivities(strSubCatId: String = "", activityId: String = "", progressShowMore: ProgressWheel? = null) {
        try {
            if (ConnectionDetector.isConnectingToInternet(this)) {

                if (curPage == 1) {
                    arrayNearbyActivities.clear()

                    try {
                        if (curPos <= 1) progressActivity.visibility=View.VISIBLE
                        else progressActivity.visibility=View.GONE

                    } catch (e: Exception) {

                    }

                }

                doAsync {
                    var hashParams = java.util.HashMap<String, String>()
                    hashParams.put("latitude", "" + MyPlaceUtils.placeModel.placeLat)
                    hashParams.put("longitude", "" + MyPlaceUtils.placeModel.placeLng)
                    hashParams.put("categoryId", "" + "")
                    hashParams.put("subCategoryId", "" + "")
                    hashParams.put("ageFrom", "")
                    hashParams.put("ageTo", "")
                    hashParams.put("activityId", "" + "")
                    hashParams.put("cityPlaceId", "" + MyPlaceUtils.placeModel.placeCityId)
                    hashParams.put("page", "" + curPage)
                    hashParams.put("providerId",providerId)

                    hashParams=SearchAndFilterStaticData.addFilterData(hashParams)

                    FastNetworking.makeRxCallPost(this@ProviderDetailActivity, Urls.NEARBY_ACTIVITIES, true, hashParams, "NearByActivities_cat_" + "",
                        object : FastNetworking.OnApiResult {
                            override fun onApiSuccess(json: JSONObject?) {
                                try {
                                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                        //saveResponseJson(act.applicationContext, strCatId!!, curPage, json.toString())
                                        parseData(json.toString(), progressShowMore)
                                    } else {
                                        hideActivityLayout()
                                    }
                                } catch (e: Exception) {
                                    hideActivityLayout()
                                }
                            }

                            override fun onApiError(error: ANError) {
                                //showErrorMsg(ErrorMsgs.ERR_API_MSG)
                                hideActivityLayout()

                                if (progressShowMore != null) {
                                    progressShowMore.visibility = View.GONE
                                }
                            }

                        })
                }

            } else {
                dialogMsg?.showErrorConnectionDialog(this@ProviderDetailActivity, onRetryListener = View.OnClickListener { dialogMsg?.dismissDialog(this@ProviderDetailActivity) })
               // showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
            }
        } catch (e: Exception) {
            Log.d("Exc_FetchNearby", "" + e)
        }
    }

    private fun parseData(strJson: String, progressShowMore: ProgressWheel? = null) {
        doAsync {
            try {
                isLoadingAlready = false
                val json = JSONObject(strJson)

                if (curPage == 1)
                    arrayNearbyActivities.clear()

                if (json.getBoolean(StaticValues.KEY_STATUS)) {
                    val jsonData = json.getJSONObject("data")
                    totalPages = jsonData.optInt("last_page", 1)
                    val (arrActivity,arrActivityForMap)=JsonParse.parseNearbyActivities(jsonData, json)
                    arrayNearbyActivities.addAll(arrActivity)
                    /*try {
                        if (arrActivityForMap.size>0)
                        {
                            (parentFragment as NearbyFragment).showFab( vi.rvNearbyActivities)
                        }
                        else{
                            (parentFragment as NearbyFragment).hideFab()
                        }
                    }catch (e:Exception){}*/
                }
                else{
                    // (parentFragment as NearbyFragment).hideFab()
                }

                this@ProviderDetailActivity.runOnUiThread {
                    fillListingAdapter()
                }
            } catch (e: Exception) {
                Log.d("ParseExc__", "" + e)
            } finally {

                if (progressShowMore != null) {
                    this@ProviderDetailActivity.runOnUiThread {
                        progressShowMore.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                progressActivity.visibility=View.GONE
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")

        rvNearbyActivities.visibility = View.VISIBLE
        rvNearbyActivities.addItemDecoration(DividerItemDecoration(applicationContext,DividerItemDecoration.VERTICAL))

        if (rvNearbyActivities.adapter == null || adapter == null) {
            rvNearbyActivities.isEnabled = true
            adapter = NearbyActivityAdapter(this, arrayNearbyActivities,showMoreListener= { pos, progressShowMore ->
                Log.d("Cur_", "" + curPage)
                Log.d("last_", "" + totalPages)
                progressShowMore.visibility = View.GONE
                if (curPage < totalPages && !isLoadingAlready) {
                    isLoadingAlready = true
                    curPage += 1
                    progressShowMore.visibility = View.VISIBLE
                    fastFetchNearbyActivities("", activityId = "", progressShowMore = progressShowMore)
                } else {
                    progressShowMore.visibility = View.GONE
                }
            },compositeDisposable = compositeDisposable)
            rvNearbyActivities.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }

        /*try {
            if (lastClickedPosition > 0 && lastClickedPosition < arrayNearbyActivities.size) {
                vi.rvNearbyActivities.scrollToPosition(lastClickedPosition)
            }
        } catch (e: Exception) {
            Log.d("Exc_LastClicked_", "" + e)
        }*/
    }


    fun hideActivityLayout(){
        linActivity.visibility=View.GONE
        linActivityDivider.visibility=View.GONE
        linActivityHeader.visibility=View.GONE
    }

    fun setViewPagerAdapter(alreadyCompleteUrlAdded:Boolean=false){
        val adapter = MyImagePagerAdapter(this, arrayImages,fromServiceProvider = true,alreadyCompleteUrlAdded = alreadyCompleteUrlAdded)
        viewPager.adapter = adapter
        worm_dots_indicator.setViewPager(viewPager)
    }
}


package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.ReviewsAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ReviewModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import kotlinx.android.synthetic.main.activity_reviews_listing.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.dialog_add_edit_review.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import retrofit2.http.Url

class ReviewsListingActivity : AppCompatActivity() {

    var arrayReviews = ArrayList<ReviewModel>()
    var adapter: ReviewsAdapter? = null
    lateinit var dialogMsg: DialogMsg
    lateinit var userModel: UserModel

    var activityId: String = ""

    var compositeDisposable = CompositeDisposable()

    var curPage=1
    var totalPages=1
    var isLoadingAlready=false

    var providerId:String=""

    companion object {
        var isReviewUpdated=false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews_listing)
        compositeDisposable = CompositeDisposable()
        init()
        dialogMsg = DialogMsg()

        getReviewsListAndSetUi()

        fastViewReviews()

    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this,MyGoogleAnalytics.REVIEWS_LISTING_SCREEN,UserModel.getUserModel(applicationContext).userEmail,this::class.java)
    }

    private fun init() {
        toolbarFragment.txtToolbarHeader.text = "Reviews"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

        userModel = UserModel.getUserModel(applicationContext)

        rvReviews.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            setHasFixedSize(true)
        }

        providerId=intent.getStringExtra("providerId")
    }

    private fun getReviewsListAndSetUi() {
        try {
            arrayReviews = intent?.extras?.getSerializable("Reviews") as ArrayList<ReviewModel>
            activityId = intent?.extras?.getString("ActivityId")!!
            fillAdapter()
        } catch (e: Exception) {

        }
    }

    private fun fillAdapter() {
        if(rvReviews.adapter==null && adapter==null) {
            adapter = ReviewsAdapter(this, arrayReviews, { posClicked ->
                val revModel = arrayReviews[posClicked]
                if (userModel.userId.trim().isNotEmpty()) {
                    if (revModel.reviewUserId == userModel.userId) {
                        showEditReviewDialog(revModel)
                    }
                }
            }, { pos, progressShowMore ->
                if(ConnectionDetector.isConnectingToInternet(applicationContext)) {
                    if (curPage < totalPages && !isLoadingAlready) {
                        progressShowMore.visibility=View.VISIBLE
                        isLoadingAlready = true
                        curPage+=1
                        fastViewReviews(progressShowMore)
                    }
                }
            })
            val animAdapter = ScaleInAnimationAdapter(adapter)
            rvReviews.adapter = animAdapter
        }else{
            adapter?.notifyDataSetChanged()
        }
    }

    private fun fastViewReviews(progressShowMore:ProgressWheel?=null){
        if(ConnectionDetector.isConnectingToInternet(applicationContext)){
            val hashParams = HashMap<String, String>()
            hashParams["activityId"] = "" + activityId
            hashParams["page"]=""+curPage
            hashParams["providerId"]=providerId

            if (providerId!="" && curPage==1)
            {
                DialogMsg.showPleaseWait(this)
            }

            val url=if (providerId=="") Urls.VIEW_REVIEWS else Urls.VIEW_PROVIDER_REVIEW
            FastNetworking.makeRxCallPost(applicationContext, url, true, hashParams, "ViewReviews", object :FastNetworking.OnApiResult{
                override fun onApiSuccess(json: JSONObject?) {
                    runOnUiThread { DialogMsg.dismissPleaseWait(this@ReviewsListingActivity) }
                    doAsync {
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {

                                if(json.has("data")) {
                                    if (json.getJSONObject("data").has("last_page"))
                                        totalPages = json.getJSONObject("data").optInt("last_page", 1)
                                }

                                if(curPage==1)
                                    arrayReviews.clear()

                                arrayReviews.addAll(JsonParse.parseReviews(json))

                            }

                        } catch (e: Exception) {
                            Log.d("ExcReviews", "" + e)
                        } finally {
                            runOnUiThread {
                                try {
                                    isLoadingAlready=false
                                    progressShowMore?.visibility=View.GONE
                                    fillAdapter()
                                }catch (e:Exception){

                                }
                            }
                        }
                    }
                }

                override fun onApiError(error: ANError) {
                    runOnUiThread { DialogMsg.dismissPleaseWait(this@ReviewsListingActivity) }
                }

            })
        }
    }

    private fun showEditReviewDialog(reviewModel: ReviewModel) {
        try {

            val dialog = MyDialog(this).getMyDialog(R.layout.dialog_add_edit_review)

            dialog.ratingBarPostReview.setOnRatingChangeListener { baseRatingBar, flValue ->
                Log.d("RatingValue", "" + flValue)
            }

            dialog.etReview.setText(reviewModel.reviewDesc.trim())
            MyProfileUtils.setSelectionEnd(dialog.etReview)
            dialog.ratingBarPostReview.rating = reviewModel.reviewRating
            dialog.btnPostReview.text = "Update Review"


            dialog.btnPostReview.setOnClickListener {
                val strReview = dialog.etReview.text.toString().trim()
                val flRatingValue = dialog.ratingBarPostReview.rating

                /*if (strReview.isEmpty()) {
                    dialogMsg.showErrorRetryDialog(this, "Alert", "Please write some review and then proceed", "OK", View.OnClickListener {
                        dialogMsg.dismissDialog(this@ReviewsListingActivity)
                    })
                    return@setOnClickListener
                }*/

                if (strReview.isNotEmpty() && strReview.length < 10) {
                    dialogMsg.showErrorRetryDialog(this, "Alert", "Your review should be at least of 10 characters", "OK", View.OnClickListener {
                        dialogMsg.dismissDialog(this@ReviewsListingActivity)
                    })
                    return@setOnClickListener
                }


                dialog.dismiss()

                val hashParams = HashMap<String, String>()
                hashParams.put("reviewRating", "" + flRatingValue)
                hashParams.put("reviewDescription", strReview)
                hashParams.put("reviewId", "" + reviewModel.reviewId)
                hashParams.put("activityId", if(activityId.trim().isEmpty()) reviewModel.reviewActivityId else activityId)

                if(reviewModel.reviewCampId.trim().isNotEmpty())
                    hashParams.put("campId",reviewModel.reviewCampId);


                if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
                    MyReviewUtils.fastAddRatingReview(this, hashParams, "AddReview", { json ->
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                isReviewUpdated=true
                                val strMsg = json.optString("message", "Your review has been successfully posted")
                                dialogMsg.showSuccessDialog(this, "Success", strMsg, "OK",
                                        View.OnClickListener {
                                            dialogMsg.dismissDialog(this)

                                            //update in the list as well
                                            val index = arrayReviews.indexOf(arrayReviews.single { it.reviewId == reviewModel.reviewId })
                                            Log.d("IndexReturned", "" + index)
                                            reviewModel.reviewRating = flRatingValue
                                            reviewModel.reviewDesc = strReview
                                            arrayReviews[index] = reviewModel
                                            adapter?.notifyDataSetChanged()

                                        }, isCancellable = false)
                            } else {
                                dialogMsg.showErrorApiDialog(this, "OK", View.OnClickListener { dialogMsg.dismissDialog(this) }, isCancellable = true)
                            }
                        } catch (e: Exception) {

                        }
                    }, compositeDisposable, fromCamp = reviewModel.reviewCampId.trim().isNotEmpty())
                } else {
                    dialogMsg.showErrorConnectionDialog(this, "OK", View.OnClickListener { dialogMsg.dismissDialog(this) }, isCancellable = true)
                }

            }

            dialog.imgCloseDialogPostReview.setOnClickListener {
                dialog.dismiss()
            }

            DialogMsg.animateViewDialog(dialog.relParentPostReview)

            dialog.show()

        } catch (e: Exception) {
            Log.d("ReviewDialogExc", "" + e)
        }
    }

}

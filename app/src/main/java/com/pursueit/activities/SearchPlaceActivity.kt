package com.pursueit.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.adapters.PlaceAddressRecentSearchAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.model.PlaceModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_search_place.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import java.util.concurrent.TimeUnit


class SearchPlaceActivity : AppCompatActivity() {

    var arrayAddressModel = ArrayList<PlaceModel>()
    var addressAdapter: PlaceAddressRecentSearchAdapter? = null


    var recentAdapter: PlaceAddressRecentSearchAdapter? = null

    var compositeDisposable = CompositeDisposable()

    lateinit var dialogMsg: DialogMsg

    private val AUTO_COMPLETE_REQ_CODE = 50

    private var isSearchAddress: Boolean = true //false when user adds into my address

    lateinit var userModel: UserModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_place)

        compositeDisposable = CompositeDisposable()

        userModel = UserModel.getUserModel(applicationContext)

        dialogMsg = DialogMsg()

        init()

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, MyPlaceUtils.apiKey);
        }

        onClickListeners()

        //fetch user specific addresses only when user is logged in
        if (userModel.userId.trim().isNotEmpty())
            fastFetchMyAddresses()

        fillRecentSearches()

    }


    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.SEARCH_PLACE_SCREEN, UserModel.getUserModel(applicationContext).userEmail, this::class.java)
    }

    private fun init() {
        toolbarFragment.txtToolbarHeader.text = "Location"
        toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
        toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

        rvAddress.layoutManager = LinearLayoutManager(this)
        rvAddress.setHasFixedSize(true)

        rvRecentSearches.layoutManager = LinearLayoutManager(this)
        rvRecentSearches.setHasFixedSize(true)

        //hide my address layout when user is not logged in
        relMyAddress.visibility = if (userModel.userId.trim().isNotEmpty()) View.VISIBLE else View.GONE

        relRecentSearches.visibility = View.GONE
    }

    private fun onClickListeners() {
        compositeDisposable.add(
            RxView.clicks(linMyLocation).throttleFirst(3000, TimeUnit.MILLISECONDS).subscribe {
                beginLocationFetch()
            })

        compositeDisposable.add(
            RxView.clicks(linSearchBar).throttleFirst(2000, TimeUnit.MILLISECONDS).subscribe {
                isSearchAddress = true
                val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, MyPlaceUtils.placeFields).setCountry("AE").build(this)
                startActivityForResult(intent, AUTO_COMPLETE_REQ_CODE)
            })

    }


    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun beginLocationFetch() {
        DialogMsg.showPleaseWait(this)
        MyPlaceUtils.initPlaceClient(applicationContext)
        val myPlaceUtils = MyPlaceUtils(this, object :
            MyPlaceUtils.OnCityFetchedListener {
            override fun onCityFetched(strCityPlaceId: String) {
                Log.d("MyPlaceModel", "" + MyPlaceUtils.placeModel)
                DialogMsg.dismissPleaseWait(this@SearchPlaceActivity)
                updateCurrentPlaceSelectedAndFinish(MyPlaceUtils.placeModel)
            }

            override fun onCityError() {
                DialogMsg.dismissPleaseWait(this@SearchPlaceActivity)
                //toast(ErrorMsgs.ERR_API_MSG)
                if (MyAppConfig.defaultPlaceModel != null) {
                    MyPlaceUtils.placeModel = MyAppConfig.defaultPlaceModel!!
                }
            }


        })
        myPlaceUtils.setAsCurrent = true
        myPlaceUtils.requestLocationPermission()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == AUTO_COMPLETE_REQ_CODE) {
                when (resultCode) {
                    AutocompleteActivity.RESULT_OK -> {

                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.d("PlaceId", "" + place.id)
                        Log.d("PlaceName", "" + place.name)
                        Log.d("PlaceLatLng", "" + place.latLng?.latitude + " - " + place.latLng?.longitude)
                        Log.d("PlaceAddress", "" + place.address)

                        setCurrentPlaceNow(place, isSearchAddress)
                    }

                    AutocompleteActivity.RESULT_ERROR -> {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("PlaceError", "" + status)
                        dialogMsg.showErrorRetryDialog(this, "Oops!", ErrorMsgs.ERR_API_MSG, "OK", View.OnClickListener { dialogMsg.dismissDialog(this@SearchPlaceActivity) })
                    }
                }
            }
        }
    }

    private fun setCurrentPlaceNow(place: Place, setCurrentPlace: Boolean = true) {
        DialogMsg.showPleaseWait(this)
        if (setCurrentPlace) {
            MyPlaceUtils.placeModel.placeId = place.id ?: ""
            MyPlaceUtils.placeModel.placeTitle = place.name ?: ""
            MyPlaceUtils.placeModel.placeLat = place.latLng?.latitude ?: 0.0
            MyPlaceUtils.placeModel.placeLng = place.latLng?.longitude ?: 0.0
            MyPlaceUtils.placeModel.placeAddress = place.address?.trim() ?: ""

            /*if (!MyPlaceUtils.placeModel.placeAddress.contains(MyPlaceUtils.placeModel.placeTitle))
                MyPlaceUtils.placeModel.placeAddress = MyPlaceUtils.placeModel.placeTitle + " - " + (place.address?.replace(MyPlaceUtils.placeModel.placeTitle, "") ?: "")*/
        }
        val myPlace = MyPlaceUtils(this, object : MyPlaceUtils.OnCityFetchedListener {
            override fun onCityFetched(strCityPlaceId: String) {
                DialogMsg.dismissPleaseWait(this@SearchPlaceActivity)
                if (setCurrentPlace) {
                    Log.d("MyPlaceSelected", MyPlaceUtils.placeModel.toString())
                    savePlaceModelOnLocal(MyPlaceUtils.placeModel)

                    updateCurrentPlaceSelectedAndFinish(MyPlaceUtils.placeModel)
                } else {
                    //when user has pressed add address button

                    val placeModel = PlaceModel(
                        place.id ?: "", place.name ?: "", place.address
                            ?: "", place.latLng?.latitude ?: 0.0, place.latLng?.longitude
                            ?: 0.0
                    ).also {
                        it.placeCityId = strCityPlaceId
                    }

                    Log.d("UserAddAddress", placeModel.toString())

                    MyProfileUtils.fastAddAddressNow(this@SearchPlaceActivity, placeModel, {
                        fastFetchMyAddresses()
                    }, compositeDisposable)
                }
            }

            override fun onCityError() {
                DialogMsg.dismissPleaseWait(this@SearchPlaceActivity)
                toast(ErrorMsgs.ERR_API_MSG)
            }

        })
        myPlace.setAsCurrent = setCurrentPlace

        if (place.latLng != null)
            myPlace.initCurrentCityCallGeocodeApi(place.latLng!!.latitude, place.latLng!!.longitude, overwritePlaceAddress = false)
        else
            toast("Couldn't fetch selected location, please try again")
    }

    private fun fastFetchMyAddresses() {
        if (ConnectionDetector.isConnectingToInternet(applicationContext)) {
            progressAddress.visibility = View.VISIBLE
            MyProfileUtils.fastFetchMyAddresses(this, {
                try {
                    progressAddress.visibility = View.GONE
                    if (addressAdapter == null) {
                        addressAdapter = PlaceAddressRecentSearchAdapter(this, MyProfileUtils.arrayMyAddressModel) { pos ->
                            updateCurrentPlaceSelectedAndFinish(MyProfileUtils.arrayMyAddressModel[pos])
                        }
                        rvAddress.adapter = addressAdapter
                    } else {
                        addressAdapter?.notifyDataSetChanged()
                    }

                    makeAddAddressClickEnable()
                } catch (e: Exception) {

                }
            }, {
                try {
                    progressAddress.visibility = View.GONE
                    makeAddAddressClickEnable()
                } catch (e: Exception) {

                }
            }, compositeDisposable)
        }
    }

    private fun makeAddAddressClickEnable() {
        compositeDisposable.add(RxView.clicks(imgAddAddress).throttleFirst(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                isSearchAddress = false
                if (MyProfileUtils.arrayMyAddressModel.size < 5) {
                    val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, MyPlaceUtils.placeFields).setCountry("AE").build(this)
                    startActivityForResult(intent, AUTO_COMPLETE_REQ_CODE)
                } else {
                    dialogMsg.showErrorRetryDialog(this, "Alert", "You cannot add more than 5 addresses in your address book", "OK", View.OnClickListener { dialogMsg.dismissDialog(this) })
                }
            })
    }


    private fun savePlaceModelOnLocal(placeModel: PlaceModel) {
        doAsync {
            try {
                val prefs = GetSetSharedPrefs(applicationContext)
                val placeDataSaved = prefs.getData("RecentPlaceSearches")
                var arrayPlaceData = ArrayList<PlaceModel>()
                val gson = Gson()

                if (placeDataSaved.trim().isEmpty()) {
                    arrayPlaceData.add(placeModel)
                } else {
                    arrayPlaceData = gson.fromJson(placeDataSaved, object :
                        TypeToken<List<PlaceModel>>() {}.type)
                    val isPlaceAlreadyThere = arrayPlaceData.any { it.placeId == placeModel.placeId && it.placeTitle == placeModel.placeTitle && it.placeAddress == placeModel.placeAddress }
                    Log.d("PlaceModel__", placeModel.toString())
                    Log.d("ArrayPlaceData__", arrayPlaceData.toString())
                    if (!isPlaceAlreadyThere) {
                        arrayPlaceData.add(placeModel)
                    }
                }

                Log.d("Before_Updating", "----------------------------------------")
                arrayPlaceData.forEach {
                    Log.d("Place_", "" + it)
                }

                Log.d("After_Updating", "------------------------------------------")


                if (arrayPlaceData.size > 4) {
                    arrayPlaceData.removeAt(0)
                }

                arrayPlaceData.forEach {
                    Log.d("Place_", "" + it)
                }

                val strJsonArrPlace = gson.toJson(arrayPlaceData)
                Log.d("PlaceJson_Local_Saved", strJsonArrPlace)
                prefs.putData("RecentPlaceSearches", strJsonArrPlace)


            } catch (e: Exception) {
                Log.d("ExcPlaceLocal", "" + e)
                GetSetSharedPrefs(applicationContext).putData("RecentPlaceSearches", "")
            }
        }

    }

    private fun fillRecentSearches() {
        val arrayRecentPlaces = getLocalRecentPlaceSearches()
        if (arrayRecentPlaces.size > 0) {
            relRecentSearches.visibility = View.VISIBLE
            recentAdapter = PlaceAddressRecentSearchAdapter(this, arrayRecentPlaces) { pos ->
                updateCurrentPlaceSelectedAndFinish(arrayRecentPlaces[pos])
            }
            rvRecentSearches.adapter = recentAdapter
        } else {
            relRecentSearches.visibility = View.GONE
        }
    }

    private fun getLocalRecentPlaceSearches(): ArrayList<PlaceModel> {
        try {
            var arrayPlaceModel = ArrayList<PlaceModel>()
            val prefs = GetSetSharedPrefs(this)
            val placeDataSaved = prefs.getData("RecentPlaceSearches")
            Log.d("PlaceJson_Local_Get", placeDataSaved)
            val gson = Gson()
            return if (placeDataSaved.trim().isEmpty()) {
                arrayPlaceModel
            } else {
                arrayPlaceModel = gson.fromJson(placeDataSaved, object :
                    TypeToken<List<PlaceModel>>() {}.type)
                arrayPlaceModel
            }
        } catch (e: Exception) {
            Log.d("ExcPlaceLocalFetch", "" + e)
            GetSetSharedPrefs(applicationContext).putData("RecentPlaceSearches", "")
        }
        return ArrayList<PlaceModel>()
    }

    private fun updateCurrentPlaceSelectedAndFinish(placeModel: PlaceModel) {
        MyPlaceUtils.placeModel = placeModel
        Log.d("MyPlaceSelected", MyPlaceUtils.placeModel.toString())
        DashboardActivity.refreshNearbyPlaces = true
        CampListingActivity.needToRefreshNearByCamps=true
        finish()
    }

}

package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    var compositeDisposable=CompositeDisposable()
    lateinit var dialogMsg:DialogMsg

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        LoginBasicsUi.setTransparentStatusBarAndBgImage(this, imgBg)
        Glide.with(applicationContext).load(R.drawable.ic_logo).into(imgLogo)

        compositeDisposable= CompositeDisposable()
        dialogMsg= DialogMsg()

        MyAppConfig.noOfAttempts=0

        fastFetchAppConfigValues()

    }

    private fun fastFetchAppConfigValues(){
        if(ConnectionDetector.isConnectingToInternet(applicationContext)){
            MyAppConfig.fastFetchAppConfigValues(applicationContext,false,null,{
                val bundle=intent.extras

                val prefs= GetSetSharedPrefs(applicationContext)

                if(prefs.getDataBoolean(StaticValues.KEY_APP_INTRO_SHOWN)) {
                    MyNavigations.goToMainActivity(this, bundle)
                }else {
                    MyNavigations.goToAppIntroActivity(this, bundle)
                }
            },{
                dialogMsg.showErrorApiDialog(this,"Retry",View.OnClickListener {
                    dialogMsg.dismissDialog(this)
                    fastFetchAppConfigValues()
                },false)
            },compositeDisposable)
        }else{
            dialogMsg.showErrorConnectionDialog(this,"Retry",View.OnClickListener {
                dialogMsg.dismissDialog(this)
                fastFetchAppConfigValues()
            },false)
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}

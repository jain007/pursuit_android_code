package com.pursueit.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.model.UserModel
import com.pursueit.utils.ErrorMsgs
import com.pursueit.utils.LoginBasicsUi
import com.pursueit.utils.MyNavigations
import kotlinx.android.synthetic.main.activity_unauthenticated.*

class UnauthenticatedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unauthenticated)

        LoginBasicsUi.setTransparentStatusBarAndBgImage(this, imgBg)

        LoginBasicsUi.resetAllUserTokensAndId(applicationContext)

        showSessionOutDialog()
    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.UNAUTHENTICATED_SCREEN, UserModel.getUserModel(applicationContext).userEmail,this::class.java)
    }

    private fun showSessionOutDialog(){
        val dialogErr=DialogMsg()
        dialogErr.showErrorRetryDialog(this,ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,"User id has been used to log into a different device. Please log in again to continue","OK",View.OnClickListener {  //""Your session is expired, please log in again to continue"
            dialogErr.dismissDialog(this)
            FastNetworking.isNavigatedToSessionExpireAlready=false
            MyNavigations.goToMainActivity(this)
            finishAffinity()
        },false)
    }

    override fun onBackPressed() {
        //super.onBackPressed()
    }
}

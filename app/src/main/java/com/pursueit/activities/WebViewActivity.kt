package com.pursueit.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.pursueit.R
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.model.UserModel
import kotlinx.android.synthetic.main.activity_web_view.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*

class WebViewActivity : AppCompatActivity() {

    var strTitle:String?=null
    var strUrl:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        try {

            strTitle=intent.extras.getString("Title")
            strUrl=intent.extras.getString("URL")

            //toolbar set
            toolbarFragment.txtToolbarHeader.text = "${intent.extras.getString("Title")}"
            toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
            toolbarFragment.imgBackNavigation.setOnClickListener { onBackPressed() }

            webView.loadUrl(strUrl)

        }catch (e:Exception){
            Log.d("ExcWebView",""+e)
        }
    }

    override fun onResume() {
        super.onResume()
        MyGoogleAnalytics.sendSession(this, MyGoogleAnalytics.WEB_VIEW_SCREEN+" - "+strTitle, UserModel.getUserModel(applicationContext).userEmail,this::class.java)
    }
}

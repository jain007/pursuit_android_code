package com.pursueit.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_activity_for_map.view.*

class ActivityForMapAdapter(
    val act: Activity,
    var arrayModel: ArrayList<ActivityModel>,
    var fromSimilarAct: Boolean = false,
    val fromPass: Boolean = false
) : RecyclerView.Adapter<ActivityForMapAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_activity_for_map, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrayModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            /*  if (fromSimilarAct)
              {
                  val layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT)
                  layoutParams.setMargins(0,0,20,0)
                  itemView.rootCardAct.layoutParams=layoutParams
              }*/

            /*itemView.txtSlotStatus.setOnClickListener {
                val model=arrayModel[adapterPosition]
                MyNavigations.goToActivityDetail(act, model, model.slotIndex)
            }*/

            itemView.linActivityForMap.setOnClickListener {
                val model = arrayModel[adapterPosition]

                if (model.activityType == ActivityModel.FLEXI_ACTIVITY) {
                    MyNavigations.goToFlexiActivityDetail(act, model, model.flexiIndex)
                } else {
                    MyNavigations.goToActivityDetail(act, model, model.slotIndex)
                }
            }
        }

        @SuppressLint("SetTextI18n")
        fun setContent(model: ActivityModel) {
            if (!fromPass) {
                PromotionOperation.setPromotionText(
                    itemView.linPromotion,
                    model.maxValuePromotionModel
                )
            } else {
                itemView.linPromotion.visibility = View.GONE
            }

            Glide.with(itemView)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_ACTIVITY + model.activityImageUrl)
                .into(itemView.imgActivity)
            itemView.txtActivityTitle.text = model.activityName.trim()
            itemView.txtActivitySubTitle.text = model.activityProviderName.trim()
            itemView.linRatingBarView.visibility =
                if (model.activityStarRatingCount > 0) View.VISIBLE else View.GONE
            itemView.linRatingBarView.isEnabled = false
            itemView.ratingActivity.rating = model.activityStarRating.toFloat()


            if (model.activityType == ActivityModel.FLEXI_ACTIVITY) {
                itemView.txtActivityPrice.visibility = View.VISIBLE
                try {
                    if (JsonParse.decimalFormat.format(model.similarFlexiModel!!.price.toDouble()).toDouble() > 0.0)
                        itemView.txtActivityPrice.text = "AED ${model.similarFlexiModel!!.price}"
                    else
                        itemView.txtActivityPrice.text = "Free"

                } catch (e: Exception) {
                    Log.e("PriceDisplay", e.toString())
                    itemView.txtActivityPrice.text = "AED ${model.similarFlexiModel!!.price}"
                }
            } else {
                if (model.isBooking) {
                    itemView.txtActivityPrice.visibility = View.VISIBLE
                    try {
                        if (JsonParse.decimalFormat.format(model.slotPricePayableForMap.toDouble()).toDouble() > 0.0)
                            itemView.txtActivityPrice.text = "AED ${model.slotDisplayPriceForMap}"
                        else
                            itemView.txtActivityPrice.text = "Free"

                    } catch (e: Exception) {
                        Log.e("PriceDisplay", e.toString())
                        itemView.txtActivityPrice.text = "AED ${model.slotDisplayPriceForMap}"
                    }
                } else {

                    itemView.txtActivityPrice.visibility = View.GONE
                }
            }
            /*itemView.txtActivityLocation.text=model.activityLocation
            if (model.startDate.trim()!="")
                itemView.txtActivityTime.text=model.startDate+","+model.slotTime
            else
                itemView.txtActivityTime.text=model.slotTime
            itemView.txtActivityAge.text=model.slotAge*/
            itemView.txtActivityStarCount.text = "(${model.activityStarRatingCount})"

            //  itemView.txtSlotStatus.text = model.slotStatusText

            itemView.txtStartDate.text = ""
            itemView.txtSession.text = ""
            itemView.txtActivityTime.text = ""

            if (model.activityType == ActivityModel.FLEXI_ACTIVITY) {
                itemView.txtSession.text = ResourceUtil.getSessionsString(act, model.similarFlexiModel?.numberOfSession?.toInt() ?: 0)


                itemView.txtActivityTime.text = "" //previously=> model.similarFlexiModel?.duration
            } else {

                //Earlier
                /*if (model.startDate.trim() != "")
                    itemView.txtStartDate.text = ChangeDateFormat.convertDate(
                        model.startDate,
                        "dd MMM yyyy",
                        "EEE, "
                    ) + "" + ChangeDateFormat.convertDate(
                        model.startDate,
                        "dd MMM yyyy",
                        "dd MMM"
                    )
                else {
                    itemView.txtStartDate.text = ""


                }*/

                itemView.txtStartDate.text = model.startDate
                Log.d("StartDate_Adapter", "" + model.startDate)

                if (model.startDate.trim().length == 3) {
                    itemView.txtStartDate.text = model.startDate + ", " + model.slotTime
                }
            }

            if (model.activityType != ActivityModel.FLEXI_ACTIVITY) {
                if (model.slotStatusText.equals("Available", true)) {
                    //   itemView.linHeader.backgroundResource = R.drawable.bg_full_slot_header_blue
                    //      itemView.txtSlotStatus.textColor= ContextCompat.getColor(itemView.context, R.color.colorAvailable)
                    //   itemView.imgHeaderIcon.visibility = View.VISIBLE
                    //     itemView.txtSlotTitle.gravity = Gravity.CENTER_VERTICAL
                    //     itemView.txtSlotTitle.setTextColor(Color.WHITE)
                    itemView.txtActivityPrice.visibility = View.VISIBLE

                } else {
                    //     itemView.linHeader.backgroundResource = R.drawable.bg_full_slot_header
                    //     itemView.imgHeaderIcon.visibility = View.GONE
                    //      itemView.txtSlotTitle.gravity = Gravity.CENTER
                    //     itemView.txtSlotTitle.setTextColor(ContextCompat.getColor(act, R.color.colorTextGreySubTitle))
                    itemView.txtActivityPrice.visibility = View.INVISIBLE
                }
            }

        }
    }


}
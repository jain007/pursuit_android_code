package com.pursueit.adapters

import android.app.Activity
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.pursueit.R
import com.pursueit.model.ActivitySlotModel
import com.pursueit.utils.JsonParse
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.row_activity_slots.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textColor
import java.util.concurrent.TimeUnit

class ActivitySlotsAdapter(var act: Activity, var arrayModel: ArrayList<ActivitySlotModel>
                           , var onSlotClicked: (pos:Int) -> Unit, var onMoreClicked: () -> Unit, val compositeDisposable: CompositeDisposable) : RecyclerView.Adapter<ActivitySlotsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_activity_slots, container, false))
    }

    override fun getItemCount(): Int {
        return if(arrayModel.size>4) 4 else arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.linWhole.backgroundResource = R.drawable.bg_full_slot
        v.txtSlotTitle.text = model.slotTitle
        v.txtSlotStatus.text = model.slotStatus
        //v.txtSlotPrice.text="AED ${model.slotPricePayable}"

        try {
            if (JsonParse.decimalFormat.format(model.slotPricePayable.toDouble()).toDouble() > 0.0)
                v.txtSlotPrice.text = "AED ${model.slotPricePayableToDisplay}"
            else
                v.txtSlotPrice.text = "Free"


        } catch (e: Exception) {
            v.txtSlotPrice.text = "AED ${model.slotPricePayableToDisplay}"
        }

        v.txtSlotAge.text=model.slotAge.trim()
        v.txtSlotTime.text=model.slotTime.trim()
        v.txtSlotLocation.text=model.slotLocation.trim()
        v.imgHeaderIcon.setImageResource(if(model.slotType.equals("group",true)) R.drawable.ic_user_group else R.drawable.ic_user_age)
        if (model.slotStatus.equals("Available", true)) {
            v.linHeader.backgroundResource = R.drawable.bg_full_slot_header_blue
            v.txtSlotStatus.textColor = ContextCompat.getColor(act, R.color.colorAvailable)
            v.imgHeaderIcon.visibility = View.VISIBLE
            v.txtSlotTitle.gravity = Gravity.CENTER_VERTICAL
            v.txtSlotTitle.setTextColor(Color.WHITE)
            v.txtSlotPrice.visibility = View.VISIBLE

        } else {
            v.linHeader.backgroundResource = R.drawable.bg_full_slot_header
            v.txtSlotStatus.textColor = ContextCompat.getColor(act, R.color.colorEnquire)
            v.imgHeaderIcon.visibility = View.GONE
            v.txtSlotTitle.gravity = Gravity.CENTER
            v.txtSlotTitle.setTextColor(ContextCompat.getColor(act, R.color.colorTextGreySubTitle))
            v.txtSlotPrice.visibility = View.INVISIBLE
        }

        //show more functionality UI
        v.linMoreOptions.visibility = if (arrayModel.size>4 && pos==itemCount-1) View.VISIBLE else View.GONE

        v.txtMoreOptionsSlots.text= ""+(arrayModel.size - 4)+" more options"

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        init {
            compositeDisposable.add(RxView.clicks(itemView.linMoreOptions).throttleFirst(800,TimeUnit.MILLISECONDS).subscribe {
                onMoreClicked()
            })
            //itemView.linMoreOptions.setOnClickListener { onMoreClicked() }

            compositeDisposable.add(RxView.clicks(itemView.linWhole).throttleFirst(800,TimeUnit.MILLISECONDS).subscribe {
                onSlotClicked(adapterPosition)
            })

            //itemView.linWhole.setOnClickListener { onSlotClicked(adapterPosition) }
        }
    }
}
package com.pursueit.adapters

import android.app.Activity
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.model.ActivityFlexiSessionModel
import com.pursueit.model.ActivitySlotModel
import com.pursueit.utils.JsonParse
import com.pursueit.utils.ResourceUtil
import kotlinx.android.synthetic.main.row_activity_slots_detail_page.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.textColor


class ActivitySlotsDetailAdapter(var act: Activity, var arrayModel: ArrayList<ActivitySlotModel>,val arrFlexiSession:ArrayList<ActivityFlexiSessionModel>,val isFlexiActivity:Boolean=false, var onSlotClicked: (pos: Int) -> Unit) :
    RecyclerView.Adapter<ActivitySlotsDetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_activity_slots_detail_page, container, false))
    }

    override fun getItemCount(): Int {
        return if (isFlexiActivity)
            arrFlexiSession.size
        else
            arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {

        if (isFlexiActivity)
            setFlexiSessionContent(holder.itemView,pos)
        else
            setContent(holder.itemView, pos)
    }

    private fun setFlexiSessionContent(v: View, pos: Int) {
        val model = arrFlexiSession[pos]
        v.txtSlotTitle.text = ResourceUtil.getSessionsString(v.context,model.numberOfSession.toInt())//v.context.resources.getQuantityString(R.plurals.session,model.numberOfSession.toInt(),model.numberOfSession.toInt()) //model.numberOfSession.trim()+" Session(s)"
        v.txtSlotStatus.text = "Flexible"//model.slotStatus.trim()
        v.txtSlotAge.text = model.ageRange.trim()
        v.txtSlotTime.text = model.duration.trim()
      //  v.txtNoOfSessions.text = "" + model.noOfEligibleSessions + " " + (if (model.noOfEligibleSessions > 1) "sessions" else "session")

        v.txtSlotType.text = "${model.sessionType.capitalize()} Activity"
        v.imgSlotType.imageResource = if (model.sessionType.equals("group", true)) R.drawable.ic_user_group else R.drawable.ic_user_age
        v.linGroup.visibility=View.VISIBLE

        if (v.txtSlotType.text.toString().trim().equals("- Activity", true)) {
            v.txtSlotType.text = "-"
            v.linGroup.visibility=View.GONE
        }

        try {
            if (JsonParse.decimalFormat.format(model.price.toDouble()).toDouble() > 0.0)
                v.txtSlotPrice.text = "AED ${model.price}"
            else
                v.txtSlotPrice.text = "Free"
        } catch (e: Exception) {
            v.txtSlotPrice.text = "AED ${model.price}"
        }

        v.txtSlotPrice.visibility = View.VISIBLE

        //rounded price display only, not for calculation
        if(model.priceRoundedToDisplay>0){
            v.txtSlotPrice.text = "AED ${model.priceRoundedToDisplay}"
        }

       /* if (model.slotStatus.equals("Available", true)) {
            v.txtSlotStatus.textColor = ContextCompat.getColor(act, R.color.colorAvailable)
            v.txtSlotPrice.visibility = View.VISIBLE
            v.txtNoOfSessions.visibility = View.VISIBLE
        } else {
            v.txtSlotStatus.textColor = ContextCompat.getColor(act, R.color.colorEnquire)
            v.txtSlotPrice.visibility = View.INVISIBLE
            v.txtNoOfSessions.visibility = View.INVISIBLE
        }*/

        v.txtLocation.text = model.location.trim()
        if (model.location.trim().isEmpty())
            v.txtLocation.text = "-"

        v.linItem.setOnClickListener {
            onSlotClicked(pos)
        }

    }


    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.txtSlotTitle.text = model.slotTitle.trim()
        v.txtSlotStatus.text = model.slotStatus.trim()
        v.txtSlotAge.text = model.slotAge.trim()
        v.txtSlotTime.text = model.slotTime.trim()
        v.txtNoOfSessions.text = "" + model.noOfEligibleSessions + " " + (if (model.noOfEligibleSessions > 1) "sessions" else "session")

        v.txtSlotType.text = "${model.slotType.capitalize()} Activity"
        v.imgSlotType.imageResource = if (model.slotType.equals("group", true)) R.drawable.ic_user_group else R.drawable.ic_user_age

        v.linGroup.visibility=View.VISIBLE
        if (v.txtSlotType.text.toString().trim().equals("- Activity", true)) {
            v.txtSlotType.text = "-"
            v.linGroup.visibility=View.GONE
        }

        try {
            if (JsonParse.decimalFormat.format(model.slotPricePayable.toDouble()).toDouble() > 0.0)
                v.txtSlotPrice.text = "AED ${model.slotPricePayableToDisplay}"
            else
                v.txtSlotPrice.text = "Free"
        } catch (e: Exception) {
            v.txtSlotPrice.text = "AED ${model.slotPricePayableToDisplay}"
        }

        if (model.slotStatus.equals("Available", true)) {
            v.txtSlotStatus.textColor = ContextCompat.getColor(act, R.color.colorAvailable)
            v.txtSlotPrice.visibility = View.VISIBLE
            v.txtNoOfSessions.visibility = View.VISIBLE
        } else {
            v.txtSlotStatus.textColor = ContextCompat.getColor(act, R.color.colorEnquire)
            v.txtSlotPrice.visibility = View.INVISIBLE
            v.txtNoOfSessions.visibility = View.INVISIBLE
        }

        v.txtLocation.text = model.slotLocation.trim()
        if (model.slotLocation.trim().isEmpty())
            v.txtLocation.text = "-"

        v.linItem.setOnClickListener {
            onSlotClicked(pos)
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
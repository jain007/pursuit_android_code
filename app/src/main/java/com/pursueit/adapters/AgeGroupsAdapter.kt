package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.AgeGroupModel
import kotlinx.android.synthetic.main.row_attendee.view.*


class AgeGroupsAdapter(var act: Activity,var arrayAgeModel:ArrayList<AgeGroupModel>, var checkChangeListener: (pos: Int, isChecked: Boolean) -> Unit) :
    RecyclerView.Adapter<AgeGroupsAdapter.ViewHolder>() {

    var onBind = true

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_attendee, container, false))
    }

    override fun getItemCount(): Int {
        return arrayAgeModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        onBind = true
        setContent(holder.itemView, pos)
        onBind = false
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayAgeModel[pos]
        v.rbAttendee.isChecked = model.groupIsSelected
        v.txtAttendeeAge.text = "${model.groupLabel}"
        v.txtAttendeeName.text = model.groupTitle

        v.rbAttendee.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!onBind)
                checkChangeListener(pos, isChecked)
        }

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.app.Activity
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.model.AppIntroModel
import kotlinx.android.synthetic.main.row_app_intro_pager.view.*
import java.util.*


class AppIntroPagerAdapter(
    internal var act: Activity,
    internal var arrayModel: ArrayList<AppIntroModel>
) : PagerAdapter() {

    internal var inflater: LayoutInflater =
        act.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    internal var position: Int = 0


    override fun getCount(): Int {
        return arrayModel.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = inflater.inflate(R.layout.row_app_intro_pager, container, false)
        initAndSet(v, position)
        container.addView(v)
        return v
    }

    private fun initAndSet(v: View, position: Int) {
        val model = arrayModel[position]

        if (model.imageRes != null)
            Glide.with(v).load(model.imageRes!!).into(v.imgAppIntro)

        v.txtAppIntro.text=model.title?.trim() ?: ""
    }


}

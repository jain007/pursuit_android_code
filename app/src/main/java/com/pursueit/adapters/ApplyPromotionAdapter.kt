package com.pursueit.adapters

import android.app.Activity
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.androidnetworking.error.ANError
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PromotionModel
import com.pursueit.utils.ConnectionDetector
import com.pursueit.utils.PromotionOperation
import com.pursueit.utils.StaticValues
import kotlinx.android.synthetic.main.row_apply_promocode.view.*
import org.jetbrains.anko.textColor
import org.json.JSONObject

class ApplyPromotionAdapter(val act: Activity,
                            val arrPromotionModel: ArrayList<PromotionModel>,
                            val currentOrderAmount: Double
                            ,
                            val getCalculationResult: (totalDiscountPrice: Double, discountedPrice: Double) -> Unit
                            ,
                            val txtPromoCodeResult: TextView) : RecyclerView.Adapter<ApplyPromotionAdapter.ViewHolder>() {
    val MAX_APPLICABLE_PROMOCODE = 2

    val arrPromotionAppliedIds = ArrayList<String>()
    val arrPromotionAppliedOfProvider = ArrayList<String>()
    val arrIsUsableWithOther = ArrayList<Boolean>()

    init {
        val arrSelectedPromotionModel = arrPromotionModel.filter { it.isApplied }
        arrSelectedPromotionModel.forEach {
            arrPromotionAppliedIds.add(it.promotionId)
            arrPromotionAppliedOfProvider.add(it.promotionCreatedBy)
            arrIsUsableWithOther.add(it.isUsableWithOther)
        }
    }

    companion object {

        /* fun clearSelectionArray()
         {
             arrPromotionAppliedIds.clear()
             arrPromotionAppliedOfProvider.clear()
             arrIsUsableWithOther.clear()
         }*/
    }

    private var dialogMsg = DialogMsg()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_apply_promocode, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrPromotionModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(arrPromotionModel[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.txtApply.setOnClickListener {
                val model = arrPromotionModel[adapterPosition]

                checkPromotion(model, applyPromoCode = {
                    model.isApplied = !model.isApplied

                    if (model.isApplied)
                        addPromotionIdAndFlag(model)
                    else
                        removePromotion(model)

                    notifyDataSetChanged()
                    //calculateFinalPromotionDiscount()
                    PromotionOperation.calculateFinalPromotionDiscount(currentOrderAmount, getCalculationResult, arrPromotionModel)
                }, onFailed = {
                    model.failedToApplyPromotion = true
                    notifyDataSetChanged()
                })
            }
        }

        fun setData(promotionModel: PromotionModel) {
            // user can apply more than 1 promo code
            // if user applies 2 promo code, one must be from admin and other must be from service provider
            // we will get userWithOther = 2 (No) always from service provider
            // but we can get userWithOther(1-Yes, 2- No) for admin's promotions

            itemView.txtPromoCode.text = promotionModel.promoCode
            if (promotionModel.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE) {
                itemView.txtCouponInfo.text = "Get " + PromotionOperation.getPromotionText(promotionModel) + " up to AED ${promotionModel.upToMaxAmount}"
            } else
                itemView.txtCouponInfo.text = "Get " + PromotionOperation.getPromotionText(promotionModel)

            if (promotionModel.promotionMinimumOrderAmount != "") {
                if(promotionModel.promotionMinimumOrderAmount!="0"){
                    itemView.txtMinimumOrderInfo.visibility =  View.VISIBLE
                    itemView.txtMinimumOrderInfo.text =  "on orders above AED ${promotionModel.promotionMinimumOrderAmount}"
                }else{
                    itemView.txtMinimumOrderInfo.visibility =  View.GONE
                }

            }



            if (promotionModel.failedToApplyPromotion) {
                itemView.txtApply.text = "Apply"
                itemView.txtApply.alpha = 0.5f
                itemView.txtApply.isEnabled = false
                itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)

                itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack), PorterDuff.Mode.SRC_IN)
            } else if (currentOrderAmount >= promotionModel.promotionMinimumOrderAmount.toDouble()) {
                //promo code can be applied
                if (promotionModel.isApplied) {
                    addPromotionIdAndFlag(promotionModel)
                    /*if (!arrPromotionAppliedIds.contains(promotionModel.promotionId))
                    {
                        arrPromotionAppliedIds.add(promotionModel.promotionId)
                    }*/
                    itemView.txtApply.alpha = 1f
                    itemView.txtApply.text = "Remove"
                    itemView.txtApply.isEnabled = true
                    itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorErrorRed)

                    itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorAccent)
                    itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorAccent), PorterDuff.Mode.SRC_IN)
                } else {
                    if (arrPromotionAppliedIds.size < MAX_APPLICABLE_PROMOCODE) {
                        if (arrIsUsableWithOther.contains(false)) {
                            itemView.txtApply.text = "Apply"
                            itemView.txtApply.alpha = 0.5f
                            itemView.txtApply.isEnabled = false
                            itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)

                            itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                            itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack), PorterDuff.Mode.SRC_IN)
                        } else if ((arrPromotionAppliedIds.size > 0 && promotionModel.isUsableWithOther) || arrPromotionAppliedIds.size == 0) {
                            // val provider=if (promotionModel.isUsableWithOther) PromotionModel.PURSUEIT_PROMOTION else PromotionModel.SERVICE_PROVIDER_PROMOTION

                            if (arrPromotionAppliedOfProvider.contains(promotionModel.promotionCreatedBy)) {
                                itemView.txtApply.text = "Apply"
                                itemView.txtApply.alpha = 0.5f
                                itemView.txtApply.isEnabled = false
                                itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)

                                itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                                itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack), PorterDuff.Mode.SRC_IN)
                            } else {
                                itemView.txtApply.alpha = 1f
                                itemView.txtApply.isEnabled = true
                                itemView.txtApply.text = "Apply"
                                itemView.txtApply.textColor =
                                    ContextCompat.getColor(itemView.context, R.color.colorSuccess)

                                itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorAccent)
                                itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorAccent), PorterDuff.Mode.SRC_IN)
                            }
                        } else {
                            itemView.txtApply.text = "Apply"
                            itemView.txtApply.alpha = 0.5f
                            itemView.txtApply.isEnabled = false
                            itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                            itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                            itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack), PorterDuff.Mode.SRC_IN)
                        }
                    } else {
                        itemView.txtApply.text = "Apply"
                        itemView.txtApply.alpha = 0.5f
                        itemView.txtApply.isEnabled = false
                        itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)

                        itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                        itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack), PorterDuff.Mode.SRC_IN)
                    }

                }
            } else {
                if (promotionModel.isApplied) {
                    addPromotionIdAndFlag(promotionModel)
                    /*if (!arrPromotionAppliedIds.contains(promotionModel.promotionId))
                    {
                        arrPromotionAppliedIds.add(promotionModel.promotionId)
                    }*/
                    itemView.txtApply.text = "Remove"
                    itemView.txtApply.alpha = 1f
                    itemView.txtApply.isEnabled = true
                    itemView.txtApply.textColor = ContextCompat.getColor(itemView.context, R.color.colorErrorRed)

                    itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorAccent)
                    itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorAccent), PorterDuff.Mode.SRC_IN)
                } else {
                    itemView.txtApply.alpha = 0.5f
                    itemView.txtApply.text = "Apply"
                    itemView.txtApply.isEnabled = false
                    itemView.txtApply.textColor =
                        ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)

                    itemView.txtPromoCode.textColor = ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack)
                    itemView.imgPromotion.setColorFilter(ContextCompat.getColor(itemView.context, R.color.colorTextDimBlack), PorterDuff.Mode.SRC_IN)
                }
            }
        }
    }

    fun addPromotionIdAndFlag(model: PromotionModel) {
        if (!arrPromotionAppliedIds.contains(model.promotionId)) {
            arrPromotionAppliedIds.add(model.promotionId)
            arrPromotionAppliedOfProvider.add(model.promotionCreatedBy)
            arrIsUsableWithOther.add(model.isUsableWithOther)
            /*if (model.isUsableWithOther)
                arrPromotionAppliedOfProvider.add(PromotionModel.PURSUEIT_PROMOTION)
            else
                arrPromotionAppliedOfProvider.add(PromotionModel.SERVICE_PROVIDER_PROMOTION)*/
        }
    }

    fun removePromotion(model: PromotionModel) {
        try {
            if (arrPromotionAppliedIds.contains(model.promotionId)) {
                arrPromotionAppliedIds.remove(model.promotionId)
                arrPromotionAppliedOfProvider.remove(model.promotionCreatedBy)
                arrIsUsableWithOther.remove(model.isUsableWithOther)
                /*if (model.isUsableWithOther)
                    arrPromotionAppliedOfProvider.remove(PromotionModel.PURSUEIT_PROMOTION)
                else
                    arrPromotionAppliedOfProvider.remove(PromotionModel.SERVICE_PROVIDER_PROMOTION)*/
            }
        } catch (e: Exception) {
            Log.e("removePromotion", e.toString())
        }
    }

    /* fun calculateFinalPromotionDiscount()
     {
         var totalDiscountPrice=0.0
         val arrAppliedPromotion=arrPromotionModel.filter { it.isApplied }
         if (arrAppliedPromotion.any{ it.offerType==PromotionModel.OFFER_TYPE_PERCENTAGE && it.offerType==PromotionModel.OFFER_TYPE_FLAT})
         {
             val percentPromotionModel=
                 arrAppliedPromotion.single { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }
             totalDiscountPrice+=calculatePromotion(currentOrderAmount,percentPromotionModel)

             val flatPromotionModel=arrAppliedPromotion.single { it.offerType==PromotionModel.OFFER_TYPE_FLAT }
             totalDiscountPrice+=calculatePromotion(currentOrderAmount,flatPromotionModel)
         }
         else if (arrAppliedPromotion.all { it.offerType==PromotionModel.OFFER_TYPE_PERCENTAGE })
         {
             arrAppliedPromotion.forEach {
                 totalDiscountPrice+=calculatePromotion(currentOrderAmount,it)
             }
         }
         else if (arrAppliedPromotion.all { it.offerType==PromotionModel.OFFER_TYPE_FLAT })
         {
             arrAppliedPromotion.forEach {
                 totalDiscountPrice+=calculatePromotion(currentOrderAmount,it)
             }
         }

         val refreshedDiscountPrice:Double
         if (totalDiscountPrice>=currentOrderAmount)
         {
             // 100% discount
             refreshedDiscountPrice=currentOrderAmount
         }
         else
         {
             refreshedDiscountPrice=totalDiscountPrice
         }
         val discountedPrice=currentOrderAmount-refreshedDiscountPrice

         getCalculationResult(refreshedDiscountPrice,discountedPrice)
     }

     private fun calculatePromotion(enteredAmount:Double, promotionModel:PromotionModel):Double
     {
         var discountValue:Double
             if (promotionModel.offerType==PromotionModel.OFFER_TYPE_PERCENTAGE)
             {
                 discountValue=enteredAmount*(promotionModel.offerValue.toFloat()/100.0f)
             }
             else{
                 discountValue=promotionModel.offerValue.toDouble()
             }
         Log.d("DiscountValue",discountValue.toString())
         if (discountValue>=promotionModel.upToMaxAmount.toDouble())
         {
             discountValue=promotionModel.upToMaxAmount.toDouble()
         }
         Log.d("DiscountValueAfterUpto",discountValue.toString())
         return discountValue
     }*/

    fun checkPromotion(promotionModel: PromotionModel,
                       applyPromoCode: () -> Unit,
                       onFailed: () -> Unit) {
        if (ConnectionDetector.isConnectingToInternet(act)) {
            DialogMsg.showPleaseWait(act)
            val hashParams = HashMap<String, String>()
            hashParams.put("promotionId", promotionModel.promotionId)
            FastNetworking.makeRxCallPost(act, Urls.CHECK_PROMOTION, true, hashParams, "Check_Promotion", object : FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(act)
                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                        // apply promo code
                        txtPromoCodeResult.visibility = View.GONE
                        applyPromoCode()
                    } else {
                        //show error
                        txtPromoCodeResult.text = json.getString("message")
                        txtPromoCodeResult.visibility = View.VISIBLE
                        onFailed()

                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(act)
                }
            })
        } else {
            dialogMsg.showErrorConnectionDialog(act, "OK", View.OnClickListener {
                dialogMsg.dismissDialog(act)
            })
        }
    }

}
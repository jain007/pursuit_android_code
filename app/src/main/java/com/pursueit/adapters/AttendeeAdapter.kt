package com.pursueit.adapters

import android.app.Activity
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.pursueit.R
import com.pursueit.activities.ActivityDetailActivity
import com.pursueit.activities.CampDetailActivity
import com.pursueit.activities.FlexiActivityDetailActivity
import com.pursueit.model.FamilyModel
import kotlinx.android.synthetic.main.row_attendee.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.toast


class AttendeeAdapter(
    var act: Activity,
    var arrayModel: ArrayList<FamilyModel>,
    var checkChangeListener: (pos: Int, isChecked: Boolean, adapter: AttendeeAdapter) -> Unit,
    var onEdit: (pos: Int) -> Unit) : RecyclerView.Adapter<AttendeeAdapter.ViewHolder>() {

    var onBind = true
    var MAX_SELECTION = 0
    var currentSelectionCount = 0
    var maxLimitReached = false

    init {
        if (act is CampDetailActivity) {
            val campAct = (act as CampDetailActivity)
            MAX_SELECTION =
                campAct.campModel!!.arrWeekModel.filter { it.isSelected }.maxBy { it.remainingSeats }!!.remainingSeats
        }
    }

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_attendee, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        onBind = true
        setContent(holder.itemView, pos)
        onBind = false

    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.rbAttendee.isChecked = model.isSelected
        v.txtAttendeeAge.text = model.familyMemberAge
        v.txtAttendeeName.text = model.familyMemberName

        v.chkAttendee.isChecked = model.isSelected

        v.rbAttendee.visibility = if (act is ActivityDetailActivity) View.VISIBLE else View.GONE

        v.rbAttendee.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!onBind && act is ActivityDetailActivity)
                checkChangeListener(pos, isChecked, this)
        }

        v.linItem.setOnClickListener {
            onEdit(pos)
        }

        if (act is CampDetailActivity || act is FlexiActivityDetailActivity || act is ActivityDetailActivity) {
            var ageFrom=0.0
            var ageTo=0.0
            if (act is CampDetailActivity ) {
                val campAct = act as CampDetailActivity
                ageFrom=campAct.campModel?.ageFrom?.toDouble() ?: 0.0
                ageTo=campAct.campModel?.ageTo?.toDouble() ?: 0.0
            }
            else if (act is FlexiActivityDetailActivity)
            {
                val flexiAct=act as FlexiActivityDetailActivity
                ageFrom=flexiAct.activityModel?.arrFlexiSession?.get(0)?.fromAge?.toDouble() ?: 0.0
                ageTo=flexiAct.activityModel?.arrFlexiSession?.get(0)?.toAge?.toDouble() ?: 0.0
            }
            else if (act is ActivityDetailActivity){
                try {
                    val actDet=act as ActivityDetailActivity
                    ageFrom=actDet.selectedSlotModel?.slotAgeFrom ?: 0.0
                    ageTo=actDet.selectedSlotModel?.slotAgeTo ?: 0.0
                }catch (e:Exception){
                    Log.e("attendeeDisable",e.toString())
                }
            }

            Log.d("FamilyAge","============================================")
            Log.d("FamilyAge","Attendee Name: ${model.familyMemberName} AttendeeAge: ${model.familyMemberAgeDouble} From: $ageFrom To: $ageTo")
            Log.d("FamilyAge","============================================")
            if (model.familyMemberAgeDouble in ageFrom..ageTo) {
                // enable selection
                //v.linRootAttendee.backgroundResource = R.drawable.bg_rounded_white_with_grey_boundary
                v.txtAttendeeName.alpha=1f
                v.txtAttendeeAge.alpha=1f
                v.imgAttendee.setColorFilter(ContextCompat.getColor(act.applicationContext,R.color.colorImageTint),PorterDuff.Mode.SRC_IN)
                if (act is ActivityDetailActivity){
                    v.rbAttendee.isEnabled = true
                    v.rbAttendee.buttonTintList=ContextCompat.getColorStateList(act.applicationContext,R.color.colorDiscount)
                }
                else {
                    v.chkAttendee.isEnabled = true
                    v.chkAttendee.buttonTintList = ContextCompat.getColorStateList(
                        act.applicationContext,
                        R.color.colorDiscount
                    )
                }
            } else {
                // disable selection
                //v.linRootAttendee.backgroundResource = R.drawable.disabled_attendee_item_bg
                v.txtAttendeeName.alpha=0.5f
                v.txtAttendeeAge.alpha=0.5f
                v.imgAttendee.setColorFilter(ContextCompat.getColor(act.applicationContext,R.color.colorDisabled),PorterDuff.Mode.SRC_IN)
                if (act is ActivityDetailActivity){
                    v.rbAttendee.isEnabled = false
                    v.rbAttendee.buttonTintList=ContextCompat.getColorStateList(act.applicationContext, R.color.colorDisabled)
                }
                else {
                    v.chkAttendee.isEnabled = false
                    v.chkAttendee.buttonTintList = ContextCompat.getColorStateList(act.applicationContext, R.color.colorDisabled)
                }
            }
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            if (act is CampDetailActivity || act is FlexiActivityDetailActivity) {
                itemView.chkAttendee.visibility = View.VISIBLE

                itemView.chkAttendee.setOnClickListener {
                    if (!maxLimitReached) {
                        if (itemView.chkAttendee.isChecked) {
                            ++currentSelectionCount
                        } else {

                            if (currentSelectionCount > 0)
                                --currentSelectionCount
                        }
                    }

                    arrayModel[adapterPosition].isSelected = itemView.chkAttendee.isChecked

                    checkChangeListener(adapterPosition, itemView.chkAttendee.isChecked, this@AttendeeAdapter)
                }



                if (act is CampDetailActivity ) //|| act is FlexiActivityDetailActivity
                {
                    itemView.chkAttendee.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                        override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                            if (p1) {
                                if (currentSelectionCount == MAX_SELECTION) {
                                    //itemView.context.toast("Maximum $MAX_SELECTION attendee can be selected")
                                    val message:String
                                    if (MAX_SELECTION>0) {
                                        message =
                                            "Oops, Only ${MAX_SELECTION} seats available for ${(act as CampDetailActivity).selectedDayOrWeekPriceModel?.sessionName} Camp."
                                    }else
                                    {
                                        message="Oops, No more seats available for ${(act as CampDetailActivity).selectedDayOrWeekPriceModel?.sessionName} Camp."
                                    }
                                    (act as CampDetailActivity).showErrorDialog(message) //"Only $MAX_SELECTION attendee can be selected"
                                    itemView.chkAttendee.isChecked = false
                                    maxLimitReached = true
                                } else {
                                    maxLimitReached = false
                                }
                            } else {
                                maxLimitReached = false
                            }
                        }

                    })
                }
            }


        }
    }
}
package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.pursueit.R
import com.pursueit.model.CampDiscountModel
import kotlinx.android.synthetic.main.row_camp_discount.view.*
import org.jetbrains.anko.backgroundResource

class CampDiscountAdapter(private val arrCampDiscountModel: ArrayList<CampDiscountModel>, val removeElevation:Boolean=false) : RecyclerView.Adapter<CampDiscountAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_camp_discount, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrCampDiscountModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrCampDiscountModel[pos])
    }

   inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            if (removeElevation) {
                itemView.cardDiscount.cardElevation = 0f
                itemView.linRootDiscount.backgroundResource=R.drawable.bg_camp_discount
                val layoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
                layoutParams.setMargins(27,0,27,15)
                itemView.linRootDiscount.layoutParams=layoutParams
               // itemView.divider.visibility=View.VISIBLE
            }
            else
            {
               // itemView.divider.visibility=View.GONE
            }

        }

        fun setContent(campDiscountModel: CampDiscountModel) {
            itemView.txtDiscountTitle.text = "GET ${campDiscountModel.discountPercent.replace(".00","")}% OFF"
            itemView.txtDiscountOn.text = campDiscountModel.discountOn
        }
    }

}
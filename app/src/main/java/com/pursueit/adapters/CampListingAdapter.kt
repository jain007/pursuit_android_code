package com.pursueit.adapters

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CampModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_camp_listing.view.*

class CampListingAdapter(val act:AppCompatActivity,private val arrCampModel: ArrayList<CampModel>, var showMoreListener: (pos: Int, progressShowMore: ProgressWheel) -> Unit) : RecyclerView.Adapter<CampListingAdapter.ViewHolder>() {

    var arrColorList =
        intArrayOf(R.drawable.drawable_tag1, R.drawable.drawable_tag2,R.drawable.drawable_tag3, R.drawable.drawable_tag4, R.drawable.drawable_tag5)

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_camp_listing, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrCampModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrCampModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
           // itemView.autoLabelCampTag.backgroundResource = R.drawable.bg_tag
           // itemView.autoLabelCampTag.isShowCross=false
           // itemView.autoLabelCampTag.backgroundColor=ContextCompat.getColor(itemView.context,android.R.color.transparent)
           //  itemView.rvTags.layoutManager=FlowLayoutManager()

            val userModel = UserModel.getUserModel(itemView.context)
            if (userModel.userId.trim().isEmpty())
            {
                itemView.sparkFavorite.visibility=View.GONE
            }

            val markFavOrUnFav=MarkFavoriteOrUnFavorite(act)

            itemView.relItem.setOnClickListener {
                MyNavigations.goToCampDetail(act,arrCampModel[adapterPosition].campId)
            }

            itemView.imgInviteIcon.setOnClickListener {
                MyAppConfig.shareApp(act,arrCampModel[adapterPosition].campName)
            }

            itemView.txtInvite.setOnClickListener {
                itemView.imgInviteIcon.performClick()
            }

            itemView.sparkFavorite.setOnClickListener {

                val model=arrCampModel[adapterPosition]
                markFavOrUnFav.fastMark(campId = model.campId,itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_CAMP
                    ,favUnFavFlag = if(model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE)

                arrCampModel[adapterPosition].isFavoriteMarked=!arrCampModel[adapterPosition].isFavoriteMarked
                itemView.sparkFavorite.playAnimation()
                itemView.sparkFavorite.isChecked=arrCampModel[adapterPosition].isFavoriteMarked


            }
        }

        fun setContent(campModel: CampModel) {

            PromotionOperation.setPromotionText(itemView.linPromotion,campModel.maxValuePromotionModel)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_CAMP + campModel.campImage).into(itemView.imgCamp)
            itemView.txtCampTitle.text = campModel.campName
            itemView.txtServiceProvider.text = campModel.serviceProviderName
            itemView.txtAddress.text = campModel.address
            itemView.txtStartingFromPrice.text = "AED " + ConvertToValidData.removeZeroAfterDecimal(campModel.startingPrice)
            itemView.imgDiscount.visibility = if (campModel.isDiscountAvailable) View.VISIBLE else View.INVISIBLE
            itemView.imgTransport.visibility = if (campModel.isTransportAvailable) View.VISIBLE else View.INVISIBLE
            itemView.txtAge.text = "Age: " + campModel.ageRageData
            itemView.txtTime.text = "Time: " + campModel.timeRangeData

            val flowLayoutManager=FlowLayoutManager()
            flowLayoutManager.isAutoMeasureEnabled=true
            itemView.rvTags.layoutManager=flowLayoutManager
            itemView.rvTags.adapter=RecyclerTagAdapter(campModel.arrTagList)

            itemView.sparkFavorite.isChecked=campModel.isFavoriteMarked
           /* itemView.autoLabelCampTag.clear()
            repeat(campModel.arrTagList.size)
            {
                itemView.autoLabelCampTag.addLabel(campModel.arrTagList[it])

                itemView.autoLabelCampTag.getLabel(it).backgroundResource=arrColorList[it % 5]
              *//*  itemView.autoLabelCampTag.getLabel(it).backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(itemView.context, arrColorList[it % 5]))*//*
                //itemView.autoLabelCampTag.getLabel(0).backgroundColor=ContextCompat.getColor(itemView.context, arrColorList[it % 5])
                    //ColorStateList.valueOf(ContextCompat.getColor(itemView.context, arrColorList[it % 5]))
            }*/

            if (adapterPosition==arrCampModel.size-1)
                showMoreListener(adapterPosition,itemView.progressShowMore)
        }
    }

}
package com.pursueit.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.activities.CampDetailActivity
import com.pursueit.model.DayOrWeekPriceModel
import kotlinx.android.synthetic.main.row_schedule_discount_selection.view.*
import org.jetbrains.anko.backgroundColor

class CampSelectionAdapter(val act:CampDetailActivity,private val arrDayOrWeekPriceModel: ArrayList<DayOrWeekPriceModel>) :
    RecyclerView.Adapter<CampSelectionAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_schedule_discount_selection, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrDayOrWeekPriceModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrDayOrWeekPriceModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
          /*  itemView.relSelection.setOnClickListener {
                itemView.chkDiscountSelection.performClick()
            }

            itemView.chkDiscountSelection.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                    arrCampSelectionModel[adapterPosition].isSelected=p1
                }
            })*/

            itemView.linSelection.setOnClickListener {
                act.setSelectedScheduleModelAndMakeChanges(arrDayOrWeekPriceModel[adapterPosition])
             //   act.showCampPaymentDialog()

                act.dismissedViaScheduleSelection=true
                if (act.bottomSheetDialogForScheduleSelection?.isShowing?:false)
                    act.bottomSheetDialogForScheduleSelection?.dismiss()

               // act.showBottomSheetDialogForWeekSelection()
            }
        }

        fun setContent(dayOrWeekPriceModel: DayOrWeekPriceModel) {
            itemView.txtTitle.text = dayOrWeekPriceModel.sessionName
            itemView.txtSelectionPrice.text = "AED ${dayOrWeekPriceModel.price}"
          //  itemView.chkDiscountSelection.isChecked=campSelectionModel.isSelected

        /*    if (adapterPosition%2!=0)
                itemView.linSelection.backgroundColor=ContextCompat.getColor(itemView.context,R.color.colorSelectionBg)
            else
                itemView.linSelection.backgroundColor=ContextCompat.getColor(itemView.context,R.color.colorWhite)*/

            if (adapterPosition==arrDayOrWeekPriceModel.size-1)
            {
                itemView.divider.visibility=View.GONE
            }
            else
            {
                itemView.divider.visibility=View.VISIBLE
            }
        }
    }

}
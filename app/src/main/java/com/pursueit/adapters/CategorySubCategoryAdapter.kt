package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.CategoryModel
import kotlinx.android.synthetic.main.row_cat_sub_cat_search.view.*


class CategorySubCategoryAdapter(var act: Activity, var arrayModel: ArrayList<CategoryModel>, var onSubCatClick: (pos: Int) -> Unit) : RecyclerView.Adapter<CategorySubCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_cat_sub_cat_search, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
//        v.txtSubCat.text=model.subCatName.trim()
        v.relItem.setOnClickListener {
            onSubCatClick(pos)
        }

        if (pos>0) {
            val prevModel=arrayModel[pos-1]
            if(model.catId==prevModel.catId){
                v.txtCatTitle.visibility = View.GONE
            }else{
                v.txtCatTitle.text = model.catName.trim()
                v.txtCatTitle.visibility = View.VISIBLE
            }
        } else {
            v.txtCatTitle.text = model.catName.trim()
            v.txtCatTitle.visibility = View.VISIBLE
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
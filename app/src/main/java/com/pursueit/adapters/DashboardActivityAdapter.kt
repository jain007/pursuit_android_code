package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CategoryModel
import com.pursueit.utils.MyGlideOptions
import kotlinx.android.synthetic.main.row_dash_activities.view.*

class DashboardActivityAdapter(
    var act: Activity,
    var arrayModel: ArrayList<CategoryModel>,
    var onCatClicked: (pos: Int) -> Unit
) : RecyclerView.Adapter<DashboardActivityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(act).inflate(R.layout.row_dash_activities, container, false)
        )
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.txtActivity.text = model.catName.trim()
        if (model.catImageDrawable == null) {
            Glide.with(v)
                .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions())
                .load(Urls.IMAGE_URL_CATEGORY + model.catImageUrl)
                .into(v.imgActivity)
        } else {
            Glide.with(v)
                .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions())
                .load(model.catImageDrawable!!)
                .into(v.imgActivity)
        }
        v.linItem.setOnClickListener { onCatClicked(pos) }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CampModel
import com.pursueit.utils.MyGlideOptions
import com.pursueit.utils.MyNavigations
import com.pursueit.utils.PromotionOperation
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_dash_camp.view.*

class DashboardCampAdapter(val act:Activity,private val arrCampModel:ArrayList<CampModel>, val onCampClicked:(pos:Int)->Unit) : RecyclerView.Adapter<DashboardCampAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.row_dash_camp,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrCampModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setContent(arrCampModel[position])
    }

   inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.linCampItem.setOnClickListener {
                MyNavigations.goToCampDetail(act,arrCampModel[adapterPosition].campId)
            }
        }

        fun setContent(campModel: CampModel)
        {
            PromotionOperation.setPromotionText(itemView.linPromotion,campModel.maxValuePromotionModel)

            itemView.txtCamp.text=campModel.campName
            Log.d("camp_image_url",Urls.IMAGE_FEATURE_CAMP+campModel.campImage)
            Glide.with(itemView.context)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_CAMP+campModel.campImage).into(itemView.imgCamp)
        }
    }

}
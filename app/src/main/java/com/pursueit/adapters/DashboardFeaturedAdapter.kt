package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.FeaturedPartnerModel
import com.pursueit.utils.MyGlideOptions
import com.pursueit.utils.MyNavigations
import kotlinx.android.synthetic.main.row_dash_featured_partners.view.*


class DashboardFeaturedAdapter(var act: Activity, var arrayModel: ArrayList<FeaturedPartnerModel>) :
    RecyclerView.Adapter<DashboardFeaturedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_dash_featured_partners, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        Glide.with(v)
            .applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(isCenterCrop = false))
            .load(Urls.IMAGE_URL_PARTNER + model.partnerImageUrl).into(v.imgFeaturedPartner)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        init {
            itemView.imgFeaturedPartner.setOnClickListener {
                MyNavigations.gotoProviderDetailActivity(act,arrayModel[adapterPosition].partnerId)
            }
        }
    }
}
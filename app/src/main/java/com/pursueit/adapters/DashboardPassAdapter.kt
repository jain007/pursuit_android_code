package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PassModel
import com.pursueit.model.SearchModel
import com.pursueit.utils.PromotionOperation
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_dash_pass.view.*

class DashboardPassAdapter(var act: DashboardActivity, var arrayModel: ArrayList<PassModel>) : RecyclerView.Adapter<DashboardPassAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_dash_pass, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView,pos)
    }

    private fun setContent(v:View,pos: Int) {
        val model=arrayModel[pos]
      //  PromotionOperation.setPromotionText(v.linPromotion,model.maxValuePromotionModel)

        Glide.with(v).load(Urls.IMAGE_FEATURE_ACTIVITY+model.passImageUrl).into(v.imgPass)
        v.txtPassActivity.text=model.passName
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        init {
            itemView.linPromotion.visibility=View.GONE
            itemView.linPass.setOnClickListener {
                val searchModel=SearchModel()
                val passModel=arrayModel[adapterPosition]
                searchModel.activityId=passModel.passId
                searchModel.name=passModel.passName
                searchModel.catId=passModel.passCatId
                searchModel.subCatId=passModel.passSubCatId
                DashboardActivity.filterActivitiesOnSearchClick(act, searchModel,fromPass = true)
            }
        }
    }
}
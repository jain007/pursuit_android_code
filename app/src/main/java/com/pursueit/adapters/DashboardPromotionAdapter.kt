package com.pursueit.adapters

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PromotionModel
import com.pursueit.utils.MyGlideOptions
import com.pursueit.utils.PromotionOperation
import kotlinx.android.synthetic.main.row_dash_promotions.view.*


class DashboardPromotionAdapter(var act: AppCompatActivity, var arrayModel: ArrayList<PromotionModel>) : RecyclerView.Adapter<DashboardPromotionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_dash_promotions, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView,pos)
    }

    private fun setContent(v:View,pos: Int) {
        val model=arrayModel[pos]
        Glide.with(v.context)
            .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
            .load(Urls.IMAGE_PROMOTION+model.promotionImage).into(v.imgPromotion)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        init {
            itemView.linPromotion.setOnClickListener {
                val model = arrayModel[adapterPosition]
                PromotionOperation.setPromotionClickListenerForListing(act,model)
            }

            /*itemView.linPromotion.setOnClickListener {
                val model=arrayModel[adapterPosition]
                if ((model.catId!="" && model.catId!="null")  || (model.activityId!="" && model.activityId!="null"))
                {
                    // go to act listing(explore)
                    DashboardActivity.filterActivitiesOnPromotion(act,model)
                }
                else if (model.campId!="" && model.campId!="null")
                {
                    // go to camp listing
                    DashboardActivity.filterCampOnPromotion(act,model)

                }
                else {
                    // goto explore as default
                    if (act is DashboardActivity)
                    {
                        DashboardActivity.showAllAct(act as DashboardActivity)
                    }
                }
            }*/
        }
    }
}
package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pursueit.R
import com.pursueit.model.FamilyModel
import kotlinx.android.synthetic.main.row_family_member.view.*


class FamilyMemberAdapter(var act: Activity, var arrayModel: ArrayList<FamilyModel>, var onEdit:(pos:Int)->Unit,var onDelete:(pos:Int)->Unit) : RecyclerView.Adapter<FamilyMemberAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_family_member, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {

        val model = arrayModel[pos]
        v.txtAttendeeAge.text = model.familyMemberAge
        v.txtAttendeeName.text = model.familyMemberName

        v.linEdit.setOnClickListener {
            onEdit(pos)
        }

        v.linDelete.setOnClickListener {
            onDelete(pos)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
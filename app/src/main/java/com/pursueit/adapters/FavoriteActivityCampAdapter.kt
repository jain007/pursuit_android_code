package com.pursueit.adapters

import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.ActivitySlotsActivity
import com.pursueit.fragments.MyFavoritesFragment
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.model.CampModel
import com.pursueit.model.FavoriteModel
import com.pursueit.utils.*
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_camp_listing.view.*
import kotlinx.android.synthetic.main.row_camp_listing.view.imgInviteIcon
import kotlinx.android.synthetic.main.row_camp_listing.view.progressShowMore
import kotlinx.android.synthetic.main.row_camp_listing.view.relItem
import kotlinx.android.synthetic.main.row_camp_listing.view.txtInvite
import kotlinx.android.synthetic.main.row_nearby_activity.view.*
import org.jetbrains.anko.startActivity

class FavoriteActivityCampAdapter(
    val act: AppCompatActivity, private val arrFavoriteActivityCampModel: ArrayList<FavoriteModel>
    , val rootView: View, val myFavoritesFragment: MyFavoritesFragment, var showMoreListener: (pos: Int, progressShowMore: ProgressWheel) -> Unit
    , val compositeDisposable:CompositeDisposable) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var viewPool: RecyclerView.RecycledViewPool? = null
    var viewPoolFlexi:RecyclerView.RecycledViewPool?=null
    var recyclerView: RecyclerView? = null

    companion object {
        const val CAMP = 1
        const val ACTIVITY = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            CAMP -> {
                CampViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_camp_listing, parent, false))
            }
            else -> { //activity
                ActivityViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.row_nearby_activity,
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return arrFavoriteActivityCampModel.size
    }

    override fun getItemViewType(position: Int): Int {
        val model = arrFavoriteActivityCampModel[position]
        return if (model.activityModel == null) {
            CAMP
        } else {
            ACTIVITY
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = arrFavoriteActivityCampModel[position]
        val viewType = if (model.activityModel == null) CAMP else ACTIVITY
        if (viewType == CAMP) {
            (holder as CampViewHolder).setContent(model.campModel!!)
        } else if (viewType == ACTIVITY) {
            if (viewPool == null)
                viewPool = RecyclerView.RecycledViewPool()
            if (viewPoolFlexi==null)
                viewPoolFlexi=RecyclerView.RecycledViewPool()
            (holder as ActivityViewHolder).setContent(position)
        }
    }

    inner class CampViewHolder(parent: View) : RecyclerView.ViewHolder(parent) {
        init {
            // itemView.autoLabelCampTag.backgroundResource = R.drawable.bg_tag
            // itemView.autoLabelCampTag.isShowCross=false
            // itemView.autoLabelCampTag.backgroundColor=ContextCompat.getColor(itemView.context,android.R.color.transparent)
            //  itemView.rvTags.layoutManager=FlowLayoutManager()

            val markFavOrUnFav = MarkFavoriteOrUnFavorite(act)

            itemView.relItem.setOnClickListener {
                val model = arrFavoriteActivityCampModel[adapterPosition].campModel!!
                MyNavigations.goToCampDetail(act, model.campId)
            }

            itemView.txtServiceProvider.setOnClickListener {
                MyNavigations.gotoProviderDetailActivity(act,arrFavoriteActivityCampModel[adapterPosition].campModel?.providerId?:"")
            }

            itemView.imgInviteIcon.setOnClickListener {
                val model = arrFavoriteActivityCampModel[adapterPosition].campModel!!
                MyAppConfig.shareApp(act, model.campName)
            }

            itemView.txtInvite.setOnClickListener {
                itemView.imgInviteIcon.performClick()
            }

            itemView.sparkFavorite.setOnClickListener {

                val mainModel = arrFavoriteActivityCampModel[adapterPosition]
                val position = adapterPosition
                val model = arrFavoriteActivityCampModel[adapterPosition].campModel!!
                if (model.isFavoriteMarked) {
                    arrFavoriteActivityCampModel.removeAt(adapterPosition)
                    notifyItemRemoved(adapterPosition)
                }
                if (arrFavoriteActivityCampModel.size==0)
                {
                    recyclerView?.visibility = View.GONE
                    myFavoritesFragment.noRecordUi.showNoFavorites()
                }

                var clickedOnUndo = false
                val snackBar =
                    Snackbar.make(rootView, "Removed from favourites", Snackbar.LENGTH_LONG).setAction("Undo",
                        object : View.OnClickListener {
                            override fun onClick(p0: View?) {
                                clickedOnUndo = true
                                arrFavoriteActivityCampModel.add(position, mainModel)
                                notifyItemInserted(position)
                                recyclerView?.smoothScrollToPosition(position)
                                recyclerView?.visibility = View.VISIBLE
                                myFavoritesFragment.noRecordUi.hideEmptyUi()
                            }

                        })
                snackBar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        if (!clickedOnUndo) // auto dismissed
                        {
                            markFavOrUnFav.fastMark(
                                campId = model.campId,
                                itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_CAMP,
                                favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE
                                ,fromFavouriteListing = true)
                            if (!model.isFavoriteMarked) {
                                arrFavoriteActivityCampModel[adapterPosition].campModel!!.isFavoriteMarked =
                                    !arrFavoriteActivityCampModel[adapterPosition].campModel!!.isFavoriteMarked
                                itemView.sparkFavorite.playAnimation()
                                itemView.sparkFavorite.isChecked =
                                    arrFavoriteActivityCampModel[adapterPosition].campModel!!.isFavoriteMarked
                            }

                        }
                    }

                    override fun onShown(sb: Snackbar?) {
                        super.onShown(sb)
                    }

                })
                snackBar.show()


            }
        }

        fun setContent(campModel: CampModel) {

            PromotionOperation.setPromotionText(itemView.linPromotion,campModel.maxValuePromotionModel)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_CAMP + campModel.campImage).into(itemView.imgCamp)
            itemView.txtCampTitle.text = campModel.campName
            itemView.txtServiceProvider.text = campModel.serviceProviderName
            itemView.txtAddress.text = campModel.address

            //itemView.txtStartingFromPrice.text = "AED " + campModel.startingPrice.replace(".0", "")
            itemView.txtStartingFromPrice.text = "AED " + ConvertToValidData.removeZeroAfterDecimal(campModel.startingPrice)
            itemView.imgDiscount.visibility = if (campModel.isDiscountAvailable) View.VISIBLE else View.INVISIBLE
            itemView.imgTransport.visibility = if (campModel.isTransportAvailable) View.VISIBLE else View.INVISIBLE
            itemView.txtAge.text = "Age: " + campModel.ageRageData
            itemView.txtTime.text = "Time: " + campModel.timeRangeData

            val flowLayoutManager = FlowLayoutManager()
            flowLayoutManager.isAutoMeasureEnabled = true
            itemView.rvTags.layoutManager = flowLayoutManager
            itemView.rvTags.adapter = RecyclerTagAdapter(campModel.arrTagList)

            itemView.sparkFavorite.isChecked = campModel.isFavoriteMarked
            /* itemView.autoLabelCampTag.clear()
             repeat(campModel.arrTagList.size)
             {
                 itemView.autoLabelCampTag.addLabel(campModel.arrTagList[it])

                 itemView.autoLabelCampTag.getLabel(it).backgroundResource=arrColorList[it % 5]
               *//*  itemView.autoLabelCampTag.getLabel(it).backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(itemView.context, arrColorList[it % 5]))*//*
                //itemView.autoLabelCampTag.getLabel(0).backgroundColor=ContextCompat.getColor(itemView.context, arrColorList[it % 5])
                    //ColorStateList.valueOf(ContextCompat.getColor(itemView.context, arrColorList[it % 5]))
            }*/

            if (adapterPosition == arrFavoriteActivityCampModel.size - 1)
                showMoreListener(adapterPosition, itemView.progressShowMore)
        }
    }

    inner class ActivityViewHolder(val v: View) : RecyclerView.ViewHolder(v) {

        init {
            val markFavOrUnFav = MarkFavoriteOrUnFavorite(act)

            itemView.txtNearbySubTitle.setOnClickListener {
                MyNavigations.gotoProviderDetailActivity(act,arrFavoriteActivityCampModel[adapterPosition].activityModel?.activityProviderId?:"")
            }

            itemView.sparkFavoriteAct.setOnClickListener {
                val mainModel = arrFavoriteActivityCampModel[adapterPosition]
                val model = mainModel.activityModel!!
                val position = adapterPosition
                if (model.isFavoriteMarked) {
                    arrFavoriteActivityCampModel.removeAt(adapterPosition)
                    notifyItemRemoved(adapterPosition)
                }
                if (arrFavoriteActivityCampModel.size==0)
                {
                    recyclerView?.visibility = View.GONE
                    myFavoritesFragment.noRecordUi.showNoFavorites()
                }

                var isUndoClicked = false
                val snackbar = Snackbar.make(rootView, "Removed from favourites", Snackbar.LENGTH_LONG)
                    .setAction("Undo") {
                        isUndoClicked = true
                        arrFavoriteActivityCampModel.add(position,mainModel)
                        notifyItemInserted(position)
                        recyclerView?.visibility = View.VISIBLE
                        recyclerView?.smoothScrollToPosition(position)
                        myFavoritesFragment.noRecordUi.hideEmptyUi()
                    }
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        if (!isUndoClicked) {
                            markFavOrUnFav.fastMark(
                                activityId = model.activityId,
                                itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_ACTIVITY,
                                favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE
                                ,fromFavouriteListing = true)

                            if (!model.isFavoriteMarked) {
                                arrFavoriteActivityCampModel[adapterPosition].activityModel!!.isFavoriteMarked =
                                    !arrFavoriteActivityCampModel[adapterPosition].activityModel!!.isFavoriteMarked
                                itemView.sparkFavoriteAct.playAnimation()
                                itemView.sparkFavoriteAct.isChecked =
                                    arrFavoriteActivityCampModel[adapterPosition].activityModel!!.isFavoriteMarked
                            }
                        }
                    }
                })
                snackbar.show()
            }
        }

        fun setContent(pos: Int) {
            if (arrFavoriteActivityCampModel.size < 1)
                return

            val model = arrFavoriteActivityCampModel[pos].activityModel!!

            if (model.isPassActivity)
                itemView.linPromotion.visibility=View.GONE
            else {
                PromotionOperation.setPromotionText(
                    itemView.linPromotion,
                    model.maxValuePromotionModel
                )
            }

            v.txtNearbyTitle.text = model.activityName.trim()
            v.txtNearbySubTitle.text = model.activityProviderName.trim()
            v.txtLocation.text = (model.activityLocation.trim() + "\n" + model.activityMsgLocationSuffix.trim()).trim()

            v.ratingBar.rating = model.activityStarRating.toFloat()
            v.txtStarCount.text = "(${model.activityStarRatingCount})"

            v.linRatingBarView.visibility = if (model.activityStarRatingCount > 0) View.VISIBLE else View.GONE
            v.linRatingBarView.isEnabled = false

            /*Log.d("Name: ", model.activityName)
            Log.d("Id: ", model.activityId)*/
            Glide.with(v).applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_ACTIVITY + model.activityImageUrl).into(v.imgActivity)

            v.sparkFavoriteAct.isChecked = model.isFavoriteMarked

            /* v.imgLike.setImageResource(if (pos % 2 == 0) R.drawable.ic_like else R.drawable.ic_unlike)
             v.imgLike.setColorFilter(ContextCompat.getColor(act, if (pos % 2 == 0) R.color.colorMainOrange else R.color.colorTextDimBlack))*/
            if (model.activityType == ActivityModel.NORMAL_ACTIVITY || model.activityType == ActivityModel.SINGLE_ACTIVITY) { // for normal activities (term, single)
                v.rvSlots.visibility = View.VISIBLE
                v.rvSession.visibility = View.GONE
                v.rvSlots.apply {
                    if (layoutManager == null) {
                        layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
                        setHasFixedSize(true)
                    }

                    setRecycledViewPool(viewPool)

                    adapter = ActivitySlotsAdapter(act, model.arraySlotModel, { position ->
                        MyNavigations.goToActivityDetail(act, model, position)
                        //NearbyActivitiesListingFragment.lastClickedPosition=pos
                    }, {
                        act.startActivity<ActivitySlotsActivity>("ActivityModel" to model)
                        //NearbyActivitiesListingFragment.lastClickedPosition=pos
                    },compositeDisposable)
                }
            }
            else
            {     //flexi activity

                v.rvSlots.visibility = View.GONE
                v.rvSession.visibility = View.VISIBLE
                v.rvSession.apply {
                    if (layoutManager == null) {
                        layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
                        setHasFixedSize(true)
                    }
                    setRecycledViewPool(viewPoolFlexi)
                    adapter = FlexiSessionAdapter(model.arrFlexiSession,pos, { position,parentPosition ->
                        //Go to flexi detail activity
                        MyNavigations.goToFlexiActivityDetail(act, model, position)
                        // MyNavigations.goToActivityDetail(act, model, position)
                        //NearbyActivitiesListingFragment.lastClickedPosition=pos
                    }, {
                        act.startActivity<ActivitySlotsActivity>("ActivityModel" to model, "isFlexiActivity" to true)
                        //NearbyActivitiesListingFragment.lastClickedPosition=pos
                    })
                }
            }



            /*v.relItem.setOnClickListener {
                //MyNavigations.goToActivityDetail(act, model, 0)
            }*/

            v.progressShowMore.visibility = View.GONE

            if (pos == itemCount - 1) {
                showMoreListener(pos, v.progressShowMore)
            }

            v.imgInviteIcon.setOnClickListener {
                MyAppConfig.shareApp(act, model.activityName)
            }

            v.txtInvite.setOnClickListener {
                MyAppConfig.shareApp(act, model.activityName)
            }
        }
    }

}
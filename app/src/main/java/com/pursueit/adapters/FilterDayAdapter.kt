package com.pursueit.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.FilterDayModel
import kotlinx.android.synthetic.main.row_filter_day.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textColor

class FilterDayAdapter(public val arrFilterDayModel: ArrayList<FilterDayModel>): RecyclerView.Adapter<FilterDayAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.row_filter_day,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrFilterDayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrFilterDayModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.txtFilterDay.setOnClickListener {
                arrFilterDayModel[adapterPosition].status=!arrFilterDayModel[adapterPosition].status
                notifyItemChanged(adapterPosition)
            }
        }

        fun setContent(filterDayModel: FilterDayModel)
        {
            itemView.txtFilterDay.text=filterDayModel.dayName.substring(0,2)
            if (filterDayModel.status)
            {
                itemView.txtFilterDay.backgroundResource=R.drawable.circle_orange
                itemView.txtFilterDay.textColor=ContextCompat.getColor(itemView.context,android.R.color.white)
            }
            else
            {
                itemView.txtFilterDay.backgroundResource=R.drawable.bg_circle_grey
                itemView.txtFilterDay.textColor=ContextCompat.getColor(itemView.context,R.color.colorTextBlack)
            }
        }
    }

}
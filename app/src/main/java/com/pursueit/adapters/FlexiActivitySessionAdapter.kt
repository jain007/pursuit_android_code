package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.activities.FlexiActivityDetailActivity
import com.pursueit.model.ActivityFlexiSessionModel
import com.pursueit.utils.ResourceUtil
import kotlinx.android.synthetic.main.row_schedule_discount_selection.view.*

class FlexiActivitySessionAdapter(val act:FlexiActivityDetailActivity, private val arrSessionModel:ArrayList<ActivityFlexiSessionModel>) : RecyclerView.Adapter<FlexiActivitySessionAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, itemType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_schedule_discount_selection, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrSessionModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrSessionModel[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.linSelection.setOnClickListener {
                act.setSelectedSessionModelAndMakeChanges(arrSessionModel[adapterPosition])
                //   act.showCampPaymentDialog()

                act.dismissedViaSessionSelection=true
                if (act.dialogSessionSelection?.isShowing?:false)
                    act.dialogSessionSelection?.dismiss()

                act.showFlexiActivityPaymentDialog()
            }
        }

        fun bind(flexiSessionModel: ActivityFlexiSessionModel)
        {
            itemView.txtTitle.text = ResourceUtil.getSessionsString(itemView.context,flexiSessionModel.numberOfSession.toInt())//itemView.context.resources.getQuantityString(R.plurals.session,flexiSessionModel.numberOfSession.toInt(),flexiSessionModel.numberOfSession.toInt()) //flexiSessionModel.numberOfSession+" Session(s)"
            itemView.txtSelectionPrice.text = "AED ${flexiSessionModel.price}"

            if (adapterPosition==arrSessionModel.size-1)
            {
                itemView.divider.visibility=View.GONE
            }
            else
            {
                itemView.divider.visibility=View.VISIBLE
            }
        }
    }

}
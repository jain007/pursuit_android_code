package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.ActivityFlexiSessionModel
import com.pursueit.utils.JsonParse
import com.pursueit.utils.ResourceUtil
import kotlinx.android.synthetic.main.row_flexi_session.view.*

class FlexiSessionAdapter(
    private val arrFlexiSessionModel: ArrayList<ActivityFlexiSessionModel>,
    val parentPosition: Int,
    var onSlotClicked: (pos: Int, parentPosition: Int) -> Unit,
    var onMoreClicked: () -> Unit
) : RecyclerView.Adapter<FlexiSessionAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_flexi_session, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (arrFlexiSessionModel.size > 4) 4 else arrFlexiSessionModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrFlexiSessionModel[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.linMoreOptions.setOnClickListener { onMoreClicked() }
            itemView.linWhole.setOnClickListener { onSlotClicked(adapterPosition, parentPosition) }
        }

        fun bind(flexiSessionModel: ActivityFlexiSessionModel) {
            itemView.txtSessionTitle.text = ResourceUtil.getSessionsString(
                itemView.context,
                flexiSessionModel.numberOfSession.toInt()
            )//itemView.context.resources.getQuantityString(R.plurals.session,flexiSessionModel.numberOfSession.toInt(),flexiSessionModel.numberOfSession.toInt()) //"${flexiSessionModel.numberOfSession} Session"
            /*if (flexiSessionModel.numberOfSession.toFloat()==1f)
                itemView.txtSessionTitle.text="${flexiSessionModel.numberOfSession} Session"
            else
                itemView.txtSessionTitle.text="${flexiSessionModel.numberOfSession} Sessions"*/
            itemView.txtSessionAge.text = flexiSessionModel.ageRange
            itemView.txtSessionTime.text = flexiSessionModel.duration
            itemView.txtSessionLocation.text = flexiSessionModel.location

            itemView.linMoreOptions.visibility =
                if (arrFlexiSessionModel.size > 4 && adapterPosition == itemCount - 1) View.VISIBLE else View.GONE

            itemView.txtMoreOptionsSlots.text =
                "" + (arrFlexiSessionModel.size - 4) + " more options"

            itemView.imgHeaderIcon.setImageResource(
                if (flexiSessionModel.sessionType.equals("group", true)) R.drawable.ic_user_group else R.drawable.ic_user_age)

            try {
                if (JsonParse.decimalFormat.format(flexiSessionModel.price.toDouble()).toDouble() > 0.0)
                    itemView.txtSlotPrice.text = "AED ${flexiSessionModel.price}"
                else
                    itemView.txtSlotPrice.text = "Free"
            } catch (e: Exception) {
                itemView.txtSlotPrice.text = "AED ${flexiSessionModel.price}"
            }

            if (flexiSessionModel.priceRoundedToDisplay > 0) {
                itemView.txtSlotPrice.text = "AED ${flexiSessionModel.priceRoundedToDisplay}"
            }
        }
    }

}
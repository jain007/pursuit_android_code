package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.ActivityFlexiSessionModel
import kotlinx.android.synthetic.main.row_flexi_session_table.view.*

class FlexiSessionTableForDetailAdapter(val arrFlexiSessionModel: ArrayList<ActivityFlexiSessionModel>): RecyclerView.Adapter<FlexiSessionTableForDetailAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.row_flexi_session_table,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrFlexiSessionModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrFlexiSessionModel[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(flexiSessionModel: ActivityFlexiSessionModel)
        {
            itemView.txtSession.text= if (flexiSessionModel.numberOfSession.toInt()==0) "Unlimited" else flexiSessionModel.numberOfSession //+" Session(s)"
            itemView.txtDuration.text=flexiSessionModel.duration
            itemView.txtPrice.text="AED "+flexiSessionModel.price
            itemView.txtValidity.text= itemView.context.resources.getQuantityString(R.plurals.day,flexiSessionModel.validDays.toInt(),flexiSessionModel.validDays.toInt())// +" Days"
        }
    }

}
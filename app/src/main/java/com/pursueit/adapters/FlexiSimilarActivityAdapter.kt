package com.pursueit.adapters

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_activity_for_map.view.*
import org.jetbrains.anko.textColor

class FlexiSimilarActivityAdapter(val act: Activity, var arrayModel: ArrayList<ActivityModel>) :
    RecyclerView.Adapter<FlexiSimilarActivityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_activity_for_map, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrayModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            /*  if (fromSimilarAct)
              {
                  val layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT)
                  layoutParams.setMargins(0,0,20,0)
                  itemView.rootCardAct.layoutParams=layoutParams
              }*/

            /* itemView.txtSlotStatus.setOnClickListener {
                 val model=arrayModel[adapterPosition]
                 MyNavigations.goToFlexiActivityDetail(act, model, model.flexiIndex)
             }*/

            itemView.linActivityForMap.setOnClickListener {
                val model = arrayModel[adapterPosition]
                MyNavigations.goToFlexiActivityDetail(act, model, model.flexiIndex)
            }

            itemView.linSessionAndTime.visibility = View.VISIBLE
            itemView.txtStartDate.visibility = View.GONE
        }

        fun setContent(model: ActivityModel) {
            PromotionOperation.setPromotionText(itemView.linPromotion, model.maxValuePromotionModel)

            Glide.with(itemView)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions()).load(
                Urls.IMAGE_FEATURE_ACTIVITY + model.activityImageUrl
            ).into(itemView.imgActivity)
            itemView.txtActivityTitle.text = model.activityName.trim()
            itemView.txtActivitySubTitle.text = model.activityProviderName.trim()
            itemView.linRatingBarView.visibility =
                if (model.activityStarRatingCount > 0) View.VISIBLE else View.GONE
            itemView.linRatingBarView.isEnabled = false
            itemView.ratingActivity.rating = model.activityStarRating.toFloat()

            itemView.txtSession.text = ResourceUtil.getSessionsString(
                act,
                model.similarFlexiModel?.numberOfSession?.toInt() ?: 0
            )
            itemView.txtActivityTime.text = model.similarFlexiModel?.duration

            itemView.txtActivityPrice.visibility = View.VISIBLE
            try {
                if (JsonParse.decimalFormat.format(model.similarFlexiModel!!.price.toDouble()).toDouble() > 0.0)
                    itemView.txtActivityPrice.text = "AED ${model.similarFlexiModel!!.price}"
                else
                    itemView.txtActivityPrice.text = "Free"

            } catch (e: Exception) {
                Log.e("PriceDisplay", e.toString())
                itemView.txtActivityPrice.text = "AED ${model.similarFlexiModel!!.price}"
            }

            //   itemView.txtSession.text= ResourceUtil.getSessionsString(itemView.context,model.similarFlexiModel!!.numberOfSession.toInt())//itemView.context.resources.getQuantityString(R.plurals.session,model.similarFlexiModel!!.numberOfSession.toInt(),model.similarFlexiModel!!.numberOfSession.toInt()) //"${flexiSessionModel.numberOfSession} Session"// model.similarFlexiModel.numberOfSession

            //   itemView.txtActivityLocation.text=model.similarFlexiModel!!.location
            /* if (model.startDate.trim()!="")
                 itemView.txtActivityTime.text=model.startDate+","+model.slotTime
             else
                 itemView.txtActivityTime.text=model.slotTime*/
            //    itemView.txtActivityAge.text=model.similarFlexiModel!!.ageRange
            itemView.txtActivityStarCount.text = "(${model.activityStarRatingCount})"


            //    itemView.txtSlotStatus.text = "Flexible"//model.slotStatusText
            //     itemView.txtActivityTime.text=model.similarFlexiModel!!.duration


            /*if (model.slotStatusText.equals("Available", true)) {
                //   itemView.linHeader.backgroundResource = R.drawable.bg_full_slot_header_blue
                itemView.txtSlotStatus.textColor= ContextCompat.getColor(itemView.context, R.color.colorAvailable)
                //   itemView.imgHeaderIcon.visibility = View.VISIBLE
                //     itemView.txtSlotTitle.gravity = Gravity.CENTER_VERTICAL
                //     itemView.txtSlotTitle.setTextColor(Color.WHITE)
                itemView.txtActivityPrice.visibility = View.VISIBLE

            } else {
                //     itemView.linHeader.backgroundResource = R.drawable.bg_full_slot_header
                itemView.txtSlotStatus.textColor = ContextCompat.getColor(itemView.context, R.color.colorEnquire)
                //     itemView.imgHeaderIcon.visibility = View.GONE
                //      itemView.txtSlotTitle.gravity = Gravity.CENTER
                //     itemView.txtSlotTitle.setTextColor(ContextCompat.getColor(act, R.color.colorTextGreySubTitle))
                itemView.txtActivityPrice.visibility = View.INVISIBLE
            }*/
        }
    }
}
package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pursueit.R
import com.pursueit.model.FlexiTimeTableModel

class FlexiTimeTableAdapter(private val arrayList:ArrayList<ArrayList<FlexiTimeTableModel?>>) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    private val HEADER_TYPE=1
    private val ITEM_TYPE=2

    override fun onCreateViewHolder(parent: ViewGroup, itemType: Int): RecyclerView.ViewHolder {
        if (itemType==HEADER_TYPE)
        {
            return HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_flexi_time_table_header,parent,false))
        }
        else
        {
            return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_flexi_time_table,parent,false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position==0)
            HEADER_TYPE
        else
            ITEM_TYPE
    }

    override fun getItemCount(): Int {
        return arrayList.size+1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        if (pos>0)
        {
            (holder as ItemViewHolder).bind(arrayList[pos-1])
        }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        fun bind(arrayRowTimeTable:ArrayList<FlexiTimeTableModel?>)
        {
            //txtSlot1
            repeat(arrayRowTimeTable.size){
                val id="txtSlot"+(it+1)
                val resId=itemView.context.resources.getIdentifier(id,"id",itemView.context.packageName)
                setRowData(arrayRowTimeTable[it],itemView.findViewById(resId))
            }

        }
        private fun setRowData(flexiTimeTableModel:FlexiTimeTableModel?, textView:TextView)
        {
            if (flexiTimeTableModel!=null)
                textView.text = flexiTimeTableModel.startTime+"-"+flexiTimeTableModel.endTime
            else
                textView.text=""
        }
    }
}
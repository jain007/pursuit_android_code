package com.pursueit.adapters

import android.app.Activity
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.MoreOptionsModel
import kotlinx.android.synthetic.main.row_more_options.view.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.padding


class MoreOptionsAdapter(var act: Activity, var arrayModel: ArrayList<MoreOptionsModel>, var onItemClick:(pos:Int)->Unit) : RecyclerView.Adapter<MoreOptionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_more_options, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.txtMoreOptionsTitle.text = model.optionTitle.trim()
        v.txtMoreOptionsSubTitle.text = model.optionSubTitle.trim()
        if (model.optionImage != 0)
            v.imgMoreOptions.imageResource = model.optionImage


        v.imgMoreOptions.setColorFilter(ContextCompat.getColor(act,model.tintColor))
        v.txtMoreOptionsTitle.setTextColor(ContextCompat.getColor(act,if(model.tintColor==R.color.colorSignOut) model.tintColor else android.R.color.black))


        if(model.optionSubTitle.trim().isEmpty()){
            v.txtMoreOptionsTitle.setPadding(0,4,0,0)
            v.txtMoreOptionsSubTitle.visibility=View.GONE
        }else{
            v.txtMoreOptionsTitle.setPadding(0,0,0,0)
            v.txtMoreOptionsSubTitle.visibility=View.VISIBLE
        }

        v.linItem.setOnClickListener {
            onItemClick(pos)
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.PlaceModel
import kotlinx.android.synthetic.main.row_place_search_my_address.view.*


class MyAddressSwipeAdapter(var act: Activity, var arrayModel: ArrayList<PlaceModel>,var onDelete:(pos:Int)->Unit) : RecyclerView.Adapter<MyAddressSwipeAdapter.ViewHolder>() {
    /*override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }*/

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_place_search_my_address, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {

        setContent(holder.itemView, pos)

        // mItemManger is member in RecyclerSwipeAdapter Class
        //mItemManger.bindView(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        //v.swipe.showMode = SwipeLayout.ShowMode.PullOut

        // Drag From Left
        //v.swipe.addDrag(SwipeLayout.DragEdge.Left, v.swipe.linDelete)

        val model = arrayModel[pos]
        v.txtPlaceTitle.text = model.placeTitle.trim()
        v.txtPlaceSubTitle.text = model.placeAddress.trim()

        v.viewBottomLine.visibility = if (pos == itemCount - 1) View.GONE else View.VISIBLE

        v.linDelete.setOnClickListener {
            //mItemManger.removeShownLayouts(v.swipe)
            //mItemManger.closeAllItems()
            onDelete(pos)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityFlexiSessionModel
import com.pursueit.model.MyBookingsModel
import com.pursueit.utils.ChangeDateFormat
import com.pursueit.utils.MyGlideOptions
import com.pursueit.utils.StaticValues
import kotlinx.android.synthetic.main.row_my_bookings.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textColor
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList


class MyBookingsAdapter(var act: Activity, var arrayModel: ArrayList<MyBookingsModel>,var onClickItem:(posClicked:Int)->Unit, var showMoreListener:(pos:Int,progressShowMore:ProgressWheel)->Unit) : RecyclerView.Adapter<MyBookingsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_my_bookings, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]

        if (model.isPassAct)
        {
            v.imgPass.visibility=View.VISIBLE
        }
        else
        {
            v.imgPass.visibility=View.GONE
        }

        v.txtMyBookingsTitle.text=model.bookingTitle.trim()
        v.txtMyBookingsSubTitle.text=model.bookingSubTitle.trim()
        v.txtMyBookingsLocation.text=model.bookingLocation.trim()
      /*  if (model.bookingFullAddress!="")
            v.txtMyBookingsLocation.text=model.bookingFullAddress.trim()*/

        if (model.bookingType==MyBookingsModel.BOOKING_TYPE_ACTIVITY || model.bookingType==MyBookingsModel.BOOKING_TYPE_FLEXI_ACTIVITY) {
            Glide.with(v).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions())
                .load(Urls.IMAGE_BOOKING_ACTIVITY + model.bookingImageUrl).into(v.imgMyBooking)

            v.txtMyBookingsTime.visibility=View.VISIBLE
            v.txtMyBookingsTime.text = model.bookingMsgTime
        }
        else       // camp
        {
            Glide.with(v).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions())
                .load(Urls.IMAGE_FEATURE_CAMP + model.bookingImageUrl).into(v.imgMyBooking)
            v.txtMyBookingsTime.visibility=View.GONE
        }

        if (model.bookingStatus.trim()!="") {

            v.txtMyBookingsStatus.text = model.bookingStatus.capitalize()

            if (model.bookingStatus.equals(StaticValues.BOOKING_STATUS_COMPLETED, true)) {
                v.txtMyBookingsStatus.backgroundResource = R.drawable.bg_completed
                v.txtMyBookingsStatus.textColor = ContextCompat.getColor(act, R.color.colorCompleted)


                if (model.bookingType==MyBookingsModel.BOOKING_TYPE_FLEXI_ACTIVITY)
                {
                    val arrBookedFlexiSession=ArrayList<ActivityFlexiSessionModel>()
                    try {
                        val jarSession= JSONArray(model!!.sessionJson)
                        repeat(jarSession.length()){
                            val job=jarSession.getJSONObject(it)
                            job.run {
                                ActivityFlexiSessionModel(getString("flexi_activity_id"),getString("no_of_sessions"),""
                                    ,getString("price"),getString("duration"),"","","","","",
                                    getString("valid_days"),"","")
                                    .also {
                                        arrBookedFlexiSession.add(it)
                                    }
                            }
                        }
                    }catch (e:Exception){
                        Log.e("bookedFlexi",e.toString())
                    }
                    val strExpiryDate= ChangeDateFormat.getDateAfterGivenDays(model.bookingDate,"yyyy-MM-dd HH:mm:ss",arrBookedFlexiSession[0].validDays.toInt(),"dd MMM yyyy")
                    v.txtMyBookingsTime.text= "on $strExpiryDate"
                }

            } else if (model.bookingStatus.equals(StaticValues.BOOKING_STATUS_UPCOMING, true) || model.bookingStatus.equals(StaticValues.CAMP_BOOKED, true)) {
                v.txtMyBookingsStatus.backgroundResource = R.drawable.bg_upcoming
                v.txtMyBookingsStatus.textColor = ContextCompat.getColor(act, R.color.colorUpcoming)
            } else if (model.bookingStatus.equals(StaticValues.BOOKING_STATUS_ONGOING, true)) {
                v.txtMyBookingsStatus.backgroundResource = R.drawable.bg_ongoing
                v.txtMyBookingsStatus.textColor = ContextCompat.getColor(act, R.color.colorOngoing)
            } else if (model.bookingStatus.equals(
                    StaticValues.BOOKING_STATUS_CANCELLED,
                    true) || model.bookingStatus.equals("canceled", true)
            ) {
                v.txtMyBookingsStatus.backgroundResource =R.drawable.bg_rejected
                v.txtMyBookingsStatus.textColor = ContextCompat.getColor(act, R.color.colorRejected)

                v.txtMyBookingsStatus.text = MyBookingsModel.getCancellationMessage(model.bookingCancelledBy).capitalize()
            }
            else if (model.bookingStatus.equals(StaticValues.BOOKING_STATUS_DECLINED, true))
            {
                v.txtMyBookingsStatus.backgroundResource = R.drawable.bg_rejected
                v.txtMyBookingsStatus.textColor = ContextCompat.getColor(act, R.color.colorRejected)
            }
            else if (model.bookingStatus.equals(StaticValues.BOOKING_STATUS_AWAITING, true))
            {
                v.txtMyBookingsStatus.backgroundResource = R.drawable.bg_awaiting
                v.txtMyBookingsStatus.textColor = ContextCompat.getColor(act, R.color.colorAwaiting)

                //change booking status title for showing
                v.txtMyBookingsStatus.text = StaticValues.BOOKING_UNDER_PROCESS.toUpperCase(Locale.ENGLISH)
            }
            v.txtMyBookingsStatus.visibility=View.VISIBLE
        }
        else
        {
            v.txtMyBookingsStatus.visibility=View.GONE
        }

        v.relItem.setOnClickListener {
            onClickItem(pos)
        }

        v.progressShowMore.visibility=View.GONE
        if(pos==itemCount-1)
            showMoreListener(pos,v.progressShowMore)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.app.Activity
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.nshmura.recyclertablayout.RecyclerTabLayout
import com.pursueit.R
import kotlinx.android.synthetic.main.custom_tab_for_my_corner.view.*
import kotlinx.android.synthetic.main.custom_tab_header.view.*


class MyCustomTabAdapter(var vp: ViewPager, private var vpAdapter:MyFragmentPagerAdapter, var act: Activity
                         , val fromMyCorner:Boolean=false, var onClickTab:(pos:Int)->Unit) :
    RecyclerTabLayout.Adapter<MyCustomTabAdapter.ViewHolder>(vp) {

    override fun onCreateViewHolder(container: ViewGroup, pos: Int): ViewHolder {

        if (fromMyCorner)
        {
            val vh = ViewHolder(LayoutInflater.from(act).inflate(R.layout.custom_tab_for_my_corner, container, false))
            return vh
        }
        else {
            return ViewHolder(LayoutInflater.from(act).inflate(R.layout.custom_tab_header, container, false))
        }
    }

    override fun getItemCount(): Int {
        return vpAdapter.count
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        var txtHeaderCustomTab:TextView
        var linParentCustomTab:LinearLayout

        if (fromMyCorner)
        {
            txtHeaderCustomTab=holder.itemView.txtHeaderCustomTab1
            linParentCustomTab=holder.itemView.linParentCustomTab1
        }else
        {
            txtHeaderCustomTab=holder.itemView.txtHeaderCustomTab
            linParentCustomTab=holder.itemView.linParentCustomTab
        }



        txtHeaderCustomTab.text=vpAdapter.mFragmentTitleList[pos].trim()
        if (pos == currentIndicatorPosition) {
            //Highlight view
            txtHeaderCustomTab.setTextColor(Color.WHITE)
            txtHeaderCustomTab.setBackgroundResource(R.drawable.bg_full_rounded_blue)
        } else {
            if(pos==1){
                txtHeaderCustomTab.setTextColor(Color.WHITE)
                txtHeaderCustomTab.setBackgroundResource(R.drawable.bg_full_rounded_saffron)
            }else {
                txtHeaderCustomTab.setTextColor(ContextCompat.getColor(act, R.color.colorTextBlack))
                txtHeaderCustomTab.setBackgroundColor(Color.TRANSPARENT)
            }
        }

        linParentCustomTab.setOnClickListener {
            onClickTab(pos)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        init {
            /*if(fromMyCorner)
            {
                itemView.linRootTab.gravity=Gravity.CENTER

                val layoutParams=ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
                itemView.linRootTab.layoutParams=layoutParams
                itemView.linRootTab.gravity=Gravity.CENTER_VERTICAL

                val linearLayoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
                itemView.linParentCustomTab.layoutParams=linearLayoutParams
                itemView.txtHeaderCustomTab.layoutParams=linearLayoutParams
            }*/
        }

    }
}
package com.pursueit.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import java.util.ArrayList


class MyFragmentPagerAdapter(fm: FragmentManager?): FragmentStatePagerAdapter(fm) {
    val mFragmentList: MutableList<Fragment> = ArrayList()
    val mFragmentTitleList: MutableList<String> = ArrayList()


    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    fun updateTitle(pos: Int, title: String) {
        mFragmentTitleList[pos] = title
    }

    fun clearFragments() {
        mFragmentList.clear()
        mFragmentTitleList.clear()
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }
}
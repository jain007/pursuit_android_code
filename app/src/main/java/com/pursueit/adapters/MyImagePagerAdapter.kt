package com.pursueit.adapters

import android.app.Activity
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.utils.MyGlideOptions
import kotlinx.android.synthetic.main.row_activity_image_pager.view.*
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri
import android.util.Log
import org.jetbrains.anko.toast


class MyImagePagerAdapter(var act: Activity, var images: ArrayList<String>, var fromCampDetail:Boolean=false, var fromServiceProvider:Boolean=false,
                          private val alreadyCompleteUrlAdded:Boolean=false) : PagerAdapter() {
    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(container)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(act).inflate(R.layout.row_activity_image_pager, container, false)
        container.addView(view)
        initImage(view, position)

        return view
    }

    private fun initImage(view: View, pos: Int) {
        var completeImageUrl = ""
        if (fromServiceProvider){
            if (alreadyCompleteUrlAdded)
            {
                completeImageUrl=images[pos]
            }
            else {
                completeImageUrl = Urls.IMAGE_SLIDER_PARTNER + images[pos]
            }
        }
        else {
            when (pos) {
                0 -> completeImageUrl =
                    if (fromCampDetail) Urls.IMAGE_FEATURE_CAMP + images[pos] else Urls.IMAGE_FEATURE_ACTIVITY + images[pos]
                else -> completeImageUrl =
                    if (fromCampDetail) Urls.IMAGE_BOOKING_CAMP + images[pos] else Urls.IMAGE_ACTIVITY + images[pos]
            }
        }

        if (images[pos].contains("img.youtube.com/vi")) {
            completeImageUrl = images[pos]
            view.imgPlay.visibility=View.VISIBLE
        }else{
            view.imgPlay.visibility=View.GONE
        }

        view.imgPlay.setOnClickListener {
            try {
                if (images[pos].contains("img.youtube.com/vi")) {
                    val urlBreaks = completeImageUrl.split("/")
                    val videoKey = urlBreaks[urlBreaks.size - 2]
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://$videoKey"))
                    intent.putExtra("force_fullscreen",true)
                    act.startActivity(intent)
                }
            }catch (e:Exception){
                Log.d("Exc_",""+e)
                act.toast("YouTube app found to play this video")
            }
        }


        Glide.with(view)
            .applyDefaultRequestOptions(if(images[pos].contains("img.youtube.com/vi")) MyGlideOptions.getRectangleRequestOptionsVideo() else MyGlideOptions.getRectangleRequestOptions())
            .load(completeImageUrl)
            .into(view.imgActivity)
    }

}
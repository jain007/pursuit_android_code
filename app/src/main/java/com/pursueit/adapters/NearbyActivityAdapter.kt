package com.pursueit.adapters

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.ActivitySlotsActivity
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.utils.PromotionOperation
import com.pursueit.model.UserModel
import com.pursueit.utils.MarkFavoriteOrUnFavorite
import com.pursueit.utils.MyAppConfig
import com.pursueit.utils.MyGlideOptions
import com.pursueit.utils.MyNavigations
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import kotlinx.android.synthetic.main.row_near_by_flexi_activity.view.*
import kotlinx.android.synthetic.main.row_nearby_activity.view.*
import org.jetbrains.anko.startActivity

class NearbyActivityAdapter(
    var act: AppCompatActivity,
    var arrayModel: ArrayList<ActivityModel>,
    var showMoreListener: (pos: Int, progressShowMore: ProgressWheel) -> Unit
    ,val compositeDisposable: CompositeDisposable) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var viewPool: RecyclerView.RecycledViewPool? = null

  //  var viewPoolForFlexi:RecyclerView.RecycledViewPool?=null

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType==ActivityModel.NORMAL_ACTIVITY || viewType==ActivityModel.SINGLE_ACTIVITY)
            ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_nearby_activity, container, false))
        else
            FlexiViewHolder(LayoutInflater.from(act).inflate(R.layout.row_near_by_flexi_activity, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun getItemViewType(position: Int): Int {
        return arrayModel[position].activityType.toInt()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        if (viewPool == null)
            viewPool = RecyclerView.RecycledViewPool()
    /*    if (viewPoolForFlexi==null)
            viewPoolForFlexi=RecyclerView.RecycledViewPool()*/



        val model=arrayModel[pos]
        //Log.d("actName",model.activityName+","+model.activityId)

        if (arrayModel[pos].activityType==ActivityModel.FLEXI_ACTIVITY) {
            if (holder is FlexiViewHolder) holder.setData(pos)
        }
        else {
            if (holder is ViewHolder) holder.setContent(pos)
        }
    }

   

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            val userModel = UserModel.getUserModel(itemView.context)
            if (userModel.userId.trim().isEmpty()) {
                itemView.sparkFavoriteAct.visibility = View.GONE
            }

          /*  itemView.txtNearbySubTitle.setOnClickListener {
                MyNavigations.gotoProviderDetailActivity(act,arrayModel[adapterPosition].activityProviderId)
            }*/

            val markFavOrUnFav = MarkFavoriteOrUnFavorite(act)
            itemView.sparkFavoriteAct.setOnClickListener {

                val model = arrayModel[adapterPosition]
                markFavOrUnFav.fastMark(
                    activityId = model.activityId,
                    itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_ACTIVITY,
                    favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE
                )

                arrayModel[adapterPosition].isFavoriteMarked = !arrayModel[adapterPosition].isFavoriteMarked
                itemView.sparkFavoriteAct.playAnimation()
                itemView.sparkFavoriteAct.isChecked = arrayModel[adapterPosition].isFavoriteMarked
            }

            itemView.imgInviteIcon.setOnClickListener {
                MyAppConfig.shareApp(act, arrayModel[adapterPosition].activityName)
            }

            itemView.txtInvite.setOnClickListener {
                MyAppConfig.shareApp(act, arrayModel[adapterPosition].activityName)
            }

            /*itemView.rvSlots.apply {
                if (layoutManager == null) {
                    layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
                    setHasFixedSize(true)
                }
            }*/
        }
         fun setContent(pos: Int) {
            if (arrayModel.size < 1)
                return

            val model = arrayModel[pos]

             // set data on promotion ribbon

             if (model.isPassActivity)
                 itemView.linPromotion.visibility=View.GONE
             else
                PromotionOperation.setPromotionText(itemView.linPromotion,model.maxValuePromotionModel)

            itemView.txtNearbyTitle.text = model.activityName.trim()
            itemView.txtNearbySubTitle.text = model.activityProviderName.trim()
            itemView.txtLocation.text = (model.activityLocation.trim() + "\n" + model.activityMsgLocationSuffix.trim()).trim()

            itemView.ratingBar.rating = model.activityStarRating.toFloat()
            itemView.txtStarCount.text = "(${model.activityStarRatingCount})"

            itemView.linRatingBarView.visibility = if (model.activityStarRatingCount > 0) View.VISIBLE else View.GONE
            itemView.linRatingBarView.isEnabled = false

            /*Log.d("Name: ", model.activityName)
            Log.d("Id: ", model.activityId)*/
            Glide.with(itemView).applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_ACTIVITY + model.activityImageUrl).into(itemView.imgActivity)

            /* itemView.imgLike.setImageResource(if (pos % 2 == 0) R.drawable.ic_like else R.drawable.ic_unlike)
             itemView.imgLike.setColorFilter(ContextCompat.getColor(act, if (pos % 2 == 0) R.color.colorMainOrange else R.color.colorTextDimBlack))*/


            try {
                if (model.activityType == ActivityModel.NORMAL_ACTIVITY || model.activityType == ActivityModel.SINGLE_ACTIVITY) { // for normal activities (term, single)
                    itemView.rvSlots.visibility = View.VISIBLE
                    itemView.rvSession.visibility = View.GONE
                    itemView.rvSlots.apply {
                        if (layoutManager == null) {
                            layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
                            //setHasFixedSize(true)
                        }

                        setRecycledViewPool(viewPool)


                        adapter = ActivitySlotsAdapter(act, model.arraySlotModel, { position ->
                            try {
                                MyNavigations.goToActivityDetail(act, arrayModel[adapterPosition], position)
                            }catch (e:Exception){
                                MyNavigations.goToActivityDetail(act, model, position)
                            }

                            //MyNavigations.goToActivityDetail(act, model, position)
                            //NearbyActivitiesListingFragment.lastClickedPosition=pos
                        }, {
                            act.startActivity<ActivitySlotsActivity>("ActivityModel" to model)
                            //NearbyActivitiesListingFragment.lastClickedPosition=pos
                        },compositeDisposable)

                    }
                } /*else { //flexi activity
                itemView.rvSlots.visibility = View.GONE
                itemView.rvSession.visibility = View.VISIBLE
                itemView.rvSession.apply {
                    if (layoutManager == null) {
                        layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
                        setHasFixedSize(true)
                    }
                    setRecycledViewPool(viewPool)
                    adapter = FlexiSessionAdapter(model.arrFlexiSession, { position ->
                        //Go to flexi detail activity
                        MyNavigations.goToFlexiActivityDetail(act, model, position)
                        // MyNavigations.goToActivityDetail(act, model, position)
                        //NearbyActivitiesListingFragment.lastClickedPosition=pos
                    }, {
                        act.startActivity<ActivitySlotsActivity>("ActivityModel" to model, "isFlexiActivity" to true)
                        //NearbyActivitiesListingFragment.lastClickedPosition=pos
                    })
                }
            }*/


            } catch (e: Exception) {
                Log.e("rvSlotError", e.toString())
            }


            /*itemView.relItem.setOnClickListener {
                //MyNavigations.goToActivityDetail(act, model, 0)
            }*/

            itemView.progressShowMore.visibility = View.GONE

            if (pos == itemCount - 1) {
                showMoreListener(pos, itemView.progressShowMore)
            }


            itemView.sparkFavoriteAct.isChecked = model.isFavoriteMarked
        }
        
    }

    inner class FlexiViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        init {
            val userModel = UserModel.getUserModel(itemView.context)
            if (userModel.userId.trim().isEmpty()) {
                itemView.sparkFavoriteActFlexi.visibility = View.GONE
            }

            /*itemView.txtNearbySubTitleFlexi.setOnClickListener {
                MyNavigations.gotoProviderDetailActivity(act,arrayModel[adapterPosition].activityProviderId)
            }*/

            val markFavOrUnFav = MarkFavoriteOrUnFavorite(act)
            itemView.sparkFavoriteActFlexi.setOnClickListener {

                val model = arrayModel[adapterPosition]
                markFavOrUnFav.fastMark(
                    activityId = model.activityId,
                    itemFlag = MarkFavoriteOrUnFavorite.ITEM_FLAG_ACTIVITY,
                    favUnFavFlag = if (model.isFavoriteMarked) MarkFavoriteOrUnFavorite.FLAG_UNFAVORITE else MarkFavoriteOrUnFavorite.FLAG_FAVORITE
                )

                arrayModel[adapterPosition].isFavoriteMarked = !arrayModel[adapterPosition].isFavoriteMarked
                itemView.sparkFavoriteActFlexi.playAnimation()
                itemView.sparkFavoriteActFlexi.isChecked = arrayModel[adapterPosition].isFavoriteMarked
            }

            itemView.imgInviteIconFlexi.setOnClickListener {
                MyAppConfig.shareApp(act, arrayModel[adapterPosition].activityName)
            }

            itemView.txtInviteFlexi.setOnClickListener {
                MyAppConfig.shareApp(act, arrayModel[adapterPosition].activityName)
            }

        }

        fun setData(pos: Int)
        {
            if (arrayModel.size < 1)
                return

            val model = arrayModel[pos]

            // set data on promotion ribbon
            PromotionOperation.setPromotionText(itemView.linPromotion,model.maxValuePromotionModel)

            itemView.txtNearbyTitleFlexi.text = model.activityName.trim()
            itemView.txtNearbySubTitleFlexi.text = model.activityProviderName.trim()
            itemView.txtLocationFlexi.text = (model.activityLocation.trim() + "\n" + model.activityMsgLocationSuffix.trim()).trim()

            itemView.ratingBarFlexi.rating = model.activityStarRating.toFloat()
            itemView.txtStarCountFlexi.text = "(${model.activityStarRatingCount})"

            itemView.linRatingBarViewFlexi.visibility = if (model.activityStarRatingCount > 0) View.VISIBLE else View.GONE
            itemView.linRatingBarViewFlexi.isEnabled = false

            /*Log.d("Name: ", model.activityName)
            Log.d("Id: ", model.activityId)*/
            Glide.with(itemView).applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_ACTIVITY + model.activityImageUrl).into(itemView.imgActivityFlexi)

            /* v.imgLike.setImageResource(if (pos % 2 == 0) R.drawable.ic_like else R.drawable.ic_unlike)
             v.imgLike.setColorFilter(ContextCompat.getColor(act, if (pos % 2 == 0) R.color.colorMainOrange else R.color.colorTextDimBlack))*/

            try {
                //flexi activity
                    itemView.rvSlotsFlexi.visibility = View.GONE
                    itemView.rvSessionFlexi.visibility = View.VISIBLE
                    itemView.rvSessionFlexi.apply {
                        if(layoutManager==null) {
                            layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
                            //setHasFixedSize(true)
                        }

                       //setRecycledViewPool(viewPool)

                   //     setRecycledViewPool(viewPoolForFlexi)
                        adapter = FlexiSessionAdapter(model.arrFlexiSession,pos, { position,parentPosition ->
                            //Go to flexi detail activity
                            Log.d("flexiModel1","first: "+pos+" and second"+parentPosition+"parent adapter:"+this@FlexiViewHolder.adapterPosition)
                            Log.d("flexiModel1",arrayModel[parentPosition].toString())
                            Log.d("flexiModel2",model.toString())
                            MyNavigations.goToFlexiActivityDetail(act, arrayModel[this@FlexiViewHolder.adapterPosition], position)
                            // MyNavigations.goToActivityDetail(act, model, position)
                            //NearbyActivitiesListingFragment.lastClickedsessionPosition=pos
                        }, {
                            act.startActivity<ActivitySlotsActivity>("ActivityModel" to arrayModel[pos], "isFlexiActivity" to true)
                            //NearbyActivitiesListingFragment.lastClickedPosition=pos
                        })
                    }

            } catch (e: Exception) {
                Log.e("rvSlotError", e.toString())
            }


            /*v.relItem.setOnClickListener {
                //MyNavigations.goToActivityDetail(act, model, 0)
            }*/

            itemView.progressShowMoreFlexi.visibility = View.GONE

            if (pos == itemCount - 1) {
                showMoreListener(pos, itemView.progressShowMoreFlexi)
            }

            itemView.sparkFavoriteActFlexi.isChecked = model.isFavoriteMarked
        }
    }
}
package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.NotificationActivity
import com.pursueit.model.NotificationModel
import com.pursueit.model.UserModel
import com.pursueit.utils.NotificationHandler
import kotlinx.android.synthetic.main.row_notification.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.imageResource


class NotificationAdapter(
    val act: NotificationActivity, private val arrNotificationModel: ArrayList<NotificationModel>
    , var showMoreListener: (pos: Int, progressShowMore: ProgressWheel) -> Unit
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

  //  var lastSavedPos=-1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_notification, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrNotificationModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = arrNotificationModel[position]
        holder.setData(model)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            val notificationHandler = NotificationHandler(act)
            itemView.relNotification.setOnClickListener {
                    notificationHandler.navigateNotification(arrNotificationModel[adapterPosition])
            }
        }

        fun setData(notificationModel: NotificationModel) {

            when(notificationModel.notificationType){
                NotificationModel.TYPE_REVIEW_ACTIVITY,NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY,NotificationModel.TYPE_REVIEW_CAMP->{
                    itemView.imgIndicator.imageResource=R.drawable.ic_review
                    itemView.imgIndicator.backgroundResource=R.drawable.circle_orange
                }
                NotificationModel.TYPE_ACTIVITY_BOOKING_REMIDER, NotificationModel.TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER->{
                    itemView.imgIndicator.imageResource=R.drawable.ic_reminder
                    itemView.imgIndicator.backgroundResource=R.drawable.circle_blue
                }
                NotificationModel.TYPE_ACTIVITY_ACCEPTED, NotificationModel.TYPE_CAMP_ACCEPTED, NotificationModel.TYPE_FLEXI_ACCEPTED->{
                    itemView.imgIndicator.imageResource=R.drawable.ic_accept
                    itemView.imgIndicator.backgroundResource=R.drawable.circle_green
                }
                NotificationModel.TYPE_ACTIVITY_REJECTED, NotificationModel.TYPE_CAMP_REJECTED, NotificationModel.TYPE_FLEXI_REJECTED
                    ,NotificationModel.TYPE_ACTIVITY_CANCELLED, NotificationModel.TYPE_FLEXI_CANCELLED, NotificationModel.TYPE_CAMP_CANCELLED->{
                    itemView.imgIndicator.imageResource=R.drawable.ic_decline
                    itemView.imgIndicator.backgroundResource=R.drawable.circle_red
                }
            }

            itemView.txtNotificationTitle.text = notificationModel.title
            itemView.txtNotificationDate.text = notificationModel.dateTime
            itemView.txtNotificationDescription.text = notificationModel.message

            if (adapterPosition ==0) {
                UserModel.setLastNotificationId(act, notificationModel.notificationId)
            }

            if (adapterPosition == arrNotificationModel.size - 1 && !act.isLoadingAlready)
                showMoreListener(adapterPosition, itemView.progressShowMore)
            else
            {
                itemView.progressShowMore.visibility=View.GONE
            }
        }
    }

}
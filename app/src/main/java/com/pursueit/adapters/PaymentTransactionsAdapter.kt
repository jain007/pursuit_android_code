package com.pursueit.adapters

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.model.PaymentTransactionModel
import kotlinx.android.synthetic.main.row_payment_transaction.view.*
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textColorResource

class PaymentTransactionsAdapter(var act: Activity, var arrayPaymentTransactionModel:ArrayList<PaymentTransactionModel>, var onLastItem:(pos:Int,progressLoadMore:ProgressWheel)->Unit
                                 , private val fromPassTransaction:Boolean) :
    RecyclerView.Adapter<PaymentTransactionsAdapter.ViewHolder>() {

    var onBind = true

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_payment_transaction, container, false))
    }

    override fun getItemCount(): Int {
        return arrayPaymentTransactionModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        onBind = true
        setContent(holder.itemView, pos)
        onBind = false
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayPaymentTransactionModel[pos]

        v.txtPaymentDate.text=model.transactionDate.trim()
        if (fromPassTransaction) {
            v.txtPaymentId.text="#"+model.transactionOrderId.trim()
            if (model.transType=="1") //credit
            {
                v.txtPaymentAmount.textColor =
                    ContextCompat.getColor(v.context, R.color.colorCredit)
                v.txtPaymentAmount.text = "+AED ${model.transactionAmount.trim()}"
                /*v.txtPaymentStatus.text="Added to wallet"
                v.txtPaymentStatus.textColorResource=R.color.colorMainBlue*/
            }
            else {
                v.txtPaymentAmount.textColor = ContextCompat.getColor(v.context, R.color.colorDebit)
                v.txtPaymentAmount.text = "-AED ${model.transactionAmount.trim()}"
                /*v.txtPaymentStatus.text="Booking"
                v.txtPaymentStatus.textColorResource=R.color.colorMainOrange*/
            }
            v.txtPaymentStatus.textColorResource=if(model.transactionStatus.trim().equals("success",true)) R.color.colorMainBlue else R.color.colorErrorRed
        }
        else {
            v.txtPaymentId.text="#"+model.transactionTrackingId.trim()
            v.txtPaymentAmount.text = "AED ${model.transactionAmount.trim()}"

            if(model.transactionStatus.contains("Refund",true))
                v.txtPaymentStatus.textColorResource=R.color.colorMainOrange
            else
                v.txtPaymentStatus.textColorResource=if(model.transactionStatus.trim().equals("success",true)) R.color.colorMainBlue else R.color.colorErrorRed
        }

        v.txtPaymentStatus.text="${model.transactionStatus.capitalize()}"

        v.viewSeparator.visibility=if(pos==itemCount-1) View.GONE else View.VISIBLE

        v.progressLoadMore.visibility=View.GONE

        if(pos==itemCount-1)
            onLastItem(pos,v.progressLoadMore)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.PlaceModel
import kotlinx.android.synthetic.main.row_place_search.view.*


class PlaceAddressRecentSearchAdapter(var act: Activity, var arrayModel: ArrayList<PlaceModel>, var onItemClick: (pos: Int) -> Unit) : RecyclerView.Adapter<PlaceAddressRecentSearchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_place_search, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.txtPlaceTitle.text = model.placeTitle.trim()
        v.txtPlaceSubTitle.text = model.placeAddress.trim()

        v.linItem.setOnClickListener {
            onItemClick(pos)
        }

        v.viewBottomLine.visibility = if (pos == itemCount - 1) View.GONE else View.VISIBLE
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
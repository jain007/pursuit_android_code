package com.pursueit.adapters

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PromotionModel
import com.pursueit.utils.MyGlideOptions
import com.pursueit.utils.PromotionOperation
import kotlinx.android.synthetic.main.row_promotion.view.*

class PromotionListingAdapter(val act:AppCompatActivity,private val arrPromotion:ArrayList<PromotionModel>, var showMoreListener: (pos: Int, progressShowMore: ProgressWheel) -> Unit) : RecyclerView.Adapter<PromotionListingAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.row_promotion,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrPromotion.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setData(arrPromotion[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.relItem.setOnClickListener {
                val model = arrPromotion[adapterPosition]
             //   act.finish()
                PromotionOperation.setPromotionClickListenerForListing(act,model)
            }
        }

        fun setData(promotionModel: PromotionModel)
        {
            Glide.with(itemView.context)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_PROMOTION+promotionModel.promotionImage).into(itemView.imgPromotion)

            if (adapterPosition==arrPromotion.size-1)
                    showMoreListener(adapterPosition,itemView.progressShowMore)
            else
                itemView.progressShowMore.visibility=View.GONE
        }
    }

}
package com.pursueit.adapters

import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import kotlinx.android.synthetic.main.row_tag.view.*

class RecyclerTagAdapter(private val arrTags:ArrayList<String>): RecyclerView.Adapter<RecyclerTagAdapter.ViewHolder>() {
    var arrColorList = intArrayOf(R.color.colorTag1,R.color.colorTag2,R.color.colorTag3,R.color.colorTag4,R.color.colorTag5)
        //intArrayOf(R.drawable.drawable_tag1, R.drawable.drawable_tag2,R.drawable.drawable_tag3, R.drawable.drawable_tag4, R.drawable.drawable_tag5)
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_tag, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrTags.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrTags[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setContent(tag:String)
        {
            itemView.txtTag.text=tag
            itemView.txtTag.background.setColorFilter(ContextCompat.getColor(itemView.context,arrColorList[adapterPosition%5]),
                PorterDuff.Mode.SRC_IN)
        }
    }
}
package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.ActivityDetailActivity
import com.pursueit.activities.FlexiActivityDetailActivity
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ReviewModel
import com.pursueit.utils.MyGlideOptions
import kotlinx.android.synthetic.main.row_review.view.*


class ReviewsAdapter(var act: Activity, var arrayModel: ArrayList<ReviewModel>, var onReviewClick:(pos:Int)->Unit,var showMoreListener:(pos:Int,progressShowMore: ProgressWheel)->Unit) : RecyclerView.Adapter<ReviewsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_review, container, false))
    }

    override fun getItemCount(): Int {
        if( (act is ActivityDetailActivity || act is FlexiActivityDetailActivity) && arrayModel.size>2)
            return 2

        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val revModel=arrayModel[pos]

        //Log.d("ReviewRating",""+revModel.reviewRating)
        v.ratingBarRow.rating=revModel.reviewRating

        v.txtUserName.text=revModel.reviewUserName.trim()
        v.txtUserReview.text=revModel.reviewDesc.trim()
        v.txtUserTime.text=revModel.reviewTimeAgo.trim()

        Glide.with(v).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions())
            .load(if(revModel.isUserSocialImage) revModel.reviewUserImageUrl else Urls.IMAGE_USER+revModel.reviewUserImageUrl)
            .into(v.imgUser)

        v.relReviewParent.setOnClickListener {
            onReviewClick(pos)
        }

        v.progressShowMore.visibility=View.GONE

        if(pos==itemCount-1)
            showMoreListener(pos,v.progressShowMore)

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
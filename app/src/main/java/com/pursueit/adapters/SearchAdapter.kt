package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.model.SearchModel
import kotlinx.android.synthetic.main.row_activity_cat_sub_cat_search.view.*


class SearchAdapter(var act: Activity, var arrayModel: ArrayList<SearchModel>, var onClickedItem: (pos: Int) -> Unit) :
    RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    var onBind = true

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_activity_cat_sub_cat_search, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        onBind = true
        setContent(holder.itemView, pos)
        onBind = false
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.txtSearchItem.text = model.name.trim()

        if (pos > 0) {
            val prevModel = arrayModel[pos - 1]
            if (model.title != prevModel.title) {
                v.linSearchTitle.visibility = View.VISIBLE
                v.txtSearchTitle.text = model.title
            } else {
                v.linSearchTitle.visibility = View.GONE
            }
        } else {
            v.txtSearchTitle.text = model.title
            v.linSearchTitle.visibility = View.VISIBLE
        }

        v.relItem.setOnClickListener { onClickedItem(pos) }

        decideIcon(v,pos)

    }

    private fun decideIcon(v: View, pos: Int) {
        val model = arrayModel[pos]
        Glide.with(v).load(if(model.flag==3) R.drawable.ic_search_activities_classes else R.drawable.ic_search_categories).into(v.imgSearchTitle)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
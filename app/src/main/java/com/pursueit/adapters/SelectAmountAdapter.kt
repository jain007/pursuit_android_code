package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import kotlinx.android.synthetic.main.row_select_amount.view.*

class SelectAmountAdapter(private val arrAmount:ArrayList<Int>, val onAmountSelected:(Int)->Unit) : RecyclerView.Adapter<SelectAmountAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_select_amount,parent,false))
    }

    override fun getItemCount(): Int {
        return arrAmount.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setData(arrAmount[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.txtAmount.setOnClickListener {
                onAmountSelected(arrAmount[adapterPosition])
            }
        }

        fun setData(amount: Int)
        {
            itemView.txtAmount.text="AED $amount"
        }
    }

}
package com.pursueit.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.ActivityModel


class SimilarActivityAdapter(var act: Activity, var arrayModel: ArrayList<ActivityModel>) : RecyclerView.Adapter<SimilarActivityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_similar_activities, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        //Glide.with(v).load(if (pos % 2 == 0) R.drawable.ic_dummy_similar_activity_1 else R.drawable.ic_dummy_similar_activity_2).into(v.imgSimilarActivity)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        fun setContent(activityModel: ActivityModel)
        {

        }
    }
}
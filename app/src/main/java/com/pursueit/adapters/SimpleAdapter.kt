package com.pursueit.adapters

import android.app.Dialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.pursueit.R
import com.pursueit.model.SimpleModel
import kotlinx.android.synthetic.main.row_simple.view.*

class SimpleAdapter(val arraySimpleModel: ArrayList<SimpleModel>, val editText: EditText, val dialog:Dialog, val multiSelect:Boolean=false) : RecyclerView.Adapter<SimpleAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_simple,parent,false))
    }

    override fun getItemCount(): Int {
        return arraySimpleModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setData(arraySimpleModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            if (multiSelect)
            {
                itemView.chkItem.visibility=View.VISIBLE
            }
            else
            {
                itemView.chkItem.visibility=View.GONE
            }

            itemView.chkItem.setOnClickListener {
                arraySimpleModel[adapterPosition].isSelected=itemView.chkItem.isChecked
            }

            itemView.txtData.setOnClickListener {
                if (multiSelect)
                {
                    itemView.chkItem.performClick()
                }
                else {
                    val model = arraySimpleModel[adapterPosition]
                    editText.setText(model.data)
                    editText.tag = model.id

                    if (dialog.isShowing)
                        dialog.dismiss()
                }
            }
        }


        fun setData(simpleModel: SimpleModel){
            itemView.txtData.text=simpleModel.data
            itemView.chkItem.isChecked= simpleModel.isSelected
        }
    }

}
package com.pursueit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.pursueit.R
import com.pursueit.model.SortModel
import kotlinx.android.synthetic.main.row_sort.view.*

class SortAdapter( val arrSort:ArrayList<SortModel>) : RecyclerView.Adapter<SortAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.row_sort,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrSort.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrSort[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.consSort.setOnClickListener {
                itemView.chkSort.performClick()

            }
            itemView.chkSort.setOnClickListener {
                arrSort.forEach { it.status=false }
                arrSort[adapterPosition].status=true
                notifyDataSetChanged()
            }
            itemView.chkSort.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {

                }
            })
        }

        fun setContent(sortModel: SortModel)
        {
            itemView.txtSort.text=sortModel.name
            itemView.chkSort.isChecked=sortModel.status

        }
    }

}
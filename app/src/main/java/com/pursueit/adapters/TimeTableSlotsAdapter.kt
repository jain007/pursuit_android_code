package com.pursueit.adapters

import android.app.Activity
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.model.EnrolModel
import kotlinx.android.synthetic.main.row_activity_time_table.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundResource
import java.util.*


class TimeTableSlotsAdapter(var act: Activity, var arrayModel: ArrayList<EnrolModel>) :
    RecyclerView.Adapter<TimeTableSlotsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(act).inflate(R.layout.row_activity_time_table, container, false))
    }

    override fun getItemCount(): Int {
        return arrayModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        setContent(holder.itemView, pos)
    }

    private fun setContent(v: View, pos: Int) {
        val model = arrayModel[pos]
        v.txtSlotDate.text = model.enrolDate
        v.txtSlotDay.text = model.enrolDay
        v.txtSlotTime.text = model.enrolTime

        v.viewUnderLine.visibility = if (pos == itemCount - 1) View.GONE else View.VISIBLE

        if (model.isDateAlreadyPassed) {
            setInActiveUi(v)
        } else {
            setActiveUi(v)
        }
    }

    private fun setActiveUi(v:View){
        val bgColorNormal = android.R.color.transparent
        val txtColorNormal = ContextCompat.getColor(act, R.color.colorTimeTableContent)

        v.txtSlotDate.backgroundResource = bgColorNormal
        v.txtSlotDate.setTextColor(txtColorNormal)

        v.txtSlotTime.backgroundResource = bgColorNormal
        v.txtSlotTime.setTextColor(txtColorNormal)

        v.txtSlotDay.backgroundResource = bgColorNormal
        v.txtSlotDay.setTextColor(txtColorNormal)

        v.linSeparator1.backgroundResource=bgColorNormal
        v.linSeparator2.backgroundResource=bgColorNormal

        v.viewSeparatorLine1.backgroundResource=R.color.colorGreyLine
        v.viewSeparatorLine2.backgroundResource=R.color.colorGreyLine
        v.viewUnderLine.backgroundResource=R.color.colorGreyLine
    }

    private fun setInActiveUi(v:View){
        val bgColorDim = R.color.colorGreyLine
        val txtColorDim = ContextCompat.getColor(act, R.color.colorTextGreySubTitle)
        v.txtSlotDate.backgroundResource = bgColorDim
        v.txtSlotDate.setTextColor(txtColorDim)

        v.txtSlotTime.backgroundResource = bgColorDim
        v.txtSlotTime.setTextColor(txtColorDim)

        v.txtSlotDay.backgroundResource = bgColorDim
        v.txtSlotDay.setTextColor(txtColorDim)

        v.linSeparator1.backgroundResource=bgColorDim
        v.linSeparator2.backgroundResource=bgColorDim

        v.viewSeparatorLine1.backgroundColor= Color.WHITE
        v.viewSeparatorLine2.backgroundColor=Color.WHITE

        v.viewUnderLine.backgroundColor=Color.WHITE
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
package com.pursueit.adapters

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pursueit.R
import com.pursueit.activities.CampDetailActivity
import com.pursueit.model.WeekModel
import com.pursueit.utils.ChangeDateFormat
import kotlinx.android.synthetic.main.row_activity_time_table.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundResource

class WeekAdapterForCampDetail(private val arrWeekModel:ArrayList<WeekModel>, val act:CampDetailActivity) : RecyclerView.Adapter<WeekAdapterForCampDetail.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.row_activity_time_table,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrWeekModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrWeekModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setContent(weekModel: WeekModel)
        {
            itemView.txtSlotDate.text=weekModel.weekName
            itemView.txtSlotDay.text=weekModel.startDate
            itemView.txtSlotTime.text=weekModel.endDate

            val lastDate = ChangeDateFormat.getDateObjFromString(weekModel.endDate, "dd MMM yyyy")
            val currentDate = ChangeDateFormat.getDateObjFromString(act.campModel!!.currentDate, "dd MMM yyyy")
            if (currentDate!!.after(lastDate) || weekModel.remainingSeats == 0) {
                // itemView.linSelection.setBackgroundColor(Color.parseColor("#CCCCCC"))

                itemView.linTimeTable.backgroundColor=  Color.parseColor("#eeeeee")//ContextCompat.getColor(act, Color.parseColor("#eeeeee"))
            } else {

                itemView.linTimeTable.backgroundColor= ContextCompat.getColor(act, R.color.white)
                //itemView.linSelection.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }
        }
    }

}
package com.pursueit.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import com.pursueit.R
import com.pursueit.activities.CampDetailActivity
import com.pursueit.model.WeekModel
import com.pursueit.utils.ChangeDateFormat
import kotlinx.android.synthetic.main.row_week_selection.view.*

class WeekSelectionAdapter(
    val act: CampDetailActivity,
    private val arrWeekModel: ArrayList<WeekModel>,
    val btnDoneCampSelection: Button, val MAX_SELECTION:Int) :
    RecyclerView.Adapter<WeekSelectionAdapter.ViewHolder>() {

    var currentSelectionCount = 0
    var maxLimitReached = false

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_week_selection, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrWeekModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.setContent(arrWeekModel[pos])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.linSelection.setOnClickListener {
                itemView.chkWeekSelection.performClick()
            }

            itemView.chkWeekSelection.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                    if (p1) {
                        if (currentSelectionCount == MAX_SELECTION) {
                            //itemView.context.toast()
                            act.showErrorDialog("Only $MAX_SELECTION week(s) can be selected")
                            itemView.chkWeekSelection.isChecked = false
                            maxLimitReached = true
                        } else {
                            maxLimitReached = false
                        }
                    } else {
                        maxLimitReached = false
                    }
                }
            })

            itemView.chkWeekSelection.setOnClickListener {
                /*  if (itemView.chkWeekSelection.isChecked)
                  {
                      if (currentSelectionCount==MAX_SELECTION)
                      {
                          itemView.context.toast("Only $MAX_SELECTION weeks can be selected")
                          return@setOnClickListener
                      }
                      ++currentSelectionCount
                  }*/
                if (!maxLimitReached) {
                    if (itemView.chkWeekSelection.isChecked) {
                        ++currentSelectionCount
                    } else {

                        if (currentSelectionCount > 0)
                            --currentSelectionCount
                    }
                }
                arrWeekModel[adapterPosition].isSelected = itemView.chkWeekSelection.isChecked
                //  notifyItemChanged(adapterPosition)

                if (currentSelectionCount < MAX_SELECTION) {
                    btnDoneCampSelection.alpha = 0.5f
                    btnDoneCampSelection.isEnabled = false
                } else {
                    btnDoneCampSelection.alpha = 1f
                    btnDoneCampSelection.isEnabled = true
                }
            }

        }


        fun setContent(weekModel: WeekModel) {
            itemView.txtTitle.text =
                Html.fromHtml("<font color=#2b2b2b>${weekModel.weekName}</font> <font color=#9f9f9f>(${weekModel.startDate} - ${weekModel.endDate})")
            itemView.chkWeekSelection.isChecked = weekModel.isSelected
            if (weekModel.isSelected) {
                ++currentSelectionCount
            }

            val lastDate = ChangeDateFormat.getDateObjFromString(weekModel.endDate, "dd MMM yyyy")
            val currentDate = ChangeDateFormat.getDateObjFromString(act.campModel!!.currentDate, "dd MMM yyyy")
            if (currentDate!!.after(lastDate) || weekModel.remainingSeats == 0) {
                // itemView.linSelection.setBackgroundColor(Color.parseColor("#CCCCCC"))
                itemView.chkWeekSelection.isEnabled = false
                itemView.linSelection.isEnabled = false

                itemView.txtTitle.alpha = 0.5f
                itemView.chkWeekSelection.buttonTintList = ContextCompat.getColorStateList(act, R.color.colorDisabled)
            } else {
                itemView.chkWeekSelection.isEnabled = true
                itemView.linSelection.isEnabled = true
                itemView.txtTitle.alpha = 1f

                itemView.chkWeekSelection.buttonTintList = ContextCompat.getColorStateList(act, R.color.colorDiscount)
                //itemView.linSelection.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }
        }
    }


}
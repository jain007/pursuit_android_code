package com.pursueit.analytics

import android.app.Activity
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics


object MyFirebaseAnalytics {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    private fun initMyFirebase(act: Activity, userEmail: String = "") {
        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(act)
        }
        mFirebaseAnalytics?.setUserId(userEmail)
    }

    fun setCurrentScreen(act: Activity, screenName: String, myClass: Class<*>, userEmail: String = "") {
        initMyFirebase(act,userEmail)
        val firebaseScreenName=screenName.replace(" ","_").replace("-","_")
        mFirebaseAnalytics?.setCurrentScreen(act, firebaseScreenName, myClass.simpleName)

        //if user prefers a particular screen, go to that screen
        setUserPropertyInFirebase(act,screenName,screenName,userEmail)
    }

    fun sendCustomEvent(act: Activity, actionTitle: String, action: String, userEmail: String = "") {
        initMyFirebase(act,userEmail)
        val bundle = Bundle()
        bundle.putString(actionTitle, action)
        mFirebaseAnalytics?.logEvent(actionTitle, bundle)
    }

    fun setUserPropertyInFirebase(act: Activity, actionTitle: String, action: String, userEmail: String = "") {
        initMyFirebase(act,userEmail)
        if (actionTitle.startsWith(MyGoogleAnalytics.ACTIVITY_DETAIL_SCREEN))
            mFirebaseAnalytics?.setUserProperty(MyGoogleAnalytics.ACTIVITY_DETAIL_SCREEN,action.replace(MyGoogleAnalytics.ACTIVITY_DETAIL_SCREEN,"").trim())
    }
}
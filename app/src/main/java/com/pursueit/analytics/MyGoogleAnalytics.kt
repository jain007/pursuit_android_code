package com.pursueit.analytics

import android.app.Activity
import android.util.Log
import com.google.android.gms.analytics.HitBuilders
import com.pursueit.BuildConfig
import com.pursueit.MyApp
import com.pursueit.R


object MyGoogleAnalytics {

    private val TAG = "GOOGLE_ANALYTICS"

    val MAIN_SCREEN = "Main Screen - Google, Facebook, Register, Login"
    val DASHBOARD = "Dashboard Screen"
    val EXPLORE = "Explore Screen"
    val MY_BOOKINGS = "My Bookings Screen"
    val MORE_OPTIONS = "More Screen"
    val MORE_SLOTS_SCREEN = "More Activity Slots Screen"
    val MY_PROFILE = "My Profile Screen"
    val LOGIN_SCREEN = "Login Screen"
    val REGISTER_SCREEN = "Register Screen"
    val FORGOT_PASSWORD_SCREEN = "Forgot Password Screen"
    val VERIFY_CODE_SCREEN = "Verification Code Screen"
    val REVIEWS_LISTING_SCREEN = "Ratings/Reviews Listing Screen"
    val SEARCH_PLACE_SCREEN = "Search Place Screen"
    val WEB_VIEW_SCREEN = "Web View Screen"
    val UNAUTHENTICATED_SCREEN = "Unauthenticated - Session Expire Screen"
    val PAYMENTS_HISTORY_SCREEN = "Payment History Screen"
    val INIT_PAYMENT_SCREEN = "Initiate Payment Screen"
    val ABOUT_US_SCREEN = "About Us Screen"

    val ACTIVITY_DETAIL_SCREEN = "Activity Detail Screen"

    val CATEGORY_PAYMENT = "Payment_Booking"
    val ACTION_MAKE_PAYMENT = "Make Payment Hit"
    val ACTION_PAYMENT_INIT = "Payment Initiated"
    val ACTION_PAYMENT_FAILED = "Payment Failed"
    val ACTION_PAYMENT_SUCCESS = "Payment Success"

    val CATEGORY_LOGIN_METHOD = "Login_Method"
    val ACTION_GOOGLE = "Google Login"
    val ACTION_FACEBOOK = "Facebook Login"
    val ACTION_NORMAL_LOGIN = "Normal Login"
    val ACTION_NORMAL_REGISTER = "Normal Registration"

    fun sendSession(act: Activity, screenName: String, userEmail: String = "", myClass: Class<*>) {
        try {

            //Firebase Analytics
            MyFirebaseAnalytics.setCurrentScreen(act, screenName, myClass,userEmail)

            val mTracker = MyApp.getDefaultTracker()
            mTracker?.setClientId(if (userEmail.trim().isEmpty()) "Guest_User_Android" else userEmail + "_Android")
            Log.i(TAG, "Screen: $screenName")
            mTracker?.setTitle("Android")
            mTracker?.setAppId(BuildConfig.APPLICATION_ID)
            mTracker?.setAppName(act.getString(R.string.app_name))
            mTracker?.setAppVersion(BuildConfig.VERSION_NAME)
            mTracker?.setTitle(screenName)
            mTracker?.setPage(screenName)
            mTracker?.setScreenName(screenName)
            mTracker?.send(HitBuilders.ScreenViewBuilder().build())

            //In case google analytics doesn't show the screen views properly
            sendCustomEvent(act, "Screen_View", screenName, userEmail)

        } catch (e: Exception) {
            Log.i("Exception_session", "" + e)
        }

    }

    fun sendCustomEvent(act: Activity, category: String, action: String, userEmail: String = "") {
        try {
            //Firebase Analytics
            if (!category.equals("Screen_View", true))
                MyFirebaseAnalytics.sendCustomEvent(act, category, action,userEmail)


            val mTracker = MyApp.getDefaultTracker()
            mTracker?.setClientId(if (userEmail.trim().isEmpty()) "Guest_User_Android" else userEmail + "_Android")
            mTracker?.send(
                HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .build()
            )

        } catch (e: Exception) {
            Log.i("Exception_event_2", "" + e)
        }

    }
}
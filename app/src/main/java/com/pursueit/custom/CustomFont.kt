package com.pursueit.custom

import android.content.res.AssetManager
import android.graphics.Typeface


object CustomFont{


    fun setFontRegular(assetManager: AssetManager): Typeface {
        return Typeface.createFromAsset(assetManager, "fonts/source_sans_pro_regular.ttf")
    }

    fun setFontSemiBold(assetManager: AssetManager): Typeface {
        return Typeface.createFromAsset(assetManager, "fonts/source_sans_pro_semi_bold.ttf")
    }

    fun setFontBold(assetManager: AssetManager): Typeface {
        return Typeface.createFromAsset(assetManager, "fonts/source_sans_pro_bold.ttf")
    }
}
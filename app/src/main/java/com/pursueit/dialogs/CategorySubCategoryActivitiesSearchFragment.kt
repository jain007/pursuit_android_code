package com.pursueit.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.pursueit.R
import com.pursueit.activities.CampListingActivity
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.SearchAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.SearchModel
import com.pursueit.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.dialog_fragment_search_cat_sub_cat.*
import kotlinx.android.synthetic.main.dialog_fragment_search_cat_sub_cat.view.*
import kotlinx.android.synthetic.main.empty_search_or_filter.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.runOnUiThread
import org.jetbrains.anko.support.v4.toast
import org.json.JSONObject
import java.util.*
import java.util.concurrent.TimeUnit


@SuppressLint("SetTextI18n")
class CategorySubCategoryActivitiesSearchFragment : DialogFragment() {

    lateinit var act: AppCompatActivity
    lateinit var dialogMsg: DialogMsg
    private var vi: View? = null

    //private var searchAdapter: CategorySubCategoryAdapter? = null

    lateinit var mySnackBar: MySnackBar

    private var compositeDisposable = CompositeDisposable()

    private var arraySearchModel = ArrayList<SearchModel>()
    private var adapter: SearchAdapter? = null

    var fromCampListing:Boolean=false

    var searchActAndCamp:Boolean=false

   // var fromPass:Boolean=false

    companion object{

        var campListingAct:CampListingActivity?=null

        fun newInstance(fromCampListing:Boolean,campFragment:CampListingActivity):CategorySubCategoryActivitiesSearchFragment
        {
            campListingAct=campFragment
            val bundle=Bundle()
            bundle.putBoolean("fromCampListing",fromCampListing)
            val fragment=CategorySubCategoryActivitiesSearchFragment()
            fragment.arguments=bundle
            return fragment
        }

        fun newInstanceToSearchActivityAndCamp():CategorySubCategoryActivitiesSearchFragment
        {
            val bundle=Bundle()
            bundle.putBoolean("searchActAndCamp",true)
            val fragment=CategorySubCategoryActivitiesSearchFragment()
            fragment.arguments=bundle
            return fragment
        }

        fun newInstanceForPass():CategorySubCategoryActivitiesSearchFragment{
            val bundle=Bundle()
            bundle.putBoolean("fromPass",true)
            val fragment=CategorySubCategoryActivitiesSearchFragment()
            fragment.arguments=bundle
            return fragment
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        act = context as AppCompatActivity
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_fragment_search_cat_sub_cat, container, false)
        view.setOnClickListener { }

        try {
            fromCampListing=this.arguments?.getBoolean("fromCampListing",false) ?:false
            searchActAndCamp=this.arguments?.getBoolean("searchActAndCamp",false)?:false
          //  fromPass=this?.arguments?.getBoolean("fromPass",false)?:false

            view.etSearchSubCatFragment.requestFocus()
            val  imm = act.getSystemService(Context.INPUT_METHOD_SERVICE) as (InputMethodManager)
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        }catch (e:Exception)
        {

        }

        vi = view
        init()
        return view
    }


    private fun init() {
        compositeDisposable = CompositeDisposable()
        dialogMsg = DialogMsg()
        if (vi != null) {
            mySnackBar = MySnackBar(act = act, view = vi!!.linParentSearchCat)
            when {
                fromCampListing -> {
                    vi!!.toolbarFragment.txtToolbarHeader.text = "Search Camp"
                    vi!!.etSearchSubCatFragment.hint="Camp name or Tag name"
                }
                searchActAndCamp -> vi!!.toolbarFragment.txtToolbarHeader.text = "Search Category, Activity, Camp"
                else -> vi!!.toolbarFragment.txtToolbarHeader.text = "Search Category & Activity"
            }
            vi!!.toolbarFragment.imgBackNavigation.setImageResource(R.drawable.ic_dialog_close)
            vi!!.toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
            vi!!.toolbarFragment.imgBackNavigation.setOnClickListener {
                dismiss()
            }

            vi!!.rvCategorySearch.apply {
                this.layoutManager = LinearLayoutManager(act.applicationContext)
                this.setHasFixedSize(true)
            }


            //search functionality
            compositeDisposable.add(RxTextView.textChangeEvents(vi!!.etSearchSubCatFragment)
                .skip(3)
                .debounce(800, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { err ->
                    Log.d("RxErrSearch", "" + err)
                }
                .subscribe {
                    val strInputName = it.text().toString().trim()
                    vi!!.imgRemoveSearch.visibility = if (strInputName.isEmpty()) View.GONE else View.VISIBLE
                    if (strInputName.length >= 3) {
                        fastFetchSearchResponse(strInputName)
                    } else {
                        vi!!.cnsEmptySearchAndFilter.visibility=View.GONE
                        arraySearchModel.clear()
                        vi!!.rvCategorySearch.adapter?.notifyDataSetChanged()
                    }
                })

            compositeDisposable.add(RxView.clicks(vi!!.imgRemoveSearch).throttleFirst(500, TimeUnit.MILLISECONDS).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                etSearchSubCatFragment.setText("")
                vi!!.imgRemoveSearch.visibility = View.GONE
            })

            //fill recent searches when coming first time
            if(arraySearchModel.isEmpty()){
                if (searchActAndCamp)
                {
                    arraySearchModel.addAll(getLocalRecentSearches(true))
                    arraySearchModel.addAll(getLocalRecentSearches(false))
                }
                else {
                    arraySearchModel.addAll(getLocalRecentSearches(fromCampListing))
                }
                if(arraySearchModel.size>0) {
                    arraySearchModel.sortWith(Comparator { obj1, obj2 -> obj1.flag.compareTo(obj2.flag) })
                    fillAdapter()
                }
            }
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun fastFetchSearchResponse(strSearchName: String = "") {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {

            vi!!.txtErrorMessage.text= "Sorry we could not find any result matching \"$strSearchName\""

            vi!!.progressSearch.visibility = View.VISIBLE
            vi!!.cnsEmptySearchAndFilter.visibility = View.GONE
            val hashParams = HashMap<String, String>()
            hashParams["searchName"] = strSearchName

            val url= if(searchActAndCamp) Urls.HOME_SCREEN_SEARCH else if (fromCampListing) Urls.SEARCH_CAMP else Urls.SEARCH_ACTIVITIES

            FastNetworking.makeRxCallPost(act.applicationContext, url, true, hashParams, "Search", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    doAsync {
                        try {
                            arraySearchModel.clear()

                            if (searchActAndCamp)
                            {
                                arraySearchModel.addAll(JsonParse.parseCampSearchResponse(json))
                                arraySearchModel.addAll(JsonParse.parseSearchResponse(json))
                            }
                            else if (fromCampListing)
                                arraySearchModel.addAll(JsonParse.parseCampSearchResponse(json))
                            else
                                arraySearchModel.addAll(JsonParse.parseSearchResponse(json))

                            runOnUiThread {
                                try {
                                    vi!!.progressSearch.visibility = View.GONE

                                    fillAdapter()

                                    if (arraySearchModel.size < 1) {
                                        vi!!.cnsEmptySearchAndFilter.visibility = View.VISIBLE
                                        vi!!.rvCategorySearch.visibility = View.GONE
                                    } else {
                                        vi!!.cnsEmptySearchAndFilter.visibility = View.GONE
                                        vi!!.rvCategorySearch.visibility = View.VISIBLE
                                    }
                                } catch (e: Exception) {
                                    Log.d("ExcCaught", "" + e)
                                }

                            }

                        } catch (e: Exception) {
                            Log.d("ExcAfterParse", "" + e)
                        }
                    }

                }

                override fun onApiError(error: ANError) {
                    vi!!.progressSearch.visibility = View.GONE
                    vi!!.cnsEmptySearchAndFilter.visibility = View.GONE
                    toast(ErrorMsgs.ERR_API_MSG)

                }


            },compositeDisposable)
        } else {
            dialogMsg.showErrorConnectionDialog(act, "OK", View.OnClickListener { dialogMsg.dismissDialog(act) })
        }
    }

    private fun fillAdapter(){
        if (adapter == null) {
            adapter = SearchAdapter(act, arraySearchModel) { posClicked ->
                if(posClicked>=0){
                    try {
                        vi?.let { v ->
                            try {
                                val imm = act.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                                imm?.hideSoftInputFromWindow(v.windowToken, 0)
                            }catch (e:Exception){
                                Log.d("ExcKeyboard",""+e)
                            }
                        }
                    }catch (e:Exception){

                    }
                    val model = arraySearchModel[posClicked]
                    if (searchActAndCamp)
                    {
                        setModelOnLocal(arraySearchModel[posClicked],(model.flag==5 || model.flag==6))
                        if (model.flag==5 || model.flag==6) // 5 for searching by camp name, 6 for searching by tag name
                        {
                           // campSearching(model)
                            DashboardActivity.filterCampOnSearchClick(act,arraySearchModel[posClicked])
                        }
                        else{

                         //   DashboardActivity.showPassActivities=model.isPassAct

                            DashboardActivity.filterActivitiesOnSearchClick(act, arraySearchModel[posClicked],fromPass = model.isPassAct)
                        }
                    }
                    else {
                        setModelOnLocal(arraySearchModel[posClicked], fromCampListing)
                        if (fromCampListing) {
                           campSearching(model)
                        }/*else if (fromPass){
                            DashboardActivity.filterActivitiesOnSearchClick(act, arraySearchModel[posClicked],fromPass)
                        } */else {

                       //     DashboardActivity.showPassActivities=model.isPassAct

                            DashboardActivity.filterActivitiesOnSearchClick(act, arraySearchModel[posClicked],fromPass = model.isPassAct)
                        }
                    }
                }
            }
            vi!!.rvCategorySearch.adapter = adapter
        } else {
            vi!!.rvCategorySearch?.adapter?.notifyDataSetChanged()
        }
    }

    private fun campSearching(model:SearchModel)
    {
        if (model.flag == 5) {
            CampListingActivity.campId = model.catId
            CampListingActivity.tagId=""
        } else if (model.flag == 6) {
            CampListingActivity.tagId = model.catId
            CampListingActivity.campId=""
        }
        CampListingActivity.searchViaCampNameOrTagName = true
        campListingAct?.clearListAndMakeApiCall(false)
        dismiss()
    }

    /*private fun fillAdapter(arrayModel: ArrayList<CategoryModel> = StaticValues.arrayCatSubCatModel) {
        *//*for(model in arrayModel){
            Log.d("Model__","" + model + "\n" + model.subCatId + "\n" + model.subCatName)
        }*//*

        val searchAdapter = CategorySubCategoryAdapter(act, arrayModel) { pos ->
            val model = arrayModel[pos]
            Log.d("SelectedModel", "" + model + "\n" + model.subCatId + "\n" + model.subCatName)
            dismiss()
            if (act is DashboardActivity) {
                DashboardActivity.selectedCatModel = model
                DashboardActivity.showSearchCatAndActivityPopUp(act)
            }
        }
        vi!!.rvCategorySearch.adapter = searchAdapter

    }

    private fun fastFetchCategoryAndSubCategory() {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext) && StaticValues.arrayCatSubCatModel.size < 1) {
            DialogMsg.showPleaseWait(act)
            FastNetworking.makeRxCallPost(act.applicationContext, Urls.VIEW_ALL_CAT_SUB_CAT, true, HashMap<String, String>(), "AllCatSubCat", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    try {
                        DialogMsg.dismissPleaseWait(act)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            doAsync {
                                JsonParse.parseCategoryAndSubCategory(json)
                                runOnUiThread {
                                    fillAdapter()
                                }
                            }

                        } else {
                            mySnackBar.showSnackBarError("Oops! No categories/subcategories found")
                        }
                    } catch (e: Exception) {
                        dialogMsg.showErrorApiDialog(act, "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(act)
                        }, true)
                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(act)
                    dialogMsg.showErrorApiDialog(act, "Retry", View.OnClickListener {
                        dialogMsg.dismissDialog(act)
                        fastFetchCategoryAndSubCategory()
                    }, true)
                }

            })
        } else {
            dialogMsg.showErrorConnectionDialog(act, "OK", View.OnClickListener {
                dialogMsg.dismissDialog(act)
            }, true)
        }
    }*/

    private fun setModelOnLocal(searchModel: SearchModel, fromCampSearching:Boolean) {
        val key=if (fromCampSearching) "RecentCampSearches" else "RecentActivitySearches"
        doAsync {
            try {

                Log.d("SearchSetOnLocal",searchModel.toString())
                val prefs = GetSetSharedPrefs(act.applicationContext)
                val searchDataSaved = prefs.getData(key)
                var arrayData = ArrayList<SearchModel>()
                val gson = Gson()

                if (searchDataSaved.trim().isEmpty()) {
                    arrayData.add(searchModel)
                } else {
                    arrayData = gson.fromJson(searchDataSaved, object : TypeToken<List<SearchModel>>() {}.type)
                    val isSearchDataAlreadyThere = arrayData.any { it.name == searchModel.name}
                    Log.d("SearchModel__", searchModel.toString())
                    Log.d("ArrayPlaceData__", arrayData.toString())

                    if (!isSearchDataAlreadyThere) {
                        arrayData.add(searchModel)
                    }else{
                        arrayData.filter { it.name== searchModel.name }.forEach {
                            it.catId = searchModel.catId
                            it.subCatId = searchModel.subCatId
                            it.flag = searchModel.flag
                            it.activityId = searchModel.activityId
                            it.isPassAct = searchModel.isPassAct
                            it.title = searchModel.title
                        }
                    }


                }

                Log.d("Before_Updating", "----------------------------------------")
                arrayData.forEach {
                    Log.d("Place_", "" + it)
                }

                Log.d("After_Updating", "------------------------------------------")


                if (arrayData.size > 4) {
                    arrayData.removeAt(0)
                }

                arrayData.forEach {
                    Log.d("Place_", "" + it)
                }

                val strJsonSearchData = gson.toJson(arrayData)
                Log.d("Local_Saved_", strJsonSearchData)
                prefs.putData(key, strJsonSearchData)


            } catch (e: Exception) {
                Log.d("ExcLocal", "" + e)
                GetSetSharedPrefs(act.applicationContext).putData(key, "")
            }
        }

    }

    private fun getLocalRecentSearches(fromCampSearching: Boolean): ArrayList<SearchModel> {
        val key=if (fromCampSearching) "RecentCampSearches" else "RecentActivitySearches"
        try {
            var arraySearchModel = ArrayList<SearchModel>()
            val prefs = GetSetSharedPrefs(act.applicationContext)
            val searchDataSaved = prefs.getData(key)
            Log.d("Local_Get", searchDataSaved)
            val gson = Gson()
            return if (searchDataSaved.trim().isEmpty()) {
                arraySearchModel
            } else {
                arraySearchModel = gson.fromJson(searchDataSaved, object : TypeToken<List<SearchModel>>() {}.type)
                arraySearchModel
            }
        } catch (e: Exception) {
            Log.d("ExcLocalFetch", "" + e)
            GetSetSharedPrefs(act.applicationContext).putData(key, "")
        }
        return ArrayList<SearchModel>()
    }
}
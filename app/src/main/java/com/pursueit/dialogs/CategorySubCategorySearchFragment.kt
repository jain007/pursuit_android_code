package com.pursueit.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.androidnetworking.error.ANError
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.CategorySubCategoryAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CategoryModel
import com.pursueit.utils.ConnectionDetector
import com.pursueit.utils.JsonParse
import com.pursueit.utils.StaticValues
import com.pursueit.utils.MySnackBar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.dialog_fragment_search_cat_sub_cat.*
import kotlinx.android.synthetic.main.dialog_fragment_search_cat_sub_cat.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.runOnUiThread
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class CategorySubCategorySearchFragment : DialogFragment() {

    lateinit var act: AppCompatActivity
    lateinit var dialogMsg: DialogMsg
    private var vi: View? = null

    //private var searchAdapter: CategorySubCategoryAdapter? = null

    lateinit var mySnackBar: MySnackBar

    private var compositeDisposable = CompositeDisposable()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        act = context as AppCompatActivity
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.dialog_fragment_search_cat_sub_cat, container, false)
        view.setOnClickListener { }
        vi = view
        init()
        return view
    }


    private fun init() {
        compositeDisposable = CompositeDisposable()
        dialogMsg = DialogMsg()
        if (vi != null) {
            mySnackBar = MySnackBar(act = act, view = vi!!.linParentSearchCat)
            vi!!.toolbarFragment.txtToolbarHeader.text = "Search Category & Activity"
            vi!!.toolbarFragment.imgBackNavigation.setImageResource(R.drawable.ic_dialog_close)
            vi!!.toolbarFragment.imgBackNavigation.visibility = View.VISIBLE
            vi!!.toolbarFragment.imgBackNavigation.setOnClickListener {
                dismiss()
            }

            vi!!.rvCategorySearch.apply {
                this.layoutManager = LinearLayoutManager(act.applicationContext)
                this.setHasFixedSize(true)
            }

            if (StaticValues.arrayCatSubCatModel.size < 1) {
                fastFetchCategoryAndSubCategory()
            } else {
                fillAdapter()
            }

            //search functionality
            compositeDisposable.add(RxTextView.textChangeEvents(vi!!.etSearchSubCatFragment)
                .skip(1)
                .debounce(600, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { err ->
                    Log.d("RxErrSearch", "" + err)
                }
                .map { charSeq ->
                    val strInput = charSeq.text().toString().trim()
                    vi!!.imgRemoveSearch.visibility =
                        if (strInput.isNotEmpty()) View.VISIBLE else View.GONE
                    val arraySearchedModel = ArrayList<CategoryModel>()
                    Log.d("StrInput", strInput)
                    for (model in StaticValues.arrayCatSubCatModel) {
                        if (model.catName.contains(strInput, true))
                            arraySearchedModel.add(model)
                    }

                    arraySearchedModel
                }
                .subscribe { arraySearchedModel ->
                    fillAdapter(arraySearchedModel)
                })

            compositeDisposable.add(
                RxView.clicks(vi!!.imgRemoveSearch).throttleFirst(
                    500,
                    TimeUnit.MILLISECONDS
                ).subscribeOn(AndroidSchedulers.mainThread()).subscribe {
                    etSearchSubCatFragment.setText("")
                })
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun fillAdapter(arrayModel: ArrayList<CategoryModel> = StaticValues.arrayCatSubCatModel) {
        for(model in arrayModel){
            Log.d("Model__","" + model + "\n" + model.subCatId + "\n" + model.subCatName)
        }

        val searchAdapter = CategorySubCategoryAdapter(act, arrayModel) { pos ->
            val model = arrayModel[pos]
            Log.d("SelectedModel", "" + model + "\n" + model.subCatId + "\n" + model.subCatName)
            dismiss()
            if (act is DashboardActivity) {
                //DashboardActivity.selectedCatModel = model
                DashboardActivity.showSearchCatAndActivityPopUp(act)
            }
        }
        vi!!.rvCategorySearch.adapter = searchAdapter

    }

    private fun fastFetchCategoryAndSubCategory() {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext) && StaticValues.arrayCatSubCatModel.size < 1) {
            DialogMsg.showPleaseWait(act)
            FastNetworking.makeRxCallPost(
                act.applicationContext,
                Urls.VIEW_ALL_CAT_SUB_CAT,
                true,
                HashMap<String, String>(),
                "AllCatSubCat",
                object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            DialogMsg.dismissPleaseWait(act)
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                doAsync {
                                    JsonParse.parseCategoryAndSubCategory(json)
                                    runOnUiThread {
                                        fillAdapter()
                                    }
                                }

                            } else {
                                mySnackBar.showSnackBarError("Oops! No categories/subcategories found")
                            }
                        } catch (e: Exception) {
                            dialogMsg.showErrorApiDialog(act, "OK", View.OnClickListener {
                                dialogMsg.dismissDialog(act)
                            }, true)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(act)
                        dialogMsg.showErrorApiDialog(act, "Retry", View.OnClickListener {
                            dialogMsg.dismissDialog(act)
                            fastFetchCategoryAndSubCategory()
                        }, true)
                    }

                })
        } else {
            dialogMsg.showErrorConnectionDialog(act, "OK", View.OnClickListener {
                dialogMsg.dismissDialog(act)
            }, true)
        }
    }
}
package com.pursueit.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.PorterDuff
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.Spanned
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.activities.CampListingActivity
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.FilterDayAdapter
import com.pursueit.adapters.SelectAmountAdapter
import com.pursueit.adapters.SortAdapter
import com.pursueit.model.AgeGroupModel
import com.pursueit.model.FilterDayModel
import com.pursueit.model.SimpleModel
import com.pursueit.model.SortModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.content_dashboard_toolbar.*
import kotlinx.android.synthetic.main.content_dashboard_toolbar.view.*
import kotlinx.android.synthetic.main.dialog_error.*
import kotlinx.android.synthetic.main.dialog_filter_act_camp.*
import kotlinx.android.synthetic.main.dialog_progress.*
import kotlinx.android.synthetic.main.dialog_select_amount.*
import kotlinx.android.synthetic.main.dialog_single_item_picker.*
import kotlinx.android.synthetic.main.dialog_sort.*
import kotlinx.android.synthetic.main.dialog_yes_cancel.*
import kotlinx.android.synthetic.main.dialog_yes_cancel_app_update.*
import org.jetbrains.anko.textColor
import java.util.*
import kotlin.collections.ArrayList

class DialogMsg {

    var myDialogMsg: Dialog? = null

    companion object {
        var myDialogPleaseWait: Dialog? = null
        fun showPleaseWait(act: Activity,
                           strWaitMsg: String = "Please wait...",
                           isCancellable: Boolean = false) {
            //if (myDialogPleaseWait == null)
            dismissPleaseWait(act)
            myDialogPleaseWait = MyDialog(act).getMyDialog(R.layout.dialog_progress)

            myDialogPleaseWait?.txtPleaseWait?.text = strWaitMsg
            myDialogPleaseWait?.setCancelable(isCancellable)

            try {
                if (!act.isFinishing)
                    myDialogPleaseWait?.show()
            } catch (e: Exception) {
                Log.d("DialogErr", "" + e)
            }
        }

        fun dismissPleaseWait(act: Activity) {
            try {
                if (myDialogPleaseWait != null) {
                    if (!act.isFinishing)
                        myDialogPleaseWait?.dismiss()
                }
            } catch (e: Exception) {
                Log.d("PleaseWaitExc", "" + e)
            }
        }

        fun animateViewDialog(view: View?, duration: Long = 350) {
            if (view != null) {
                Log.d("OS_VERSION", "" + Build.VERSION.SDK_INT)
                when {
                    Build.MANUFACTURER.equals("Xiaomi", true) && Build.VERSION.SDK_INT < Build.VERSION_CODES.N -> {

                    }
                    else -> view.post {
                        try {
                            val anim = ScaleAnimation(
                                1.0f,
                                1.0f,
                                0.0f,
                                1.0f,
                                Animation.RELATIVE_TO_SELF,
                                0.5f,
                                Animation.RELATIVE_TO_SELF,
                                0.5f
                            )
                            anim.duration = 400
                            view.startAnimation(anim)
                        } catch (e: Exception) {
                            Log.d("Exc_Animation_Device", "" + e)
                        } catch (e: Error) {
                            Log.d("Exc_Error_Device", "" + e)
                        }
                    }
                }
            }

        }
    }

    fun showSelectAmountDialog(act: Activity, txtSelectAmount: TextView) {
        val dialog = MyDialog(act).getMyDialog(R.layout.dialog_select_amount)
        dialog.rvSelectAmount.layoutManager = LinearLayoutManager(act)
        dialog.rvSelectAmount.addItemDecoration(DividerItemDecoration(act, DividerItemDecoration.VERTICAL))

        dialog.rvSelectAmount.adapter = SelectAmountAdapter(getAmountArray()) { amount ->
            txtSelectAmount.text = "AED $amount"
            txtSelectAmount.tag = amount.toString()
            if (dialog.isShowing)
                dialog.dismiss()
        }
        dialog.setCancelable(true)

        dialog.imgCloseSelectAmountDialog.setOnClickListener {
            if (dialog.isShowing)
                dialog.dismiss()
        }

        if (!act.isFinishing)
            dialog.show()
    }


    fun showItemPickerDialog(act: Activity,
                             editText: EditText,
                             array: ArrayList<SimpleModel>,
                             title: String,
                             multiSelect: Boolean = false,
                             showSearchEditText: Boolean = false
                             ,
                             searchHint: String = "") {
        val dialog = MyDialog(act).getMyDialog(R.layout.dialog_single_item_picker)

        if (multiSelect)
            dialog.btnDoneSelection.visibility = View.VISIBLE
        else
            dialog.btnDoneSelection.visibility = View.GONE

        dialog.txtItemTitle.text = title
        dialog.rvItem.layoutManager = LinearLayoutManager(act)
        dialog.rvItem.addItemDecoration(DividerItemDecoration(act, DividerItemDecoration.VERTICAL))


        lateinit var adapter: com.pursueit.adapters.SimpleAdapter

        dialog.txtClearData.setOnClickListener {
            if (multiSelect) {
                array.forEach {
                    it.isSelected = false
                }
                adapter.notifyDataSetChanged()
            } else {
                editText.setText("")
                editText.tag = ""
                dialog.imgItemClose.performClick()
            }
        }

        fun setAdapter() {
            adapter = com.pursueit.adapters.SimpleAdapter(array, editText, dialog, multiSelect)

            dialog.rvItem.adapter = adapter
        }

        if (showSearchEditText) {
            dialog.etSearchItem.hint = searchHint
            dialog.etSearchItem.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(str: Editable?) {
                    str.toString().run {
                        if (array.any { it.data.contains(this, true) }) {
                            val filteredArray = array.filter { it.data.contains(this, true) }
                            adapter = com.pursueit.adapters.SimpleAdapter(
                                ArrayList(filteredArray),
                                editText,
                                dialog,
                                multiSelect
                            )
                            dialog.rvItem.adapter = adapter
                            Log.d("filteredArray", filteredArray.size.toString())
                            if (filteredArray.isEmpty()) {
                                dialog.txtNoRecordFound.visibility = View.VISIBLE
                                dialog.rvItem.visibility = View.GONE
                            } else {
                                dialog.txtNoRecordFound.visibility = View.GONE
                                dialog.rvItem.visibility = View.VISIBLE
                            }
                        } else if (this.trim() == "") {
                            dialog.txtNoRecordFound.visibility = View.GONE
                            dialog.rvItem.visibility = View.VISIBLE
                            setAdapter()
                        } else {
                            dialog.txtNoRecordFound.visibility = View.VISIBLE
                            dialog.rvItem.visibility = View.GONE
                        }
                    }

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

            })
        } else {
            dialog.etSearchItem.visibility = View.GONE
        }

        /* val adapter=com.pursueit.adapters.SimpleAdapter(array,editText,dialog,multiSelect)

         dialog.rvItem.adapter=adapter*/

        setAdapter()

        dialog.setCancelable(true)

        dialog.btnDoneSelection.setOnClickListener {
            if (multiSelect) {
                //   if (adapter.arraySimpleModel.any { it.isSelected })
                //   {
                if (adapter.arraySimpleModel.any { it.isSelected }) {
                    val filteredArray = adapter.arraySimpleModel.filter { it.isSelected }
                    editText.setText(filteredArray.joinToString { it.data })
                    editText.tag = (filteredArray.joinToString(separator = ",") { it.id })

                } else {
                    editText.setText("")
                    editText.tag = ""
                }
                dialog.imgItemClose.performClick()
                //  }
                /* else
                 {
                     showErrorRetryDialog(act, "Alert", "Please select at lease one interest", "OK", View.OnClickListener {
                         dismissDialog(act)
                     })
                 }*/
            }
        }

        dialog.imgItemClose.setOnClickListener {
            if (dialog.isShowing)
                dialog.dismiss()

        }
        if (!act.isFinishing)
            dialog.show()
    }

    private fun getAmountArray(): ArrayList<Int> {
        val minAmount = 50
        val maxAmount = 1000
        val interval = 50

        val arrAmount = ArrayList<Int>()
        for (amount in minAmount..maxAmount step interval) {
            Log.d("amount", amount.toString())
            arrAmount.add(amount)
        }
        return arrAmount
    }

    fun showErrorRetryDialog(
        act: Activity,
        strTitle: String,
        strMsg: String,
        strBtnText: String = "Retry",
        onRetryListener: View.OnClickListener,
        isCancellable: Boolean = true
    ) {
        /* myDialogMsg = LovelyStandardDialog(act)
             .setTopColorRes(R.color.colorErrorRed)
             .setIcon(R.drawable.ic_error)
             .setTitle(strTitle)
             .setMessage(strMsg)
             .setPositiveButton(strBtnText,onRetryListener)
             .show()*/

        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_error)
        myDialogMsg?.setCancelable(isCancellable)
        myDialogMsg?.linHeaderDialog?.setBackgroundColor(ContextCompat.getColor(act, R.color.colorErrorRed))
        myDialogMsg?.txtErrorHeader?.text = strTitle
        myDialogMsg?.txtErrorMsg?.text = strMsg
        myDialogMsg?.txtOkRetry?.text = strBtnText
        myDialogMsg?.txtOkRetry?.setOnClickListener(onRetryListener)

        try {
            animateViewDialog(myDialogMsg?.linErrParent)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }

    }

    fun showSuccessDialog(
        act: Activity,
        strTitle: String,
        strMsg: String,
        strBtnText: String = "OK",
        onRetryListener: View.OnClickListener,
        isCancellable: Boolean = true
        , spannedMsg: Spanned? = null) {
        /* myDialogMsg = LovelyStandardDialog(act)
             .setTopColorRes(R.color.colorErrorRed)
             .setIcon(R.drawable.ic_error)
             .setTitle(strTitle)
             .setMessage(strMsg)
             .setPositiveButton(strBtnText,onRetryListener)
             .show()*/

        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_error)
        myDialogMsg?.setCancelable(isCancellable)
        myDialogMsg?.imgError?.setImageResource(R.drawable.ic_success)
        myDialogMsg?.linHeaderDialog?.setBackgroundColor(ContextCompat.getColor(act, R.color.colorMainBlue))
        myDialogMsg?.txtErrorHeader?.text = strTitle
        if (spannedMsg != null)
            myDialogMsg?.txtErrorMsg?.text = spannedMsg
        else
            myDialogMsg?.txtErrorMsg?.text = strMsg
        myDialogMsg?.txtOkRetry?.text = strBtnText
        myDialogMsg?.txtOkRetry?.setOnClickListener(onRetryListener)

        myDialogMsg?.txtOkRetry?.setTextColor(ContextCompat.getColor(act, R.color.colorMainBlue))

        try {
            animateViewDialog(myDialogMsg?.linErrParent)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }

    }

    fun showErrorApiDialog(
        act: Activity,
        strBtnText: String = "Retry",
        onRetryListener: View.OnClickListener,
        isCancellable: Boolean = true
    ) {
        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_error)
        myDialogMsg?.linHeaderDialog?.setBackgroundColor(ContextCompat.getColor(act, R.color.colorErrorRed))
        myDialogMsg?.txtErrorHeader?.text = ErrorMsgs.ERROR_API_TITLE
        myDialogMsg?.txtErrorMsg?.text = ErrorMsgs.ERR_API_MSG
        myDialogMsg?.txtOkRetry?.text = strBtnText
        myDialogMsg?.txtOkRetry?.setOnClickListener(onRetryListener)

        myDialogMsg?.setCancelable(isCancellable)

        try {
            animateViewDialog(myDialogMsg?.linErrParent)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }
    }

    fun showErrorConnectionDialog(
        act: Activity,
        strBtnText: String = "Retry",
        onRetryListener: View.OnClickListener,
        isCancellable: Boolean = true) {
        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_error)
        myDialogMsg?.imgError?.setImageResource(R.drawable.ic_error_connection)
        myDialogMsg?.linHeaderDialog?.setBackgroundColor(ContextCompat.getColor(act, R.color.colorErrorRed))
        myDialogMsg?.txtErrorHeader?.text = ErrorMsgs.ERROR_API_TITLE
        myDialogMsg?.txtErrorMsg?.text = ErrorMsgs.ERR_CONNECTION_MSG
        myDialogMsg?.txtOkRetry?.text = strBtnText
        myDialogMsg?.setCancelable(isCancellable)
        myDialogMsg?.txtOkRetry?.setOnClickListener(onRetryListener)

        try {
            animateViewDialog(myDialogMsg?.linErrParent)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }
    }

    fun showYesCancelDialog(
        act: Activity,
        strMsg: String,
        onYesListener: View.OnClickListener,
        isCancellable: Boolean = true,
        strBtnYesText: String = "Yes",
        strBtnNoText: String = "Cancel"
    ) {

        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_yes_cancel)
        myDialogMsg?.setCancelable(isCancellable)
        myDialogMsg?.txtErrorMsgDialogYesCancel?.text = strMsg

        myDialogMsg?.txtYes?.text = strBtnYesText
        myDialogMsg?.txtCancel?.text = strBtnNoText

        myDialogMsg?.txtYes?.setOnClickListener(onYesListener)
        myDialogMsg?.txtCancel?.setOnClickListener {
            myDialogMsg?.dismiss()
        }

        try {
            animateViewDialog(myDialogMsg?.linErrParentYesCancel)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }

    }

    fun showYesCancelAppUpdateAvailable(
        act: Activity,
        onYesListener: View.OnClickListener,
        isMandatoryUpdate: Boolean = false
    ) {
        try {
            myDialogMsg?.dismiss()
        } catch (e: Exception) {
        }


        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_yes_cancel_app_update)
        myDialogMsg?.setCancelable(false)

        try {
            Glide.with(act).load(R.drawable.ic_logo).into(myDialogMsg!!.imgLogo)
        } catch (e: Exception) {

        }
        myDialogMsg?.txtYesAppUpdate?.setOnClickListener(onYesListener)

        myDialogMsg?.txtCancelAppUpdate?.visibility = if (isMandatoryUpdate) View.GONE else View.VISIBLE
        myDialogMsg?.txtCancelAppUpdate?.setOnClickListener {
            myDialogMsg?.dismiss()
        }

        try {
            // animateViewDialog(myDialogMsg?.linParentAppUpdate)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }
    }

    fun showSortingDialog(
        act: Activity, isComingFromNearbyFragment: Boolean = false,
        isComingFromCamp: Boolean = false) {
        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_sort)
        myDialogMsg?.rvSort?.layoutManager = LinearLayoutManager(act, LinearLayoutManager.VERTICAL, false)
        val adapter: SortAdapter
        if (SearchAndFilterStaticData.arrSortModel.size > 0)
            adapter = SortAdapter(SearchAndFilterStaticData.arrSortModel)
        else
            adapter = SortAdapter(getSortingOptionArray())
        myDialogMsg?.rvSort?.adapter = adapter

        myDialogMsg?.btnClearSort?.setOnClickListener {
            val sortAdapter = SortAdapter(getSortingOptionArray())
            myDialogMsg?.rvSort?.adapter = sortAdapter
            SearchAndFilterStaticData.arrSortModel = ArrayList()

            if (isComingFromCamp && act is CampListingActivity) {
                // need to handle filer for camp
                CampListingActivity.needToRefreshNearByCamps = true
                // if (fragment is CampListingFragment)
                act.clearListAndMakeApiCall()
            } else if (act is DashboardActivity) {
                act.retainSelectedTab = isComingFromNearbyFragment
                Log.d("RetainSelected", "" + act.retainSelectedTab)
                act.handleExploreNearbyClick(false, passAct = DashboardActivity.showPassActivities)
            }
        }

        myDialogMsg?.btnApplySort?.setOnClickListener {
            SearchAndFilterStaticData.arrSortModel = adapter.arrSort
            SearchAndFilterStaticData.sorting = adapter.arrSort.single { it.status }.id
            if (myDialogMsg?.isShowing == true)
                myDialogMsg?.dismiss()

            if (isComingFromCamp && act is CampListingActivity) {
                // need to handle filer for camp
                CampListingActivity.needToRefreshNearByCamps = true
                // if (fragment is CampListingFragment)
                act.clearListAndMakeApiCall()
            } else if (act is DashboardActivity) {
                act.retainSelectedTab = isComingFromNearbyFragment
                Log.d("RetainSelected", "" + act.retainSelectedTab)
                act.handleExploreNearbyClick(isActualMenuItemClick = false, passAct = DashboardActivity.showPassActivities)
            }
        }

        myDialogMsg?.imgCloseDialogSort?.setOnClickListener {
            if (myDialogMsg?.isShowing == true) {
                myDialogMsg?.dismiss()

            }
        }

        if (!act.isFinishing)
            myDialogMsg?.show()
    }

    private fun getSortingOptionArray(): ArrayList<SortModel> {
        val arrSort = ArrayList<SortModel>()
        arrSort.add(SortModel("1", "Sort by nearest", true))
        arrSort.add(SortModel("2", "Popularity"))
        arrSort.add(SortModel("3", "Sort by price: low to high"))
        arrSort.add(SortModel("4", "Sort by price: high to low"))
        return arrSort
    }

    fun showNewFilterDialog(act: Activity, isComingFromNearbyFragment: Boolean = false,
                            isComingFromCamp: Boolean = false, filterApplied: () -> Unit) {
        fun resetTextSelectionData(textView: TextView) {
            textView.tag = "0"
            textView.setBackgroundResource(R.drawable.bg_filter_option)
            textView.textColor = ContextCompat.getColor(textView.context, R.color.colorTextBlack)
        }

        fun setTextSelectionData(textView: TextView) {
            textView.tag = "1"
            textView.setBackgroundResource(R.drawable.bg_filter_selected)
            textView.textColor = ContextCompat.getColor(textView.context, android.R.color.white)
        }

        fun changeSelectorUi(textView: TextView) {
            if (textView.tag == "0") {
                setTextSelectionData(textView)
                /*textView.tag="1"
                textView.setBackgroundResource(R.drawable.bg_filter_selected)
                textView.textColor=ContextCompat.getColor(textView.context,android.R.color.white)*/
            } else {
                resetTextSelectionData(textView)
                /*textView.tag="0"
                textView.setBackgroundResource(R.drawable.bg_filter_option)
                textView.textColor=ContextCompat.getColor(textView.context,R.color.colorTextBlack)*/
            }
        }

        fun changeSelectorPremiseFilterUi(textViewActive: TextView, textViewDeActive: TextView) {

            if (textViewActive.tag == "0")
                setTextSelectionData(textViewActive)
            else
                resetTextSelectionData(textViewActive)

            resetTextSelectionData(textViewDeActive)
        }

        fun resetImageUiData(imageView: ImageView) {
            imageView.tag = "0"
            imageView.setBackgroundResource(R.drawable.bg_circle_grey)
            imageView.setColorFilter(ContextCompat.getColor(act, android.R.color.black), PorterDuff.Mode.SRC_IN)
        }

        fun setImageUiData(imageView: ImageView) {
            imageView.tag = "1"
            imageView.setBackgroundResource(R.drawable.circle_orange)
            imageView.setColorFilter(ContextCompat.getColor(act, android.R.color.white), PorterDuff.Mode.SRC_IN)
        }

        fun changeOptionImageOnSelection(imageView: ImageView) {
            if (imageView.tag == "0") {
                setImageUiData(imageView)
                /*imageView.tag="1"
                imageView.setBackgroundResource(R.drawable.circle_orange)
                imageView.setColorFilter(ContextCompat.getColor(act,android.R.color.white),PorterDuff.Mode.SRC_IN)*/

            } else {
                resetImageUiData(imageView)
                /*imageView.tag="0"
                imageView.setBackgroundResource(R.drawable.bg_circle_grey)
                imageView.setColorFilter(ContextCompat.getColor(act,android.R.color.black),PorterDuff.Mode.SRC_IN)*/
            }

        }

        var selectedAgeGroupModel: AgeGroupModel? = SearchAndFilterStaticData.selectedAgeGroupModel
        Log.d("AgeGroup__Initial", "" + selectedAgeGroupModel)

        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_filter_act_camp)
        myDialogMsg?.rvDays?.layoutManager = LinearLayoutManager(act, LinearLayoutManager.HORIZONTAL, false)
        // set previous config
        if (SearchAndFilterStaticData.arrFilterDaysModel.size > 0) {
            val newArrayOfDays = ArrayList<FilterDayModel>()
            newArrayOfDays.addAll(SearchAndFilterStaticData.arrFilterDaysModel)
            myDialogMsg?.rvDays?.adapter = FilterDayAdapter(newArrayOfDays)
        } else
            myDialogMsg?.rvDays?.adapter = FilterDayAdapter(getFilterDayModel())

        SearchAndFilterStaticData.selectedAgeGroupModel?.let {
            myDialogMsg?.txtSelectAge?.text = "${it.groupTitle} (${it.groupLabel})"
        }

        myDialogMsg!!.txtSelectDate.text = ChangeDateFormat.convertDate(SearchAndFilterStaticData.date, "yyyy-MM-dd", "dd MMMM yyyy")

        try {
            myDialogMsg!!.timeRangeSeekBar.tickStart = 0f
            myDialogMsg!!.timeRangeSeekBar.tickEnd = 24f
            //  val timeTo=if (SearchAndFilterStaticData.timeTo=="23:59") "24" else SearchAndFilterStaticData.timeTo.replace(":",".")
            myDialogMsg!!.timeRangeSeekBar.setRangePinsByValue(
                SearchAndFilterStaticData.timeFrom.toFloat(),
                SearchAndFilterStaticData.timeTo.toFloat())
        } catch (e: Exception) {
            Log.e("timeRangeSeekBar", e.toString())
        }

        try {
            if (MyAppConfig.startPriceForFilter != "" || MyAppConfig.maxPriceForFilter != "") {
                if (MyAppConfig.startPriceForFilter != "")
                    myDialogMsg!!.priceRangeSeekBar.tickStart = MyAppConfig.startPriceForFilter.toFloat()
                if (MyAppConfig.maxPriceForFilter != "")
                    myDialogMsg!!.priceRangeSeekBar.tickEnd = MyAppConfig.maxPriceForFilter.toFloat()
                /* myDialogMsg!!.priceRangeSeekBar.left=MyAppConfig.startPriceForFilter.toInt()
                 myDialogMsg!!.priceRangeSeekBar.right=MyAppConfig.maxPriceForFilter.toInt()*/
                /*myDialogMsg!!.priceRangeSeekBar.setRangePinsByValue(
                    MyAppConfig.startPriceForFilter.toFloat(),
                    MyAppConfig.maxPriceForFilter.toFloat())*/
            }
            myDialogMsg!!.priceRangeSeekBar.setTickInterval(100f)
            if (SearchAndFilterStaticData.priceFrom != "" || SearchAndFilterStaticData.priceTo != "") {
                /*if (SearchAndFilterStaticData.priceFrom!="")
                 myDialogMsg!!.priceRangeSeekBar.tickStart=SearchAndFilterStaticData.priceFrom.toFloat()
                 if (SearchAndFilterStaticData.priceTo!="")
                 myDialogMsg!!.priceRangeSeekBar.tickEnd=SearchAndFilterStaticData.priceTo.toFloat()*/
                myDialogMsg!!.priceRangeSeekBar.setRangePinsByValue(
                    SearchAndFilterStaticData.priceFrom.toFloat(),
                    SearchAndFilterStaticData.priceTo.toFloat()
                )
            }


        } catch (e: Exception) {
            Log.e("priceRangeSeekBar", e.toString())
        }

        if (SearchAndFilterStaticData.gender.equals("male", true))
            setImageUiData(myDialogMsg!!.imgMale)
        else if (SearchAndFilterStaticData.gender.contains("female", true))
            setImageUiData(myDialogMsg!!.imgFemale)
        else if (SearchAndFilterStaticData.gender.contains("both")) {
            setImageUiData(myDialogMsg!!.imgMale)
            setImageUiData(myDialogMsg!!.imgFemale)
        }

        if (SearchAndFilterStaticData.actType.contains("group")) {
            setTextSelectionData(myDialogMsg!!.txtTypeGroup)
        }
        if (SearchAndFilterStaticData.actType.contains("private")) {
            setTextSelectionData(myDialogMsg!!.txtTypeIndividual)
        }

        //1-> Your Premise, 2-> Company Venue
        when (SearchAndFilterStaticData.premise) {
            "1" -> setTextSelectionData(myDialogMsg!!.txtPremiseHome)
            "2" -> setTextSelectionData(myDialogMsg!!.txtPremiseInstitutes)
        }

        myDialogMsg?.btnClearFilter?.setOnClickListener {
            SearchAndFilterStaticData.run {
                selectedDays = ""
                gender = ""
                //  sorting = ""
                timeFrom = ""
                timeTo = ""
                actType = ""
                priceFrom = ""
                priceTo = ""
                date = ""
                selectedAgeGroupModel = null

                this.selectedAgeGroupModel = null
                arrFilterDaysModel.clear()

                myDialogMsg?.rvDays?.adapter = FilterDayAdapter(getFilterDayModel())
                myDialogMsg?.txtSelectAge?.text = ""
                myDialogMsg?.txtSelectDate?.text = ""

                premise = ""

                myDialogMsg!!.timeRangeSeekBar.setRangePinsByValue(0f, 24f)

                if (MyAppConfig.startPriceForFilter != "" || MyAppConfig.maxPriceForFilter != "") {
                    if (MyAppConfig.startPriceForFilter != "")
                        myDialogMsg!!.priceRangeSeekBar.tickStart = MyAppConfig.startPriceForFilter.toFloat()

                    if (MyAppConfig.maxPriceForFilter != "")
                        myDialogMsg!!.priceRangeSeekBar.tickEnd = MyAppConfig.maxPriceForFilter.toFloat()
                    /* myDialogMsg!!.priceRangeSeekBar.left=MyAppConfig.startPriceForFilter.toInt()
                     myDialogMsg!!.priceRangeSeekBar.right=MyAppConfig.maxPriceForFilter.toInt()*/
                    /*myDialogMsg!!.priceRangeSeekBar.setRangePinsByValue(
                        MyAppConfig.startPriceForFilter.toFloat(),
                        MyAppConfig.maxPriceForFilter.toFloat())*/
                    myDialogMsg!!.priceRangeSeekBar.setRangePinsByValue(MyAppConfig.startPriceForFilter.toFloat()
                        , MyAppConfig.maxPriceForFilter.toFloat())
                }

                // myDialogMsg!!.priceRangeSeekBar.setRangePinsByValue(1f, 80f)

                resetImageUiData(myDialogMsg!!.imgMale)
                resetImageUiData(myDialogMsg!!.imgFemale)

                resetTextSelectionData(myDialogMsg!!.txtTypeGroup)
                resetTextSelectionData(myDialogMsg!!.txtTypeIndividual)

                resetTextSelectionData(myDialogMsg!!.txtPremiseHome)
                resetTextSelectionData(myDialogMsg!!.txtPremiseInstitutes)

                if (isComingFromCamp && act is CampListingActivity) {
                    // need to handle filer for camp
                    CampListingActivity.needToRefreshNearByCamps = true
                    // if (fragment is CampListingFragment)


                    //act.clearListAndMakeApiCall()

                } else if (act is DashboardActivity) {
                    act.retainSelectedTab = isComingFromNearbyFragment
                    Log.d("RetainSelected", "" + act.retainSelectedTab)

                    //clear all inputs, make api call, but don't dismiss dialog
                    if (isComingFromNearbyFragment)
                        act.handleExploreNearbyClick(isActualMenuItemClick = false, passAct = DashboardActivity.showPassActivities)
                }
            }

            //DOn't close dialog on clear click as per client feedback
            //myDialogMsg?.imgCloseDialogFilter?.performClick()

            SearchAndFilterStaticData.isFilterApplied = false
            SearchAndFilterStaticData.isAgeFilterApplied = false

            if (act is DashboardActivity)
                act.toolbar.imgFilterIndicatorDashboard.visibility = View.GONE
            else if (act is CampListingActivity)
                (act as CampListingActivity).exploreToolbarInitialization.decideFilterBadgeVisibility()
        }

        myDialogMsg?.setCancelable(true)
        myDialogMsg?.btnApply?.setOnClickListener {
            // need to save state into SearchAndFilterStaticData
            val adapter = myDialogMsg?.rvDays?.adapter as FilterDayAdapter
            val daysCommaSep = adapter.arrFilterDayModel.filter { it.status }.joinToString(",", transform = {
                it.dayName.toLowerCase(Locale.ENGLISH)
            })
            Log.d("daysCommaSep", daysCommaSep)

            SearchAndFilterStaticData.selectedDays = daysCommaSep
            Log.d("dateOnApply", myDialogMsg?.txtSelectDate?.text.toString())
            SearchAndFilterStaticData.date =
                ChangeDateFormat.convertDate(myDialogMsg?.txtSelectDate?.text.toString(), "dd MMMM yyyy", "yyyy-MM-dd")

            SearchAndFilterStaticData.timeFrom = myDialogMsg?.timeRangeSeekBar?.leftPinValue ?: ""
            /*if (myDialogMsg?.timeRangeSeekBar?.rightPinValue=="24")
                SearchAndFilterStaticData.timeTo = "23:59"
            else*/
            SearchAndFilterStaticData.timeTo = myDialogMsg?.timeRangeSeekBar?.rightPinValue ?: ""

            /*if (myDialogMsg!!.priceRangeSeekBar.leftPinValue.toFloat()==MyAppConfig.startPriceForFilter.toFloat())
            {
                SearchAndFilterStaticData.priceFrom = ""
            }
            else
            {
                SearchAndFilterStaticData.priceFrom = myDialogMsg?.priceRangeSeekBar?.leftPinValue ?: ""
            }
            if (myDialogMsg!!.priceRangeSeekBar.rightPinValue.toFloat()==MyAppConfig.maxPriceForFilter.toFloat())
            {
                SearchAndFilterStaticData.priceTo = ""
            }
            else {
                SearchAndFilterStaticData.priceTo = myDialogMsg?.priceRangeSeekBar?.rightPinValue ?: ""
            }*/

            SearchAndFilterStaticData.priceFrom = myDialogMsg?.priceRangeSeekBar?.leftPinValue ?: ""
            SearchAndFilterStaticData.priceTo = myDialogMsg?.priceRangeSeekBar?.rightPinValue ?: ""

            SearchAndFilterStaticData.gender = if (myDialogMsg!!.imgMale.tag == "1" && myDialogMsg!!.imgFemale.tag == "1") "both" else if (myDialogMsg!!.imgMale.tag == "1") "male" else if (myDialogMsg!!.imgFemale.tag == "1") "Female" else ""

            SearchAndFilterStaticData.actType = if (myDialogMsg!!.txtTypeGroup.tag == "1" && myDialogMsg!!.txtTypeIndividual.tag == "1") "group,private" else if (myDialogMsg!!.txtTypeGroup.tag == "1") "group" else if (myDialogMsg!!.txtTypeIndividual.tag == "1") "private" else ""

            SearchAndFilterStaticData.premise = when {
                myDialogMsg!!.txtPremiseHome.tag == "1" -> "1"
                myDialogMsg!!.txtPremiseInstitutes.tag == "1" -> "2"
                else -> ""
            }

            SearchAndFilterStaticData.arrFilterDaysModel = ArrayList()
            SearchAndFilterStaticData.arrFilterDaysModel.addAll(adapter.arrFilterDayModel)

            //set age group filter if needed
            SearchAndFilterStaticData.selectedAgeGroupModel = selectedAgeGroupModel
            Log.d("AgeGroup_AfterChange", "" + selectedAgeGroupModel)


            // filterApplied()

            SearchAndFilterStaticData.isFilterApplied = !SearchAndFilterStaticData.areAllFiltersEmpty()

            if (isComingFromCamp && act is CampListingActivity) {
                // need to handle filer for camp
                CampListingActivity.needToRefreshNearByCamps = true
                // if (fragment is CampListingFragment)
                act.clearListAndMakeApiCall()
            } else if (act is DashboardActivity) {
                act.retainSelectedTab = isComingFromNearbyFragment
                Log.d("RetainSelected", "" + act.retainSelectedTab)

                act.handleExploreNearbyClick(isActualMenuItemClick = false, passAct = DashboardActivity.showPassActivities)
            }

            myDialogMsg?.dismiss()




            if (act is DashboardActivity) {
                act.toolbar.imgFilterIndicatorDashboard.visibility = if (SearchAndFilterStaticData.isFilterApplied) View.VISIBLE else View.GONE

            } else if (act is CampListingActivity) {
                act.exploreToolbarInitialization.decideFilterBadgeVisibility()
            }
        }

        val dateTime = GetDateTime(act)
        myDialogMsg?.txtSelectDate?.setOnClickListener {
            dateTime.showDateDialog(
                myDialogMsg!!.txtSelectDate,
                false,
                false,
                false,
                myDialogMsg!!.txtSelectDate.text.toString()
            )
        }
        myDialogMsg?.imgSelectDate?.setOnClickListener {
            myDialogMsg?.txtSelectDate?.performClick()
        }

        myDialogMsg?.txtTypeGroup?.setOnClickListener {
            changeSelectorUi(myDialogMsg!!.txtTypeGroup)
        }

        myDialogMsg?.txtTypeIndividual?.setOnClickListener {
            changeSelectorUi(myDialogMsg!!.txtTypeIndividual)
        }

        myDialogMsg?.txtPremiseHome?.setOnClickListener {
            changeSelectorPremiseFilterUi(myDialogMsg!!.txtPremiseHome, myDialogMsg!!.txtPremiseInstitutes)
        }

        myDialogMsg?.txtPremiseInstitutes?.setOnClickListener {
            changeSelectorPremiseFilterUi(myDialogMsg!!.txtPremiseInstitutes, myDialogMsg!!.txtPremiseHome)
        }

        myDialogMsg?.imgMale?.setOnClickListener {
            changeOptionImageOnSelection(myDialogMsg!!.imgMale)
        }

        myDialogMsg?.imgFemale?.setOnClickListener {
            changeOptionImageOnSelection(myDialogMsg!!.imgFemale)
        }



        myDialogMsg?.txtSelectAge?.setOnClickListener {
            DashboardActivity.showFilterDialogNew(
                act, isComingFromNearbyFragment,
                isComingFromCamp,
                fromNewFilterDialog = true,
                ageSelectedListener = {
                    myDialogMsg!!.txtSelectAge.text = "${it.groupTitle} (${it.groupLabel})"
                    selectedAgeGroupModel = it

                }
                , onClearFilter = {
                    myDialogMsg!!.txtSelectAge.text = ""
                    selectedAgeGroupModel = null
                    MyAppConfig.arrayAgeGroupModel.forEach { it.groupIsSelected = false }
                    //SearchAndFilterStaticData.selectedAgeGroupModel = null*/
                },
                selectedAgeGroupModel = selectedAgeGroupModel
            )
        }

        myDialogMsg?.imgAge?.setOnClickListener {
            myDialogMsg?.txtSelectAge?.performClick()
        }



        myDialogMsg?.imgCloseDialogFilter?.setOnClickListener {
            if (myDialogMsg?.isShowing == true)
                myDialogMsg?.dismiss()
        }
        if (!act.isFinishing)
            myDialogMsg?.show()
    }

    private fun getFilterDayModel(): ArrayList<FilterDayModel> {
        val arrFilterDayModel = ArrayList<FilterDayModel>()
        arrFilterDayModel.add(FilterDayModel("0", "SUNDAY"))
        arrFilterDayModel.add(FilterDayModel("1", "MONDAY"))
        arrFilterDayModel.add(FilterDayModel("2", "TUESDAY"))
        arrFilterDayModel.add(FilterDayModel("3", "WEDNESDAY"))
        arrFilterDayModel.add(FilterDayModel("4", "THURSDAY"))
        arrFilterDayModel.add(FilterDayModel("5", "FRIDAY"))
        arrFilterDayModel.add(FilterDayModel("6", "SATURDAY"))
        return arrFilterDayModel
    }

    fun showYesCancelLoginRegisterDialog(
        act: Activity,
        strMsg: String,
        onYesListener: View.OnClickListener,
        isCancellable: Boolean = true
    ) {

        myDialogMsg = MyDialog(act).getMyDialog(R.layout.dialog_yes_cancel)
        myDialogMsg?.setCancelable(isCancellable)
        myDialogMsg?.txtErrorMsgDialogYesCancel?.text = strMsg

        myDialogMsg?.txtTitleYesCancel?.text = ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE
        myDialogMsg?.txtYes?.text = "Login"
        myDialogMsg?.txtCancel?.text = "Not Now"

        myDialogMsg?.txtYes?.setOnClickListener(onYesListener)
        myDialogMsg?.txtCancel?.setOnClickListener {
            myDialogMsg?.dismiss()
        }

        try {
            animateViewDialog(myDialogMsg?.linErrParentYesCancel)
            myDialogMsg?.show()
        } catch (e: Exception) {

        }

    }

    fun dismissDialog(act: Activity) {
        try {
            if (myDialogMsg != null) {
                if (!act.isFinishing)
                    myDialogMsg?.dismiss()
            }
        } catch (e: Exception) {

        }
    }


}
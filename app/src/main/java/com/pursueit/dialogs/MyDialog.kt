package com.pursueit.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window


class MyDialog  (private var act: Activity) {

    fun getMyDialog(layout: Int): Dialog {
        val d = Dialog(act)
        d.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        d.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        d.setContentView(layout)
        return d
    }
}
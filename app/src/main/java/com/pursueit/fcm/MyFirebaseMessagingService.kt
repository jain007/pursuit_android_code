package com.pursueit.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pursueit.BuildConfig
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.model.MyBookingsModel
import com.pursueit.model.NotificationModel
import com.pursueit.model.NotificationModel.Companion.CHANNEL_DEFAULT
import com.pursueit.model.UserModel
import com.pursueit.utils.GetSetSharedPrefs
import com.pursueit.utils.JsonParse
import org.json.JSONArray
import org.json.JSONObject
import java.net.MalformedURLException
import java.net.URL
import kotlin.random.Random


class MyFirebaseMessagingService: FirebaseMessagingService() {

    //google account for FCM: pursueit2019@gmail.com

    private val TAG = "FCMMessaging_Pursueit"

    companion object {
     //   val CHANNEL_ID = "Pursueit Notification"

        fun getNotificationIcon(): Int {
            val useWhiteIcon = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) R.drawable.ic_notification_white_icon else R.drawable.ic_app_icon_circle
        }

        fun subscribeTopicAll(){
            val strTopicAll=if(BuildConfig.FLAVOR=="production") "ProductionAndroid" else "DevAndroid"
            FirebaseMessaging.getInstance().subscribeToTopic("PursueIt-AllUsers-$strTopicAll")
                    .addOnCompleteListener { task ->
                        Log.d("TopicSubscribed", "PursueIt-AllUsers-$strTopicAll")
                    }
        }

        fun unsubscribeTopicAll(){
            val strTopicAll=if(BuildConfig.FLAVOR=="production") "ProductionAndroid" else "DevAndroid"
            FirebaseMessaging.getInstance().unsubscribeFromTopic("PursueIt-AllUsers-$strTopicAll")
                .addOnCompleteListener { task ->
                    Log.d("TopicUnsubscribed", "PursueIt-AllUsers-$strTopicAll")
                }
        }

        fun unsubscribeTopicAllOtherFlavours(){
            val strUnsubscribeTopicAll=if(BuildConfig.FLAVOR=="production") "DevAndroid" else "ProductionAndroid"
            FirebaseMessaging.getInstance().unsubscribeFromTopic("PursueIt-AllUsers-$strUnsubscribeTopicAll")
                    .addOnCompleteListener { task->
                        Log.d("TopicUnsubscribed", "PursueIt-AllUsers-$strUnsubscribeTopicAll")
                    }
        }
    }

    override fun onMessageReceived(remoteMsg: RemoteMessage?) {
        super.onMessageReceived(remoteMsg)
        try {
            Log.d("RemoteMsg", "" + remoteMsg)
            if (remoteMsg != null) {
                try {
                    Log.d("notificationPayload",""+remoteMsg.notification?.body)
                }catch (e:Exception)
                {
                    Log.e("notificationPayload",e.toString())
                }

                Log.d("ContentData", "" + remoteMsg.data)
                Log.d(TAG, "------------------------------------------")
                val dataMap = remoteMsg.data
                for (entry in dataMap.entries) {
                    val key = entry.key
                    val value = entry.value
                    Log.d("Key_value", "Key: $key\nValue: $value")
                }

                if (UserModel.getUserModel(applicationContext).userId.trim().isEmpty()) {
                    Log.d("fcm","user not logged in, not showing notification")
                    return
                }

                val notificationModel=getNotificationModel(JSONObject(remoteMsg.data as Map<*, *>)) //JSONObject(dataMap["data"])
                showNotification(notificationModel)

                /*applicationContext.runOnUiThread {
                    if (dataMap != null) {
                        //toast(dataMap.toString())
                        var intNotifyFlag: Int
                        try {
                            intNotifyFlag = dataMap["notification_flag"]!!.toInt()
                        } catch (e: Exception) {
                            Log.d(TAG, "Invalid Flag value")
                            intNotifyFlag = 1
                        }
                        val notifyModel = parseIncomingNotification(dataMap["id"], dataMap["title"], dataMap["msg"], dataMap["image"], intNotifyFlag)
                        sendNotification(notifyModel)
                    }
                }*/

            }
        } catch (e: Exception) {
            Log.d("Exc_IncomingNoti", "" + e)
        }
    }

    private fun getNotificationModel(jsonObject: JSONObject):NotificationModel
    {
        var moduleId=""
        var moduleName=""
        var moduleImage=""
        var bookingModel:MyBookingsModel?=null
      /*  when(jsonObject.optString("notification_type",""))
        {
            NotificationModel.TYPE_REVIEW_ACTIVITY,NotificationModel.TYPE_ACTIVITY_BOOKING_REMIDER,NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY
                , NotificationModel.TYPE_REVIEW_CAMP, NotificationModel.TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER
                ,NotificationModel.TYPE_ACTIVITY_ACCEPTED, NotificationModel.TYPE_ACTIVITY_REJECTED,
                NotificationModel.TYPE_CAMP_ACCEPTED, NotificationModel.TYPE_CAMP_REJECTED,
                NotificationModel.TYPE_FLEXI_ACCEPTED, NotificationModel.TYPE_FLEXI_REJECTED->{*/
                jsonObject.run {
                    moduleId=optString("activity_id","")
                    moduleName=optString("activity_name","")
                    moduleImage=optString("activity_feature_img","")

                    bookingModel= MyBookingsModel(optString("booking_id"))
                    bookingModel?.bookingThresholdDate=
                        JsonParse.getFormattedServerDateJava(optString("threshold_cancellation_date", ""))
                    bookingModel?.bookingAmtAfterTax=optString("booking_amount","")
                    bookingModel?.campWeekId=optString("week_ids")
                    bookingModel?.sessionJson=optString("booking_sessions_json")

                  /*  try {
                        val jarSession=JSONArray(bookingModel?.sessionJson)
                        jarSession.getJSONObject(0).run {
                            bookingModel?.day
                        }
                    }catch (e:Exception){}*/

                    bookingModel?.bookingDate=optString("booking_date","")


                    if (jsonObject.optString("notification_type","").equals(NotificationModel.TYPE_REVIEW_CAMP) || jsonObject.optString("notification_type","").contains("camp",true))
                    {
                        moduleId=optString("camp_id","")
                        moduleName=optString("camp_name","")
                        moduleImage=optString("camp_feature_img","")
                    }
                }

         //   }

            /*NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY->{

            }*/

           /* NotificationModel.TYPE_REVIEW_CAMP->{
                jsonObject.run {
                    moduleId=optString("camp_id","")
                    moduleName=optString("camp_name","")
                    moduleImage=optString("camp_feature_img","")
                }
            }*/

           /* ->{
                jsonObject.run {
                    moduleId=optString("activity_id","")
                    moduleName=optString("activity_name","")
                }

            }*/
     //   }

        return NotificationModel(moduleId,jsonObject.optString("title",""),jsonObject.optString("body","")
                ,jsonObject.optString("notification_type",""),jsonObject.optString("enrolment_id","")
                ,jsonObject.optString("time_slot_id",""),moduleName,moduleImage,bookingModel)
    }


    private fun showNotification(notificationModel: NotificationModel)
    {
        val notificationManager=applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val  notificationBuilder:NotificationCompat.Builder

        val CHANNEL_NAME=getChannelNameAccordingToNotificationType(notificationModel.notificationType)

        val intent:Intent = Intent(applicationContext, DashboardActivity::class.java)
        //if (MyApp.IS_APP_IN_OPEN_STATE) {
        intent.putExtra("finishDashboardAct",true)
       /* }
        else {
            intent = Intent(applicationContext, SplashActivity::class.java)
            intent.putExtra("finishDashboardAct",false)
        }*/

      //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) //Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

        //intent.putExtra("notificationModel",notificationModel)

        intent.putExtra("notificationModel",GetSetSharedPrefs.convertToJsonString(notificationModel))

       // intent.putExtra("fromNotification","1")


        val pendingIntent=PendingIntent.getActivity(applicationContext, Random.nextInt(),intent,PendingIntent.FLAG_ONE_SHOT)

        if (android.os.Build.VERSION.SDK_INT>=android.os.Build.VERSION_CODES.O)
        {
            notificationBuilder=NotificationCompat.Builder(applicationContext, CHANNEL_NAME)
            val notificationChannel=NotificationChannel(CHANNEL_NAME,CHANNEL_NAME,NotificationManager.IMPORTANCE_DEFAULT)

            val audioAttributes=AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION).build()

            notificationChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION),audioAttributes)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        else
        {
            notificationBuilder=NotificationCompat.Builder(applicationContext)
        }

        val textStyle=NotificationCompat.BigTextStyle()
        textStyle.bigText(notificationModel.message)

        notificationBuilder
            .setContentTitle(notificationModel.title)
            .setContentText(notificationModel.message)
            //.setSmallIcon(R.drawable.ic_app_notification)
            .setContentIntent(pendingIntent)
            .setStyle(textStyle)
            .setAutoCancel(true)
            .setColor(Color.parseColor("#FF5D3E"))
            .setSmallIcon(R.drawable.ic_app_notification)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))

        notificationManager.notify(System.currentTimeMillis().toInt(),notificationBuilder.build())
    }

    private fun getChannelNameAccordingToNotificationType(notificationType:String):String
    {
        return when(notificationType) {
            NotificationModel.TYPE_REVIEW_CAMP,NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY,NotificationModel.TYPE_REVIEW_ACTIVITY->{
                NotificationModel.CHANNEL_REVIEW_REMINDER_NOTIFICATION
            }
            NotificationModel.TYPE_ACTIVITY_BOOKING_REMIDER, NotificationModel.TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER->{
                NotificationModel.CHANNEL_BOOKING_REMINDER_NOTIFICATION
            }
            NotificationModel.TYPE_ACTIVITY_ACCEPTED,NotificationModel.TYPE_ACTIVITY_REJECTED,NotificationModel.TYPE_CAMP_ACCEPTED,NotificationModel.TYPE_CAMP_REJECTED,
                NotificationModel.TYPE_FLEXI_ACCEPTED, NotificationModel.TYPE_FLEXI_REJECTED, NotificationModel.TYPE_ACTIVITY_CANCELLED, NotificationModel.TYPE_FLEXI_CANCELLED
                ,NotificationModel.TYPE_CAMP_CANCELLED->{
                NotificationModel.CHANNEL_BOOKING_STATUS_NOTIFICATION
            }
            else->{
                CHANNEL_DEFAULT
            }
        }
    }

 /*   private fun parseIncomingNotification(strId: String?, strTitle: String?, strMsg: String?, strImage: String?, notifyFlag: Int = 1): NotificationModel {
        val model = NotificationModel()
        if (strId != null)
            model.notifyId = strId
        if (strTitle != null)
            model.notifyTitle = strTitle
        if (strMsg != null)
            model.notifyDesc = strMsg
        if (strImage != null)
            model.notifyImage = strImage

        model.notifyFlag = notifyFlag

        return model
    }

    private fun sendNotification(notificationModel: NotificationModel) {
        val intent: Intent
        if (notificationModel.notifyFlag == 3) //for app update intent
        {
            intent = Intent(Intent.ACTION_VIEW)
            intent.data =  Uri.parse(StaticValues.PLAY_STORE_BASE_URL+BuildConfig.APPLICATION_ID)

        } else {
            intent = Intent(applicationContext, SplashActivity::class.java)
            intent.putExtra("NotificationModel", notificationModel)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)


        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
            .setSmallIcon(getNotificationIcon())
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_app_icon_circle))
            .setContentTitle(notificationModel.notifyTitle)
            .setContentText(notificationModel.notifyDesc)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setChannelId(CHANNEL_ID)
            .setContentIntent(pendingIntent)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
            notificationBuilder.color = ContextCompat.getColor(baseContext, R.color.colorAccent)


        //initiate notification sending
        doAsync {
            var bmp: Bitmap? = null
            try {
                if (notificationModel.notifyImage.trim().isNotEmpty())
                    bmp = getBitmap(notificationModel.notifyImage)
            } catch (e: Exception) {
                Log.v("bmpDoInBck", "" + e)
            }

            applicationContext.runOnUiThread {
                val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                try {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        val notificationChannel = NotificationChannel(CHANNEL_ID, notificationModel.notifyTitle, NotificationManager.IMPORTANCE_DEFAULT)
                        notificationManager.createNotificationChannel(notificationChannel)
                    }

                    if (bmp != null) {
                        notificationBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(bmp).setSummaryText(notificationModel.notifyDesc.trim()))
                    } else {
                        val textStyle = NotificationCompat.BigTextStyle().bigText(notificationModel.notifyDesc.trim())
                        textStyle.setBigContentTitle(notificationModel.notifyTitle)
                        notificationBuilder.setStyle(textStyle)
                    }

                    notificationManager.notify(notificationModel.notifyId.toInt(), notificationBuilder.build())
                } catch (e: Exception) {
                    Log.v("NotifyingException", "" + e)
                }

            }
        }

    }*/


    private fun getBitmap(url: String): Bitmap? {
        var newUrl: URL? = null
        try {
            newUrl = URL(url)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }

        try {
            val bmp = BitmapFactory.decodeStream(newUrl!!.openConnection().getInputStream())
            return if (bmp == null) null else Bitmap.createScaledBitmap(bmp, 700, 350, true)
        } catch (e: Exception) {
            Log.v("BitmapException", "" + e)
        }

        return null
    }


    override fun onNewToken(token: String?) {
        super.onNewToken(token)
    }
}
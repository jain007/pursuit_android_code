package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.CampListingAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CampModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.content_layout_header_nearby.view.*
import kotlinx.android.synthetic.main.fragment_camp_listing.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class CampListingFragment : Fragment() {

    lateinit var act: Activity
    lateinit var vi: View

    var adapter: CampListingAdapter? = null
    var arrCampModel = ArrayList<CampModel>()


    lateinit var noActivityFound: NoRecordFound

    var curPage = 1
    var totalPages = 1
    var isLoadingAlready = false


 //   var curPos = 0
    //lateinit var userModel: UserModel

    lateinit var exploreToolbarInitialization:ExploreToolbarInitialization

    companion object {
        var needToRefreshNearByCamps=false
        var searchViaCampNameOrTagName=false
        var campId=""
        var tagId=""
        var campName=""

        /*fun filterInstance(campId:String, tagId:String):CampListingFragment
        {
            CampListingFragment.campId=campId

            val fragment=CampListingFragment()
            return fragment
        }*/
        //var lastClickedPosition = 0

        /*fun saveResponseJson(context: Context, strCatId: String, pageNo: Int, strResponse: String) {
            val prefs = GetSetSharedPrefs(context)
            prefs.putData("Activities_$strCatId" + "_$pageNo", strResponse)
        }*/

       /* fun resetMakeResponseEmptyAllCategories(context: Context) {
            doAsync {
                val prefs = GetSetSharedPrefs(context)
                val arrayKeys = prefs.prefs.all.keys.filter { it.startsWith("Activities_") }
                for (strKey in arrayKeys) {
                    Log.d("SharedPreferencesKeys", strKey)
                    prefs.putData(strKey, "")
                }
            }

        }*/

       /* fun getSavedResponseJson(context: Context, strCatId: String, pageNo: Int): String {
            val prefs = GetSetSharedPrefs(context)
            return prefs.getData("Activities_$strCatId" + "_$pageNo")
        }*/



    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        act = context as Activity
        //userModel = UserModel.getUserModel(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_camp_listing, container, false)
        view.setOnClickListener { }
        vi = view

        init()
      //  dummyCamp()
       // setAdapter()
        return view
    }

    override fun onResume() {
        super.onResume()
        vi.txtPlaceHeader.text=MyPlaceUtils.replaceUAECountry(MyPlaceUtils.placeModel.placeAddress.trim())
        if (needToRefreshNearByCamps)
        {
            needToRefreshNearByCamps=false
            clearListAndMakeApiCall(true)
        }
        else if (searchViaCampNameOrTagName)
        {
            searchViaCampNameOrTagName=false
            clearListAndMakeApiCall(false)
        }
    }

    private fun init() {
        needToRefreshNearByCamps=false

        noActivityFound = NoRecordFound(act, vi)
        noActivityFound.init()
        noActivityFound.hideEmptyUi()

        vi.rvCampListing.itemAnimator = null
        vi.rvCampListing.isEnabled = false
        vi.rvCampListing.layoutManager = LinearLayoutManager(act, LinearLayoutManager.VERTICAL, false)

        vi.swipeRefresh.isEnabled = false

        swipeRefreshFunctionality()

        initiateWork()

      /*  exploreToolbarInitialization=ExploreToolbarInitialization(act,vi,this)
        exploreToolbarInitialization.init()*/
    }

    private fun swipeRefreshFunctionality() {
        vi.swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        vi.swipeRefresh.setOnRefreshListener {
            clearListAndMakeApiCall()
        }
    }

    fun clearListAndMakeApiCall(clearSearchData:Boolean=false)
    {
        if (clearSearchData)
        {
            campId=""
            tagId=""
            campName=""
        }

        exploreToolbarInitialization.decideFilterBadgeVisibility()

        arrCampModel.clear()
        adapter?.notifyDataSetChanged()

        curPage = 1
        noActivityFound.hideEmptyUi()


        /*for (i in 1..totalPages)
            saveResponseJson(act.applicationContext, "" + strCatId, i, "")*/

        //initFetchingResponse(loadMoreOrSwipeRefresh = true)

        fastFetchNearbyCamp()
    }

   /* fun dummyCamp() {
        val campModel = CampModel("1", "Summer Camp", "")
        campModel.serviceProviderName = "Recreation Club"
        campModel.address = "Dia Emirates Hills"
        campModel.startingPrice = "200"
        campModel.isDiscountAvailable = true
        campModel.isTransportAvailable = true
        campModel.ageRageData = "8 to 10 years"
        campModel.timeRangeData = "9 am to 2 pm"

        val arrTags=ArrayList<String>()
        arrTags.run {
            add("Fun")
            add("Dance")
            add("Ping Pong")
            add("Fun")
            add("Dance")
        }
        campModel.arrTagList=arrTags

        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
        arrCampModel.add(campModel)
    }

    fun setAdapter() {
        vi.rvCampListing.layoutManager = LinearLayoutManager(act)
        vi.rvCampListing.adapter = CampListingAdapter(act, arrCampModel) { pos, progressShowMore ->

        }
    }*/

    override fun onStart() {
        super.onStart()
    }

    private fun initiateWork() {
        //curPage = 1

        noActivityFound.hideEmptyUi()


        if (MyPlaceUtils.placeModel.placeAddress.trim().isEmpty()) {
            showErrorMsg("Please select your location first")
            return
        }

        fastFetchNearbyCamp()
    }


    private fun fastFetchNearbyCamp(progressShowMore: ProgressWheel? = null) {
        try {
            if (ConnectionDetector.isConnectingToInternet(act)) {

                noActivityFound.hideEmptyUi()

                if (curPage == 1) {
                    arrCampModel.clear()

                    try {
                        if (curPage <= 1)
                            vi.rvCampListing.showShimmerAdapter()
                        else
                            hideShimmer()

                    } catch (e: Exception) {

                    }
                }

                doAsync {
                    val hashParams = HashMap<String, String>()
                    hashParams.put("latitude", "" + MyPlaceUtils.placeModel.placeLat)
                    hashParams.put("longitude", "" + MyPlaceUtils.placeModel.placeLng)
                    hashParams.put("ageFrom", DashboardActivity.ageFrom)
                    hashParams.put("ageTo", DashboardActivity.ageTo)
                    hashParams.put("cityPlaceId", "" + MyPlaceUtils.placeModel.placeCityId)
                    hashParams.put("tagsId", tagId)
                    hashParams.put("campId", campId)
                    hashParams.put("campName",campName)
                    hashParams.put("page", "" + curPage)


                    Log.d("camp_listing_API",Urls.NEAR_BY_CAMP)

                    FastNetworking.makeRxCallPost(act,
                        Urls.NEAR_BY_CAMP,
                        true,
                        hashParams,
                        "NearByCamps",
                        object : FastNetworking.OnApiResult {
                            override fun onApiSuccess(json: JSONObject?) {
                                try {
                                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                        //saveResponseJson(act.applicationContext, strCatId!!, curPage, json.toString())
                                        parseData(json.toString(), progressShowMore)
                                    } else {
                                        showErrorUi(getString(R.string.error_message))
                                    }
                                } catch (e: Exception) {
                                    Log.d("ExcParse_CampList", "" + e)
                                    showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                                }
                            }

                            override fun onApiError(error: ANError) {
                                //showErrorMsg(ErrorMsgs.ERR_API_MSG)
                                showErrorUi(ErrorMsgs.ERR_API_MSG)

                                if (progressShowMore != null) {
                                    progressShowMore.visibility = View.GONE
                                }
                            }

                        })
                }

            } else {
                showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
            }
        } catch (e: Exception) {
            Log.d("Exc_FetchNearby", "" + e)
        }
    }

    private fun parseData(strJson: String, progressShowMore: ProgressWheel? = null) {
        doAsync {
            try {
                isLoadingAlready = false
                val json = JSONObject(strJson)

                if (curPage == 1)
                    arrCampModel.clear()

                if (json.getBoolean(StaticValues.KEY_STATUS)) {
                    val jsonData = json.getJSONObject("data")
                    totalPages = jsonData.optInt("last_page", 1)
                    arrCampModel.addAll(JsonParse.parseCampModel(jsonData,json))
                }

                act.runOnUiThread {
                    fillListingAdapter()
                }
            } catch (e: Exception) {
                Log.d("ParseExc__", "" + e)
            } finally {
                act.runOnUiThread {
                    vi.swipeRefresh.isEnabled = true
                    vi.swipeRefresh.isRefreshing = false
                }
                if (progressShowMore != null) {
                    act.runOnUiThread {
                        progressShowMore.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun showErrorMsg(strMsg: String) {
        try {
            if (!act.isFinishing) {
                vi.rvCampListing.hideShimmerAdapter()
            }
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                vi.rvCampListing.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }

    private fun showErrorUi(strMsg: String) {
        try {
            if (!act.isFinishing) {
                hideShimmer()
                vi.rvCampListing.visibility = View.GONE
                noActivityFound.showNoActivities(strMsg, View.OnClickListener {
                    noActivityFound.hideEmptyUi()
                    fastFetchNearbyCamp()
                })
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")
        noActivityFound.hideEmptyUi()
        vi.rvCampListing.visibility = View.VISIBLE

        if (vi.rvCampListing.adapter == null || adapter == null) {
            vi.rvCampListing.isEnabled = true
            adapter = CampListingAdapter(act as AppCompatActivity, arrCampModel) { pos, progressShowMore ->
                Log.d("Cur_", "" + curPage)
                Log.d("last_", "" + totalPages)
                progressShowMore.visibility = View.GONE
                if (curPage < totalPages && !isLoadingAlready) {
                    isLoadingAlready = true
                    curPage += 1
                    progressShowMore.visibility = View.VISIBLE
                    fastFetchNearbyCamp(progressShowMore)
                } else {
                    progressShowMore.visibility = View.GONE
                }
            }
            vi.rvCampListing.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        try {
            if (intent!=null) {
                val campId = intent.getStringExtra("campId")
                val model=arrCampModel.single { it.campId==campId }
                val isFavorite=intent.getBooleanExtra("isFavorite",false)
                val pos=arrCampModel.indexOf(model)
                model.isFavoriteMarked=isFavorite
                arrCampModel[pos]=model
                adapter?.notifyItemChanged(pos)
            }

        }catch (e:Exception)
        {

        }
    }

}

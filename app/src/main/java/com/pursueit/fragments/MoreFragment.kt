package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.adapters.MoreOptionsAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.Urls
import com.pursueit.model.MoreOptionsModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.fragment_more_nav.view.*
import java.util.*
import kotlin.concurrent.schedule


class MoreFragment : Fragment() {

    lateinit var vi: View
    lateinit var act: Activity

    var arrayMoreOptions = ArrayList<MoreOptionsModel>()

    lateinit var userModel: UserModel

    private var isActive = true

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        act = context as Activity
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_more_nav, container, false)
        view.setOnClickListener { }
        vi = view
        init()
        return view
    }

    private fun init() {
        isActive = true

        userModel = UserModel.getUserModel(act.applicationContext)

        vi.toolbarFragment.txtToolbarHeader.text = "More"

        vi.rvMoreOptions.layoutManager = LinearLayoutManager(act)

        Glide.with(vi).load(R.drawable.ic_bg_header_more).into(vi.imgBgHeader)

        fillAdapter()

        setUserDetailsUi()

        MyGoogleAnalytics.sendSession(act, MyGoogleAnalytics.MORE_OPTIONS, userModel.userEmail, this::class.java)

    }

    override fun onStop() {
        isActive = false
        super.onStop()
    }


    override fun onResume() {
        super.onResume()

        userModel= UserModel.getUserModel(act)
        if (userModel.currentPassWallet.trim()!="" && userModel.currentPassWallet.trim()!="null")
            vi.txtPassBalance.text="${StaticValues.PASS_WALLET_BALANCE}: AED "+userModel.currentPassWallet+StaticValues.getSuffixValidTill(userModel)
        else
            vi.txtPassBalance.text="${StaticValues.PASS_WALLET_BALANCE}: AED 0"

        Timer().schedule(250) {
            fillUserDetails()
        }

    }

    private fun fillUserDetails() {
        try {

            if (isActive) {
                MyProfileUtils.fastRefreshProfile(act.applicationContext, { curUserModel ->

                    if (curUserModel != null) {
                        userModel = curUserModel
                        if (isActive) {
                            if (userModel.isUserActive) {
                                setUserDetailsUi()
                            } else {
                                val dialogMsg = DialogMsg()
                                dialogMsg.showErrorRetryDialog(
                                    act,
                                    ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,
                                    ErrorMsgs.ERR_MSG_USER_DE_ACTIVE,
                                    "Log in",
                                    View.OnClickListener {
                                        dialogMsg.dismissDialog(act)
                                        LoginBasicsUi.logOutUser(act)
                                    },
                                    false
                                )
                            }
                        }

                    }

                })
            }

        } catch (e: Exception) {

        }
    }

    private fun setUserDetailsUi() {
        try {
            if (!act.isFinishing) {
                Log.d("ComingHere", "Yes")
                if (userModel.userImage.trim().length > 5) {
                    Glide.with(vi).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true))
                        .load(Urls.IMAGE_USER + userModel.userImage).into(vi.imgUserProfile)
                } else if (userModel.userSocialImageUrl.trim().length > 5) {
                    Glide.with(vi).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true))
                        .load(userModel.userSocialImageUrl).into(vi.imgUserProfile)
                } else {
                    Glide.with(vi).applyDefaultRequestOptions(MyGlideOptions.getSquareRequestOptions(true))
                        .load(R.drawable.ic_placeholder_square).into(vi.imgUserProfile)
                }

                vi.txtUserName.text = ((userModel.userFNAme + " " + userModel.userLName).trim())
                vi.txtUserEmail.text = userModel.userEmail.trim()
                if (userModel.currentPassWallet.trim()!="" && userModel.currentPassWallet.trim()!="null")
                    vi.txtPassBalance.text="${StaticValues.PASS_WALLET_BALANCE}: AED "+userModel.currentPassWallet+ StaticValues.getSuffixValidTill(userModel)
                else
                    vi.txtPassBalance.text="${StaticValues.PASS_WALLET_BALANCE}: AED 0"
            }
        } catch (e: Exception) {

        }
    }

    private fun fillAdapter() {
        arrayMoreOptions.clear()
        arrayMoreOptions.add(
            MoreOptionsModel(
                optionTitle = "Profile Settings",
                optionSubTitle = "Update Profile, Family Details",
                optionImage = R.drawable.ic_menu_more_settings
            )
        )
        arrayMoreOptions.add(
            MoreOptionsModel(
                optionTitle = "Payments",
                optionSubTitle = "Add Payment Method & Transaction History",
                optionImage = R.drawable.ic_menu_more_payments
            )
        )
        arrayMoreOptions.add(
            MoreOptionsModel(
                optionTitle = "Invite a friend",
                optionSubTitle = "Share Pursueit with your friends",
                optionImage = R.drawable.ic_menu_more_invite_friend
            )
        )
        arrayMoreOptions.add(
            MoreOptionsModel(
                optionTitle = "About Us",
                optionSubTitle = "Know more about Pursueit",
                optionImage = R.drawable.ic_menu_more_about_us
            )
        )
        arrayMoreOptions.add(MoreOptionsModel(optionTitle= "My Pass", optionSubTitle = "Add money to your wallet & buy pass activities"
            ,optionImage = R.drawable.ic_add_money ))

        arrayMoreOptions.add(
            MoreOptionsModel(
                optionTitle = "Pass Transaction",
                optionSubTitle = "All your pass transaction History",
                optionImage = R.drawable.ic_menu_more_payments
            )
        )

        arrayMoreOptions.add(MoreOptionsModel(optionTitle= "Contact Support", optionSubTitle = "Share your feedback"
            ,optionImage = R.drawable.ic_app_support ))

        arrayMoreOptions.add(
            MoreOptionsModel(
                optionTitle = "Sign out",
                optionSubTitle = "",
                optionImage = R.drawable.ic_menu_more_sign_out,
                tintColor = R.color.colorSignOut
            )
        )

        val moreOptionsAdapter = MoreOptionsAdapter(act, arrayMoreOptions) { pos ->
            when (arrayMoreOptions[pos].optionTitle) {
                "Sign out" -> {
                    if (ConnectionDetector.isConnectingToInternet(act)) {
                        val dialogSure = DialogMsg()
                        dialogSure.showYesCancelDialog(
                            act,
                            "Are you sure you want to sign out from ${getString(R.string.app_name)}?",
                            View.OnClickListener {
                                dialogSure.dismissDialog(act)
                                if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {
                                    MyProfileUtils.fastUpdateFcmToken(act, true)
                                }
                                else{
                                    MySnackBar(act as AppCompatActivity, vi.linRootMoreNav).showSnackBarError(ErrorMsgs.ERR_CONNECTION_MSG)
                                }
                                //LoginBasicsUi.logOutUser(act)
                            },
                            true)
                    } else {
                        val dialogMsg = DialogMsg()
                        dialogMsg.showErrorConnectionDialog(act, "OK", View.OnClickListener {
                            dialogMsg.dismissDialog(act)
                        })
                    }
                }
                "Profile Settings" -> {
                    MyNavigations.goToMyProfile(act)
                }
                "About Us" -> {
                    MyNavigations.goToAboutUs(act)
                }

                "Invite a friend" -> {
                    MyAppConfig.shareApp(act)
                }

                "Pass Transaction"->{
                    MyNavigations.goToPaymentTransactions(act,fromPassTransaction = true)
                }

                "Payments" -> {
                    MyNavigations.goToPaymentTransactions(act)
                }

                "My Pass"->{
                    MyNavigations.gotoAddMoneyActivity(act)
                }

                "Contact Support"->{
                    MyNavigations.gotoAppSupportActivity(act)
                }
            }
        }
        vi.rvMoreOptions.adapter = moreOptionsAdapter
    }

    override fun onStart() {
        super.onStart()
        isActive = true
    }
}
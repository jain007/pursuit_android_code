package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.ActivityDetailActivity
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.MyBookingsAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.model.ActivitySlotModel
import com.pursueit.model.MyBookingsModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.content_layout_header_my_corner.view.*
import kotlinx.android.synthetic.main.fragment_my_bookings.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.runOnUiThread
import org.json.JSONObject


class MyBookingsFragment : Fragment() {
    lateinit var vi: View
    lateinit var act: Activity

    var adapter: MyBookingsAdapter? = null
    var arrayMyBookingModel = ArrayList<MyBookingsModel>()

    lateinit var noRecordUi: NoRecordFound

    private var curPage = 1
    private var totalPages = 1
    private var isLoadingAlready = false

    companion object {
        var selectedBookingIndex=0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedBookingIndex=0
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        act = context as Activity
    }

    override fun onResume() {
        super.onResume()
        refreshBookings()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_my_bookings, container, false)
        view.setOnClickListener { }
        vi = view
        init()
        return view
    }

    private fun init() {
        vi.toolbarFragment.txtToolbarHeader.text = "My Bookings"

        noRecordUi = NoRecordFound(act, vi).also { it.init() }

        vi.rvBookings.isEnabled = false
        vi.rvBookings.layoutManager = LinearLayoutManager(act)
        vi.rvBookings.setHasFixedSize(true)
        vi.swipeRefresh.isEnabled = false
        swipeRefreshFunctionality()
        MyGoogleAnalytics.sendSession(act, MyGoogleAnalytics.MY_BOOKINGS,UserModel.getUserModel(act).userEmail,this::class.java)
    }

    private fun swipeRefreshFunctionality()
    {
        vi.swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        vi.swipeRefresh.setOnRefreshListener {
            arrayMyBookingModel.clear()
            adapter?.notifyDataSetChanged()

            curPage=1
            noRecordUi.hideEmptyUi()
            fastViewMyBookings()
        }
    }


    override fun onStart() {
        super.onStart()

        if (adapter != null)
            fillBookingsAdapter()
        else
            fastViewMyBookings()
    }



    /*private fun fillMyBookingsAdapter(){
        arrayMyBookingModel.clear()
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Up coming"))
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Completed"))
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Up coming"))
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Completed"))
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Up coming"))
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Completed"))
        arrayMyBookingModel.add(MyBookingsModel(bookingTitle = "Marshal Arts",bookingSubTitle = "MPA Martial Arts",bookingImage = R.drawable.ic_dummy_my_booking,bookingLocation = "Lorem Ipsum is simply",bookingStatus = "Up coming"))

    }*/

    private fun fastViewMyBookings(progressShowMore: ProgressWheel? = null, fromSwipeToRefresh:Boolean=false) {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {
            noRecordUi.hideEmptyUi()

            if (curPage == 1)
                vi.rvBookings.showShimmerAdapter()

            val hashParams = HashMap<String, String>()
            hashParams["page"] = "" + curPage

            FastNetworking.makeRxCallPost(act.applicationContext, Urls.VIEW_MY_BOOKINGS, true, hashParams, "ViewBookings", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    doAsync {
                        try {
                            if (fromSwipeToRefresh)
                                vi.swipeRefresh.isRefreshing=false
                            if (curPage == 1)
                                arrayMyBookingModel.clear()

                            if (json!!.has("data")) {
                                totalPages = json.getJSONObject("data").optInt("last_page", 1)
                            }

                            arrayMyBookingModel.addAll(JsonParse.parseMyBookings(json))

                        } catch (e: Exception) {
                            Log.d("Exc_MyBookings", "" + e)
                        } finally {

                            isLoadingAlready = false
                            runOnUiThread {
                                progressShowMore?.visibility = View.GONE
                                fillBookingsAdapter()
                            }
                        }
                    }
                    vi.swipeRefresh.isEnabled = true
                    vi.swipeRefresh.isRefreshing = false
                }

                override fun onApiError(error: ANError) {
                    if (fromSwipeToRefresh)
                        vi.swipeRefresh.isRefreshing=false
                    isLoadingAlready = false
                    progressShowMore?.visibility = View.GONE
                    if (curPage == 1) {
                        vi.rvBookings.hideShimmerAdapter()
                        noRecordUi.showMsgError(strDesc = ErrorMsgs.ERR_API_MSG, onClickListener = View.OnClickListener {
                            noRecordUi.hideEmptyUi()
                            fastViewMyBookings(progressShowMore)
                        })
                    }
                }

            })
        } else {
            if (curPage == 1) {
                vi.rvBookings.hideShimmerAdapter()
                noRecordUi.showMsgError(strDesc = ErrorMsgs.ERR_CONNECTION_MSG, onClickListener = View.OnClickListener {
                    noRecordUi.hideEmptyUi()
                    fastViewMyBookings(progressShowMore)
                })
            }
        }
    }

    private fun fillBookingsAdapter() {
        if (curPage == 1) {
            vi.rvBookings.hideShimmerAdapter()
            vi.rvBookings.isEnabled=true
        }
        //arrayMyBookingModel.clear()
        if (arrayMyBookingModel.size > 0) {
            noRecordUi.hideEmptyUi()

            if (adapter == null) {
                adapter = MyBookingsAdapter(act, arrayMyBookingModel, this::onBookingItemClicked, this::onShowMorePaginate)
                vi.rvBookings.adapter = adapter
            } else {
                adapter?.notifyDataSetChanged()
            }
        } else {
            noRecordUi.showNoBookings {
                if(act is DashboardActivity){
                    NearbyFragment.selectedPos=-1
                    act.bottomNavView.selectedItemId=R.id.itemNearby
                }
            }
        }
    }

    private fun onBookingItemClicked(posClicked: Int) {
        val bookingModel = arrayMyBookingModel[posClicked]
        if (bookingModel.bookingType.equals(MyBookingsModel.BOOKING_TYPE_ACTIVITY,true)) {
            val activityModel = ActivityModel(activityId = bookingModel.activityId)
            activityModel.categoryId=bookingModel.actCategoryId
            activityModel.isPassActivity = bookingModel.isPassAct

            activityModel.arraySlotModel.add(ActivitySlotModel(slotId = bookingModel.timeSlotId).also {
                it.slotEnrolmentId = bookingModel.enrolmentId
            })
            selectedBookingIndex = posClicked
            MyNavigations.goToActivityDetail(act, activityModel, 0, true, bookingModel)
        }
        else if (bookingModel.bookingType.equals(MyBookingsModel.BOOKING_TYPE_FLEXI_ACTIVITY,true))
        {
            val activityModel = ActivityModel(activityId = bookingModel.activityId)
            activityModel.categoryId=bookingModel.actCategoryId
            MyNavigations.goToFlexiActivityDetail(act,activityModel,0,true,bookingModel)
        }
        else{
            MyNavigations.goToCampDetail(act,bookingModel.campId,true,bookingModel.bookingStatus,bookingModel)
        }
    }

    private fun onShowMorePaginate(pos: Int, progressShowMore: ProgressWheel) {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {
            if (curPage < totalPages && !isLoadingAlready) {
                progressShowMore.visibility = View.VISIBLE
                isLoadingAlready = true
                curPage += 1
                fastViewMyBookings(progressShowMore)
            }
        }
    }

    private fun refreshBookings(){
        if(ActivityDetailActivity.isBookingStatusCancelled){
            Log.d("SelectedIndex_",""+ selectedBookingIndex)
            ActivityDetailActivity.isBookingStatusCancelled=false

            val selectedModel=arrayMyBookingModel[selectedBookingIndex]
            selectedModel.bookingStatus=StaticValues.BOOKING_STATUS_CANCELLED
            selectedModel.bookingMsgTime=""
            arrayMyBookingModel.set(selectedBookingIndex,selectedModel)
            if(vi.rvBookings.adapter!=null)
                vi.rvBookings.adapter?.notifyDataSetChanged()
        }
    }


}
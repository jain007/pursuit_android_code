package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.pursueit.R
import com.pursueit.adapters.MyCustomTabAdapter
import com.pursueit.adapters.MyFragmentPagerAdapter
import com.pursueit.adapters.NearbyActivityAdapter
import com.pursueit.custom.CustomFont
import kotlinx.android.synthetic.main.fragment_my_corner.view.*
import org.jetbrains.anko.textColor

class MyCornerFragment: Fragment() {

    lateinit var act: Activity
    lateinit var vi: View

    var adapter: NearbyActivityAdapter? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        act = context as Activity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_my_corner, container, false)
        view.setOnClickListener { }
        vi = view
        init()
        return view
    }


    override fun onResume() {
        super.onResume()

    }

    private fun init() {

        val adapter = MyFragmentPagerAdapter(childFragmentManager)
        adapter.addFragment(MyBookingsFragment(), "My Bookings")
        adapter.addFragment(MyFavoritesFragment(), "Favourites")
        
        vi.viewPager.adapter = adapter


        val vpAdapter = MyCustomTabAdapter(vi.viewPager, adapter, act,fromMyCorner = true) { posClicked ->
            vi.viewPager.currentItem = posClicked
            Log.d("PosClickedTab", "" + posClicked + " - " + adapter.mFragmentTitleList[posClicked])
        }


        //vi.recycler_tab_layout.layoutManager=LinearLayoutManager(act)
        vi.recycler_tab_layout.setUpWithAdapter(vpAdapter)
        //vi.tabLayout.setViewPager(vi.viewPager)

        //val lyTabs = vi.tabLayout.getChildAt(0) as LinearLayout
        //changeTabsTitleTypeFace(lyTabs, 0)

        vi.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(pos: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(pos: Int) {
                //changeTabsTitleTypeFace(lyTabs, pos)
            }

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            val adapter= vi.viewPager.adapter as MyFragmentPagerAdapter
            adapter.mFragmentList.get(vi.viewPager.currentItem).onActivityResult(requestCode, resultCode, data)
        }catch (e:Exception)
        {
            Log.e("MyCorner:onActResult",e.toString())
        }

    }

    fun changeTabsTitleTypeFace(ly: LinearLayout, position: Int) {
        for (j in 0 until ly.childCount) {
            val tvTabTitle = ly.getChildAt(j) as TextView
            tvTabTitle.typeface = CustomFont.setFontSemiBold(assetManager = resources.assets)
            if (j == position) {
                tvTabTitle.textColor = Color.WHITE
            }else{

                tvTabTitle.textColor = ContextCompat.getColor(act, R.color.colorTextGreySubTitle)
            }
        }
    }
}
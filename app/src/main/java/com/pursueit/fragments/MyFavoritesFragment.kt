package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.FavoriteActivityCampAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.FavoriteModel
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_my_bookings.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class MyFavoritesFragment : Fragment() {

    lateinit var vi: View
    lateinit var act: Activity

    var adapter: FavoriteActivityCampAdapter? = null
    var arrayModel = ArrayList<FavoriteModel>()

    lateinit var noRecordUi: NoRecordFound

    var curPage = 1
    var totalPages = 1
    var isLoadingAlready = false

    var compositeDisposable = CompositeDisposable()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        act = context as Activity
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_my_bookings, container, false)
        view.setOnClickListener { }
        vi = view
        init()
        return view
    }



    private fun init() {
        vi.rvBookings.layoutManager = LinearLayoutManager(act)
        vi.rvBookings.setHasFixedSize(true)
        vi.rvBookings.hideShimmerAdapter()

        noRecordUi = NoRecordFound(act, vi).also { it.init() }
        noRecordUi.showNoFavorites()

        vi.swipeRefresh.isEnabled = false

        swipeRefreshFunctionality()
    }

    private fun swipeRefreshFunctionality()
    {
        vi.swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        vi.swipeRefresh.setOnRefreshListener {
            arrayModel.clear()
            adapter?.notifyDataSetChanged()

            curPage=1
            noRecordUi.hideEmptyUi()
            fastFetchMyFavoriteList()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("MyFavoritesFragment","onStart")
        if (adapter!=null)
            fillListingAdapter()
        else
            fastFetchMyFavoriteList()
    }

    override fun onResume() {
        super.onResume()

        Log.d("MyFavoritesFragment","onResume")
        if (MarkFavoriteOrUnFavorite.favouriteUpdated) {
            fastFetchMyFavoriteList()
            MarkFavoriteOrUnFavorite.favouriteUpdated=false
        }
    }


    private fun fastFetchMyFavoriteList(progressShowMore: ProgressWheel? = null, fromSwipeToRefresh:Boolean=false) {
        if (ConnectionDetector.isConnectingToInternet(act)) {
            noRecordUi.hideEmptyUi()

            if (curPage == 1) {
                arrayModel.clear()

                try {
                    if (curPage <= 1) {
                        vi.rvBookings.showShimmerAdapter()
                    } else {
                        hideShimmer()
                    }
                } catch (e: Exception) {

                }
            }

            var hashParams = HashMap<String, String>()
            hashParams.put("page", "" + curPage)

            doAsync {
                FastNetworking.makeRxCallPost(
                    act,
                    Urls.VIEW_FAVORITE,
                    true,
                    tag = "View_Favorite",hashParams = hashParams,
                    onApiResult = object :
                        FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {
                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    //saveResponseJson(act.applicationContext, strCatId!!, curPage, json.toString())
                                    parseData(json.toString(), progressShowMore)
                                } else {
                                    if (fromSwipeToRefresh || vi.swipeRefresh.isRefreshing)
                                        vi.swipeRefresh.isRefreshing=false
                                   // showErrorUi("Coming soon")
                                    if (curPage<=1) {
                                        vi.rvBookings.visibility = View.GONE
                                        noRecordUi.showNoFavorites()
                                    }
                                }
                            } catch (e: Exception) {
                                Log.d("ExcParse_CampList", "" + e)
                                showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                            }
                        }

                        override fun onApiError(error: ANError) {
                            showErrorUi(ErrorMsgs.ERR_API_MSG)

                            if (fromSwipeToRefresh || vi.swipeRefresh.isRefreshing)
                                vi.swipeRefresh.isRefreshing=false

                            if (progressShowMore != null) {
                                progressShowMore.visibility = View.GONE
                            }
                        }

                    })
            }
        }
    }



    private fun showErrorUi(strMsg: String) {
        try {
            if (!act.isFinishing) {
                hideShimmer()
                vi.rvBookings.visibility = View.GONE
                noRecordUi.showNoActivities(strMsg, View.OnClickListener {
                    noRecordUi.hideEmptyUi()
                    fastFetchMyFavoriteList()
                })
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                vi.rvBookings.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }

    fun parseData(strJson: String, progressShowMore: ProgressWheel? = null) {
        doAsync {
            try {
                isLoadingAlready = false
                val json = JSONObject(strJson)

                if (curPage == 1)
                    arrayModel.clear()

                if (json.getBoolean(StaticValues.KEY_STATUS)) {
                    val jsonData = json.getJSONObject("data")
                    totalPages = jsonData.optInt("last_page", 1)
                    arrayModel.addAll(JsonParse.parseFavoriteData(jsonData,json))
                }

                act.runOnUiThread {
                    fillListingAdapter()
                }
            } catch (e: Exception) {
                Log.d("ParseExc__", "" + e)
            } finally {
                act.runOnUiThread {
                    vi.swipeRefresh.isEnabled = true
                    vi.swipeRefresh.isRefreshing = false
                }
                if (progressShowMore != null) {
                    act.runOnUiThread {
                        progressShowMore.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")

        try {
            if (arrayModel.isNotEmpty()) {
                noRecordUi.hideEmptyUi()
                vi.rvBookings.visibility = View.VISIBLE

                if (vi.rvBookings.adapter == null || adapter == null) {
                    vi.rvBookings.isEnabled = true
                    adapter = FavoriteActivityCampAdapter(act as AppCompatActivity, arrayModel, vi.rootMyBooking, this, showMoreListener = { pos, progressShowMore ->
                        Log.d("Cur_", "" + curPage)
                        Log.d("last_", "" + totalPages)
                        progressShowMore.visibility = View.GONE
                        if (curPage < totalPages && !isLoadingAlready) {
                            isLoadingAlready = true
                            curPage += 1
                            progressShowMore.visibility = View.VISIBLE
                            fastFetchMyFavoriteList(progressShowMore)
                        } else {
                            progressShowMore.visibility = View.GONE
                        }
                    }, compositeDisposable = compositeDisposable)
                    vi.rvBookings.adapter = adapter
                } else {
                    adapter?.notifyDataSetChanged()
                }
            } else {
                noRecordUi.showNoFavorites()
            }
        }catch (e:Exception){

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (data!=null)
            {
                if (data.hasExtra("campId"))
                {
                    val campId=data.getStringExtra("campId")
                    val isFavorite=data.getBooleanExtra("isFavorite",false)
                    repeat(arrayModel.size){
                        val model=arrayModel[it]
                        if (model.campModel!=null)
                        {
                            if (campId==model.campModel.campId)
                            {
                                if (!isFavorite)
                                {
                                    arrayModel.removeAt(it)
                                    adapter?.notifyItemRemoved(it)
                                }
                            }
                        }
                    }
                }
                else if (data.hasExtra("actId"))
                {
                    val actId=data.getStringExtra("actId")
                    val isFavorite=data.getBooleanExtra("isFavorite",false)
                    repeat(arrayModel.size){
                        val model=arrayModel[it]
                        if (model.activityModel!=null)
                        {
                            if (model.activityModel.activityId==actId)
                            {
                                if (!isFavorite)
                                {
                                    arrayModel.removeAt(it)
                                    adapter?.notifyItemRemoved(it)
                                }
                            }
                        }
                    }
                }

                if (arrayModel.size==0)
                {
                    vi.rvBookings.visibility = View.GONE
                    noRecordUi.showNoFavorites()
                }
                else{
                    noRecordUi.hideEmptyUi()
                }
            }
        }catch (e:Exception)
        {

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}

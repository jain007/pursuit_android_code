package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.NearbyActivityAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.model.CategoryModel
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.empty_search_or_filter.view.*
import kotlinx.android.synthetic.main.fragment_nearby_listing.view.*
import kotlinx.android.synthetic.main.row_review.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class NearbyActivitiesListingFragment : Fragment() {

    lateinit var act: Activity
    lateinit var vi: View

    var adapter: NearbyActivityAdapter? = null
    private var arrayNearbyActivities = ArrayList<ActivityModel>()

    private var categoryModel: CategoryModel? = null
    lateinit var noActivityFound: NoRecordFound

    var curPage = 1
    var totalPages = 1
    var isLoadingAlready = false

    //lateinit var userModel: UserModel

    var curPos = 0

    var compositeDisposable = CompositeDisposable()

    var arrActivityForMap: ArrayList<ActivityModel>? = null
    private var subCatId = ""

    companion object {
        //var lastClickedPosition = 0

        fun newInstance(categoryModel: CategoryModel, pos: Int): Fragment {
            val fragment = NearbyActivitiesListingFragment()
            val bundle = Bundle()
            bundle.putSerializable("categoryModel", categoryModel)
            bundle.putInt("Pos", pos)
            fragment.arguments = bundle
            return fragment
        }

        fun saveResponseJson(context: Context, strCatId: String, pageNo: Int, strResponse: String) {
            val prefs = GetSetSharedPrefs(context)
            prefs.putData("Activities_$strCatId" + "_$pageNo", strResponse)
        }

        fun resetMakeResponseEmptyAllCategories(context: Context) {
            doAsync {
                val prefs = GetSetSharedPrefs(context)
                val arrayKeys = prefs.prefs.all.keys.filter { it.startsWith("Activities_") }
                for (strKey in arrayKeys) {
                    Log.d("SharedPreferencesKeys", strKey)
                    prefs.putData(strKey, "")
                }
            }

        }

        fun getSavedResponseJson(context: Context, strCatId: String, pageNo: Int): String {
            val prefs = GetSetSharedPrefs(context)
            return prefs.getData("Activities_$strCatId" + "_$pageNo")
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        act = context as Activity
        //userModel = UserModel.getUserModel(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_nearby_listing, container, false)
        view.setOnClickListener { }
        vi = view

        init()
        return view
    }

    private fun setListener() {

    }

    private fun init() {
        noActivityFound = NoRecordFound(act, vi, true)
        noActivityFound.init()
        noActivityFound.hideEmptyUi()
        vi.cnsEmptySearchAndFilter.visibility = View.GONE

        vi.rvNearbyActivities.itemAnimator = null
        vi.rvNearbyActivities.isEnabled = false
        vi.rvNearbyActivities.layoutManager =
            LinearLayoutManager(act, LinearLayoutManager.VERTICAL, false)

        vi.swipeRefresh.isEnabled = false

        swipeRefreshFunctionality()

        initiateWork()

    }

    private fun swipeRefreshFunctionality() {
        vi.swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        vi.swipeRefresh.setOnRefreshListener {

            arrayNearbyActivities.clear()
            adapter?.notifyDataSetChanged()

            curPage = 1
            noActivityFound.hideEmptyUi()
            vi.cnsEmptySearchAndFilter.visibility = View.GONE

            for (i in 1..totalPages)
                saveResponseJson(act.applicationContext, "" + categoryModel?.catId, i, "")

            initFetchingResponse(loadMoreOrSwipeRefresh = true, subCatId = subCatId)

        }
    }

    override fun onStart() {
        super.onStart()
    }

    private fun initiateWork() {
        //curPage = 1


//        strCatId = this.arguments?.getString("CategoryId") ?: ""
        categoryModel = this.arguments?.getSerializable("categoryModel") as CategoryModel
        curPos = this.arguments?.getInt("Pos") ?: 0
        if (categoryModel?.catId!!.isNotEmpty()) {
            vi.chipSubCategoriesScroll.visibility = View.VISIBLE
            val chip: RadioButton =
                layoutInflater.inflate(
                    R.layout.subcategory,
                    vi.rgSubCategories,
                    false
                ) as RadioButton
            chip.id = View.generateViewId()
            chip.tag = "0"
            chip.text = "All"
            vi.rgSubCategories.addView(chip)
            chip.isChecked = true
        } else {
            vi.chipSubCategoriesScroll.visibility = View.GONE
        }
        for (model in categoryModel?.subCategories!!) {
            val chip: RadioButton =
                layoutInflater.inflate(
                    R.layout.subcategory,
                    vi.rgSubCategories,
                    false
                ) as RadioButton
            chip.id = View.generateViewId()
            chip.tag = model.subCatId
            chip.text = model.subCatName
            vi.rgSubCategories.addView(chip)
        }
        vi.rgSubCategories.setOnCheckedChangeListener { group: RadioGroup, checkedId: Int ->
            val checkedRadioButton: RadioButton = group.findViewById(checkedId)
            val tag: String = checkedRadioButton.tag as String
            subCatId = if (tag == "0") {
                ""
            } else {
                tag
            }
            fastFetchNearbyActivities(
                strSubCatId = subCatId,
                activityId = ""
            )
        }
        noActivityFound.hideEmptyUi()
        vi.cnsEmptySearchAndFilter.visibility = View.GONE

        if (MyPlaceUtils.placeModel.placeAddress.trim().isEmpty()) {
            showErrorMsg("Please select your location first")
            return
        }

        initFetchingResponse(loadMoreOrSwipeRefresh = false)
    }

    private fun initFetchingResponse(
        progressShowMore: ProgressWheel? = null,
        loadMoreOrSwipeRefresh: Boolean, subCatId: String = ""
    ) {
        if (categoryModel?.catId != null) {
            //fillAdapter()
            /*val strJson = getSavedResponseJson(act.applicationContext, strCatId!!, pageNo = curPage)
            if (strJson.trim().isNotEmpty()) {
                Log.d("LocalFound", "_$strCatId")
                parseData(strJson, progressShowMore)
            } else {
                Log.d("LocalNotFound", "_$strCatId")
                initFastFetch(progressShowMore)
            }*/

            /*Log.d("NearbyList_Cat_Id", "" + strCatId)
            Log.d("NearbyList_Page", "" + curPage)
            Log.d("Nearby_List_Size_", "" + arrayNearbyActivities.size)
            Log.d("Nearby_List_Adapter", "" + vi.rvNearbyActivities.adapter)*/

            if (loadMoreOrSwipeRefresh || arrayNearbyActivities.size == 0 || vi.rvNearbyActivities.adapter == null) {
                initFastFetch(progressShowMore, subCatId)
            }
        }
    }

    private fun initFastFetch(progressShowMore: ProgressWheel? = null, subCatId: String = "") {
        var strSubCatId = subCatId
        var strActivityId = ""
        if (DashboardActivity.selectedSearchModel != null) {
            try {
                if (categoryModel?.catId == DashboardActivity.selectedSearchModel!!.catId) {
                    strSubCatId = DashboardActivity.selectedSearchModel!!.subCatId
                    strActivityId = DashboardActivity.selectedSearchModel!!.activityId
                }
            } catch (e: Exception) {

            }
        } else if (DashboardActivity.selectedPromotionModel != null) {
            try {
                if (categoryModel?.catId == DashboardActivity.selectedPromotionModel!!.catId) {
                    strSubCatId = DashboardActivity.selectedPromotionModel!!.subCateId
                    strActivityId = DashboardActivity.selectedPromotionModel!!.activityId
                }
            } catch (e: Exception) {

            }

        }
        fastFetchNearbyActivities(
            strSubCatId,
            activityId = strActivityId,
            progressShowMore = progressShowMore
        )
    }

    private fun fastFetchNearbyActivities(
        strSubCatId: String = "",
        activityId: String = "",
        progressShowMore: ProgressWheel? = null
    ) {
        try {
            if (ConnectionDetector.isConnectingToInternet(act)) {

                noActivityFound.hideEmptyUi()
                vi.cnsEmptySearchAndFilter.visibility = View.GONE

                if (curPage == 1) {
                    arrayNearbyActivities.clear()

                    try {
                        if (curPos <= 1) vi.rvNearbyActivities.showShimmerAdapter()
                        else hideShimmer()

                        if (curPos > 1 && !vi.swipeRefresh.isRefreshing) {
                            vi.progressWheelNearby.visibility = View.VISIBLE
                        }

                    } catch (e: Exception) {

                    }
                }

                doAsync {
                    var hashParams = HashMap<String, String>()
                    hashParams.put("latitude", "" + MyPlaceUtils.placeModel.placeLat)
                    hashParams.put("longitude", "" + MyPlaceUtils.placeModel.placeLng)
                    hashParams.put("categoryId", "" + categoryModel?.catId)
                    hashParams.put("subCategoryId", "" + strSubCatId)
                    hashParams.put("ageFrom", DashboardActivity.ageFrom)
                    hashParams.put("ageTo", DashboardActivity.ageTo)
                    hashParams.put("activityId", "" + activityId)
                    hashParams.put("cityPlaceId", "" + MyPlaceUtils.placeModel.placeCityId)
                    hashParams.put("page", "" + curPage)

                    Log.d("Rx_ShowPassActivities", "" + DashboardActivity.showPassActivities)

                    if (DashboardActivity.showPassActivities)
                        hashParams.put("showPassActivities", "1")

                    hashParams = SearchAndFilterStaticData.addFilterData(hashParams)

                    FastNetworking.makeRxCallPost(act,
                        Urls.NEARBY_ACTIVITIES,
                        true,
                        hashParams,
                        "NearByActivities_cat_${categoryModel?.catId}",
                        object : FastNetworking.OnApiResult {
                            override fun onApiSuccess(json: JSONObject?) {
                                try {
                                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                        //saveResponseJson(act.applicationContext, strCatId!!, curPage, json.toString())
                                        parseData(json.toString(), progressShowMore)
                                    } else {
                                        showErrorUi(getString(R.string.error_message))
                                    }
                                } catch (e: Exception) {
                                    Log.d("ExcParse_${categoryModel?.catId}", "" + e)
                                    showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                                } finally {
                                    vi.progressWheelNearby.visibility = View.GONE
                                }
                            }

                            override fun onApiError(error: ANError) {
                                //showErrorMsg(ErrorMsgs.ERR_API_MSG)
                                showErrorUi(ErrorMsgs.ERR_API_MSG)
                                vi.progressWheelNearby.visibility = View.GONE

                                if (progressShowMore != null) {
                                    progressShowMore.visibility = View.GONE
                                }
                            }

                        })
                }

            } else {
                showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
            }
        } catch (e: Exception) {
            Log.d("Exc_FetchNearby", "" + e)
        }
    }

    private fun parseData(strJson: String, progressShowMore: ProgressWheel? = null) {
        doAsync {
            try {
                isLoadingAlready = false
                val json = JSONObject(strJson)

                if (curPage == 1)
                    arrayNearbyActivities.clear()

                if (json.getBoolean(StaticValues.KEY_STATUS)) {
                    val jsonData = json.getJSONObject("data")
                    totalPages = jsonData.optInt("last_page", 1)
                    val (arrActivity, arrActivityForMap) = JsonParse.parseNearbyActivities(jsonData, json)
                    arrayNearbyActivities.addAll(arrActivity)
                    this@NearbyActivitiesListingFragment.arrActivityForMap = arrActivityForMap
                    /*try {
                        if (arrActivityForMap.size>0)
                        {
                            (parentFragment as NearbyFragment).showFab( vi.rvNearbyActivities)
                        }
                        else{
                            (parentFragment as NearbyFragment).hideFab()
                        }
                    }catch (e:Exception){}*/
                } else {
                    // (parentFragment as NearbyFragment).hideFab()
                }

                act.runOnUiThread {
                    fillListingAdapter()
                }
            } catch (e: Exception) {
                Log.d("ParseExc__", "" + e)
            } finally {
                act.runOnUiThread {
                    vi.swipeRefresh.isEnabled = true
                    vi.swipeRefresh.isRefreshing = false
                }
                if (progressShowMore != null) {
                    act.runOnUiThread {
                        progressShowMore.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun showErrorMsg(strMsg: String) {
        try {
            /*if (!act.isFinishing) {
                vi.rvNearbyActivities.hideShimmerAdapter()
            }*/
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                vi.rvNearbyActivities.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }

    private fun showErrorUi(strMsg: String) {
        try {
            if (!act.isFinishing) {
                hideShimmer()
                vi.rvNearbyActivities.visibility = View.GONE
                vi.cnsEmptySearchAndFilter.visibility = View.VISIBLE

                /*if (SearchAndFilterStaticData.isFilterApplied || SearchAndFilterStaticData.isAgeFilterApplied)
                {
                    vi.cnsEmptySearchAndFilter.visibility=View.VISIBLE
                }
                else {
                    noActivityFound.showNoActivities(strMsg, View.OnClickListener {
                        noActivityFound.hideEmptyUi()
                        initFastFetch()
                    })
                }*/
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")
        noActivityFound.hideEmptyUi()
        vi.rvNearbyActivities.visibility = View.VISIBLE

        if (vi.rvNearbyActivities.adapter == null || adapter == null) {
            vi.rvNearbyActivities.isEnabled = true
            adapter = NearbyActivityAdapter(
                act as AppCompatActivity,
                arrayNearbyActivities,
                showMoreListener = { pos, progressShowMore ->
                    Log.d("Cur_", "" + curPage)
                    Log.d("last_", "" + totalPages)
                    progressShowMore.visibility = View.GONE
                    if (curPage < totalPages && !isLoadingAlready) {
                        isLoadingAlready = true
                        curPage += 1
                        progressShowMore.visibility = View.VISIBLE
                        initFetchingResponse(progressShowMore, loadMoreOrSwipeRefresh = true)
                    } else {
                        progressShowMore.visibility = View.GONE
                    }
                },
                compositeDisposable = compositeDisposable
            )
            vi.rvNearbyActivities.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }

        /*try {
            if (lastClickedPosition > 0 && lastClickedPosition < arrayNearbyActivities.size) {
                vi.rvNearbyActivities.scrollToPosition(lastClickedPosition)
            }
        } catch (e: Exception) {
            Log.d("Exc_LastClicked_", "" + e)
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        try {
            if (intent != null) {
                val actId = intent.getStringExtra("actId")
                val model = arrayNearbyActivities.single { it.activityId == actId }
                val pos = arrayNearbyActivities.indexOf(model)
                val isFavorite = intent.getBooleanExtra("isFavorite", false)
                model.isFavoriteMarked = isFavorite
                arrayNearbyActivities[pos] = model
                adapter?.notifyItemChanged(pos)
            }
        } catch (e: Exception) {

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}
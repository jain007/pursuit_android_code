package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.academiceye.utils.HideFABOnRecyclerViewScroll
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.MyCustomTabAdapter
import com.pursueit.adapters.MyFragmentPagerAdapter
import com.pursueit.adapters.NearbyActivityAdapter
import com.pursueit.analytics.MyGoogleAnalytics
import com.pursueit.dialogs.CategorySubCategoryActivitiesSearchFragment
import com.pursueit.dialogs.DialogMsg
import com.pursueit.model.ActivityModel
import com.pursueit.model.CategoryModel
import com.pursueit.model.UserModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.action_layout_filter_option.view.*
import kotlinx.android.synthetic.main.action_layout_sort.view.*
import kotlinx.android.synthetic.main.content_layout_header_nearby.view.*
import kotlinx.android.synthetic.main.fragment_nearby.view.*
import kotlinx.android.synthetic.main.fragment_nearby_listing.*
import java.util.*

class NearbyFragment : Fragment() {

    lateinit var act: Activity
    lateinit var vi: View

    var adapter: NearbyActivityAdapter? = null
    var arrayNearbyActivities = ArrayList<ActivityModel>()

    var dialogMsg: DialogMsg? = null

    val fabHideForRv = HideFABOnRecyclerViewScroll()

    val fragmentAdapter: MyFragmentPagerAdapter by lazy {
        MyFragmentPagerAdapter(childFragmentManager)
    }

    companion object {
        var selectedPos = -1 //-1 to show all (0th fragment), else particular fragment
        fun newInstance(): NearbyFragment {
            return NearbyFragment()
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        act = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_nearby, container, false)
        view.setOnClickListener { }
        vi = view
        init()
        setListener()
        return view
    }

    private fun decideFilterIndicator() {
        // decide bubble indicator for filter button inside option menu (menu_nearby_activities)
        try {
            val menuItem = vi.toolbarFragment.menu.findItem(R.id.itemFilter)
            val viewFilter = menuItem.actionView
            viewFilter.relFilterItem.setOnClickListener {
                if (act is DashboardActivity) {
                    // hiding filter dialog for time being
                    if (dialogMsg == null)
                        dialogMsg = DialogMsg()
                    dialogMsg?.showNewFilterDialog(act, true, filterApplied = {})

                    //just showing age filter
                    //DashboardActivity.showFilterDialogNew(act,true)
                }
            }
            viewFilter.imgFilterIndicator.visibility =
                if (SearchAndFilterStaticData.isFilterApplied || SearchAndFilterStaticData.isAgeFilterApplied) View.VISIBLE else View.GONE //if (DashboardActivity.ageFrom.trim().isNotEmpty() && DashboardActivity.ageTo.trim().isNotEmpty()) View.VISIBLE else View.GONE

            val itemSort = vi.toolbarFragment.menu.findItem(R.id.itemSort)
            itemSort.actionView.relSortItem.setOnClickListener {
                if (dialogMsg == null)
                    dialogMsg = DialogMsg()
                dialogMsg?.showSortingDialog(act, true)
            }
        } catch (e: Exception) {
            Log.d("ViewIndicatorExc", "" + e)
        }
    }

    override fun onResume() {
        super.onResume()
        if (vi.viewPager.adapter != null) {
            // decide bubble indicator for filter button inside option menu (menu_nearby_activities)
            decideFilterIndicator()
        }
    }

    private fun init() {

        vi.linFABLocation.visibility = View.GONE

        MyGoogleAnalytics.sendSession(
            act,
            MyGoogleAnalytics.EXPLORE,
            UserModel.getUserModel(act.applicationContext).userEmail,
            this::class.java
        )

        if (vi.viewPager.adapter == null) {

            //toolbar set (with menu options)
            vi.toolbarFragment.inflateMenu(R.menu.menu_nearby_activities)
            vi.toolbarFragment.setOnMenuItemClickListener { item ->
                when (item?.itemId) {
                    R.id.itemSearch -> {
                        if (act is DashboardActivity) {
                            /*   if (DashboardActivity.showPassActivities)
                                   InitFragment.initDialogFragment((act as DashboardActivity).supportFragmentManager, CategorySubCategoryActivitiesSearchFragment.newInstanceForPass())
                               else*/
                            InitFragment.initDialogFragment(
                                (act as DashboardActivity).supportFragmentManager,
                                CategorySubCategoryActivitiesSearchFragment()
                            )
                        }
                    }
                }
                return@setOnMenuItemClickListener true
            }

            // decide bubble indicator for filter button inside option menu (menu_nearby_activities)
            decideFilterIndicator()

            vi.toolbarFragment.setOnClickListener {
                MyNavigations.goToSearchPlace(act)
            }

            if (MyPlaceUtils.placeModel.placeAddress.trim().isEmpty()) {
                if (dialogMsg == null)
                    dialogMsg = DialogMsg()
                dialogMsg?.showErrorRetryDialog(
                    act,
                    "Alert",
                    "Please select your location first",
                    "Select Location",
                    View.OnClickListener {
                        dialogMsg?.dismissDialog(act)
                        MyNavigations.goToSearchPlace(act)
                    },
                    false
                )
                return
            }

            //  val adapter = MyFragmentPagerAdapter(childFragmentManager)
            fragmentAdapter.addFragment(
                NearbyActivitiesListingFragment.newInstance(
                    CategoryModel(),
                    0
                ), "All"
            )
            /*fragmentAdapter.addFragment(
                OnlineClassesFragment.newInstance(), "Online Classes"
            )*/

            var pos = 1
            for (catModel in StaticValues.arrayDashboardCategories) {
                if (catModel.catId.isEmpty()) {
                    fragmentAdapter.addFragment(
                        OnlineClassesFragment.newInstance(), "Online Classes"
                    )
                } else {
                    fragmentAdapter.addFragment(
                        NearbyActivitiesListingFragment.newInstance(
                            catModel,
                            pos
                        ), catModel.catName.trim()
                    )
                }
                pos++
            }

            vi.viewPager.adapter = fragmentAdapter

            val vpAdapter = MyCustomTabAdapter(vi.viewPager, fragmentAdapter, act) { posClicked ->
                vi.viewPager.currentItem = posClicked
                Log.d(
                    "PosClickedTab",
                    "" + posClicked + " - " + fragmentAdapter.mFragmentTitleList[posClicked]
                )
            }


            vi.viewPager.offscreenPageLimit = vpAdapter.itemCount

            vi.recycler_tab_layout.setUpWithAdapter(vpAdapter)

            //vi.tabLayout.setViewPager(vi.viewPager)

            //val lyTabs = vi.tabLayout.getChildAt(0) as LinearLayout
            //changeTabsTitleTypeFace(lyTabs, 0)


            vi.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(pos: Int) {

                }

                override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

                }

                override fun onPageSelected(pos: Int) {
                    if (pos >= 0)
                        selectedPos = pos - 1

                    try {
                        fabHideForRv.setListener(
                            fragmentAdapter.mFragmentList[pos].rvNearbyActivities,
                            vi.fabLocations
                        )
                    } catch (e: Exception) {
                        Log.e("fabHideForRv listener", e.toString())
                    }


                    //Log.d("PosPageChange", "" + pos)
                    //changeTabsTitleTypeFace(lyTabs, pos)
                }

            })

            if (selectedPos >= 0) {
                vi.viewPager.currentItem = selectedPos + 1
            }


            try {
                vi.txtPlaceHeader.text =
                    MyPlaceUtils.replaceUAECountry(MyPlaceUtils.placeModel.placeAddress.trim())
            } catch (e: Exception) {
                Log.d("PlaceHeadingExc", "" + e)
            }

        }
    }


    fun showFab(rvNearbyActivities: RecyclerView) {
        vi.linFABLocation.visibility = View.VISIBLE
        /*vi.fabLocations.show()
        try {
            fabHideForRv.setListener(rvNearbyActivities, vi.fabLocations)
        }catch (e:Exception)
        {
            Log.e("fabHideForRv listener",e.toString())
        }*/
    }

    fun hideFab() {
        vi.linFABLocation.visibility = View.GONE
        //vi.fabLocations.hide()
    }

    private fun setListener() {
        vi.fabLocations.setOnClickListener {
            val fragmentNearByAct =
                (fragmentAdapter.mFragmentList[vi.viewPager.currentItem] as NearbyActivitiesListingFragment)
            val arrNearByActivityForMap = fragmentNearByAct.arrActivityForMap
            if (arrNearByActivityForMap != null && arrNearByActivityForMap.size > 0)
                MyNavigations.gotoLocationActivity(act, arrNearByActivityForMap)
        }
    }

    /*fun changeTabsTitleTypeFace(ly: LinearLayout, position: Int) {
        for (j in 0 until ly.childCount) {
            val tvTabTitle = ly.getChildAt(j) as TextView
            tvTabTitle.typeface = CustomFont.setFontSemiBold(assetManager = resources.assets)
            if (j == position) {
                tvTabTitle.textColor = Color.WHITE
            } else {

                tvTabTitle.textColor = ContextCompat.getColor(act, R.color.colorTextGreySubTitle)
            }
        }
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = fragmentAdapter.mFragmentList[vi.viewPager.currentItem]
        if (fragment is NearbyActivitiesListingFragment) {
            fragment.onActivityResult(
                requestCode,
                resultCode,
                data
            )
        } else if (fragment is OnlineClassesFragment) {
            fragment.onActivityResult(
                requestCode,
                resultCode,
                data
            )
        }
    }

}
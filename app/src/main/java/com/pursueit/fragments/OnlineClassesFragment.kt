package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.NearbyActivityAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.ActivityModel
import com.pursueit.model.CategoryModel
import com.pursueit.utils.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.empty_search_or_filter.view.*
import kotlinx.android.synthetic.main.fragment_nearby_listing.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.HashMap

class OnlineClassesFragment : Fragment() {

    lateinit var act: Activity
    lateinit var vi: View

    var adapter: NearbyActivityAdapter? = null
    private var arrayNearbyActivities = ArrayList<ActivityModel>()

    lateinit var noActivityFound: NoRecordFound

    var curPage = 1
    var totalPages = 1
    var isLoadingAlready = false

    //lateinit var userModel: UserModel


    var compositeDisposable = CompositeDisposable()

    var arrActivityForMap: ArrayList<ActivityModel>? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_online_classes, container, false)
        view.setOnClickListener { }
        vi = view

        init()
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            OnlineClassesFragment().apply {
/*
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
*/
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        act = context as Activity
    }

    private fun init() {
        noActivityFound = NoRecordFound(act, vi, true)
        noActivityFound.init()
        noActivityFound.hideEmptyUi()
        vi.cnsEmptySearchAndFilter.visibility = View.GONE

        vi.rvNearbyActivities.itemAnimator = null
        vi.rvNearbyActivities.isEnabled = false
        vi.rvNearbyActivities.layoutManager =
            LinearLayoutManager(act, LinearLayoutManager.VERTICAL, false)

        vi.swipeRefresh.isEnabled = false

        swipeRefreshFunctionality()

        initiateWork()

    }

    private fun swipeRefreshFunctionality() {
        vi.swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        vi.swipeRefresh.setOnRefreshListener {

            arrayNearbyActivities.clear()
            adapter?.notifyDataSetChanged()

            curPage = 1
            noActivityFound.hideEmptyUi()
            vi.cnsEmptySearchAndFilter.visibility = View.GONE

            initFetchingResponse(loadMoreOrSwipeRefresh = true)

        }
    }

    override fun onStart() {
        super.onStart()
    }

    private fun initiateWork() {
        noActivityFound.hideEmptyUi()
        vi.cnsEmptySearchAndFilter.visibility = View.GONE

        if (MyPlaceUtils.placeModel.placeAddress.trim().isEmpty()) {
            showErrorMsg("Please select your location first")
            return
        }

        initFetchingResponse(loadMoreOrSwipeRefresh = false)
    }

    private fun initFetchingResponse(
        progressShowMore: ProgressWheel? = null,
        loadMoreOrSwipeRefresh: Boolean
    ) {
        if (loadMoreOrSwipeRefresh || arrayNearbyActivities.size == 0 || vi.rvNearbyActivities.adapter == null) {
            initFastFetch(progressShowMore)
        }
    }

    private fun initFastFetch(progressShowMore: ProgressWheel? = null) {
        fastFetchNearbyActivities(
            progressShowMore = progressShowMore
        )
    }

    private fun fastFetchNearbyActivities(
        progressShowMore: ProgressWheel? = null
    ) {
        try {
            if (ConnectionDetector.isConnectingToInternet(act)) {

                noActivityFound.hideEmptyUi()
                vi.cnsEmptySearchAndFilter.visibility = View.GONE

                if (curPage == 1) {
                    arrayNearbyActivities.clear()
                    vi.rvNearbyActivities.showShimmerAdapter()
                } else {
                    vi.progressWheelNearby.visibility = View.VISIBLE
                }

                doAsync {
                    var hashParams = HashMap<String, String>()
//                    hashParams.put("key", "page" )
                    hashParams.put("page", "" + curPage)
                    hashParams.put("latitude", "" + MyPlaceUtils.placeModel.placeLat)
                    hashParams.put("longitude", "" + MyPlaceUtils.placeModel.placeLng)
                    hashParams.put("cityPlaceId", "" + MyPlaceUtils.placeModel.placeCityId)
//                    Log.d("Rx_ShowPassActivities", "" + DashboardActivity.showPassActivities)

//                    if (DashboardActivity.showPassActivities)
//                        hashParams.put("showPassActivities", "1")

                    hashParams = SearchAndFilterStaticData.addFilterData(hashParams)

                    FastNetworking.makeRxCallPost(act,
                        Urls.ONLINE_CLASSES,
                        true,
                        hashParams,
                        "OnlineClasses",
                        object : FastNetworking.OnApiResult {
                            override fun onApiSuccess(json: JSONObject?) {
                                try {
                                    if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                        //saveResponseJson(act.applicationContext, strCatId!!, curPage, json.toString())
                                        parseData(json.toString(), progressShowMore)
                                    } else {
                                        showErrorUi(getString(R.string.error_message))
                                    }
                                } catch (e: Exception) {
                                    showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                                } finally {
                                    vi.progressWheelNearby.visibility = View.GONE
                                }
                            }

                            override fun onApiError(error: ANError) {
                                //showErrorMsg(ErrorMsgs.ERR_API_MSG)
                                showErrorUi(ErrorMsgs.ERR_API_MSG)
                                vi.progressWheelNearby.visibility = View.GONE

                                if (progressShowMore != null) {
                                    progressShowMore.visibility = View.GONE
                                }
                            }

                        })
                }

            } else {
                showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
            }
        } catch (e: Exception) {
            Log.d("Exc_FetchNearby", "" + e)
        }
    }

    private fun parseData(strJson: String, progressShowMore: ProgressWheel? = null) {
        doAsync {
            try {
                isLoadingAlready = false
                val json = JSONObject(strJson)

                if (curPage == 1)
                    arrayNearbyActivities.clear()

                if (json.getBoolean(StaticValues.KEY_STATUS)) {
                    val jsonData = json.getJSONObject("data")
                    totalPages = jsonData.optInt("last_page", 1)
                    val (arrActivity, arrActivityForMap) = JsonParse.parseNearbyActivities(
                        jsonData,
                        json
                    )
                    arrayNearbyActivities.addAll(arrActivity)
                    this@OnlineClassesFragment.arrActivityForMap = arrActivityForMap
                    /*try {
                        if (arrActivityForMap.size>0)
                        {
                            (parentFragment as NearbyFragment).showFab( vi.rvNearbyActivities)
                        }
                        else{
                            (parentFragment as NearbyFragment).hideFab()
                        }
                    }catch (e:Exception){}*/
                } else {
                    // (parentFragment as NearbyFragment).hideFab()
                }

                act.runOnUiThread {
                    fillListingAdapter()
                }
            } catch (e: Exception) {
                Log.d("ParseExc__", "" + e)
            } finally {
                act.runOnUiThread {
                    vi.swipeRefresh.isEnabled = true
                    vi.swipeRefresh.isRefreshing = false
                }
                if (progressShowMore != null) {
                    act.runOnUiThread {
                        progressShowMore.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun showErrorMsg(strMsg: String) {
        try {
            /*if (!act.isFinishing) {
                vi.rvNearbyActivities.hideShimmerAdapter()
            }*/
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                vi.rvNearbyActivities.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }

    private fun showErrorUi(strMsg: String) {
        try {
            if (!act.isFinishing) {
                hideShimmer()
                vi.rvNearbyActivities.visibility = View.GONE
                vi.cnsEmptySearchAndFilter.visibility = View.VISIBLE

                /*if (SearchAndFilterStaticData.isFilterApplied || SearchAndFilterStaticData.isAgeFilterApplied)
                {
                    vi.cnsEmptySearchAndFilter.visibility=View.VISIBLE
                }
                else {
                    noActivityFound.showNoActivities(strMsg, View.OnClickListener {
                        noActivityFound.hideEmptyUi()
                        initFastFetch()
                    })
                }*/
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")
        noActivityFound.hideEmptyUi()
        vi.rvNearbyActivities.visibility = View.VISIBLE

        if (vi.rvNearbyActivities.adapter == null || adapter == null) {
            vi.rvNearbyActivities.isEnabled = true
            adapter = NearbyActivityAdapter(
                act as AppCompatActivity,
                arrayNearbyActivities,
                showMoreListener = { pos, progressShowMore ->
                    Log.d("Cur_", "" + curPage)
                    Log.d("last_", "" + totalPages)
                    progressShowMore.visibility = View.GONE
                    if (curPage < totalPages && !isLoadingAlready) {
                        isLoadingAlready = true
                        curPage += 1
                        progressShowMore.visibility = View.VISIBLE
                        initFetchingResponse(progressShowMore, loadMoreOrSwipeRefresh = true)
                    } else {
                        progressShowMore.visibility = View.GONE
                    }
                },
                compositeDisposable = compositeDisposable
            )
            vi.rvNearbyActivities.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }

        /*try {
            if (lastClickedPosition > 0 && lastClickedPosition < arrayNearbyActivities.size) {
                vi.rvNearbyActivities.scrollToPosition(lastClickedPosition)
            }
        } catch (e: Exception) {
            Log.d("Exc_LastClicked_", "" + e)
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        try {
            if (intent != null) {
                val actId = intent.getStringExtra("actId")
                val model = arrayNearbyActivities.single { it.activityId == actId }
                val pos = arrayNearbyActivities.indexOf(model)
                val isFavorite = intent.getBooleanExtra("isFavorite", false)
                model.isFavoriteMarked = isFavorite
                arrayNearbyActivities[pos] = model
                adapter?.notifyItemChanged(pos)
            }
        } catch (e: Exception) {

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}
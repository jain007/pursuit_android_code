package com.pursueit.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidnetworking.error.ANError
import com.pnikosis.materialishprogress.ProgressWheel
import com.pursueit.R
import com.pursueit.adapters.PromotionListingAdapter
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PromotionModel
import com.pursueit.utils.*
import kotlinx.android.synthetic.main.activity_promotion_listing.view.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.HashMap

class PromotionListingFragment : Fragment() {

    var rootView:View?=null
    val arrPromotionModel=ArrayList<PromotionModel>()
    var curPage=1
    var totalPages = 1
    var isLoadingAlready = false

    lateinit var noActivityFound: NoRecordFound

    var adapter: PromotionListingAdapter? = null

    var act:Activity? = null

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        act=getActivity()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        act=activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_promotion_listing, container, false)
        view.setOnClickListener { }
        rootView= view

        init()

        rootView!!.imgBack.setOnClickListener {
            act!!.onBackPressed()
        }

        return rootView
    }

    private fun init() {

        rootView!!.txtPlaceHeader.text="Promotions"

        rootView!!.imgBack.visibility = View.VISIBLE

        rootView!!.rvPromotionListing.itemAnimator = null
        rootView!!.rvPromotionListing.isEnabled = false
        rootView!!.rvPromotionListing.layoutManager = LinearLayoutManager(act, LinearLayoutManager.VERTICAL, false)
        rootView!!.rvPromotionListing.addItemDecoration(ItemOffsetDecorationForLinearVertical(resources.getDimension(R.dimen.dim_10).toInt()))

        rootView!!.swipeRefresh.isEnabled = false

        noActivityFound = NoRecordFound(act!!, rootView!!.relListingParent)
        noActivityFound.init()
        noActivityFound.hideEmptyUi()

        rootView!!.swipeRefresh.isEnabled = false
        swipeRefreshFunctionality()

        initiateWork()
    }

    private fun swipeRefreshFunctionality() {
        rootView!!.swipeRefresh.setColorSchemeResources(R.color.colorMainBlue)
        rootView!!.swipeRefresh.setOnRefreshListener {
            clearListAndMakeApiCall()
        }
    }

    private fun clearListAndMakeApiCall() {


        arrPromotionModel.clear()
        rootView!!.rvPromotionListing?.adapter?.notifyDataSetChanged()

        curPage = 1
        noActivityFound.hideEmptyUi()


        /*for (i in 1..totalPages)
            saveResponseJson(act.applicationContext, "" + strCatId, i, "")*/

        //initFetchingResponse(loadMoreOrSwipeRefresh = true)

        fastFetchPromotionListing()
    }

    private fun initiateWork() {

        noActivityFound.hideEmptyUi()
        fastFetchPromotionListing()
    }

    private fun hideShimmer() {
        try {
            if (curPage == 1)
                rootView!!.rvPromotionListing.hideShimmerAdapter()
        } catch (e: Exception) {

        }
    }


    private fun fastFetchPromotionListing(progressShowMore: ProgressWheel?=null)
    {
        if (ConnectionDetector.isConnectingToInternet(act!!))
        {
            if (curPage==1)
            {
                arrPromotionModel.clear()

                try {
                    if (curPage<=1)
                    {
                        rootView!!.rvPromotionListing.showShimmerAdapter()
                    }
                    else{
                        hideShimmer()
                    }
                }catch (e:Exception){}
            }

            doAsync {
                val hashParams = HashMap<String, String>()
                hashParams["page"] = "" + curPage
                FastNetworking.makeRxCallPost(act!!,
                    Urls.VIEW_PROMOTION,true,tag = "View_Promotion",hashParams = hashParams,
                    onApiResult = object : FastNetworking.OnApiResult {
                        override fun onApiSuccess(json: JSONObject?) {

                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonData=json.getJSONObject("data")
                                    totalPages=jsonData.optInt("last_page")
                                    val jsonPromotionArray=jsonData.getJSONArray("data")
                                    val array= JsonParse.parsePromotion(jsonPromotionArray,fromHome = true)

                                    if (StaticValues.arrGlobalPromotion.isNotEmpty()) {
                                        arrPromotionModel.addAll(StaticValues.arrGlobalPromotion)
                                    }

                                    arrPromotionModel.addAll(array)

                                    act!!.runOnUiThread {
                                        fillListingAdapter()
                                    }
                                }
                                else{
                                    showErrorUi(getString(R.string.error_message))
                                }
                            }catch (e:Exception)
                            {
                                Log.d("ExcParse_CampList", "" + e)
                                showErrorUi(ErrorMsgs.ERR_PARSE_TITLE)
                            }


                        }

                        override fun onApiError(error: ANError) {
                            showErrorUi(ErrorMsgs.ERR_API_MSG)

                            act!!.runOnUiThread {  if (progressShowMore != null) {
                                progressShowMore.visibility = View.GONE
                            }}

                        }

                    })
            }
        }
        else{
            showErrorMsg(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }

    private fun showErrorUi(strMsg: String) {
        try {
            if (!act!!.isFinishing) {
                hideShimmer()
                rootView!!.rvPromotionListing.visibility = View.GONE
                noActivityFound.showNoActivities(strMsg, View.OnClickListener {
                    noActivityFound.hideEmptyUi()
                    fastFetchPromotionListing()
                })
            }
        } catch (e: Exception) {
            Log.d("Exc_NoActivity", "" + e)
        }
    }

    private fun showErrorMsg(strMsg: String) {
        try {
            if (!act!!.isFinishing) {
                rootView!!.rvPromotionListing.hideShimmerAdapter()
            }
        } catch (e: Exception) {
            Log.d("Exc_", "" + e)
        }
    }

    private fun fillListingAdapter() {

        try {
            if (curPage == 1) {
                hideShimmer()
            }
        } catch (e: Exception) {

        }

        //Log.d("ComingInFillAdapter", "Yes")
        noActivityFound.hideEmptyUi()
        rootView!!.rvPromotionListing.visibility = View.VISIBLE

        if (rootView!!.rvPromotionListing.adapter == null || adapter == null) {
            rootView!!.rvPromotionListing.isEnabled = true
            adapter = PromotionListingAdapter((act as AppCompatActivity),arrPromotionModel) { pos, progressShowMore ->
                Log.d("Cur_", "" + curPage)
                Log.d("last_", "" + totalPages)
                progressShowMore.visibility = View.GONE
                if (curPage < totalPages && !isLoadingAlready) {
                    isLoadingAlready = true
                    curPage += 1
                    progressShowMore.visibility = View.VISIBLE
                    fastFetchPromotionListing(progressShowMore)
                } else {
                    progressShowMore.visibility = View.GONE
                }
            }
            rootView!!.rvPromotionListing.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }

}
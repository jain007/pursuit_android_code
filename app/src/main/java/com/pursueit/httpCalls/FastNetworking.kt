package com.pursueit.httpCalls

import android.content.Context
import android.content.Intent
import android.util.Log
import com.androidnetworking.error.ANError
import com.pursueit.MyApp
import com.pursueit.activities.UnauthenticatedActivity
import com.pursueit.utils.StaticValues
import com.pursueit.utils.LoginBasicsUi
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import java.io.File
import java.util.*


object FastNetworking {

    var compositeDisposable = CompositeDisposable()

    var isNavigatedToSessionExpireAlready=false

    fun makeRxCallPost(context: Context, url: String, includeHeaders: Boolean = false, hashParams: HashMap<String, String> = HashMap(), tag: String = "RxCall_", onApiResult: OnApiResult, compositeDisposable: CompositeDisposable? = null) {

        Log.d("Rx_" + tag + "_URL", url)
        hashParams["uuidToken"] = MyApp.ANDROID_ID

        when(url){
            Urls.LOGIN_USER_SOCIAL,Urls.LOGIN_USER,Urls.REGISTER_GUEST_USER,Urls.REGISTER_USER, Urls.UPDATE_FCM_TOKEN -> hashParams["loginFrom"] = "android"
        }


        val hashHeaders: HashMap<String, String> = HashMap()

        if (includeHeaders) {

            if (StaticValues.AUTH_TOKEN.trim().isEmpty())
                StaticValues.reviveHeaders(context)

            hashHeaders[StaticValues.KEY_AUTH] = "Bearer ${StaticValues.AUTH_TOKEN}"
            hashHeaders[StaticValues.KEY_ACCEPT] = "application/json"

            Log.d("Rx_${tag}_HEADERS", "--Headers--")
            hashHeaders.forEach {
                Log.d("Rx_${tag}_HEADERS", it.key + "=" + it.value)
            }
        }



        val reqBuilder = Rx2AndroidNetworking.post(url).addBodyParameter(hashParams)

        if (includeHeaders)
            reqBuilder.addHeaders(hashHeaders)

        reqBuilder.build()
            .jsonObjectObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { json -> json }
            .subscribe(object : Observer<JSONObject> {
                override fun onComplete() {
                    Log.d("Rx-$tag", "RxNetworkCompleted")
                }

                override fun onSubscribe(d: Disposable) {
                    if (compositeDisposable != null)
                        compositeDisposable.add(d)
                    else
                        FastNetworking.compositeDisposable.add(d)
                }

                override fun onNext(json: JSONObject) {
                    var proceedSuccess = true
                    try {
                        Log.d("Rx_${tag}_Response", "" + json)
                        if (json.getBoolean("status")) {
                            if (json.has("auth_token"))
                                StaticValues.putHeaders(context, json.getString("auth_token"))
                        } else {
                            if (isTokenUnauthenticated(json)) {
                                proceedSuccess = false
                                goToUnauthenticatedActivity(context)
                            }
                        }
                    } catch (e: Exception) {
                        Log.d("Exc_Token", "" + e)
                    }

                    if (proceedSuccess)
                        onApiResult.onApiSuccess(json)

                }

                override fun onError(e: Throwable) {
                    Log.d("RxError-$tag", "" + e)
                    if (e is ANError) {
                        Log.d("Rx-Error-$tag", "" + e.errorCode + "\n" + e.errorBody)
                        try {
                            val json = JSONObject(e.errorBody)
                            if (isTokenUnauthenticated(json)) {
                                goToUnauthenticatedActivity(context)
                            } else {
                                onApiResult.onApiError(e)
                            }
                        } catch (ex: Exception) {
                            onApiResult.onApiError(e)
                        }

                    }
                }

            })

    }

    fun makeRxCallPostImageUpload(context: Context, url: String, includeHeaders: Boolean = false, imageKey: String, imageFile: File?, hashParams: HashMap<String, String> = HashMap(), tag: String = "RxCall_", onApiResult: OnApiResult, compositeDisposable: CompositeDisposable? = null) {

        Log.d("Rx_" + tag + "_URL", url)
        Log.d("Params", "--Params--")
        hashParams.put("uuidToken", MyApp.ANDROID_ID)
        hashParams.forEach {
            Log.d("Parameter", it.key + "=" + it.value)
        }

        val hashHeaders: HashMap<String, String> = HashMap()

        if (includeHeaders) {

            if (StaticValues.AUTH_TOKEN.trim().isEmpty())
                StaticValues.reviveHeaders(context)

            hashHeaders.put(StaticValues.KEY_AUTH, "Bearer ${StaticValues.AUTH_TOKEN}")
            hashHeaders.put(StaticValues.KEY_ACCEPT, "application/json")

            Log.d("Headers", "--Headers--")
            hashHeaders.forEach {
                Log.d("Rx_${tag}_HEADERS", it.key + "=" + it.value)
            }
        }


        val reqBuilder = Rx2AndroidNetworking.upload(url)
        if (imageFile != null)
            reqBuilder.addMultipartFile(imageKey, imageFile)

        reqBuilder.addMultipartParameter(hashParams)


        if (includeHeaders)
            reqBuilder.addHeaders(hashHeaders)

        reqBuilder.build()
            .jsonObjectObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { json -> json }
            .subscribe(object : Observer<JSONObject> {
                override fun onComplete() {
                    Log.d("Rx_${tag}_", "RxNetworkCompleted")
                }

                override fun onSubscribe(d: Disposable) {
                    if (compositeDisposable != null)
                        compositeDisposable.add(d)
                    else
                        FastNetworking.compositeDisposable.add(d)
                }

                override fun onNext(json: JSONObject) {
                    var proceedSuccess = true
                    try {
                        Log.d("Rx_${tag}_Response", "" + json)
                        if (json.getBoolean("status")) {
                            if (json.has("auth_token"))
                                StaticValues.putHeaders(context, json.getString("auth_token"))
                        } else {
                            if (isTokenUnauthenticated(json)) {
                                proceedSuccess = false
                                goToUnauthenticatedActivity(context)
                            }
                        }
                    } catch (e: Exception) {
                        Log.d("Exc_Token", "" + e)
                    }

                    if (proceedSuccess)
                        onApiResult.onApiSuccess(json)

                }

                override fun onError(e: Throwable) {
                    Log.d("RxError-$tag", "" + e)
                    if (e is ANError) {
                        Log.d("Rx-Error-$tag", "" + e.errorCode + "\n" + e.errorBody)
                        try {
                            val json = JSONObject(e.errorBody)
                            if (isTokenUnauthenticated(json)) {
                                goToUnauthenticatedActivity(context)
                            } else {
                                onApiResult.onApiError(e)
                            }
                        } catch (ex: Exception) {
                            onApiResult.onApiError(e)
                        }
                    }
                }

            })

    }

    fun makeRxCallGet(context: Context, url: String, hashParams: HashMap<String, String> = HashMap(), tag: String = "RxCall_", onApiResult: OnApiResult) {

        Log.d("Rx_" + tag + "_URL", url)
        Log.d("Params", "--Params--")
        hashParams.forEach {
            Log.d("Rx_${tag}_Params", it.key + "=" + it.value)
        }


        val reqBuilder = Rx2AndroidNetworking.get(url).addQueryParameter(hashParams)

        reqBuilder.build()
            .jsonObjectObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { json -> json }
            .subscribe(object : Observer<JSONObject> {
                override fun onComplete() {
                    Log.d("Rx-$tag", "RxNetworkCompleted")
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onNext(json: JSONObject) {
                    try {
                        Log.d("Rx_${tag}_Response", "" + json)
                        onApiResult.onApiSuccess(json)
                    } catch (e: Exception) {
                        Log.d("Exc_Token", "" + e)
                    }

                }

                override fun onError(e: Throwable) {
                    Log.d("RxError-$tag", "" + e)
                    if (e is ANError) {
                        Log.d("Rx-Error-$tag", "" + e.errorCode + "\n" + e.errorBody)
                        onApiResult.onApiError(e)
                    }
                }

            })

    }

    private fun isTokenUnauthenticated(json: JSONObject?): Boolean {
        try {

            if (!json!!.getBoolean(StaticValues.KEY_STATUS)) {
                val strMsg = json.optString("message", "")
                return strMsg.equals("Unauthenticated.", true)
            }
        } catch (e: Exception) {

        }

        return false
    }

    private fun goToUnauthenticatedActivity(context: Context) {
        if(!isNavigatedToSessionExpireAlready) {
            isNavigatedToSessionExpireAlready=true
            LoginBasicsUi.resetAllUserTokensAndId(context)
            val intent = Intent(context, UnauthenticatedActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }


    interface OnApiResult {
        fun onApiSuccess(json: JSONObject?)
        fun onApiError(error: ANError)
    }

}
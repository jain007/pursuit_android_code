package com.pursueit.httpCalls

import com.pursueit.BuildConfig


object Urls {

    //In every header
    //Authorization: Bearer received_token
    //Accept: application/json

    //Image Urls
    const val IMAGE_URL_CATEGORY = BuildConfig.IMAGE_BASE_URL + "cat-image/thumb/"
    const val IMAGE_URL_PARTNER = BuildConfig.IMAGE_BASE_URL + "provider-logo/thumb/250/"
    const val IMAGE_SLIDER_PARTNER = BuildConfig.IMAGE_BASE_URL + "provider-image/thumb/"
    const val IMAGE_FEATURE_ACTIVITY = BuildConfig.IMAGE_BASE_URL + "activity-feature-image/thumb/"
    const val IMAGE_BOOKING_ACTIVITY = BuildConfig.IMAGE_BASE_URL + "activity-feature-image/thumb/"
    const val IMAGE_ACTIVITY = BuildConfig.IMAGE_BASE_URL + "activity-image/thumb/"
    const val IMAGE_USER = BuildConfig.IMAGE_BASE_URL + "customer-profile-image/"
    const val IMAGE_FEATURE_CAMP = BuildConfig.IMAGE_BASE_URL + "camp-feature-image/thumb/"
    const val IMAGE_BOOKING_CAMP = BuildConfig.IMAGE_BASE_URL + "camp-image/thumb/"
    const val IMAGE_PROMOTION = BuildConfig.IMAGE_BASE_URL + "promotion/"


    const val REGISTER_GUEST_USER = BuildConfig.BASE_URL + "signup-guest-customer"
    //uuidToken= &loginFrom=android

    const val REGISTER_USER = BuildConfig.BASE_URL + "signup-customer"
    //uuidToken=&firstName=&lastname=&emailId=&password= &loginFrom=android

    const val LOGIN_USER = BuildConfig.BASE_URL + "login-customer"
    //emailId= &password= &loginFrom=android

    const val LOGIN_USER_SOCIAL = BuildConfig.BASE_URL + "login-customer-social"
    //socialId  &uuidToken  &emailId  &imageUrl  &firstName  &lastName
    //&customerRegType= facebook google
    //&&loginFrom=android


    const val RESEND_OTP = BuildConfig.BASE_URL + "resend-verification-code"
    //emailId

    const val VIEW_USER_PROFILE = BuildConfig.BASE_URL + "view-customer-profile"
    //auth token

    const val EDIT_USER_PROFILE = BuildConfig.BASE_URL + "edit-customer-profile"
    //firstName,lastName,phoneNumber,profilePicture

    const val FORGOT_PASSWORD = BuildConfig.BASE_URL + "forgot-customer-password"
    //emailId
    //Auth token

    const val RESET_PASSWORD = BuildConfig.BASE_URL + "set-customer-new-password"
    //emailId=&newPassword=

    const val CHANGE_USER_PASS = BuildConfig.BASE_URL + "change-customer-password"
    //oldPassword (optional) , newPassword

    const val UPDATE_USER_EMAIL_VERIFY_STATUS =
        BuildConfig.BASE_URL + "customer-verification-status"

    const val HOME_SCREEN_CATEGORIES_PARTNERS = BuildConfig.BASE_URL + "home-screen-data-v2"
    //auth token

    const val NEARBY_ACTIVITIES = BuildConfig.BASE_URL + "get-near-by-activity-v4"
    //Old: get-near-by-activity-v2
    /* const val NEARBY_ACTIVITIES=BuildConfig.BASE_URL+"get-near-by-activity"*/
    //auth token

    const val SINGLE_ACTIVITY_DETAIL = BuildConfig.BASE_URL + "get-single-activity-v2"
    //activityId &enrolmentId

    const val VIEW_USER_FAMILY_MEMBERS = BuildConfig.BASE_URL + "view-customer-family"
    //auth token

    const val ADD_USER_FAMILY_MEMBER = BuildConfig.BASE_URL + "add-customer-family"
    //gender,name,interests,schoolName,dateOfBirth,preferredActivities, familyId

    const val DELETE_USER_FAMILY_MEMBER = BuildConfig.BASE_URL + "delete-customer-family"
    //familyId

    const val SEND_USER_ENQUIRY = BuildConfig.BASE_URL + "customer-enquiry"
    //providerId,activityId,timeSlotId,name,emailId,phoneNumber,message

    const val VIEW_ADDRESSES = BuildConfig.BASE_URL + "view-customer-address"
    //auth token in header

    const val ADD_ADDRESS = BuildConfig.BASE_URL + "add-customer-address"
    //addressId (In edit case),location,latitude,longitude,placeId,title

    const val DELETE_ADDRESS = BuildConfig.BASE_URL + "delete-customer-address"
    //addressId

    const val UPDATE_FCM_TOKEN = BuildConfig.BASE_URL + "update-fcm-token"
    //fcmToken,loginFrom(ios, android, website)


    const val VIEW_ALL_CAT_SUB_CAT = BuildConfig.BASE_URL + "category-auto-complete"

    const val VIEW_REVIEWS = BuildConfig.BASE_URL + "view-activity-review"
    //activityId

    const val ADD_REVIEW = BuildConfig.BASE_URL + "add-activity-review"
    //activityId,reviewRating,reviewDescription
    //in case of edit, pass reviewId

    const val ADD_BOOKING = BuildConfig.BASE_URL + "customer-booking-v2"

    //providerId &activityId &enrolmentId &familyId &pricePayable &sessionJson &device
    //promotionDetails:[{"promotion_id":"2","promotion_type":"1","promotion_value":"50","promotion_code":"50","discounted_amount":"50"},{"promotion_id":"3","promotion_type":"1","promotion_value":"50","promotion_code":"50","discounted_amount":"50"}]

    const val VIEW_MY_BOOKINGS = BuildConfig.BASE_URL + "customer-book-details-v2"
    /*const val VIEW_MY_BOOKINGS=BuildConfig.BASE_URL+"customer-book-details"*/

    const val VIEW_CONFIGURATION = BuildConfig.BASE_URL + "app-configuration"

    const val CANCEL_BOOKING = BuildConfig.BASE_URL + "cancel-customer-booking"
    //booking_id, total_refund_amount, cancelled_device

    const val CANCEL_PASS_ACTIVITY_BOOKING = BuildConfig.BASE_URL + "cancel-customer-pass-booking"
    //booking_id, total_refund_amount, cancelled_device

    const val SEARCH_ACTIVITIES = BuildConfig.BASE_URL + "get-search-activity"
    //Token in header
    // Param: searchName

    const val UPDATE_USER_LOCATION = BuildConfig.BASE_URL + "update-customer-location"
    //token in header
    //iosLocation,androidLocation

    const val UPDATE_TRANSACTION = BuildConfig.BASE_URL + "save-customer-transaction"
    //bookingId, providerId, orderId, enteredAmount,transactionStatus,trackingId,bankReferenceId,transactionResponse

    const val VIEW_PAYMENT_TRANSACTIONS = BuildConfig.BASE_URL + "view-customer-transaction"
    //Auth token

    const val NEAR_BY_CAMP = BuildConfig.BASE_URL + "get-near-by-camp-v2"
    //latitude:25.2604136
    //longitude:55.321520400000054
    //cityPlaceId:ChIJRcbZaklDXz4RYlEphFBu5r0
    //ageFrom:1
    //ageTo:10
    //campId:
    //campName:cam
    //tagsId:1

    // to search camp by camp name or tag name
    const val SEARCH_CAMP = BuildConfig.BASE_URL + "get-search-camp"
    //searchName:a


    const val VIEW_CAMP_DETAIL = BuildConfig.BASE_URL + "get-single-camp-v2"
    //  campId:2

    const val VIEW_CAMP_REVIEWS = BuildConfig.BASE_URL + "view-camp-review"
    // campId:1

    const val ADD_CAMP_REVIEW = BuildConfig.BASE_URL + "add-camp-review"
/*
    reviewId:2
    campId:1
    reviewRating:4
    reviewDescription:Good camp
*/

    val DELETE_CAMP_REVIEW = BuildConfig.BASE_URL + "delete-camp-review"
    // reviewId:1

    const val CAMP_BOOKING = BuildConfig.BASE_URL + "customer-camp-booking"
    /*providerId:1
    campId:1
    familyId:1
    device:1
    pricePayable:1
    totalBeforeTax:1
    vatAmount:1
    vatRate:1
    sessionJson:1
    orderId:1
    weekIds:1,2
    bookingForFlag:7*/

    const val HOME_SCREEN_SEARCH = BuildConfig.BASE_URL + "home-screen-search"
    //searchName:d

    const val ADD_REMOVE_FAVORITE = BuildConfig.BASE_URL + "add-customer-favorite"
/*
    activityId:1
    campId:
    favoriteFlag:0
    addRemoveFlog:1

    activityId Or campId , favoriteFlag (0-activity,1-camp), addRemoveFlog(1- add, 0 - remove)
*/


    const val VIEW_FAVORITE = BuildConfig.BASE_URL + "view-customer-favorite-v2"

    const val VIEW_SIMILAR_ACTIVITIES = BuildConfig.BASE_URL + "other-similar-activity-v2"
/*
    latitude:25.2604136
    longitude:55.321520400000054
    categoryId:11
    cityPlaceId:ChIJRcbZaklDXz4RYlEphFBu5r0
    activityId:21
*/

    const val VIEW_FLEXI_SIMILAR_ACTIVITIES =
        BuildConfig.BASE_URL + "other-similar-flexi-activity-v2"

    const val FLEXI_BOOKING = BuildConfig.BASE_URL + "customer-flexi-booking"
/*
    providerId:35
    activityId:74
    familyId:139
    device:android
    pricePayable:0
    sessionJson:[{"flexi_activity_id":"12","flexiact_price_manage_id":"26","valid_days":"45","price":"200","no_of_sessions":"20","duration":"60"}]
    totalBeforeTax:398.5
    vatRate:5.0
    vatAmount:19.93
    orderId:1556801724473
    activityFlexiId:7
    flexiact_price_manage_id:15
*/

    const val CHECK_CLASH = BuildConfig.BASE_URL + "check-clashing-activity"
    /*activityId:66
    familyId:209
    SessionsJson:[{"enrolment_slot_id":"2465","enrolment_slot_date":"2019-07-07","enrolment_slot_time":"9:30 - 10:00","enrolment_slot_week":"Sunday","enrolment_slot_duration":"60"},{"enrolment_slot_id":"2466","enrolment_slot_date":"2019-07-28","enrolment_slot_time":"13:00 - 14:00","enrolment_slot_week":"Sunday","enrolment_slot_duration":"60"}]*/


    const val VIEW_PROMOTION = BuildConfig.BASE_URL + "view-promotion"

    const val CHECK_PROMOTION = BuildConfig.BASE_URL + "check-promotion"
    // promotionId:  1


    const val VIEW_NOTIFICATION = BuildConfig.BASE_URL + "view-notification"

    const val APP_SUPPORT = BuildConfig.BASE_URL + "app-support"
    // http://pursueit.v2rsolution.in/api/app-support

    const val VIEW_SERVICE_PROVIDER = BuildConfig.BASE_URL + "view-service-provider-details"
    // http://pursueit.v2rsolution.in/api/view-service-provider-details

    const val ADD_MONEY = BuildConfig.BASE_URL + "add-money-to-wallet"
    // http://pursueit.v2rsolution.in/api/add-money-to-wallet
    /* enteredAmount:150
    order_id:1001141*/

    const val GET_SCHOOL_AND_INTEREST = BuildConfig.BASE_URL + "school-interest-list"
    // http://pursueit.v2rsolution.in/api/school-interest-list


    const val UPDATE_CUSTOMER_WALLET = BuildConfig.BASE_URL + "update-customer-pass-wallet"
    //http://pursueit.v2rsolution.in/api/update-customer-pass-wallet
    // orderId:123456

    const val PASS_TRANSACTION = BuildConfig.BASE_URL + "view-customer-pass-transaction"

    const val SEND_OTP = BuildConfig.BASE_URL + "send-otp"
    //http://pursueit.v2rsolution.in/api/send-otp
    //mobileNumber

    const val VIEW_PROVIDER_REVIEW = BuildConfig.BASE_URL + "view-provider-review"

    //http://pursueit.v2rsolution.in/api/view-provider-review
    //parameter: providerId
    const val ONLINE_CLASSES = BuildConfig.BASE_URL + "get-online-activities"
}


package com.pursueit.model

import java.io.Serializable

data class ActivityFlexiSessionModel(val flexiPriceManageId:String,val numberOfSession:String, val ageRange:String, val price:String
                                     , val duration:String, val location:String, val sessionType:String,val fullAddress:String
    ,val latitude:String, val longitude:String, val validDays:String, val fromAge:String, val toAge:String, val priceRoundedToDisplay:Int=0 /*rounded price display only, not for calculation*/):Serializable
{
    var intDuration=""
}
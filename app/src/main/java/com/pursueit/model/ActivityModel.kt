package com.pursueit.model

import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

data class ActivityModel(var activityId: String = "", var activityName: String = "", var activityImage: Int = 0, var activityImageUrl: String = ""):Serializable {


    var isPassActivity:Boolean=false

    var activityType=-1 //refer to companion object in this class

    var activityProviderId = ""
    var activityProviderName = ""
    var activityProviderPhone = ""
    var activityProviderAboutUs=""
    var activityProviderLogo=""

    var activityLocation = ""
    var activityLocationFullAddress = ""
    var activityStarRating = 0.0F
    var activityStarRatingCount = 0
    var arraySlotModel = ArrayList<ActivitySlotModel>()

    var activityDesc=""

    var arrayImages=ArrayList<String>()

    var activityAdditionalInfo:String=""
    var activityOtherInfo:String=""
    var activityCancellationPolicy:String=""

    var isReviewGiven=false

    var activityMsgLocationSuffix=""

    var activityCancellationHours=48

    var isActivityBookingCancellable=true

    var curServerDateJava: Date?=null

    var isFavoriteMarked:Boolean=false

    var categoryId=""


    var lat=0.0
    var lng=0.0

    var slotPricePayableForMap=""
    var slotDisplayPriceForMap=""
    var slotAge=""
    var startDate=""
    var slotTime=""
    var isBooking=false
    var slotDistance=0.0
    var slotStatusText=""
    var slotIndex=-1

    var arrFlexiSession=ArrayList<ActivityFlexiSessionModel>()

    //row wise
    var arrFlexiTimeTable=ArrayList<ArrayList<FlexiTimeTableModel?>>()
    //var maxColumnTimeTable=0

    var activityFlexiId=""
    var flexiStartingPrice=""
    var flexiStartingSessionNumber=""

    var arrPromotionModel=ArrayList<PromotionModel>()
    var maxValuePromotionModel:PromotionModel?=null

    var similarFlexiModel:ActivityFlexiSessionModel?=null
    var flexiIndex=-1

companion object{
    const val FLEXI_ACTIVITY=1
    const val NORMAL_ACTIVITY=0
    const val SINGLE_ACTIVITY=2
}
}

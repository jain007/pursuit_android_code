package com.pursueit.model

import java.io.Serializable
import java.util.*

data class ActivitySlotModel(var slotId: String = "", var slotTitle: String = "", var slotAge: String = "", var slotTime: String = "",var slotPrice:String="", var slotStatus:String="",var slotDay:String="", var slotDate:String="",var slotLocation:String=""):Serializable{
    var slotType:String="-" //group or single

    var slotActivityId:String=""

    //in available slots, we sort by this date
    var slotNextAvailableSessionDate:Date?=null

    //in enquire, we sort by this integer value (this is week number, which is useless)
    var slotEnquireSortParam=0

    var slotEnquireStartDateTime:Date?=null

    //distance
    var slotDistance=0.0

    var arrayEnrolModel=ArrayList<EnrolModel>()
    var noOfSessions=0
    var noOfEligibleSessions=0


    var slotLat=0.0
    var slotLong=0.0

    var slotEnrolStartDate:String=""
    var slotEnrolEndDate:String=""

    var slotEnrolmentId:String=""

    var slotPricePayable:String=""
    var slotPricePayableToDisplay:String=""

    var slotAgeFrom=0.0
    var slotAgeTo=0.0

    var slotVenuePlaceId=""

    var slotLocationFullAddress=""

    var isSingleActivity=false
}
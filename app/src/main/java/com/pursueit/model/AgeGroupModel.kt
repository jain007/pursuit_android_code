package com.pursueit.model


data class AgeGroupModel(var groupId: String = "", var groupTitle: String = "", var groupAgeFrom: String = "", var groupAgeTo: String = "") {
    var groupIsSelected=false
    var groupLabel=""
}
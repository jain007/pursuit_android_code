package com.pursueit.model

data class AppIntroModel(
    var imageRes:Int?=null,
    var title:String?=null
)
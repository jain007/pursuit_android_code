package com.pursueit.model

class CampDiscountModel(val discountPercent:String, val discountOn:String, val flag:Int, val lastDateToApply:String)
{
    // Earlier
    // sibling discount at 0 pos
    // early reg discount at 1st pos

    companion object{
        val SIBLING_DISCOUNT=1
        val EARLY_REG_DISCOUNT=2
    }
}
package com.pursueit.model

data class CampModel(val campId:String, val campName:String, val campImage:String)
{
    var isFavoriteMarked:Boolean=false

    var serviceProviderName:String=""
    var address:String=""
    var venueFullAddress:String=""
    var startingPrice=""
    var isDiscountAvailable=false
    var isTransportAvailable=false
    var ageRageData=""
    var timeRangeData=""
    var arrTagList=ArrayList<String>()

    var arrayImages= java.util.ArrayList<String>()
    var venueLat=""
    var venueLng=""

    var arrWeekModel=ArrayList<WeekModel>()
    var arrDayOrWeekPriceModel=ArrayList<DayOrWeekPriceModel>()
    var arrDiscountModel=ArrayList<CampDiscountModel>()

    var additionalInfo=""
    var providerLogo=""
    var providerDescription=""
    var providerId=""

    var currentDate="" // "dd MMM yyyy"
    var isReviewGiven=false
    var ageFrom=""
    var ageTo=""

    var campAbout=""

    var arrPromotionModel=ArrayList<PromotionModel>()
    var maxValuePromotionModel:PromotionModel?=null
}
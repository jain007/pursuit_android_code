package com.pursueit.model

import android.graphics.drawable.Drawable
import java.io.Serializable


data class CategoryModel(
    var catId: String = "",
    var catName: String = "",
    var catImageUrl: String = "",
    var activityId: String = "",
    var catImageDrawable: Drawable? = null
) : Serializable {
    var isSubCategory = false
    val subCategories = ArrayList<SubCategoryModel>()
    var subCatId = ""
    var subCatName = ""
}

data class SubCategoryModel(var subCatId: String, var subCatName: String) : Serializable
package com.pursueit.model

data class DayOrWeekPriceModel(val id:String, val sessionName:String, val price:String, val maxWeekSelection:Int, val scheduleFlag:String)
package com.pursueit.model

import java.io.Serializable
import java.util.*

data class EnrolModel(var enrolId:String=""):Serializable{
    var enrolDay:String=""
    var enrolTime:String=""
    var enrolDate:String=""
    var enrolDuration:String=""

    var enrolDateJava: Date?=null

    var isDateAlreadyPassed=false //if true grey out those items, and these items will not be included in billing/payment
}
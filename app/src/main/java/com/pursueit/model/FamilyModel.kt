package com.pursueit.model

import android.util.Log
import com.pursueit.utils.JsonParse
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList

data class FamilyModel(var familyId: String = "", var familyMemberName: String = "", var familyDob: String = "", var familySchoolName: String = "", var familyGender: String = "", var familyInterests: String = "", var familyPrefLocation: String = "") {
    var isSelected: Boolean = false
    var familyMemberAge = ""
    var familyMemberAgeDouble = 0.0

    var familySchoolId:String=""
    //    var arrInterestModel=ArrayList<InterestModel>()
    var familyInterestId:String=""
    /*
    */

    fun calculateAndSetAge() {
        val dateDob = JsonParse.getFormattedServerDateJava(familyDob, "yyyy-MM-dd")
        if (dateDob != null) {
            val dateToday = Date()
            val timeBetween = dateToday.time - dateDob.time
            val yearsBetween = timeBetween / 3.15576e+10

            val ageInt = Math.floor(yearsBetween).toInt()
            val dFormat = DecimalFormat("###.#")
            familyMemberAgeDouble = dFormat.format(yearsBetween).toDouble()
            Log.d("Separator","---------------------------------------------------------")
            Log.d("Age_Member",""+familyMemberName)

            if (familyMemberAgeDouble >= 5) {
                familyMemberAge = "$ageInt Years"
                familyMemberAgeDouble=ageInt.toDouble()
                Log.d("Age_Decimal_Above_5", "" + familyMemberAgeDouble)
            } else {
                //Log.d("Age___OneLiner",""+0.5*(Math.floor(familyMemberAgeDouble/0.5)))
                Log.d("Numerator",""+Math.floor(familyMemberAgeDouble * 2))
                familyMemberAgeDouble=Math.floor(familyMemberAgeDouble * 2) / 2.0
                Log.d("Age_Decimal_Below_5",""+familyMemberAgeDouble)
                familyMemberAge = dFormat.format(familyMemberAgeDouble) +" Years"
            }
            Log.d("Age_Final", "" + familyMemberAge)
        }
    }
}
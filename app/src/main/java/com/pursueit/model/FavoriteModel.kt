package com.pursueit.model

data class FavoriteModel(val campModel: CampModel?=null, val activityModel:ActivityModel?=null)
package com.pursueit.model


data class FeaturedPartnerModel(var partnerId: String = "", var partnerName: String = "", var partnerImage: Int = 0, var partnerImageUrl: String = "")
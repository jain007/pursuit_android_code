package com.pursueit.model

data class FilterDayModel(val id:String, val dayName:String, var status:Boolean=false)
package com.pursueit.model

import java.io.Serializable

data class FlexiTimeTableModel(val startTime:String, val endTime:String, val dayName:String, val dayNumber:Int):Serializable
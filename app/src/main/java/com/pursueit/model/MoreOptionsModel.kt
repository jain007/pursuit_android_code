package com.pursueit.model

import com.pursueit.R


data class MoreOptionsModel(var optionTitle:String="",var optionSubTitle:String="",var optionImage:Int=0, var tintColor:Int= R.color.colorTextGreySubTitle){

}
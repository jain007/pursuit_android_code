package com.pursueit.model

import android.app.Activity
import android.view.View
import com.pursueit.dialogs.DialogMsg
import java.io.Serializable
import java.util.*

data class MyBookingsModel(var bookingId: String = "", var bookingTitle: String = "", var bookingSubTitle: String = "", var bookingImage: Int = 0, var bookingImageUrl: String = "", var bookingStatus: String = "", var bookingLocation:String="",var bookingTime:String=""):Serializable {
    var bookingMsgTime:String=""
    var bookingLat:Double=0.0
    var bookingLng:Double=0.0
    var activityId=""
    var timeSlotId=""
    var enrolmentId=""

    var actCategoryId=""

    var sessionJson=""

    var bookingDate=""

    /*var bookingAmtBeforeTax=""
    var bookingVatAmount=""*/
    var bookingAmtAfterTax=""

    var bookingThresholdDate: Date?=null

    var campId=""
    //val arrayEnrolModel=ArrayList<EnrolModel>()

    var bookingType=""
    // activity, camp

    var campWeekId=""

    var isPassAct:Boolean=false

    var bookingCancelledBy=""

    var cancelReason=""

    //var bookingFullAddress=""


    companion object{
        val BOOKING_TYPE_ACTIVITY="activity"
        val BOOKING_TYPE_CAMP="camp"
        val BOOKING_TYPE_FLEXI_ACTIVITY="flexi activity"

        val CANCLLED_BY_USER="customer"
        val CANCELLED_BY_SERVICE_PROVIDER="provider"
        val CANCELLED_BY_ADMIN="admin"

        fun getCancellationMessage(cancelledBy:String):String{
            return when(cancelledBy){
                CANCELLED_BY_ADMIN->{
                    "Booking Cancelled by Admin"
                }
                CANCELLED_BY_SERVICE_PROVIDER->{
                    "Booking Cancelled by Service Provider"
                }
                else->{ //CANCLLED_BY_USER
                    "Booking Cancelled"
                }
            }
        }

        fun showCancelReason(act:Activity, cancelReason:String,dialogMsg: DialogMsg){
            if (cancelReason.trim().isEmpty())
                return
                dialogMsg?.showErrorRetryDialog(act, "Cancel Reason", cancelReason, "OK", View.OnClickListener {
                    dialogMsg?.dismissDialog(act)
                })
        }
    }

}
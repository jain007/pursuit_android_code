package com.pursueit.model

import java.io.Serializable

data class NotificationModel(val moduleId:String, val title:String, val message:String,  val notificationType:String,
                             val enrollmentId:String, val timeSlotId:String, val moduleName:String, val moduleImageUrl:String
                            ,val bookingsModel: MyBookingsModel?):Serializable
{
    var dateTime:String=""
    var notificationId=""

    companion object{
        // Notification type constants

        const val TYPE_REVIEW_ACTIVITY="activity"
        const val TYPE_REVIEW_FLEXI_ACTIVITY="flexi activity"
        const val TYPE_REVIEW_CAMP="camp"

        // for single and term booking activity
        const val TYPE_ACTIVITY_BOOKING_REMIDER="booking activity"
        const val TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER="booking flexi activity"

        const val TYPE_ACTIVITY_ACCEPTED="booking activity accepted"
        const val TYPE_ACTIVITY_REJECTED="booking activity declined"
        const val TYPE_CAMP_ACCEPTED="booking camp accepted"
        const val TYPE_CAMP_REJECTED="booking camp declined"
        const val TYPE_FLEXI_ACCEPTED="booking flexi accepted"
        const val TYPE_FLEXI_REJECTED="booking flexi declined"

        const val TYPE_ACTIVITY_CANCELLED="booking activity cancelled"
        const val TYPE_FLEXI_CANCELLED="booking flexi cancelled"
        const val TYPE_CAMP_CANCELLED="booking camp cancelled"

        /*1. booking activity accepted
        2. booking activity declined
        3. booking camp accepted
        4. booking camp declined
        5. booking flexi accepted
        6. booking flexi declined*/


        // channel names
        const val CHANNEL_BOOKING_REMINDER_NOTIFICATION="Booking Reminder"
        const val CHANNEL_REVIEW_REMINDER_NOTIFICATION="Review Reminder"
        const val CHANNEL_BOOKING_STATUS_NOTIFICATION="Booking Status"
        val CHANNEL_DEFAULT="Pursue It"
    }
}
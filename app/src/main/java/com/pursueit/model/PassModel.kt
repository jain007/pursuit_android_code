package com.pursueit.model


data class PassModel(var passId: String = "", var passName: String = "", var passImageUrl: String = "", var passCatId:String, var passSubCatId:String)
{
    var arrPromotionModel=ArrayList<PromotionModel>()
    var maxValuePromotionModel:PromotionModel?=null
}
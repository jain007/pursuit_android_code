package com.pursueit.model


data class PaymentTransactionModel(var transactionId:String=""){
    var transactionOrderId:String=""
    var transactionStatus=""
    var transactionAmount=""
    var transactionDate=""
    var transactionTrackingId=""

    var transType=""

}
package com.pursueit.model


data class PlaceModel(var placeId: String = "", var placeTitle: String = "", var placeAddress: String = "", var placeLat: Double = 0.0, var placeLng: Double = 0.0,var placeCityId:String=""){
    var placeAddressIdDb=""
}
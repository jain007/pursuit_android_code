package com.pursueit.model

import java.io.Serializable

data class PromotionModel(var promotionId: String = "", val promotionValidOn:String, var catId:String, var activityId:String, var campId:String
    , val promoCode:String, val offerType:String, val offerValue:String, val promotionAvailableFor:String, val promotionMinimumOrderAmount:String,
                          val upToMaxAmount:String, val promotionImage:String, val isUsableWithOther:Boolean):Serializable
{
    var isApplied:Boolean=false
    var discountAmount:Double=0.0
    var subCateId:String=""
    var promotionCreatedBy=""
    var failedToApplyPromotion=false

    var campPromotionCount=""
    var onlyOneCampId=""
/*
    Promotion_offer_type = 1 = Flat Rate 2- percentage
    Promotion_available_for = 1- All 2- Specific customer
    Promotion_with_other = 1 - YEs 2 - No
*/
    companion object{
        const val VALID_ON_CATEGORY="1"
        const val VALID_ON_ACTIVITY="2"
        const val VALID_ON_CAMP="3"
        const val VALID_ON_REVIEW="4"

        const val OFFER_TYPE_FLAT="1"
        const val OFFER_TYPE_PERCENTAGE="2"

        const val AVAILABLE_FOR_ALL="1"
        const val AVAILABLE_FOR_SPECIFIC_CUSTOMER="2"

        /*const val PURSUEIT_PROMOTION="1"
        const val SERVICE_PROVIDER_PROMOTION="2"*/

    const val CREATED_BY_SERVICE_PROVIDER="1"
    const val CREATED_BY_ADMIN="2"

    }
}
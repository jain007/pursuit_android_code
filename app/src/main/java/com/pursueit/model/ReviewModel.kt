package com.pursueit.model

import android.util.Log
import com.pursueit.utils.ChangeDateFormat
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


data class ReviewModel(var reviewId: String = ""):Serializable{
    var reviewDesc: String = ""
    var reviewUserName: String = ""
    var reviewUserImageUrl: String = ""
    var reviewTimeAgo: String = ""
    var reviewRating: Float = 0f
    var reviewUserId=""
    var isUserSocialImage=false
    var reviewCampId=""
    var reviewActivityId = ""

    fun covertTimeToText(dataDate: String): String? {

        var convTime: String? = null

        val prefix = ""
        val suffix = "ago"

        try {
            /*val dateFormat = SimpleDateFormat("")
            val targetFormat = SimpleDateFormat("")*/

            convTime = ChangeDateFormat.convertDate(dataDate,"yyyy-MM-dd HH:mm:ss","dd MMM yyyy HH:mm")

            /*val nowTime = Date()

            val dateDiff = nowTime.time - pasTime.time

            val second = TimeUnit.MILLISECONDS.toSeconds(dateDiff)
            val minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff)
            val hour = TimeUnit.MILLISECONDS.toHours(dateDiff)
            val day = TimeUnit.MILLISECONDS.toDays(dateDiff)

            if (second < 60) {
                convTime = "$second Seconds $suffix"
            } else if (minute < 60) {
                convTime = "$minute Minutes $suffix"
            } else if (hour < 24) {
                convTime = "$hour Hours $suffix"
            } else if (day >= 7) {
                *//*if (day > 30) {
                    convTime = "${day / 30} Months $suffix"
                } else if (day > 360) {
                    convTime =  "${day / 360} Years $suffix"
                } else {
                    convTime = "${day / 7} Week $suffix"
                }*//*
            } else if (day < 7) {
                convTime = "$day Days $suffix"
            }*/

        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("ConvTimeE", ""+e)
        }

        return convTime

    }
}
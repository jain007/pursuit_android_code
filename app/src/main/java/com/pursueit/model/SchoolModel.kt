package com.pursueit.model

data class SchoolModel(val schoolId:String, val schoolName:String)
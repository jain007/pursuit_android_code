package com.pursueit.model


data class SearchModel(var name:String=""){
    var flag=1 //1-> search result is Category, 2-> search result is sub category, 3-> search result is activity

    var title=""

    var catId:String=""
    var subCatId:String=""
    var activityId:String=""
    var isPassAct:Boolean=false

    override fun toString(): String {
        return "Name: $name Title: $title CatId: $catId SubCatId: $subCatId ActivityId: $activityId isPassAct: $isPassAct"
    }
}
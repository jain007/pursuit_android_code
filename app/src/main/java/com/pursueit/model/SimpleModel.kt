package com.pursueit.model

data class SimpleModel (val id:String, val data:String, var isSelected:Boolean=false)
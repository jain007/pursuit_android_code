package com.pursueit.model

data class SortModel(val id:String, val name:String, var status:Boolean=false)
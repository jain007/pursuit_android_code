package com.pursueit.model

import android.content.Context
import android.util.Log
import com.pursueit.utils.ChangeDateFormat
import com.pursueit.utils.GetSetSharedPrefs
import com.pursueit.utils.getValidString
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.runOnUiThread
import org.json.JSONObject
import java.text.DecimalFormat


data class UserModel(var userId: String = "") {


    var userEmail: String = ""
    var userHasPassword: Boolean = true
    var userGender: String = "" //male female
    var userFNAme: String = ""
    var userLName: String = ""
    var userEmailVerified: Boolean = true
    var userRegType: String = "normal" //normal  facebook  google
    var isUserActive: Boolean = true //in API 0->False, 1->True
    var userPhone: String = ""
    var userPhoneVerified = false
    var userSocialId = ""
    var userSocialImageUrl = ""
    var userImage = ""
    var userFcmToken = ""

    var currentPassWallet=""
    var passExpiryDays=""
    var passExpiryDate=""
    var universalPassExpiry=""

    var isPhoneVerified=false

    var lastNotificationId=""

    override fun toString(): String {
        return "User: id-$userId UserName-$userFNAme $userLName email-$userEmail phone-$userPhone"
    }


    companion object {
        private const val KEY_USER_EMAIL = "UserEmail"
        private const val KEY_USER_PASS = "UserPass"
        private const val KEY_USER_ID = "UserId"
        private const val KEY_USER_GENDER = "UserGender"
        private const val KEY_USER_FIRST_NAME = "UserFName"
        private const val KEY_USER_LAST_NAME = "UserLName"
        private const val KEY_USER_EMAIL_VERIFIED = "UserEmailVerified"
        private const val KEY_USER_REG_TYPE = "UserRegType"
        private const val KEY_USER_ACTIVE = "UserActive"
        private const val KEY_USER_PHONE = "UserPhone"
        private const val KEY_USER_PHONE_VERIFIED = "UserPhoneVerified"
        private const val KEY_SOCIAL_ID = "UserSocialId"
        private const val KEY_SOCIAL_IMAGE_URL = "UserSocialImageUrl"
        private const val KEY_USER_IMAGE = "UserImage"
        private const val KEY_USER_FCM_TOKEN = "UserFcmToken"
        private const val KEY_LAST_NOTIFICATION_ID="lastNotificationId"

        private const val KEY_CURRENT_PASS_WALLET="currentPassWallet"
        private const val KEY_EXPIRY_DAYS="passExpiryDays"
        private const val KEY_EXPIRY_DATE="passExpiryDate"
        private const val KEY_UNIVERSAL_PASS_EXPIRY="universalPassExpiry"


        fun setLastNotificationId(context: Context,notificationId:String)
        {
            val prefs = GetSetSharedPrefs(context)
            prefs.putData(KEY_LAST_NOTIFICATION_ID,notificationId)
        }

        fun getLastNotificationId(context: Context):String
        {
            val prefs = GetSetSharedPrefs(context)
            val notificationId=prefs.getData(KEY_LAST_NOTIFICATION_ID)
            return if (notificationId.trim()=="") "0" else notificationId
        }

        fun setUserModel(context: Context, model: UserModel) {
            val prefs = GetSetSharedPrefs(context)
            Log.d("UserSet_UserId","_"+model.userId)
            prefs.putData(KEY_USER_ID, model.userId)
            prefs.putData(KEY_USER_EMAIL, model.userEmail)
            prefs.putDataBoolean(KEY_USER_PASS, model.userHasPassword)
            prefs.putData(KEY_USER_GENDER, model.userGender)
            prefs.putData(KEY_USER_FIRST_NAME, model.userFNAme)
            prefs.putData(KEY_USER_LAST_NAME, model.userLName)
            prefs.putDataBoolean(KEY_USER_EMAIL_VERIFIED, model.userEmailVerified)
            prefs.putData(KEY_USER_REG_TYPE, model.userRegType)
            prefs.putDataBoolean(KEY_USER_ACTIVE, model.isUserActive)
            prefs.putData(KEY_USER_PHONE, model.userPhone)
            prefs.putDataBoolean(KEY_USER_PHONE_VERIFIED, model.userPhoneVerified)
            prefs.putData(KEY_SOCIAL_ID, model.userSocialId)
            prefs.putData(KEY_SOCIAL_IMAGE_URL, model.userSocialImageUrl)
            prefs.putData(KEY_USER_IMAGE, model.userImage)
            prefs.putData(KEY_USER_FCM_TOKEN, model.userFcmToken)

            prefs.putData(KEY_UNIVERSAL_PASS_EXPIRY,model.universalPassExpiry)
            prefs.putData(KEY_CURRENT_PASS_WALLET,model.currentPassWallet)
            prefs.putData(KEY_EXPIRY_DAYS,model.passExpiryDays)
            prefs.putData(KEY_EXPIRY_DATE,model.passExpiryDate)
        }

        fun getUserModel(context: Context): UserModel {
            val prefs = GetSetSharedPrefs(context)
            //Log.d("UserGet_UserId","_"+prefs.getData(KEY_USER_ID))
            return UserModel(prefs.getData(KEY_USER_ID)).also {
                it.userEmail = prefs.getData(KEY_USER_EMAIL)
                it.userHasPassword = prefs.getDataBoolean(KEY_USER_PASS)
                it.userGender = prefs.getData(KEY_USER_GENDER)
                it.userFNAme = prefs.getData(KEY_USER_FIRST_NAME)
                it.userLName = prefs.getData(KEY_USER_LAST_NAME)
                it.userEmailVerified = prefs.getDataBoolean(KEY_USER_EMAIL_VERIFIED)
                it.userRegType = prefs.getData(KEY_USER_REG_TYPE)
                it.isUserActive = prefs.getDataBoolean(KEY_USER_ACTIVE)
                it.userPhone = prefs.getData(KEY_USER_PHONE)
                it.userPhoneVerified = prefs.getDataBoolean(KEY_USER_PHONE_VERIFIED)
                it.userSocialId = prefs.getData(KEY_SOCIAL_ID)
                it.userSocialImageUrl = prefs.getData(KEY_SOCIAL_IMAGE_URL)
                it.userImage = prefs.getData(KEY_USER_IMAGE)
                it.userFcmToken = prefs.getData(KEY_USER_FCM_TOKEN)

                it.universalPassExpiry=prefs.getData(KEY_UNIVERSAL_PASS_EXPIRY)
                it.currentPassWallet =prefs.getData(KEY_CURRENT_PASS_WALLET)
                it.passExpiryDays=prefs.getData(KEY_EXPIRY_DAYS)
                it.passExpiryDate=prefs.getData(KEY_EXPIRY_DATE)
            }
        }


        fun parseUserJson(context: Context, json: JSONObject, userSuccessListener: (userModel: UserModel) -> Unit, exceptionListener: (e: Exception) -> Unit, saveUserModelToLocal: Boolean = true) {
            doAsync {
                try {
                    val jsonData = json.getJSONObject("data")
                    val userModel = UserModel()
                    userModel.userId = jsonData.optString("customer_id", "")
                    userModel.userFNAme = if (jsonData.optString("customer_fname", "") == "null") "" else jsonData.optString("customer_fname", "")
                    userModel.userLName = if (jsonData.optString("customer_lname", "") == "null") "" else jsonData.optString("customer_lname", "")
                    userModel.userEmail = if (jsonData.optString("email", "") == "null") "" else jsonData.optString("email", "")
                    userModel.userHasPassword = jsonData.optString("password", "0") == "1"
                    userModel.userEmailVerified = jsonData.optString("is_email_verified", "0") == "1"
                    userModel.userPhone = if (jsonData.optString("customer_phone_number", "") == "null") "" else jsonData.optString("customer_phone_number", "")
                    userModel.userRegType = jsonData.optString("customer_reg_type", "normal")
                    userModel.isUserActive = jsonData.optString("customer_status", "0") == "1"
                    userModel.userImage = if (jsonData.optString("customer_image", "") == "null") "" else jsonData.optString("customer_image", "")
                    userModel.userSocialImageUrl = if (jsonData.optString("customer_image_url", "") == "null") "" else jsonData.optString("customer_image_url", "")
                    userModel.userSocialId = if (jsonData.optString("customer_social_id", "") == "null") "" else jsonData.optString("customer_social_id", "")
                    userModel.userFcmToken = jsonData.optString("customer_fcm_token", "")
                    userModel.userPhoneVerified = jsonData.optString("is_mobile_verified", "0") == "1"

                    userModel.universalPassExpiry=jsonData.getValidString("pass_expiry")
                    userModel.passExpiryDays=jsonData.optString("pass_expiry_days")
                    userModel.passExpiryDate=ChangeDateFormat.convertDate(jsonData.optString("pass_expiry_date"),"yyyy-MM-dd","dd MMM yyyy")

                    try {
                        userModel.currentPassWallet = DecimalFormat("##.##").format(jsonData.optString("pass_wallet","0").toDouble())
                    }catch (e:Exception){
                        userModel.currentPassWallet = jsonData.optString("pass_wallet")
                    }
                    if (saveUserModelToLocal)
                        setUserModel(context, userModel)

                    context.runOnUiThread {
                        userSuccessListener(userModel)
                    }
                } catch (e: Exception) {
                    Log.d("ExcUserParse", "" + e)
                    context.runOnUiThread {
                        exceptionListener(e)
                    }
                }
            }
        }

    }
}
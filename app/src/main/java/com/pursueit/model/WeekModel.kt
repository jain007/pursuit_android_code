package com.pursueit.model

data class WeekModel (val id:String, val weekName:String, val startDate:String, val endDate:String, val remainingSeats:Int)
{
    var isSelected=false
}
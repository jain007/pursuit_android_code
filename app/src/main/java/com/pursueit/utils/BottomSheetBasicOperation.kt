package com.pursueit.utils

import android.app.Activity
import android.support.design.widget.BottomSheetBehavior
import android.view.View

class BottomSheetBasicOperation(val activity: Activity, val view: View, val blackTransparentView: View)
{
    var bottomSheetBehavior: BottomSheetBehavior<View> = BottomSheetBehavior.from(view)

    init {
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {

            }

            override fun onStateChanged(p0: View, newState: Int) {
                when(newState)
                {
                    BottomSheetBehavior.STATE_EXPANDED->{
                        blackTransparentView.visibility=View.VISIBLE
                    }

                    BottomSheetBehavior.STATE_COLLAPSED->{
                        blackTransparentView.visibility=View.GONE
                    }
                }
            }

        })
    }

    private fun setCloseListener()
    {
        /*activity.imgClose.setOnClickListener {
            toggleBottomSheet()
            // Check if no view has focus:
            val view = activity.currentFocus
            if (view != null) {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }*/
    }

   /* fun setupCloseAndResetListener(vararg arrEditText:EditText)
    {
        setCloseListener()
        activity.txtReset.setOnClickListener {
            for (i in 0 until arrEditText.size)
            {
                arrEditText[i].setText("")
                arrEditText[i].tag=""

                if (arrEditText[i].id== com.academiceye.R.id.etSection)
                {
                    arrEditText[i].setOnClickListener {  }
                }
            }
        }
    }*/

    fun toggleBottomSheet()
    {
        if (bottomSheetBehavior.state==BottomSheetBehavior.STATE_EXPANDED)
        {
            bottomSheetBehavior.state=BottomSheetBehavior.STATE_COLLAPSED
        }
        else
        {
            bottomSheetBehavior.state=BottomSheetBehavior.STATE_EXPANDED
        }
    }
}
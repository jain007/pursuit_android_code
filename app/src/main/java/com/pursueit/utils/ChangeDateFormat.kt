package com.pursueit.utils

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

object ChangeDateFormat
{
    fun convertDate(strSourceDate:String, strSourceDateFormat:String, strTargetDateFormat:String):String
    {
        try {
            val sourceDateFormat=SimpleDateFormat(strSourceDateFormat)
            val sourceDate=sourceDateFormat.parse(strSourceDate)
            val targetDateFormat=SimpleDateFormat(strTargetDateFormat)
            return targetDateFormat.format(sourceDate)
        }catch (e:Exception)
        {
            Log.e("convertDate,UnparsDt",e.toString())
        }
       return ""
    }

    fun getDateFromString(strDate:String,strDateFormat:String):Date?{
        try{
            val dateFormat=SimpleDateFormat(strDateFormat, Locale.ENGLISH)
            return dateFormat.parse(strDate)
        }catch (e:Exception){

        }
        return null
    }

    fun getCurrentDate():String
    {
        try {
            val date=Calendar.getInstance().time
            val simpleDateFormat=SimpleDateFormat("dd-MM-yyyy")
            return simpleDateFormat.format(date)
        }catch (e:Exception)
        {
            return ""
        }
    }

    fun getDateObjFromString(date:String, currentFormat:String):Date?
    {
        try {
            val simpleDateFormat=SimpleDateFormat(currentFormat)
            return simpleDateFormat.parse(date)
        }catch (e:Exception)
        {
            return null
        }

    }

    private fun convertDateIntoString(date:Date, targetFormat:String):String
    {
        return try {
            val simpleDateFormt=SimpleDateFormat(targetFormat)
            simpleDateFormt.format(date)
        }catch (e:Exception) {
            ""
        }
    }

    fun getDateAfterGivenDays(date:String,dateFormat:String, daysAfter:Int, outputDateFormat:String):String
    {
        return try {
            val strBookingDate=convertDate(date,dateFormat,"dd MMM yyyy")
            val dateObjOfBooking=getDateObjFromString(strBookingDate,"dd MMM yyyy")
            val cal=Calendar.getInstance()
            cal.time=dateObjOfBooking
            cal.add(Calendar.DAY_OF_MONTH,daysAfter)
            val expiryDate=cal.time

            convertDateIntoString(expiryDate,outputDateFormat)
            //txtValidityInfo.text="Session will expire on $strExpiryDate"
        }catch (e:Exception) {
            Log.e("parseExcep",e.toString())
            ""
        }
    }
}
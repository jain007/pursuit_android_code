package com.pursueit.utils

import java.lang.Exception
import java.text.DecimalFormat

object ConvertToValidData
{
    fun removeZeroAfterDecimal(numericData: String):String
    {
        try {
            val data=DecimalFormat("##.##").format(numericData.toDouble())
            return data
        }catch (e:Exception){
            return ""
        }
    }
}
package com.pursueit.utils

import android.graphics.Rect
import android.text.method.TransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import java.lang.Exception


class DoNothingTransformationMethod:TransformationMethod{
    override fun onFocusChanged(view: View?, sourceText: CharSequence?, focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        try{
            val editTextView=view as EditText
            editTextView.setSelection(sourceText!!.length)
        }catch (e:Exception){
            Log.d("ExcTransformation",""+e)
        }
    }

    override fun getTransformation(source: CharSequence?, view: View?): CharSequence? {
        return source
    }

}
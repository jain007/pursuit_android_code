package com.pursueit.utils


object ErrorMsgs{
    var ERR_AUTH_LOGIN_REGISTER_TITLE="Authentication Error"
    var ERR_PARSE_TITLE="Parse Error"

    var ERROR_API_TITLE="Oops!"
    var ERR_API_MSG="Some error occurred"
    var ERR_CONNECTION_MSG="Seems like you are not connected to internet! Please check your internet connection and then retry"

    var ERR_MSG_USER_DE_ACTIVE="Your account has been de-activated by admin."
}
package com.pursueit.utils

import android.util.Log
import android.view.View
import com.pursueit.R
import com.pursueit.activities.CampListingActivity
import com.pursueit.activities.DashboardActivity
import com.pursueit.dialogs.CategorySubCategoryActivitiesSearchFragment
import com.pursueit.dialogs.DialogMsg
import kotlinx.android.synthetic.main.action_layout_filter_option.view.*
import kotlinx.android.synthetic.main.action_layout_sort.view.*
import kotlinx.android.synthetic.main.content_layout_header_nearby.view.*

class ExploreToolbarInitialization(val act: CampListingActivity,val vi: View) {// ,val fragment:CampListingFragment

    // customized according to camp listing, please make changes if you want to use it somewhere else

    var viewFilter:View?=null

    fun init() {
        vi.toolbarFragment.inflateMenu(R.menu.menu_nearby_activities)
        vi.toolbarFragment.setOnMenuItemClickListener { item ->
            when (item?.itemId) {
                R.id.itemSearch -> {
                    if (act is CampListingActivity) {
                        InitFragment.initDialogFragment(act.supportFragmentManager,
                            CategorySubCategoryActivitiesSearchFragment.newInstance(true,act)
                        )
                    }
                }
            }
            return@setOnMenuItemClickListener true
        }

        decideFilterIndicator()

        vi.toolbarFragment.setOnClickListener {
            MyNavigations.goToSearchPlace(act)
        }
    }


    private fun decideFilterIndicator() {
        // decide bubble indicator for filter button inside option menu (menu_nearby_activities)
        try {
            val menuItem = vi.toolbarFragment.menu.findItem(R.id.itemFilter)
            viewFilter = menuItem.actionView
            val dialogMsg=DialogMsg()
            viewFilter!!.relFilterItem.setOnClickListener {
              //  if (act is DashboardActivity) {
               //     DashboardActivity.showFilterDialogNew(act,false, isComingFromCamp=true) //,fragment = fragment
              //  }
                dialogMsg.showNewFilterDialog(act,false, isComingFromCamp = true, filterApplied = {})
            }

            val itemSort=vi.toolbarFragment.menu.findItem(R.id.itemSort)
            itemSort.actionView.relSortItem.setOnClickListener {
                dialogMsg.showSortingDialog(act,false, isComingFromCamp = true)
            }
                decideFilterBadgeVisibility()
        } catch (e: Exception) {
            Log.d("ViewIndicatorExc", "" + e)
        }
    }

    fun decideFilterBadgeVisibility()
    {
        viewFilter!!.imgFilterIndicator.visibility =if (SearchAndFilterStaticData.isFilterApplied || SearchAndFilterStaticData.isAgeFilterApplied) View.VISIBLE else View.GONE
        //viewFilter!!.imgFilterIndicator.visibility =if (DashboardActivity.ageFrom.trim().isNotEmpty() && DashboardActivity.ageTo.trim().isNotEmpty()) View.VISIBLE else View.GONE // if (SearchAndFilterStaticData.isAgeFilterApplied) View.VISIBLE else View.GONE
    }
}
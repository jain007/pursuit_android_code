package com.pursueit.utils

import org.json.JSONObject

fun JSONObject.getValidString(key:String):String{
    var value=""
    if (has(key))
        value=getString(key)
    if (value=="null" || value=="nan")
        value=""
    return value
}
package com.pursueit.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class GetDateTime(internal var activity: Activity) {
    private var year: Int = 1990
    private var month: Int = 0
    private var day: Int = 1
    fun showDateDialog(txtChooseDate: TextView, showDate: Boolean, blockPreviousDates: Boolean, blockFutureDates: Boolean, preSelectedDate: String = "",editTextToClear:EditText?=null) {
        // Launch Date Picker Dialog

        if (preSelectedDate != "") {
            year = ChangeDateFormat.convertDate(preSelectedDate, "dd MMMM yyyy", "yyyy").toInt()
            month = ChangeDateFormat.convertDate(preSelectedDate, "dd MMMM yyyy", "MM").toInt() - 1
            day = ChangeDateFormat.convertDate(preSelectedDate, "dd MMMM yyyy", "dd").toInt()
        } else {
            val c = Calendar.getInstance()
            year = c.get(Calendar.YEAR)
            month = c.get(Calendar.MONTH)
            day = c.get(Calendar.DAY_OF_MONTH)
        }


        val dpd = DatePickerDialog(activity, AlertDialog.THEME_HOLO_LIGHT, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in textbox
            this.year = year
            this.month = monthOfYear
            this.day = dayOfMonth
            val convertedDate = ChangeDateFormat.convertDate("$dayOfMonth-${monthOfYear + 1}-$year", "dd-MM-yyyy", "dd MMMM yyyy")

            if (editTextToClear!=null)
            {
                editTextToClear.setText("")
            }

            if (showDate) {

                txtChooseDate.text = ("Date: " + convertedDate)
            } else txtChooseDate.text = convertedDate
        }, year, month, day)

        val c = Calendar.getInstance()
        year = c.get(Calendar.YEAR)
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)
        // c.set(Calendar.MONTH, month + 1)
        if (blockPreviousDates) {
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            //dpd.datePicker.maxDate = c.timeInMillis
        } else if (blockFutureDates) {
            dpd.datePicker.maxDate = c.timeInMillis
        }
        dpd.setTitle("")
        dpd.show()
    }

    fun setTime(txtTime: TextView, is24HourView: Boolean, showTime: Boolean, preTime: String = "",selectedDate: String) {

        val mcurrentTime = Calendar.getInstance()
        var hour: Int
        var minute: Int
        if (preTime != "") {
            hour = preTime.substring(0, preTime.indexOf(":")).toInt()
            minute = preTime.substring(preTime.indexOf(":") + 1, preTime.indexOf(" ")).toInt()//ConvertDate.convertDate(preTime,"hh mm a","mm").toInt()
            if (preTime.substring(preTime.length-2).equals("pm",true) && hour!=12)
                hour+=12

        } else {
            hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
            minute = mcurrentTime.get(Calendar.MINUTE)
        }

        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { timePicker, hourOfDay, selectedMinute ->

            if (isPastTime(selectedDate,hourOfDay, selectedMinute))
            {
               // activity.toast("Custom messages cannot be schedule in past time.")
                return@OnTimeSetListener
            }

            var hourOfDay = hourOfDay

            if (!is24HourView) {
                var format = ""
                if (hourOfDay == 0) {

                    hourOfDay += 12

                    format = "AM"
                } else if (hourOfDay == 12) {

                    format = "PM"

                } else if (hourOfDay > 12) {

                    hourOfDay -= 12

                    format = "PM"

                } else {

                    format = "AM"
                }
                var hour = ""
                if (hourOfDay < 10) hour = "0" + hourOfDay
                else hour = hourOfDay.toString() + ""
                var minute = ""
                if (selectedMinute < 10) minute = "0" + selectedMinute
                else minute = "" + selectedMinute
                if (showTime) txtTime.text = "Time: $hour:$minute $format"
                else txtTime.text = "$hour:$minute $format"
            } else {
                if (showTime) txtTime.text = "Time: $hourOfDay:$selectedMinute"
                else txtTime.text = hourOfDay.toString() + ":" + selectedMinute
            }
        }, hour, minute, is24HourView)//Yes 24 hour time
        // mTimePicker.setTitle("Select Time");
        mTimePicker.show()
    }

    fun isPastTime(selectedDate:String, hourOfDay:Int,minute:Int):Boolean
    {
        val dateFormat = SimpleDateFormat("dd MMMM yyyy")
        val currentDate = dateFormat.format(Calendar.getInstance().time)
        if (currentDate.equals(selectedDate)) {
            val datetime = Calendar.getInstance()
            val c = Calendar.getInstance()
            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay)
            datetime.set(Calendar.MINUTE, minute)

            Log.d("selected hour:",datetime.get(Calendar.HOUR_OF_DAY).toString())
            Log.d("selected minute:",datetime.get(Calendar.MINUTE).toString())

            Log.d("current hour:",c.get(Calendar.HOUR_OF_DAY).toString())
            Log.d("current minute:",c.get(Calendar.MINUTE).toString())

            val diff= datetime.timeInMillis - c.timeInMillis
            Log.d("diff",diff.toString())

            if (diff in 0..300000)
            {
                activity.toast("Event time should be at least 5 minutes later from now")
                return true
            }

            if (datetime.timeInMillis >= c.timeInMillis) {

                return false
            } else {
                activity.toast("Custom messages cannot be schedule in past time.")
                return true
            }
        }else
        {
            return false
        }
    }
}

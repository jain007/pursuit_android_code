package com.pursueit.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pursueit.model.SimpleModel


class GetSetSharedPrefs(val context: Context){

    companion object{
        val gson=Gson()
         fun <T> convertToJsonString(t: T): String {
            return gson.toJson(t).toString()
        }

        fun <T> convertToModel(jsonString: String, cls: Class<T>): T? {
            return try {
                gson.fromJson(jsonString, cls)
            } catch (e: Exception) {
                null
            }
        }
    }

    fun setArrayModel(key: String, arrayList: ArrayList<*>) {
        putData(key, convertToJsonString(arrayList))
    }
    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)

    fun <T> getArrayModel(key:String):ArrayList<T>{
        return try {
            //ArrayList(getArrayModel<List<SelectionModel>>(getData(ARRAY_PERMIT_TYPE)))
            gson.fromJson<ArrayList<T>>(getData(key))
        } catch (e: Exception) {
            ArrayList()
        }
    }

    fun getSimpleArrayModel(key:String):ArrayList<SimpleModel>{
        return try {
            //ArrayList(getArrayModel<List<SelectionModel>>(getData(ARRAY_PERMIT_TYPE)))
            gson.fromJson<ArrayList<SimpleModel>>(getData(key))
        } catch (e: Exception) {
            ArrayList()
        }
    }

    var prefs: SharedPreferences = context.getSharedPreferences("App_Settings", Context.MODE_PRIVATE)

    lateinit var editor: SharedPreferences.Editor

    init {
        prefs= context.getSharedPreferences("App_Settings", Context.MODE_PRIVATE)
    }

    fun putData(key: String, value: String) {
        editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }


    fun getData(key: String): String {
        return prefs.getString(key, "") ?: ""
    }

    fun putDataInt(key: String, value: Int) {
        editor = prefs.edit()
        editor.putInt(key, value)
        editor.apply()
    }


    fun getDataInt(key: String): Int {
        return prefs.getInt(key, 0)
    }

    fun putDataBoolean(key: String, value: Boolean) {
        editor = prefs.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getDataBoolean(key: String): Boolean {
        return prefs.getBoolean(key, false)
    }
}
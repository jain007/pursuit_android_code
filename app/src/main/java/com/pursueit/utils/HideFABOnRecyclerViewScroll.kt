package com.academiceye.utils

import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.view.View

class HideFABOnRecyclerViewScroll
{
    fun setListener(recyclerView:RecyclerView, fab:FloatingActionButton)
    {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy>0 && fab.visibility== View.VISIBLE)
                {
                    fab.hide()
                }
                else if (dy<0 && fab.visibility!=View.VISIBLE)
                {
                    fab.show()
                }
            }
        })
    }
}
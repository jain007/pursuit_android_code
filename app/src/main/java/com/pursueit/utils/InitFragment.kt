package com.pursueit.utils

import android.app.Activity
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import com.pursueit.R


class InitFragment(private val act: Activity, private val addOrReplace: String="add") {
    private var tagName = ""


    fun initFragment(fragment: Fragment?, addToBackStack: Boolean=true, manager: FragmentManager) {
        if (fragment != null) {

            val fragmentTransaction = manager.beginTransaction()

            if (addOrReplace.equals("add", ignoreCase = true))
                fragmentTransaction.add(R.id.frameContainer, fragment)
            else
                fragmentTransaction.replace(R.id.frameContainer, fragment)

            /* if (fragmentManager.getFragments().contains(fragment)) {
                fragmentManager.popBackStack();
            }*/

            if (addToBackStack)
                fragmentTransaction.addToBackStack(null)

            fragmentTransaction.commit()
        }
    }


    companion object {
        fun initDialogFragment(mSupportFragmentManager: FragmentManager, mDialogFragment: DialogFragment, frameId: Int = android.R.id.content) {
            val fragmentTransaction = mSupportFragmentManager.beginTransaction()
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction.add(frameId, mDialogFragment).addToBackStack(null).commit()
        }
    }


}
package com.pursueit.utils

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class ItemOffsetDecorationForLinearVertical(private val mItemOffset: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        if (parent.getChildAdapterPosition(view) == 0) { //|| parent.getChildAdapterPosition(view)==(parent.adapter?.itemCount?:0)-1
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset)
        } else {
            outRect.set(mItemOffset, 0, mItemOffset, mItemOffset)
        }
    }
}
package com.pursueit.utils

import android.util.Log
import com.pursueit.model.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

object JsonParse {

    fun getValidPassAmount(passAmount: String?): Double {
        Log.d("PassAmount__", "" + passAmount)
        val passAmountProcessed = passAmount?.trim() ?: ""
        if (passAmountProcessed != "null" && passAmountProcessed != "") {
            Log.d("PassAmountProcessed__", "" + passAmountProcessed)
            return try {
                passAmountProcessed.toDouble()
            } catch (e: Exception) {
                0.0
            }
        }
        return 0.0
    }

    val decimalFormat = DecimalFormat("######.##")

    fun parsePaymentHistory(
        json: JSONObject?,
        fromPassTransaction: Boolean
    ): ArrayList<PaymentTransactionModel> {
        val arrayPaymentTransactionModel = ArrayList<PaymentTransactionModel>()
        try {
            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                val jsonData = json.getJSONObject("data")
                val jsonDataArray = jsonData.getJSONArray("data")
                if (!fromPassTransaction) {
                    repeat(jsonDataArray.length()) { pos ->
                        val obj = jsonDataArray.getJSONObject(pos)
                        val paymentTransactionModel =
                            PaymentTransactionModel(obj.optString("trans_id")).also { model ->
                                with(model) {
                                    transactionTrackingId = obj.optString("trans_tracking_id", "")
                                    transactionOrderId = obj.optString("trans_order_id", "")
                                    transactionAmount = obj.optString("trans_amount", "")
                                    transactionStatus = obj.optString("trans_status", "")
                                    transactionDate = getFormattedDateCancellation(
                                        obj.optString(
                                            "created_at",
                                            ""
                                        )
                                    )

                                    //1->Booking 2->Refund
                                    if (obj.optString("trans_type", "1").equals("2", true)) {
                                        transactionStatus = "Refunded"
                                    }
                                }
                            }
                        arrayPaymentTransactionModel.add(paymentTransactionModel)
                    }
                } else {
                    repeat(jsonDataArray.length()) { pos ->
                        val obj = jsonDataArray.getJSONObject(pos)
                        val paymentTransactionModel =
                            PaymentTransactionModel(obj.optString("pass_trans_id")).also { model ->
                                with(model) {
                                    transactionOrderId = obj.optString("pass_trans_order_id", "")
                                    transactionAmount = obj.optString("pass_amount", "")
                                    transactionStatus = obj.optString("pass_trans_status", "")
                                    transactionDate = getFormattedDateCancellation(
                                        obj.optString(
                                            "created_at",
                                            ""
                                        )
                                    )

                                    //1 = Credit, 2 = Debit
                                    //if (obj.optString("pass_trans_type", "1").equals("2", true)) {
                                    transType = obj.optString("pass_trans_type", "1")
                                    //    }
                                }
                            }
                        arrayPaymentTransactionModel.add(paymentTransactionModel)
                    }
                }
            }
        } catch (e: Exception) {
            Log.e("Exc_PayTrans", e.toString())
        }
        return arrayPaymentTransactionModel
    }

    fun getNotificationModel(jsonArray: JSONArray): ArrayList<NotificationModel> {
        var moduleId = ""
        var moduleName = ""
        var moduleImage = ""
        var bookingModel: MyBookingsModel? = null
        val arrNotificationModel = ArrayList<NotificationModel>()
        repeat(jsonArray.length())
        {
            val jsonObject = jsonArray.getJSONObject(it)
            /*  when (jsonObject.optString("notification_type", "")) {
                  NotificationModel.TYPE_REVIEW_ACTIVITY, NotificationModel.TYPE_ACTIVITY_BOOKING_REMIDER, NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY
                      , NotificationModel.TYPE_REVIEW_CAMP, NotificationModel.TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER -> {*/
            jsonObject.getJSONObject("get_customer_booking").run {

                try {
                    this.getJSONObject("get_activity").run {
                        moduleId = optString("activity_id", "")
                        moduleName = optString("activity_name", "")
                        moduleImage = optString("activity_feature_img", "")
                    }
                } catch (e: Exception) {
                }


                bookingModel = MyBookingsModel(optString("booking_id"))
                bookingModel?.bookingThresholdDate =
                    JsonParse.getFormattedServerDateJava(
                        optString(
                            "threshold_cancellation_date",
                            ""
                        )
                    )
                bookingModel?.bookingDate = optString("booking_date", "")
                bookingModel?.bookingAmtAfterTax = optString("booking_amount", "")
                bookingModel?.campWeekId = optString("week_ids")
                bookingModel?.sessionJson = optString("booking_sessions_json")
                bookingModel?.bookingStatus = optString("booking_status")

                bookingModel?.bookingCancelledBy = optString("booking_cancel_by", "")

                bookingModel?.cancelReason = optString("cancel_reason", "")

                if (jsonObject.optString("notification_type", "")
                        .equals(NotificationModel.TYPE_REVIEW_CAMP) || jsonObject.optString(
                        "notification_type",
                        ""
                    ).contains(NotificationModel.TYPE_REVIEW_CAMP, true)
                ) {
                    try {
                        this.getJSONObject("get_camp").run {
                            moduleId = optString("camp_id", "")
                            moduleName = optString("camp_name", "")
                            moduleImage = optString("camp_feature_img", "")
                        }
                    } catch (e: Exception) {
                    }

                }
            }

            //       }

            /*NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY->{

        }*/

            /* NotificationModel.TYPE_REVIEW_CAMP->{
             jsonObject.run {
                 moduleId=optString("camp_id","")
                 moduleName=optString("camp_name","")
                 moduleImage=optString("camp_feature_img","")
             }
         }*/

            /* ->{
             jsonObject.run {
                 moduleId=optString("activity_id","")
                 moduleName=optString("activity_name","")
             }

         }*/
            //    }

            NotificationModel(
                moduleId,
                jsonObject.optString("title", ""),
                jsonObject.optString("description", "")
                ,
                jsonObject.optString("notification_type", ""),
                jsonObject.optString("enrolment_id", "")
                ,
                jsonObject.optString("time_slot_id", ""),
                moduleName,
                moduleImage,
                bookingModel
            ).also {
                it.dateTime = ChangeDateFormat.convertDate(
                    jsonObject.getString("created_at"),
                    "yyyy-MM-dd HH:mm:ss",
                    "dd MMM yyyy HH:mm"
                )
                it.notificationId = jsonObject.getString("notification_id")
            }.also {
                arrNotificationModel.add(it)
            }
        }
        return arrNotificationModel
    }

    fun parsePromotion(
        jsonPromotionArray: JSONArray,
        campOrActProviderId: String = "",
        fromHome: Boolean = false
    ): ArrayList<PromotionModel> {
        val arrPromotionModel = ArrayList<PromotionModel>()
        try {
            repeat(jsonPromotionArray.length()) {
                val jobPromotion = jsonPromotionArray.getJSONObject(it)
                jobPromotion.run {
                    val validOn = getString("promotion_valid_on")
                    var result = false

                    //Log.d("fromHome", fromHome.toString())

                    val createdBy = optString("promotion_created_by", "")
                    val providerId = optString("provider_id", "")
                    result =
                        ((validOn != PromotionModel.VALID_ON_REVIEW && createdBy != PromotionModel.CREATED_BY_SERVICE_PROVIDER) ||
                                (validOn != PromotionModel.VALID_ON_REVIEW && createdBy == PromotionModel.CREATED_BY_SERVICE_PROVIDER && (campOrActProviderId == providerId || fromHome))
                                || (validOn == PromotionModel.VALID_ON_REVIEW && (getString("get_review_details") != "null" || optString(
                            "get_camp_review_details",
                            "null"
                        ) != "null")))

                    //Log.d("cond1", (validOn != PromotionModel.VALID_ON_REVIEW && createdBy != PromotionModel.CREATED_BY_SERVICE_PROVIDER).toString())
                    //Log.d("cond2", (validOn != PromotionModel.VALID_ON_REVIEW && createdBy == PromotionModel.CREATED_BY_SERVICE_PROVIDER && (campOrActProviderId == providerId) || fromHome).toString())
                    //Log.d("cond3", (validOn == PromotionModel.VALID_ON_REVIEW && (getString("get_review_details") != "null" || getString("get_camp_review_details") != "null")).toString())

                    /*Old condition
                    validOn!=PromotionModel.VALID_ON_REVIEW ||
                            (validOn==PromotionModel.VALID_ON_REVIEW && (getString("get_review_details")!="null" || getString("get_camp_review_details")!="null"))*/

                    if (result) {

                        val isUsableWithOther = if (optString(
                                "promotion_created_by",
                                ""
                            ) == PromotionModel.CREATED_BY_SERVICE_PROVIDER
                        ) true else optString("promotion_with_other") == "1"

                        PromotionModel(
                            getString("promotion_id"),
                            getString("promotion_valid_on"),
                            optString("cat_id"),
                            optString("activity_id"),
                            optString("camp_id"),
                            optString("promotion_promo_code"),
                            optString("promotion_offer_type"),
                            optString("promotion_offer_value"),
                            optString("promotion_available_for"),
                            optString("promotion_mimimum_order_amount", "0"),
                            optString("promotion_upto_max_amount"),
                            optString("promotion_image"), isUsableWithOther
                        ).run {
                            campPromotionCount = optString("total_camp_promotion", "0")
                            onlyOneCampId = optString("camp_promotion_id")
                            if (has("total_category_promotion") && getString("total_category_promotion") == "1")
                                catId = getString("cat_promotion_id")
                            promotionCreatedBy = optString("promotion_created_by", "")
                            subCateId = optString("sub_cat_id", "")
                            this
                        }.also {
                            arrPromotionModel.add(it)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.e("Exc_ParsePromotion", e.toString())
        }
        return arrPromotionModel
    }

    fun parseCampSearchResponse(json: JSONObject?): ArrayList<SearchModel> {
        val arrayModel = ArrayList<SearchModel>()
        try {
            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                val jsonData = json.getJSONObject("data")
                val jarCamp = jsonData.getJSONArray("comp")
                repeat(jarCamp.length()) {
                    val job = jarCamp.getJSONObject(it)

                    job.run {
                        SearchModel(getString("camp_name"))
                    }.apply {
                        catId = job.getString("camp_id")
                        flag = 5   // by camp
                        title = "Camp"
                    }.also {
                        arrayModel.add(it)
                    }
                }

                val jarTags = jsonData.getJSONArray("tags")
                repeat(jarTags.length()) {
                    val job = jarTags.getJSONObject(it)
                    job.run {
                        SearchModel(getString("tag"))
                    }.apply {
                        catId = job.getString("camp_tag_id")
                        flag = 6  // by tag
                        title = "Tags"
                    }.also {
                        arrayModel.add(it)
                    }
                }
            }
        } catch (e: Exception) {

        }
        return arrayModel
    }

    fun parseSearchResponse(json: JSONObject?): ArrayList<SearchModel> {
        val arrayModel = ArrayList<SearchModel>()
        try {
            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                val jsonData = json.getJSONObject("data")

                val arrayJsonCat = jsonData.getJSONArray("category")
                repeat(arrayJsonCat.length()) { pos ->
                    val obj = arrayJsonCat.getJSONObject(pos)
                    arrayModel.add(SearchModel(obj.optString("cat_name")).also {
                        it.catId = obj.optString("cat_id", "")
                        it.flag = 1 //category
                        it.title = "Categories"
                    })
                }

                val arrayJsonSubCat = jsonData.getJSONArray("sub_category")
                repeat(arrayJsonSubCat.length()) { pos ->
                    val obj = arrayJsonSubCat.getJSONObject(pos)
                    arrayModel.add(SearchModel(obj.optString("sub_cat_name")).also {
                        it.catId = obj.optString("cat_id", "")
                        it.subCatId = obj.optString("sub_cat_id", "")
                        it.flag = 2 //sub category
                        it.title = "Categories"
                    })
                }

                val arrayJsonActivities = jsonData.getJSONArray("activity")
                repeat(arrayJsonActivities.length()) { pos ->
                    val obj = arrayJsonActivities.getJSONObject(pos)
                    arrayModel.add(SearchModel(obj.optString("activity_name")).also {
                        it.catId = obj.optString("cat_id", "")
                        it.subCatId = obj.optString("sub_cat_id", "")
                        it.activityId = obj.optString("activity_id", "")
                        it.flag = 3 //activity
                        it.title = "Class"
                        it.isPassAct = obj.optString("activity_type_pass", "") == "1"
                    })
                }
            }
        } catch (e: Exception) {
            Log.d("ParseSearchExc", "" + e)
        }
        return arrayModel
    }


    fun parseMyBookings(json: JSONObject?): ArrayList<MyBookingsModel> {
        val arrayBookingModel = ArrayList<MyBookingsModel>()
        try {

            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                val jsonArr = json.getJSONObject("data").getJSONArray("data")
                repeat(jsonArr.length()) { pos ->
                    val obj = jsonArr.getJSONObject(pos)

                    if (obj.getString("booking_type").equals("camp", true)) {
                        MyBookingsModel(obj.optString("booking_id", ""))
                            .apply {
                                bookingType = obj.optString("booking_type")
                                campWeekId = obj.optString("week_ids")

                                val jobCamp = obj.getJSONObject("get_camp")
                                bookingImageUrl = jobCamp.getString("camp_feature_img")
                                bookingLocation = obj.getString("camp_venue_name")
                                bookingStatus =
                                    obj.getString("booking_status") //StaticValues.BOOKING_STATUS_COMPLETED
                                bookingCancelledBy = obj.optString("booking_cancel_by", "")
                                cancelReason = obj.optString("cancel_reason", "")


                                bookingDate = obj.optString("booking_date", "")

                                if (bookingStatus.trim() == "" || bookingStatus.equals(
                                        StaticValues.BOOKING_STATUS_UPCOMING,
                                        true
                                    )
                                ) {
                                    bookingStatus = StaticValues.CAMP_BOOKED
                                }
                                bookingSubTitle =
                                    obj.optString(
                                        "provider_name",
                                        ""
                                    ) + " (For " + obj.optString("family_name") + ")"
                                bookingTitle = jobCamp.optString("camp_name", "")
                                campId = jobCamp.getString("camp_id")

                                try {
                                    bookingLat = obj.optString("camp_venue_lat", "0").toDouble()
                                    bookingLng = obj.optString("camp_venue_long", "0").toDouble()
                                } catch (e: Exception) {

                                }

                                bookingAmtAfterTax = obj.optString("booking_amount", "0.0")
                            }.also {
                                arrayBookingModel.add(it)
                            }
                    } else {
                        val bookingModel = MyBookingsModel(
                            obj.optString(
                                "booking_id",
                                ""
                            )
                        ).also { myBookingsModel ->
                            myBookingsModel.isPassAct =
                                obj.optString("activity_type_pass", "") == "1"
                            myBookingsModel.actCategoryId = obj.optString("cat_id")
                            myBookingsModel.bookingType = obj.optString("booking_type")
                            myBookingsModel.sessionJson = obj.getString("booking_sessions_json")
                            myBookingsModel.bookingDate = obj.getString("booking_date")
                            myBookingsModel.bookingImageUrl =
                                obj.optString("activity_feature_img", "")
                            myBookingsModel.bookingCancelledBy =
                                obj.optString("booking_cancel_by", "")
                            myBookingsModel.cancelReason = obj.optString("cancel_reason", "")

                            if (myBookingsModel.bookingType.equals("flexi activity")) {
                                val strFlexi = obj.optString("get_flexi_activity", "null")
                                if (strFlexi != "null")
                                    myBookingsModel.bookingLocation =
                                        obj.getJSONObject("get_flexi_activity")
                                            .getString("venue_name")
                                else {
                                    val strFlexiPremise =
                                        obj.optString("get_flexi_activity_premise", "null")
                                    if (strFlexiPremise != "null") {
                                        myBookingsModel.bookingLocation = "At Your Premise"
                                    }
                                }
                                //myBookingsModel.bookingLocation =obj.getJSONObject("get_flexi_activity").getString("venue_name")
                            } else {
                                myBookingsModel.bookingLocation =
                                    MyPlaceUtils.replaceUAECountry(obj.optString("venue_name", ""))
                            }
                            myBookingsModel.bookingStatus =
                                obj.optString("booking_status", "")
                                    .capitalize() //StaticValues.BOOKING_STATUS_COMPLETED

                            if (myBookingsModel.isPassAct) {
                                if (myBookingsModel.bookingStatus.equals(
                                        StaticValues.BOOKING_STATUS_AWAITING,
                                        true
                                    )
                                ) {
                                    myBookingsModel.bookingStatus =
                                        StaticValues.BOOKING_STATUS_UPCOMING
                                }
                            }
                            myBookingsModel.bookingSubTitle =
                                obj.optString(
                                    "provider_name",
                                    ""
                                ) + " (For " + obj.optString("family_name") + ")"
                            myBookingsModel.bookingTitle = obj.optString("activity_name", "")

                            if (myBookingsModel.bookingStatus.equals(
                                    StaticValues.BOOKING_STATUS_UPCOMING,
                                    true
                                )
                            ) {
                                myBookingsModel.bookingMsgTime =
                                    "Starting from " + filterDateForBookings(obj.optString("enrolment_start_date"))
                            } else if (myBookingsModel.bookingStatus.equals(
                                    StaticValues.BOOKING_STATUS_COMPLETED,
                                    true
                                )
                            ) {
                                myBookingsModel.bookingMsgTime = ""
                            }

                            myBookingsModel.activityId = obj.optString("activity_id", "")
                            myBookingsModel.timeSlotId = obj.optString("time_slot_id", "")

                            try {
                                myBookingsModel.bookingLat =
                                    obj.optString("venue_lat", "0").toDouble()
                                myBookingsModel.bookingLng =
                                    obj.optString("venue_long", "0").toDouble()
                            } catch (e: Exception) {

                            }

                            myBookingsModel.enrolmentId = obj.optString("enrolment_id", "")

                            myBookingsModel.bookingAmtAfterTax =
                                obj.optString("booking_amount", "0.0")

                            myBookingsModel.bookingThresholdDate =
                                getFormattedServerDateJava(
                                    obj.optString(
                                        "threshold_cancellation_date",
                                        ""
                                    )
                                )

                            //ongoing status work
                            val curServerDate =
                                getFormattedServerDateJava(json.optString("current_date", ""))
                            if (myBookingsModel.bookingStatus.equals(
                                    StaticValues.BOOKING_STATUS_ONGOING,
                                    true
                                ) && curServerDate != null
                            ) {
                                if (obj.has("get_enrolment")) {
                                    if (obj.getString("get_enrolment") != "null") {
                                        val objEnrol = obj.getJSONObject("get_enrolment")

                                        myBookingsModel.enrolmentId =
                                            objEnrol.optString("enrolment_id", "")

                                        val arrayJsonEnrolSlots =
                                            objEnrol.getJSONArray("get_enrolment_slot")
                                        for (indexPos in 0 until arrayJsonEnrolSlots.length()) {
                                            val objEnrolSlot =
                                                arrayJsonEnrolSlots.getJSONObject(indexPos)

                                            val strDateEnrolServer = objEnrolSlot.optString(
                                                "enrolment_slot_date",
                                                ""
                                            ) + " " + objEnrolSlot.optString("enrolment_slot_time")
                                            //Log.d("EnrolDateBooking", strDateEnrolServer)
                                            val enrolDateJava =
                                                getFormattedServerDateJava(
                                                    strDateEnrolServer,
                                                    "yyyy-MM-dd HH:mm:ss"
                                                )
                                            if (enrolDateJava != null) {
                                                if (enrolDateJava.after(curServerDate)) {
                                                    myBookingsModel.bookingMsgTime =
                                                        "Next class on " + filterDateForBookings(
                                                            strDateEnrolServer
                                                        )
                                                    break
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            //remove prefix starting from in case of single session
                            if (obj.has("get_enrolment")) {
                                if (obj.getString("get_enrolment") != "null") {
                                    val objEnrol = obj.getJSONObject("get_enrolment")
                                    if (objEnrol.optString("enrolment_slot_type", "").trim()
                                            .equals("single session", true)
                                    )
                                        myBookingsModel.bookingMsgTime =
                                            myBookingsModel.bookingMsgTime.replace(
                                                "Starting from ",
                                                ""
                                            ).trim()
                                }
                            }
                        }

                        arrayBookingModel.add(bookingModel)
                    }
                }
            }
        } catch (e: Exception) {
            Log.d("ParseBookingsExc", "" + e)
        }



        return arrayBookingModel
    }

    fun parseReviews(json: JSONObject): ArrayList<ReviewModel> {
        val arrayReviewModel = ArrayList<ReviewModel>()
        try {
            if (json.getBoolean(StaticValues.KEY_STATUS)) {
                val jsonDataParent = json.getJSONObject("data")
                val jsonData = jsonDataParent.getJSONArray("data")
                repeat(jsonData.length()) { index ->
                    val obj = jsonData.getJSONObject(index)
                    val revModel = ReviewModel(obj.optString("review_id", "")).also { reviewModel ->

                        reviewModel.reviewDesc = obj.getValidString("review_description")

                        reviewModel.reviewCampId = obj.optString("camp_id", "")
                        reviewModel.reviewActivityId = obj.optString("activity_id", "")

                        val socialImageUrl = obj.optString("customer_image_url", "")
                        val normalImageUrl = obj.optString("customer_image", "")
                        if (normalImageUrl.trim().length > 4) {
                            reviewModel.reviewUserImageUrl = normalImageUrl
                            reviewModel.isUserSocialImage = false
                        } else if (socialImageUrl.trim().length > 4) {
                            reviewModel.reviewUserImageUrl = socialImageUrl
                            reviewModel.isUserSocialImage = true
                        }

                        reviewModel.reviewUserId = obj.optString("customer_id", "")

                        val userName =
                            (obj.optString(
                                "customer_fname",
                                ""
                            ) + " " + obj.optString("customer_lname", "")).trim()
                        reviewModel.reviewUserName = userName

                        try {
                            reviewModel.reviewRating = obj.optString("review_rating", "0").toFloat()
                        } catch (e: Exception) {
                            reviewModel.reviewRating = 0.0F
                        }

                        try {
                            reviewModel.reviewTimeAgo =
                                reviewModel.covertTimeToText(obj.getString("created_at"))!!
                        } catch (e: Exception) {
                            reviewModel.reviewTimeAgo = ""
                        }

                    }

                    arrayReviewModel.add(revModel)
                }

            }
        } catch (e: Exception) {
            Log.d("ParseReviewsExc", "" + e)
        }
        return arrayReviewModel
    }

    fun parseCategoryAndSubCategory(json: JSONObject) {
        try {
            if (json.getBoolean(StaticValues.KEY_STATUS)) {
                StaticValues.arrayCatSubCatModel.clear()
                val jsonData = json.getJSONArray("data")
                repeat(jsonData.length()) { index ->
                    val objCat = jsonData.getJSONObject(index)
                    /*val model=CategoryModel(objCat.optString("cat_id",""),objCat.optString("cat_name","")).also {catModel->
                        catModel.isSubCategory=false
                    }*/
                    //StaticValues.arrayCatSubCatModel.add(model)
                    val arraySubCat = objCat.getJSONArray("get_sub_category")
                    repeat(arraySubCat.length()) { indexSub ->
                        val objSubCat = arraySubCat.getJSONObject(indexSub)
                        val subCatModel = CategoryModel(
                            objCat.optString("cat_id", ""),
                            objCat.optString("cat_name", "")
                        ).also { subCatModel ->
                            subCatModel.isSubCategory = true
                            subCatModel.subCatId = objSubCat.optString("sub_cat_id", "")
                            subCatModel.subCatName = objSubCat.optString("sub_cat_name", "")
                        }
                        StaticValues.arrayCatSubCatModel.add(subCatModel)
                    }
                }

                for (model in StaticValues.arrayCatSubCatModel)
                    Log.d("Model__", "" + model + "\n" + model.subCatId + "\n" + model.subCatName)
            }
        } catch (e: Exception) {
            Log.d("ParseCatSubCat", "" + e)
        }
    }

    fun parseFavoriteData(json: JSONObject, jsonMainParent: JSONObject?): ArrayList<FavoriteModel> {
        val arrFavoriteModel = ArrayList<FavoriteModel>()
        val jarData = json.getJSONArray("data")
        repeat(jarData.length()) {
            val job = jarData.getJSONObject(it)
            if (job.getString("favorite_flag") == "0")// activity
            {
                if (job.getString("get_activity").trim() != "null" && job.getString("get_activity")
                        .trim() != ""
                ) {
                    val activityModel = parseOneActivity(job.getJSONObject("get_activity"), job)
                    activityModel.isFavoriteMarked = true
                    arrFavoriteModel.add(FavoriteModel(activityModel = activityModel))
                }
            } else    //camp
            {
                val campModel = getCampModel(job.getJSONObject("get_camp"), jsonMainParent)
                campModel.isFavoriteMarked = true
                arrFavoriteModel.add(FavoriteModel(campModel = campModel))
            }
        }
        return arrFavoriteModel
    }

    fun parseCampModel(json: JSONObject, parentJson: JSONObject?): ArrayList<CampModel> {
        val arrModel = ArrayList<CampModel>()
        val jarData = json.getJSONArray("data")
        repeat(jarData.length()) {
            val job = jarData.getJSONObject(it)
            /*job.run {
                CampModel(getString("camp_id"),getString("camp_name"),getString("camp_feature_img"))
            }.apply {
                serviceProviderName=job.getString("provider_name")
            }.apply {
                address=job.getString("venue_location")
            }.apply {
                val jarPrice=job.getJSONArray("get_camp_price_management")
                var price=0.0
                repeat(jarPrice.length())
                {
                    try {
                        val job=jarPrice.getJSONObject(it)
                        val priceFromServer=job.getString("price").toDouble()
                        if (it==0)
                            price=priceFromServer
                        else if (priceFromServer<price)
                            price=priceFromServer
                    }catch (e:Exception)
                    {}
                }
                startingPrice=price.toString()
            }.apply {
                isDiscountAvailable=(job.getString("camp_sibling_discount_status")=="1" || job.getString("camp_early_reg_discount_status")=="1")
            }.apply {
                isTransportAvailable=false //need to parse later
            }.apply {
                ageRageData=job.getString("camp_age_group")
            }.apply {
                timeRangeData=ChangeDateFormat.convertDate(job.getString("camp_time_from"),"HH:mm:ss","HH:mm")+" to "+ChangeDateFormat.convertDate(job.getString("camp_time_to"),"HH:mm:ss","HH:mm")
            }.apply {
                arrTagList=ArrayList(job.getString("tag_names").split(","))
            }*/

            getCampModel(job, parentJson).also {
                arrModel.add(it)
            }
        }
        return arrModel
    }

    private fun getCampModel(job: JSONObject, jsonMainParent: JSONObject?): CampModel {
        job.run {
            CampModel(getString("camp_id"), getString("camp_name"), getString("camp_feature_img"))
        }.apply {
            isFavoriteMarked = job.optString("is_favorite") == "1"
        }.apply {
            serviceProviderName = job.getString("provider_name")
        }.apply {
            address = job.getString("venue_name")
        }.apply {
            venueFullAddress = job.getString("venue_full_address")
        }.apply {
            val jarPrice = job.getJSONArray("get_camp_price_management")
            var price = 0.0
            repeat(jarPrice.length())
            {
                try {
                    val jobInner = jarPrice.getJSONObject(it)
                    val priceFromServer = jobInner.getString("price").toDouble()
                    if (it == 0)
                        price = priceFromServer
                    else if (priceFromServer < price)
                        price = priceFromServer
                } catch (e: Exception) {
                }
            }
            startingPrice = price.toString()
        }.apply {
            isDiscountAvailable =
                (job.getString("camp_sibling_discount_status") == "1" || job.getString("camp_early_reg_discount_status") == "1")
        }.apply {
            isTransportAvailable = job.getString("camp_transportation_status") == "1"
        }.apply {
            ageRageData = job.getString("camp_age_group") + " yr"
        }.apply {
            timeRangeData = ChangeDateFormat.convertDate(
                job.getString("camp_time_from"),
                "HH:mm:ss",
                "HH:mm"
            ) + " to " + ChangeDateFormat.convertDate(
                job.getString("camp_time_to"),
                "HH:mm:ss",
                "HH:mm"
            )
        }.apply {
            val jarTags = JSONArray(job.getString("tag_names"))
            repeat(jarTags.length()) {
                arrTagList.add(jarTags.getString(it))
            }
        }.apply {
            PromotionOperation.setPromotionDataInCampModel(
                job,
                this,
                this.providerId,
                job.optString("max_camp_price", "0.0"),
                jsonMainParent
            )
        }.also {
            return it
        }
    }

    /* fun parseNearByActivity(json: JSONObject){
         val arrayActivityModel = ArrayList<ActivityModel>()
         val jarData=json.getJSONArray("data")
         repeat(jarData.length()){
             val job=jarData.getJSONObject(it)
             job.run {
                 NearByActivityModel().run {
                     activityId=optString("activity_id")
                     catId=optString("cat_id")
                     providerName=optString("provider_name")
                     activityName=optString("activity_name")
                     getJSONObject("get_time_slot_similiar").also {

                     }

                 }
             }
         }
     }*/

    fun parseNearbyActivities(
        json: JSONObject,
        jsonMainParent: JSONObject?
    ): Pair<ArrayList<ActivityModel>, ArrayList<ActivityModel>> {
        val arrayActivityModel = ArrayList<ActivityModel>()
        val arrayActivityModelNearby = ArrayList<ActivityModel>()
        try {
            //if (json.getBoolean(StaticValues.KEY_STATUS)) {
            val jsonData = json.getJSONArray("data")
            repeat(jsonData.length()) { i ->
                val jsonObj = jsonData.getJSONObject(i)
                arrayActivityModel.add(parseOneActivity(jsonObj, jsonMainParent))
                //Log.d("Act__Loc", arrayActivityModel[i].activityLocation)
            }

            for (actModel in arrayActivityModel) {

                val arraySlots = actModel.arraySlotModel//.sortedBy { it.slotDistance }

                //Log.d("Slot_Detail_Activity", "" + actModel.activityId + " -- " + actModel.activityName)

                var index = 0
                //    var i = 0
                for (slotModel in arraySlots) {

                    val mActModel = ActivityModel(
                        actModel.activityId,
                        actModel.activityName,
                        actModel.activityImage,
                        actModel.activityImageUrl
                    ).also { model ->

                        model.activityType = actModel.activityType
                        model.activityDesc = actModel.activityDesc

                        model.arrPromotionModel = actModel.arrPromotionModel
                        model.maxValuePromotionModel = actModel.maxValuePromotionModel

                        model.activityProviderId = actModel.activityProviderId
                        model.activityProviderName = actModel.activityProviderName
                        model.activityProviderPhone = actModel.activityProviderPhone
                        model.activityProviderAboutUs = actModel.activityProviderAboutUs
                        model.activityProviderLogo = actModel.activityProviderLogo

                        model.activityAdditionalInfo = actModel.activityAdditionalInfo

                        model.activityOtherInfo = actModel.activityOtherInfo

                        model.activityCancellationPolicy = actModel.activityCancellationPolicy

                        model.isReviewGiven = actModel.isReviewGiven

                        model.activityCancellationHours = actModel.activityCancellationHours

                        model.arrayImages.clear()
                        model.arrayImages.addAll(actModel.arrayImages)

                        model.activityStarRating = actModel.activityStarRating
                        model.activityStarRatingCount = actModel.activityStarRatingCount

                        model.activityMsgLocationSuffix = ""
                        model.activityLocation = slotModel.slotLocation.trim()
                        model.arraySlotModel.clear()
                        model.arraySlotModel.addAll(actModel.arraySlotModel.filter { sModel -> sModel.slotDistance == slotModel.slotDistance } as ArrayList<ActivitySlotModel>)

                        model.slotPricePayableForMap = slotModel.slotPricePayable
                        model.slotDisplayPriceForMap = slotModel.slotPricePayableToDisplay
                        model.slotAge = slotModel.slotAge

                        //Earlier
                        //model.startDate = slotModel.slotEnrolStartDate

                        //Now
                        model.startDate = slotModel.slotTitle
                        model.startDate =
                            if (model.startDate.trim().isNotEmpty() && !model.startDate.trim()
                                    .equals("null", true)
                            ) model.startDate.trim().replace("From ", "") else ""
                        //Log.d("StartDate_From_Omit", "" + model.startDate)
                        if (model.startDate.trim().length > 4 && model.activityType == ActivityModel.NORMAL_ACTIVITY) {
                            model.startDate = model.startDate.substring(0, 3)
                                .trim() + ", " + "Starting" + " " + model.startDate.substring(5)
                                .trim()
                        }

                        model.slotTime = slotModel.slotTime
                        model.lat = slotModel.slotLat
                        model.lng = slotModel.slotLong
                        model.slotDistance = slotModel.slotDistance
                        model.isBooking = slotModel.arrayEnrolModel.size > 0
                        model.slotStatusText = slotModel.slotStatus
                        model.slotIndex = index


                        //Log.d("Location__", model.activityLocation)
                        ++index
                    }

                    //Log.d("Slot_Detail", "ActivityId: " + slotModel.slotActivityId + "--" + "SlotId: ${slotModel.slotId} -- " + slotModel.slotAge + " -- " + slotModel.slotLocation + " -- " + slotModel.slotDistance)

                    arrayActivityModelNearby.add(mActModel)

                    /*  if (i == 0) {
                          arrayActivityModelNearby.add(mActModel)
                      } else {
                          val prevSlotModel = arraySlots[i - 1]
                          if (slotModel.slotDistance != prevSlotModel.slotDistance) {
                              arrayActivityModelNearby.add(mActModel)
                          }
                      }

                      i += 1*/

                    break
                }

                val arrFlexiSession = actModel.arrFlexiSession
                var flexiIndex = 0
                for (flexiSession in arrFlexiSession) {

                    val mActModel = ActivityModel(
                        actModel.activityId,
                        actModel.activityName,
                        actModel.activityImage,
                        actModel.activityImageUrl
                    ).also { model ->

                        model.activityType = actModel.activityType
                        model.activityDesc = actModel.activityDesc

                        model.arrPromotionModel = actModel.arrPromotionModel
                        model.maxValuePromotionModel = actModel.maxValuePromotionModel

                        model.activityProviderId = actModel.activityProviderId
                        model.activityProviderName = actModel.activityProviderName
                        model.activityProviderPhone = actModel.activityProviderPhone
                        model.activityProviderAboutUs = actModel.activityProviderAboutUs
                        model.activityProviderLogo = actModel.activityProviderLogo

                        model.activityAdditionalInfo = actModel.activityAdditionalInfo

                        model.activityOtherInfo = actModel.activityOtherInfo

                        model.activityCancellationPolicy = actModel.activityCancellationPolicy

                        model.isReviewGiven = actModel.isReviewGiven

                        model.activityCancellationHours = actModel.activityCancellationHours

                        model.arrayImages.clear()
                        model.arrayImages.addAll(actModel.arrayImages)

                        model.activityStarRating = actModel.activityStarRating
                        model.activityStarRatingCount = actModel.activityStarRatingCount

                        model.activityMsgLocationSuffix = ""

                        model.similarFlexiModel = arrFlexiSession[flexiIndex]
                        model.flexiIndex = flexiIndex


                    }
                    ++flexiIndex

                    arrayActivityModelNearby.add(mActModel)

                    break
                }
            }


            //}
        } catch (e: Exception) {
            Log.d("JsonExc", "" + e)
        }

        arrayActivityModelNearby.sortBy { it.slotDistance }

        return Pair(arrayActivityModel, arrayActivityModelNearby)
    }

    fun parseCampDetailResponse(json: JSONObject, jsonMain: JSONObject): CampModel {
        json.run {
            getCampModel(json, jsonMain)
        }.apply {
            isReviewGiven = json.getString("is_review_given") != "0"
        }.apply {
            currentDate =
                ChangeDateFormat.convertDate(
                    jsonMain.getString("current_date"),
                    "yyyy-MM-dd HH:mm:ss",
                    "dd MMM yyyy"
                )
        }.apply {
            campAbout = json.getString("camp_about")
        }.apply {
            ageFrom = json.getString("camp_age_from")
            ageTo = json.getString("camp_age_to")
        }.apply {
            val arrImages = ArrayList<String>()
            arrImages.add(json.getString("camp_feature_img"))

            val jarPhoto = json.getJSONArray("get_camp_photo_gallery")
            repeat(jarPhoto.length()) {
                val job = jarPhoto.getJSONObject(it)
                job.run {
                    arrImages.add(getString("photo_gallery_image"))
                }
            }

            val jarVideo = json.getJSONArray("get_camp_video_gallery")
            repeat(jarVideo.length()) {
                val job = jarVideo.getJSONObject(it)
                job.run {
                    arrImages.add("http://img.youtube.com/vi/" + getString("video_gallery") + "/hqdefault.jpg")
                    //arrImages.add("https://www.youtube.com/watch?v="+getString("video_gallery"))
                }
            }
            arrayImages = arrImages
        }.apply {
            venueLat = json.optString("venue_lat", "0.0")
            venueLng = json.optString("venue_long", "0.0")
        }.apply {
            val arrDayOrWeekPriceModel = ArrayList<DayOrWeekPriceModel>()
            val jarPrice = json.getJSONArray("get_camp_price_management")
            repeat(jarPrice.length()) {
                val job = jarPrice.getJSONObject(it)
                job.run {
                    DayOrWeekPriceModel(
                        getString("camp_price_management_id"),
                        getString("session_lable")
                        ,
                        getString("price"),
                        optString("max_week_selection").toInt(),
                        optString("session_type")
                    )
                }.also {
                    arrDayOrWeekPriceModel.add(it)
                }
            }

            this.arrDayOrWeekPriceModel = arrDayOrWeekPriceModel
        }.apply {
            val arrWeekModel = ArrayList<WeekModel>()

            val jarCampSchedule = json.getJSONArray("get_camp_schedule")
            repeat(jarCampSchedule.length()) {
                val job = jarCampSchedule.getJSONObject(it)
                job.run {
                    WeekModel(
                        job.getString("camp_schedule_id"),
                        job.getString("number_of_week")
                        ,
                        ChangeDateFormat.convertDate(
                            job.getString("start_date"),
                            "yyyy-MM-dd",
                            "dd MMM yyyy"
                        )
                        ,
                        ChangeDateFormat.convertDate(
                            job.getString("end_date"),
                            "yyyy-MM-dd",
                            "dd MMM yyyy"
                        )
                        ,
                        job.getInt("booking_remaining")
                    )
                }.also {
                    arrWeekModel.add(it)
                }
            }
            this.arrWeekModel = arrWeekModel
        }.apply {
            val arrDiscountModel = ArrayList<CampDiscountModel>()
            if (json.getString("camp_sibling_discount_status") == "1") {
                CampDiscountModel(
                    json.getString("camp_sibling_discount_percent"),
                    "Sibling Discount (when booked together)"
                    ,
                    CampDiscountModel.SIBLING_DISCOUNT,
                    ""
                )
                    .also {
                        arrDiscountModel.add(it)
                    }
            }

            if (json.getString("camp_early_reg_discount_status") == "1") {

                // Date check to ensure that current date is before the discount expiry date
                val currentDate = ChangeDateFormat.getDateObjFromString(currentDate, "dd MMM yyyy")

                val lastDateToApplyEarlyRegDiscount = ChangeDateFormat.getDateObjFromString(
                    json.getString("camp_early_reg_before_date"),
                    "yyyy-MM-dd"
                )

                if (currentDate != null && lastDateToApplyEarlyRegDiscount != null) {
                    Log.d("CurDate_Camp", "" + currentDate)
                    Log.d("EarlyRegDate_Camp", "" + lastDateToApplyEarlyRegDiscount)
                    if (currentDate.before(lastDateToApplyEarlyRegDiscount) || currentDate.time == lastDateToApplyEarlyRegDiscount.time) {
                        arrDiscountModel.add(
                            CampDiscountModel(
                                json.getString("camp_early_reg_discount_percent"),
                                "For Registration before " + ChangeDateFormat.convertDate(
                                    json.getString("camp_early_reg_before_date"),
                                    "yyyy-MM-dd",
                                    "dd MMMM yyyy"
                                ),
                                CampDiscountModel.EARLY_REG_DISCOUNT,
                                json.getString("camp_early_reg_before_date")
                            )
                        )
                    }
                }


            }
            this.arrDiscountModel = arrDiscountModel

        }.apply {
            additionalInfo = json.getString("camp_additional_information")
        }.apply {
            providerId = json.getString("provider_id")
            providerLogo = json.getString("provider_logo")
            providerDescription = json.getString("provider_about_us")
        }.also {
            return it
        }
    }

    fun parseOneActivity(
        jsonObj: JSONObject,
        jsonMainParent: JSONObject?,
        bookingThresholdDate: Date? = null
    ): ActivityModel {
        try {
            return ActivityModel(
                jsonObj.optString("activity_id", ""),
                jsonObj.optString("activity_name", ""),
                activityImageUrl = jsonObj.optString("activity_feature_img", "")
            )
                .also { model ->
                    model.activityId = jsonObj.optString("activity_id", "")
                    model.isPassActivity = jsonObj.optString("activity_type_pass", "") == "1"
                    model.activityType = jsonObj.optString("activity_type", "-1").toInt()
                    model.categoryId = jsonObj.optString("cat_id")
                    model.activityDesc = jsonObj.optString("activity_description", "")
                    model.isFavoriteMarked = jsonObj.optString("is_favorite") == "1"
                    model.activityProviderId = jsonObj.optString("provider_id", "")
                    model.activityProviderName = jsonObj.optString("provider_name", "")
                    model.activityProviderPhone = jsonObj.optString("provider_phone_number", "")
                    model.activityProviderAboutUs = jsonObj.optString("provider_about_us", "")
                    model.activityProviderLogo = jsonObj.optString("provider_logo", "")

                    //Log.d("actPass", model.activityId + "         " + jsonObj.optString("activity_type_pass", "---"))


                    model.activityAdditionalInfo =
                        jsonObj.optString("activity_eligibility_info", "")
                    if (model.activityAdditionalInfo.equals("null", true))
                        model.activityAdditionalInfo = "N/A"

                    model.activityOtherInfo = jsonObj.optString("activity_material_info", "")
                    if (model.activityOtherInfo.equals("null", true))
                        model.activityOtherInfo = "N/A"

                    model.activityCancellationPolicy = jsonObj.optString("cancelation_policy", "")
                    if (model.activityCancellationPolicy.equals("null", true))
                        model.activityCancellationPolicy = "N/A"

                    model.isReviewGiven = jsonObj.optInt("is_review_given", 0) > 0

                    model.activityCancellationHours =
                        jsonObj.optString("cancelation_hours", "0").toInt()

                    val arraySlotsAsItIs = ArrayList<ActivitySlotModel>()

                    model.arraySlotModel.clear()
                    if (jsonObj.has("get_time_slot")) {
                        var key =
                            if (jsonObj.has("get_time_slot")
                            ) "get_time_slot" else "get_time_slot"
                        val jsonArrTimeSlots = jsonObj.getJSONArray(key)

                        //Log.d("ActivityInfo_", model.activityId + " -" + model.activityName + " - Slots: " + jsonArr.length())
                        if (jsonArrTimeSlots.length() > 0) {

                            repeat(jsonArrTimeSlots.length()) { posSlot ->
                                val objSlot = jsonArrTimeSlots.getJSONObject(posSlot)

                                if (posSlot == 0) {
                                    model.activityLocation =
                                        MyPlaceUtils.replaceUAECountry(
                                            objSlot.optString(
                                                "venue_name",
                                                ""
                                            ).trim()
                                        )
                                    model.activityLocationFullAddress =
                                        objSlot.optString("venue_full_address", "").trim()
                                }

                                val slotModel =
                                    ActivitySlotModel(objSlot.optString("time_slot_id")).also { slotModel ->

                                        slotModel.slotActivityId = model.activityId

                                        slotModel.slotEnquireSortParam =
                                            objSlot.optString("time_slot_week_num", "0").toInt()

                                        //Log.d("Obj__SlotTime___",objSlot.optString("time_slot_start_from",""));
                                        slotModel.slotEnquireStartDateTime = getEnquireDateTime(
                                            objSlot.optString(
                                                "time_slot_week",
                                                ""
                                            ).capitalize() + " Jan 2017 " + objSlot.optString(
                                                "time_slot_start_from",
                                                ""
                                            )
                                        )

                                        slotModel.slotAge = objSlot.optString(
                                            "time_slot_age_group_label",
                                            ""
                                        ) + " yr"

                                        try {
                                            slotModel.slotAgeFrom =
                                                objSlot.optString("time_slot_age_from", "0")
                                                    .toDouble()
                                            slotModel.slotAgeTo =
                                                objSlot.optString("time_slot_age_to", "0")
                                                    .toDouble()
                                        } catch (e: Exception) {
                                            Log.e("Exc_ActivityExc1", "" + e)
                                        }


                                        //time calculation
                                        val strStartTime =
                                            getFormattedStartTime(objSlot.optString("time_slot_start_from"))
                                        val strEndTime = addMinutesToTime(
                                            strStartTime,
                                            objSlot.optInt("time_slot_duration", 0)
                                        )
                                        slotModel.slotTime = "$strStartTime - $strEndTime"

                                        slotModel.slotLocation =
                                            MyPlaceUtils.replaceUAECountry(
                                                objSlot.optString(
                                                    "venue_name",
                                                    ""
                                                ).trim()
                                            ).trim()
                                        slotModel.slotLocationFullAddress =
                                            objSlot.optString("venue_full_address", "").trim()

                                        try {
                                            slotModel.slotLat =
                                                objSlot.optString("venue_lat", "0.0").toDouble()
                                            slotModel.slotLong =
                                                objSlot.optString("venue_long", "0.0").toDouble()
                                        } catch (e: Exception) {
                                            Log.e("Exc_ActivityExc2", "" + e)
                                        }

                                        slotModel.slotVenuePlaceId =
                                            objSlot.optString("venue_place_id", "_").trim()

                                        slotModel.slotDistance =
                                            objSlot.optString("distance", "0.0").toDouble()
                                    }

                                var strHeading = objSlot.optString("time_slot_week", "")
                                if (strHeading.trim().length >= 3)
                                    strHeading = strHeading.substring(0, 3).trim()
                                        .toUpperCase(Locale.ENGLISH)

                                //enrolment details
                                try {
                                    val jsonArrEnrolment = objSlot.getJSONArray("get_enrolment")
                                    if (jsonArrEnrolment.length() > 0) {
                                        repeat(jsonArrEnrolment.length()) { indexEnrolment ->

                                            //Log.d("EnrolmentIndex",""+indexEnrolment)
                                            val jsonObjEnrol =
                                                jsonArrEnrolment.getJSONObject(indexEnrolment)

                                            val mySlotModel = slotModel.copy().also { mySlModel ->

                                                mySlModel.isSingleActivity =
                                                    jsonObjEnrol.optString(
                                                        "enrolment_slot_type",
                                                        ""
                                                    ).equals("single session", true)

                                                //copy properties from parent slot model
                                                mySlModel.slotActivityId = slotModel.slotActivityId
                                                mySlModel.slotEnquireSortParam =
                                                    slotModel.slotEnquireSortParam
                                                mySlModel.slotAgeFrom = slotModel.slotAgeFrom
                                                mySlModel.slotAgeTo = slotModel.slotAgeTo
                                                mySlModel.slotAge = slotModel.slotAge
                                                mySlModel.slotTime = slotModel.slotTime
                                                mySlModel.slotLat = slotModel.slotLat
                                                mySlModel.slotLong = slotModel.slotLong
                                                mySlModel.slotVenuePlaceId =
                                                    slotModel.slotVenuePlaceId
                                                mySlModel.slotDistance = slotModel.slotDistance

                                                mySlModel.slotLocation = slotModel.slotLocation
                                                mySlModel.slotLocationFullAddress =
                                                    slotModel.slotLocationFullAddress
                                                mySlModel.slotEnquireStartDateTime =
                                                    slotModel.slotEnquireStartDateTime

                                                //Log.d("SlotLoc__",mySlModel.slotLocation)
                                                //slotModel.slotLocation=MyPlaceUtils.replaceUAECountry(objSlot.optString("venue_name", "").trim()).trim()
                                                mySlModel.slotStatus = "Available"

                                                //mySlModel.slotTitle = "From ${strHeading.toUpperCase()}, " + getFormattedDate(jsonObjEnrol.optString("enrolment_start_date", "")).toUpperCase()
                                                var appendFrom = ""
                                                appendFrom = if (mySlModel.isSingleActivity)
                                                    ""
                                                else
                                                    "From"

                                                mySlModel.slotTitle =
                                                    appendFrom + " ${getFormattedDateEnrolmentStartValid(
                                                        jsonObjEnrol.optString(
                                                            "next_batch_date",
                                                            ""
                                                        )
                                                    ).trim()}"

                                                if (jsonObjEnrol.optString("next_batch_date", "")
                                                        .contains(" ")
                                                ) {
                                                    try {
                                                        val strStartTimeSlot =
                                                            jsonObjEnrol.optString(
                                                                "next_batch_date",
                                                                ""
                                                            ).split(" ")[1]
                                                        val strStartTimeToDisplay =
                                                            getFormattedStartTime(strStartTimeSlot)
                                                        val strEndTimeToDisplay = addMinutesToTime(
                                                            strStartTimeToDisplay,
                                                            jsonObjEnrol.optString(
                                                                "next_batch_duration",
                                                                "0"
                                                            ).toInt()
                                                        )
                                                        slotModel.slotTime =
                                                            "$strStartTimeToDisplay - $strEndTimeToDisplay"
                                                        mySlModel.slotTime = slotModel.slotTime
                                                    } catch (e: Exception) {
                                                        Log.e("TimeSlotExcNextBatch", "" + e)
                                                    }
                                                }


                                                mySlModel.slotNextAvailableSessionDate =
                                                    getFormattedServerDateJava(
                                                        jsonObjEnrol.optString(
                                                            "next_batch_date",
                                                            ""
                                                        )
                                                    )


                                                mySlModel.slotType =
                                                    jsonObjEnrol.optString("enrolment_type", "")
                                                mySlModel.slotPrice =
                                                    jsonObjEnrol.optString("enrolment_price", "")

                                                mySlModel.slotEnrolStartDate =
                                                    getFormattedDateTimeTable(
                                                        jsonObjEnrol.optString(
                                                            "enrolment_start_date",
                                                            ""
                                                        )
                                                    )
                                                mySlModel.slotEnrolEndDate =
                                                    getFormattedDateTimeTable(
                                                        jsonObjEnrol.optString(
                                                            "enrolment_end_date",
                                                            ""
                                                        )
                                                    )

                                                mySlModel.slotEnrolmentId =
                                                    jsonObjEnrol.optString("enrolment_id", "")
                                                //Log.d("SlotEnrolmentId_",slotModel.slotEnrolmentId)

                                                try {
                                                    mySlModel.noOfSessions =
                                                        jsonObjEnrol.getString("enrolment_session")
                                                            .toInt()
                                                } catch (e: Exception) {
                                                    Log.e("NoOfSessionsExc", "" + e)
                                                    mySlModel.noOfSessions = 0
                                                }

                                                try {
                                                    mySlModel.arrayEnrolModel =
                                                        ArrayList<EnrolModel>()
                                                    if (jsonObjEnrol.has("get_enrolment_slot")) {
                                                        val jsonArrEnrol =
                                                            jsonObjEnrol.getJSONArray("get_enrolment_slot")
                                                        if (jsonArrEnrol.length() > 0) {
                                                            repeat(jsonArrEnrol.length()) { indexEnrol ->
                                                                val objEnrol =
                                                                    jsonArrEnrol.getJSONObject(
                                                                        indexEnrol
                                                                    )
                                                                mySlModel.arrayEnrolModel.add(
                                                                    EnrolModel(
                                                                        objEnrol.optString(
                                                                            "enrolment_slot_id",
                                                                            ""
                                                                        )
                                                                    ).also { enrolModel ->
                                                                        enrolModel.enrolDate =
                                                                            getFormattedDateTimeTable(
                                                                                objEnrol.optString(
                                                                                    "enrolment_slot_date",
                                                                                    ""
                                                                                )
                                                                            )
                                                                        enrolModel.enrolDay =
                                                                            objEnrol.getString("enrolment_slot_week")
                                                                                .trim().capitalize()

                                                                        //time calculation
                                                                        val strStartTimeEnrol =
                                                                            getFormattedStartTime(
                                                                                objEnrol.optString("enrolment_slot_time")
                                                                            )
                                                                        val strEndTimeEnrol =
                                                                            addMinutesToTime(
                                                                                strStartTimeEnrol,
                                                                                objEnrol.optString(
                                                                                    "enrolment_slot_duration",
                                                                                    "0"
                                                                                ).toInt()
                                                                            )

                                                                        enrolModel.enrolTime =
                                                                            "$strStartTimeEnrol - $strEndTimeEnrol"
                                                                        enrolModel.enrolDuration =
                                                                            objEnrol.optString(
                                                                                "enrolment_slot_duration",
                                                                                "0"
                                                                            )

                                                                        val strEnrolStartDateTime =
                                                                            objEnrol.optString(
                                                                                "enrolment_slot_date",
                                                                                ""
                                                                            ) + " " + objEnrol.optString(
                                                                                "enrolment_slot_time"
                                                                            )

                                                                        //Log.d("StrEnrolStartDateTime",strEnrolStartDateTime);
                                                                        enrolModel.enrolDateJava =
                                                                            getFormattedServerDateJava(
                                                                                strEnrolStartDateTime
                                                                            )

                                                                        //calculate if cur server date is ahead of enrol date, for indication in time table (grey background) and for no of sessions to be paid by the user
                                                                        if (jsonMainParent != null) {
                                                                            if (jsonMainParent.has("current_date")) {
                                                                                val strServerDate =
                                                                                    jsonMainParent.optString(
                                                                                        "current_date",
                                                                                        ""
                                                                                    )
                                                                                //Log.d("ServerDate", strServerDate)
                                                                                val currentServerDate =
                                                                                    getFormattedServerDateJava(
                                                                                        strServerDate
                                                                                    )

                                                                                model.curServerDateJava =
                                                                                    currentServerDate

                                                                                //Log.d("FormattedServerDate", "" + currentServerDate)
                                                                                if (currentServerDate != null && enrolModel.enrolDateJava != null) {
                                                                                    Log.d(
                                                                                        "DT_CurDate",
                                                                                        "" + currentServerDate
                                                                                    )
                                                                                    Log.d(
                                                                                        "DT_EnrolDate",
                                                                                        "" + enrolModel.enrolDateJava
                                                                                    )
                                                                                    //Log.d("FormattedEnrolDate", "" + enrolModel.enrolDateJava)
                                                                                    enrolModel.isDateAlreadyPassed =
                                                                                        currentServerDate.after(
                                                                                            enrolModel.enrolDateJava
                                                                                        ) //|| currentServerDate.equals(enrolModel)
                                                                                    //Log.d("DateAlreadyPassed",""+enrolModel.isDateAlreadyPassed)
                                                                                    //Log.d("isDateAlreadyPassed", enrolModel.isDateAlreadyPassed.toString())
                                                                                    //Log.d("-------------", "----------------------")
                                                                                }
                                                                            }
                                                                        }
                                                                    })
                                                            }
                                                        }
                                                    }


                                                    //calculate price on the basis of enrolment slots to attend and show the same in activity listing slots
                                                    try {
                                                        val singlePrice =
                                                            decimalFormat.format(mySlModel.slotPrice.toDouble())
                                                                .toDouble()
                                                        val countSessionsToAttend =
                                                            mySlModel.arrayEnrolModel.filter { enrolModel -> !enrolModel.isDateAlreadyPassed }.size
                                                        mySlModel.noOfEligibleSessions =
                                                            countSessionsToAttend
                                                        mySlModel.slotPricePayable =
                                                            "" + (countSessionsToAttend * singlePrice)
                                                        mySlModel.slotPricePayableToDisplay =
                                                            "" + Math.round(mySlModel.slotPricePayable.toDouble())
                                                        if (mySlModel.slotPricePayable.trim()
                                                                .endsWith(".0")
                                                        ) {
                                                            mySlModel.slotPricePayableToDisplay =
                                                                mySlModel.slotPricePayable.substring(
                                                                    0,
                                                                    mySlModel.slotPricePayable.length - 2
                                                                )
                                                        }


                                                    } catch (e: Exception) {
                                                        Log.e("ExcJsonArrEnrol", "" + e)
                                                        mySlModel.slotPricePayable =
                                                            mySlModel.slotPrice
                                                        mySlModel.slotPricePayableToDisplay =
                                                            mySlModel.slotPricePayable
                                                    }


                                                } catch (e: Exception) {
                                                    Log.e("ExcJsonArrEnrol", "" + e)
                                                }
                                            }


                                            //model.arraySlotModel.add(mySlotModel)
                                            arraySlotsAsItIs.add(mySlotModel)

                                            //check if booking is cancellable or not, in case of activity detail page only, on 1st index of enrolment
                                            if (jsonMainParent != null && indexEnrolment == 0) {
                                                try {
                                                    if (jsonMainParent.has("current_date")) {
                                                        val strServerDateTime =
                                                            jsonMainParent.optString(
                                                                "current_date",
                                                                ""
                                                            )
                                                        val curJavaDate =
                                                            getFormattedServerDateJava(
                                                                strServerDateTime
                                                            )

                                                        //Log.d("CurDate",""+curJavaDate)
                                                        //Log.d("CancellationDate",""+bookingThresholdDate)

                                                        if (bookingThresholdDate != null && curJavaDate != null) {
                                                            //val duration = bookingThresholdDate.time -curJavaDate.time
                                                            //val diffInHours = TimeUnit.MILLISECONDS.toHours(duration)
                                                            //Log.d("HoursDifference",""+diffInHours)
                                                            model.isActivityBookingCancellable =
                                                                !curJavaDate.after(
                                                                    bookingThresholdDate
                                                                )
                                                        }

                                                        //Log.d("IsBookingCancellable", "" + model.isActivityBookingCancellable)

                                                        /*val jsonArrEnrolSlots = jsonObjEnrol.getJSONArray("get_enrolment_slot")
                                                    if (jsonArrEnrolSlots.length() > 0) {

                                                        val jsonObjEnrolSlotFirstItem = jsonArrEnrolSlots.getJSONObject(0)
                                                        val strEnrolStartDateTime = jsonObjEnrolSlotFirstItem.optString("enrolment_slot_date", " ") + " " + jsonObjEnrolSlotFirstItem.optString("enrolment_slot_time", " ")
                                                        val curEnrolStartDateJava = getFormattedServerDateJava(strEnrolStartDateTime)

                                                        Log.d("Dates___", "$strServerDateTime - $strEnrolStartDateTime")
                                                        val duration = curEnrolStartDateJava!!.time - curJavaDate!!.time
                                                        val diffInHours = TimeUnit.MILLISECONDS.toHours(duration)

                                                        Log.d("DifferenceHours","$diffInHours")
                                                        model.isActivityBookingCancellable = diffInHours >= model.activityCancellationHours
                                                    }*/
                                                    }
                                                } catch (e: Exception) {
                                                    Log.e("Exc_Dates_Cancelling", "" + e)
                                                }
                                            }
                                        }

                                    } else {
                                        slotModel.slotStatus = "Enquire"
                                        slotModel.slotTitle = strHeading
                                        arraySlotsAsItIs.add(slotModel)
                                        //model.arraySlotModel.add(slotModel)
                                    }

                                    /*if (model.arraySlotModel.size >= 4)
                                    slotModel.showMore = it == jsonArrTimeSlots.length() - 1
                                else
                                    slotModel.showMore = false*/

                                    //model.arraySlotModel.add(slotModel)

                                } catch (e: Exception) {
                                    Log.e("EnrolParseExc", "" + e)

                                }

                            }

                        }
                    }

                    if (jsonObj.has("get_time_slot_online")) {
                        var key =
                            if (jsonObj.has("get_time_slot_online")
                            ) "get_time_slot_online" else "get_time_slot_online"
                        val jsonArrTimeSlots = jsonObj.getJSONArray(key)

                        //Log.d("ActivityInfo_", model.activityId + " -" + model.activityName + " - Slots: " + jsonArr.length())
                        if (jsonArrTimeSlots.length() > 0) {

                            repeat(jsonArrTimeSlots.length()) { posSlot ->
                                val objSlot = jsonArrTimeSlots.getJSONObject(posSlot)

                                if (posSlot == 0) {
                                    model.activityLocation =
                                        MyPlaceUtils.replaceUAECountry(
                                            objSlot.optString(
                                                "venue_name",
                                                ""
                                            ).trim()
                                        )
                                    model.activityLocationFullAddress =
                                        objSlot.optString("venue_full_address", "").trim()
                                }

                                val slotModel =
                                    ActivitySlotModel(objSlot.optString("time_slot_id")).also { slotModel ->

                                        slotModel.slotActivityId = model.activityId

                                        slotModel.slotEnquireSortParam =
                                            objSlot.optString("time_slot_week_num", "0").toInt()

                                        //Log.d("Obj__SlotTime___",objSlot.optString("time_slot_start_from",""));
                                        slotModel.slotEnquireStartDateTime = getEnquireDateTime(
                                            objSlot.optString(
                                                "time_slot_week",
                                                ""
                                            ).capitalize() + " Jan 2017 " + objSlot.optString(
                                                "time_slot_start_from",
                                                ""
                                            )
                                        )

                                        slotModel.slotAge = objSlot.optString(
                                            "time_slot_age_group_label",
                                            ""
                                        ) + " yr"

                                        try {
                                            slotModel.slotAgeFrom =
                                                objSlot.optString("time_slot_age_from", "0")
                                                    .toDouble()
                                            slotModel.slotAgeTo =
                                                objSlot.optString("time_slot_age_to", "0")
                                                    .toDouble()
                                        } catch (e: Exception) {
                                            Log.e("Exc_ActivityExc1", "" + e)
                                        }


                                        //time calculation
                                        val strStartTime =
                                            getFormattedStartTime(objSlot.optString("time_slot_start_from"))
                                        val strEndTime = addMinutesToTime(
                                            strStartTime,
                                            objSlot.optInt("time_slot_duration", 0)
                                        )
                                        slotModel.slotTime = "$strStartTime - $strEndTime"

                                        slotModel.slotLocation =
                                            MyPlaceUtils.replaceUAECountry(
                                                objSlot.optString(
                                                    "venue_name",
                                                    ""
                                                ).trim()
                                            ).trim()
                                        slotModel.slotLocationFullAddress =
                                            objSlot.optString("venue_full_address", "").trim()

                                        try {
                                            slotModel.slotLat =
                                                objSlot.optString("venue_lat", "0.0").toDouble()
                                            slotModel.slotLong =
                                                objSlot.optString("venue_long", "0.0").toDouble()
                                        } catch (e: Exception) {
                                            Log.e("Exc_ActivityExc2", "" + e)
                                        }

                                        slotModel.slotVenuePlaceId =
                                            objSlot.optString("venue_place_id", "_").trim()

                                        slotModel.slotDistance =
                                            objSlot.optString("distance", "0.0").toDouble()
                                    }

                                var strHeading = objSlot.optString("time_slot_week", "")
                                if (strHeading.trim().length >= 3)
                                    strHeading = strHeading.substring(0, 3).trim()
                                        .toUpperCase(Locale.ENGLISH)

                                //enrolment details
                                try {
                                    val jsonArrEnrolment = objSlot.getJSONArray("get_enrolment")
                                    if (jsonArrEnrolment.length() > 0) {
                                        repeat(jsonArrEnrolment.length()) { indexEnrolment ->

                                            //Log.d("EnrolmentIndex",""+indexEnrolment)
                                            val jsonObjEnrol =
                                                jsonArrEnrolment.getJSONObject(indexEnrolment)

                                            val mySlotModel = slotModel.copy().also { mySlModel ->

                                                mySlModel.isSingleActivity =
                                                    jsonObjEnrol.optString(
                                                        "enrolment_slot_type",
                                                        ""
                                                    ).equals("single session", true)

                                                //copy properties from parent slot model
                                                mySlModel.slotActivityId = slotModel.slotActivityId
                                                mySlModel.slotEnquireSortParam =
                                                    slotModel.slotEnquireSortParam
                                                mySlModel.slotAgeFrom = slotModel.slotAgeFrom
                                                mySlModel.slotAgeTo = slotModel.slotAgeTo
                                                mySlModel.slotAge = slotModel.slotAge
                                                mySlModel.slotTime = slotModel.slotTime
                                                mySlModel.slotLat = slotModel.slotLat
                                                mySlModel.slotLong = slotModel.slotLong
                                                mySlModel.slotVenuePlaceId =
                                                    slotModel.slotVenuePlaceId
                                                mySlModel.slotDistance = slotModel.slotDistance

                                                mySlModel.slotLocation = slotModel.slotLocation
                                                mySlModel.slotLocationFullAddress =
                                                    slotModel.slotLocationFullAddress
                                                mySlModel.slotEnquireStartDateTime =
                                                    slotModel.slotEnquireStartDateTime

                                                //Log.d("SlotLoc__",mySlModel.slotLocation)
                                                //slotModel.slotLocation=MyPlaceUtils.replaceUAECountry(objSlot.optString("venue_name", "").trim()).trim()
                                                mySlModel.slotStatus = "Available"

                                                //mySlModel.slotTitle = "From ${strHeading.toUpperCase()}, " + getFormattedDate(jsonObjEnrol.optString("enrolment_start_date", "")).toUpperCase()
                                                var appendFrom = ""
                                                appendFrom = if (mySlModel.isSingleActivity)
                                                    ""
                                                else
                                                    "From"

                                                mySlModel.slotTitle =
                                                    appendFrom + " ${getFormattedDateEnrolmentStartValid(
                                                        jsonObjEnrol.optString(
                                                            "next_batch_date",
                                                            ""
                                                        )
                                                    ).trim()}"

                                                if (jsonObjEnrol.optString("next_batch_date", "")
                                                        .contains(" ")
                                                ) {
                                                    try {
                                                        val strStartTimeSlot =
                                                            jsonObjEnrol.optString(
                                                                "next_batch_date",
                                                                ""
                                                            ).split(" ")[1]
                                                        val strStartTimeToDisplay =
                                                            getFormattedStartTime(strStartTimeSlot)
                                                        val strEndTimeToDisplay = addMinutesToTime(
                                                            strStartTimeToDisplay,
                                                            jsonObjEnrol.optString(
                                                                "next_batch_duration",
                                                                "0"
                                                            ).toInt()
                                                        )
                                                        slotModel.slotTime =
                                                            "$strStartTimeToDisplay - $strEndTimeToDisplay"
                                                        mySlModel.slotTime = slotModel.slotTime
                                                    } catch (e: Exception) {
                                                        Log.e("TimeSlotExcNextBatch", "" + e)
                                                    }
                                                }


                                                mySlModel.slotNextAvailableSessionDate =
                                                    getFormattedServerDateJava(
                                                        jsonObjEnrol.optString(
                                                            "next_batch_date",
                                                            ""
                                                        )
                                                    )


                                                mySlModel.slotType =
                                                    jsonObjEnrol.optString("enrolment_type", "")
                                                mySlModel.slotPrice =
                                                    jsonObjEnrol.optString("enrolment_price", "")

                                                mySlModel.slotEnrolStartDate =
                                                    getFormattedDateTimeTable(
                                                        jsonObjEnrol.optString(
                                                            "enrolment_start_date",
                                                            ""
                                                        )
                                                    )
                                                mySlModel.slotEnrolEndDate =
                                                    getFormattedDateTimeTable(
                                                        jsonObjEnrol.optString(
                                                            "enrolment_end_date",
                                                            ""
                                                        )
                                                    )

                                                mySlModel.slotEnrolmentId =
                                                    jsonObjEnrol.optString("enrolment_id", "")
                                                //Log.d("SlotEnrolmentId_",slotModel.slotEnrolmentId)

                                                try {
                                                    mySlModel.noOfSessions =
                                                        jsonObjEnrol.getString("enrolment_session")
                                                            .toInt()
                                                } catch (e: Exception) {
                                                    Log.e("NoOfSessionsExc", "" + e)
                                                    mySlModel.noOfSessions = 0
                                                }

                                                try {
                                                    mySlModel.arrayEnrolModel =
                                                        ArrayList<EnrolModel>()
                                                    if (jsonObjEnrol.has("get_enrolment_slot")) {
                                                        val jsonArrEnrol =
                                                            jsonObjEnrol.getJSONArray("get_enrolment_slot")
                                                        if (jsonArrEnrol.length() > 0) {
                                                            repeat(jsonArrEnrol.length()) { indexEnrol ->
                                                                val objEnrol =
                                                                    jsonArrEnrol.getJSONObject(
                                                                        indexEnrol
                                                                    )
                                                                mySlModel.arrayEnrolModel.add(
                                                                    EnrolModel(
                                                                        objEnrol.optString(
                                                                            "enrolment_slot_id",
                                                                            ""
                                                                        )
                                                                    ).also { enrolModel ->
                                                                        enrolModel.enrolDate =
                                                                            getFormattedDateTimeTable(
                                                                                objEnrol.optString(
                                                                                    "enrolment_slot_date",
                                                                                    ""
                                                                                )
                                                                            )
                                                                        enrolModel.enrolDay =
                                                                            objEnrol.getString("enrolment_slot_week")
                                                                                .trim().capitalize()

                                                                        //time calculation
                                                                        val strStartTimeEnrol =
                                                                            getFormattedStartTime(
                                                                                objEnrol.optString("enrolment_slot_time")
                                                                            )
                                                                        val strEndTimeEnrol =
                                                                            addMinutesToTime(
                                                                                strStartTimeEnrol,
                                                                                objEnrol.optString(
                                                                                    "enrolment_slot_duration",
                                                                                    "0"
                                                                                ).toInt()
                                                                            )

                                                                        enrolModel.enrolTime =
                                                                            "$strStartTimeEnrol - $strEndTimeEnrol"
                                                                        enrolModel.enrolDuration =
                                                                            objEnrol.optString(
                                                                                "enrolment_slot_duration",
                                                                                "0"
                                                                            )

                                                                        val strEnrolStartDateTime =
                                                                            objEnrol.optString(
                                                                                "enrolment_slot_date",
                                                                                ""
                                                                            ) + " " + objEnrol.optString(
                                                                                "enrolment_slot_time"
                                                                            )

                                                                        //Log.d("StrEnrolStartDateTime",strEnrolStartDateTime);
                                                                        enrolModel.enrolDateJava =
                                                                            getFormattedServerDateJava(
                                                                                strEnrolStartDateTime
                                                                            )

                                                                        //calculate if cur server date is ahead of enrol date, for indication in time table (grey background) and for no of sessions to be paid by the user
                                                                        if (jsonMainParent != null) {
                                                                            if (jsonMainParent.has("current_date")) {
                                                                                val strServerDate =
                                                                                    jsonMainParent.optString(
                                                                                        "current_date",
                                                                                        ""
                                                                                    )
                                                                                //Log.d("ServerDate", strServerDate)
                                                                                val currentServerDate =
                                                                                    getFormattedServerDateJava(
                                                                                        strServerDate
                                                                                    )

                                                                                model.curServerDateJava =
                                                                                    currentServerDate

                                                                                //Log.d("FormattedServerDate", "" + currentServerDate)
                                                                                if (currentServerDate != null && enrolModel.enrolDateJava != null) {
                                                                                    Log.d(
                                                                                        "DT_CurDate",
                                                                                        "" + currentServerDate
                                                                                    )
                                                                                    Log.d(
                                                                                        "DT_EnrolDate",
                                                                                        "" + enrolModel.enrolDateJava
                                                                                    )
                                                                                    //Log.d("FormattedEnrolDate", "" + enrolModel.enrolDateJava)
                                                                                    enrolModel.isDateAlreadyPassed =
                                                                                        currentServerDate.after(
                                                                                            enrolModel.enrolDateJava
                                                                                        ) //|| currentServerDate.equals(enrolModel)
                                                                                    //Log.d("DateAlreadyPassed",""+enrolModel.isDateAlreadyPassed)
                                                                                    //Log.d("isDateAlreadyPassed", enrolModel.isDateAlreadyPassed.toString())
                                                                                    //Log.d("-------------", "----------------------")
                                                                                }
                                                                            }
                                                                        }
                                                                    })
                                                            }
                                                        }
                                                    }


                                                    //calculate price on the basis of enrolment slots to attend and show the same in activity listing slots
                                                    try {
                                                        val singlePrice =
                                                            decimalFormat.format(mySlModel.slotPrice.toDouble())
                                                                .toDouble()
                                                        val countSessionsToAttend =
                                                            mySlModel.arrayEnrolModel.filter { enrolModel -> !enrolModel.isDateAlreadyPassed }.size
                                                        mySlModel.noOfEligibleSessions =
                                                            countSessionsToAttend
                                                        mySlModel.slotPricePayable =
                                                            "" + (countSessionsToAttend * singlePrice)
                                                        mySlModel.slotPricePayableToDisplay =
                                                            "" + Math.round(mySlModel.slotPricePayable.toDouble())
                                                        if (mySlModel.slotPricePayable.trim()
                                                                .endsWith(".0")
                                                        ) {
                                                            mySlModel.slotPricePayableToDisplay =
                                                                mySlModel.slotPricePayable.substring(
                                                                    0,
                                                                    mySlModel.slotPricePayable.length - 2
                                                                )
                                                        }


                                                    } catch (e: Exception) {
                                                        Log.e("ExcJsonArrEnrol", "" + e)
                                                        mySlModel.slotPricePayable =
                                                            mySlModel.slotPrice
                                                        mySlModel.slotPricePayableToDisplay =
                                                            mySlModel.slotPricePayable
                                                    }


                                                } catch (e: Exception) {
                                                    Log.e("ExcJsonArrEnrol", "" + e)
                                                }
                                            }


                                            //model.arraySlotModel.add(mySlotModel)
                                            arraySlotsAsItIs.add(mySlotModel)

                                            //check if booking is cancellable or not, in case of activity detail page only, on 1st index of enrolment
                                            if (jsonMainParent != null && indexEnrolment == 0) {
                                                try {
                                                    if (jsonMainParent.has("current_date")) {
                                                        val strServerDateTime =
                                                            jsonMainParent.optString(
                                                                "current_date",
                                                                ""
                                                            )
                                                        val curJavaDate =
                                                            getFormattedServerDateJava(
                                                                strServerDateTime
                                                            )

                                                        //Log.d("CurDate",""+curJavaDate)
                                                        //Log.d("CancellationDate",""+bookingThresholdDate)

                                                        if (bookingThresholdDate != null && curJavaDate != null) {
                                                            //val duration = bookingThresholdDate.time -curJavaDate.time
                                                            //val diffInHours = TimeUnit.MILLISECONDS.toHours(duration)
                                                            //Log.d("HoursDifference",""+diffInHours)
                                                            model.isActivityBookingCancellable =
                                                                !curJavaDate.after(
                                                                    bookingThresholdDate
                                                                )
                                                        }

                                                        //Log.d("IsBookingCancellable", "" + model.isActivityBookingCancellable)

                                                        /*val jsonArrEnrolSlots = jsonObjEnrol.getJSONArray("get_enrolment_slot")
                                                    if (jsonArrEnrolSlots.length() > 0) {

                                                        val jsonObjEnrolSlotFirstItem = jsonArrEnrolSlots.getJSONObject(0)
                                                        val strEnrolStartDateTime = jsonObjEnrolSlotFirstItem.optString("enrolment_slot_date", " ") + " " + jsonObjEnrolSlotFirstItem.optString("enrolment_slot_time", " ")
                                                        val curEnrolStartDateJava = getFormattedServerDateJava(strEnrolStartDateTime)

                                                        Log.d("Dates___", "$strServerDateTime - $strEnrolStartDateTime")
                                                        val duration = curEnrolStartDateJava!!.time - curJavaDate!!.time
                                                        val diffInHours = TimeUnit.MILLISECONDS.toHours(duration)

                                                        Log.d("DifferenceHours","$diffInHours")
                                                        model.isActivityBookingCancellable = diffInHours >= model.activityCancellationHours
                                                    }*/
                                                    }
                                                } catch (e: Exception) {
                                                    Log.e("Exc_Dates_Cancelling", "" + e)
                                                }
                                            }
                                        }

                                    } else {
                                        slotModel.slotStatus = "Enquire"
                                        slotModel.slotTitle = strHeading
                                        arraySlotsAsItIs.add(slotModel)
                                        //model.arraySlotModel.add(slotModel)
                                    }

                                    /*if (model.arraySlotModel.size >= 4)
                                    slotModel.showMore = it == jsonArrTimeSlots.length() - 1
                                else
                                    slotModel.showMore = false*/

                                    //model.arraySlotModel.add(slotModel)

                                } catch (e: Exception) {
                                    Log.e("EnrolParseExc", "" + e)

                                }

                            }

                        }
                    }

                    //first group into 2 arrays as per status, then we will consider only unique enrolment id then we sort according to date & time
                    val arrayAvailableSlots =
                        arraySlotsAsItIs.filter { it.slotStatus.equals("Available", true) }
                            .distinctBy { it.slotEnrolmentId }
                            .sortedBy { it.slotNextAvailableSessionDate }

                    val arrayEnquireSlots =
                        arraySlotsAsItIs.filter { it.slotStatus.equals("Enquire", true) }
                            .sortedBy { it.slotEnquireStartDateTime }

                    model.arraySlotModel.clear()
                    model.arraySlotModel.addAll(arrayAvailableSlots)
                    model.arraySlotModel.addAll(arrayEnquireSlots)

                    val countOfLocations =
                        model.arraySlotModel.distinctBy { slModel -> slModel.slotVenuePlaceId }.size
                    model.activityMsgLocationSuffix =
                        if (countOfLocations > 1) "+${countOfLocations - 1} more" else ""

                    //Log.d("ActivityInfoArraySlot_", "Slot Size: " + model.arraySlotModel.size)


                    if (jsonObj.has("get_flexi_activity") || jsonObj.has("get_flexi_activity_premise") || jsonObj.has("get_flexi_activity_online")) {

                        val key =
                            if (jsonObj.has("get_flexi_activity") &&
                                jsonObj.optJSONArray("get_flexi_activity")?.length()!! > 0) "get_flexi_activity"
                            else if (jsonObj.has("get_flexi_activity_premise") &&
                                jsonObj.optJSONArray("get_flexi_activity_premise")?.length()!! > 0) "get_flexi_activity_premise"
                            else "get_flexi_activity_online"


                        val jarFlexi = jsonObj.getJSONArray(key)
                        val arrFlexiModel = ArrayList<ActivityFlexiSessionModel>()
                        /* repeat(jarFlexi.length())
                         {*/
                        if (jarFlexi.length() > 0)
                            jarFlexi.getJSONObject(0).run {
                                model.activityFlexiId = optString("flexi_activity_id", "")
                                val ageGroup = optString("flexi_act_age_group", "")
                                val fromAge = optString(
                                    "flexi_act_age_from",
                                    ""
                                )//ageGroup.substring(0,ageGroup.indexOf("-"))//
                                val toAge = optString(
                                    "flexi_act_age_to",
                                    ""
                                )//ageGroup.substring(ageGroup.indexOf("-")+1)//




                                val venueName = if (key == "get_flexi_activity_online")
                                    optString("venue_name", "Online Class")
                                else
                                    optString("venue_name", "At Your Premise")





                                /* try {
                                     if (optString("your_premise_status","0")=="1") {
                                         venueName="Your premise"
                                     }
                                     else
                                     {
                                         venueName = getJSONObject("get_venue").optString("venue_name", "")
                                     }
                                 }catch (e:Exception){
                                     Log.e("venueName",e.toString())
                                 }*/

                                model.activityLocation = venueName
                                val flexiActType = optString("flexi_act_type", "")
                                val fullAddress = optString("venue_full_address", "")
                                val lat = optString("venue_lat", "")
                                val lng = optString("venue_long", "")

                                val jarTimeSlot = getJSONArray("get_price_management")
                                var startingPrice = 0.0
                                var startingSessionNumber = ""
                                repeat(jarTimeSlot.length())
                                {

                                    jarTimeSlot.getJSONObject(it).run {
                                        val price = getString("price")
                                        var priceRoundedToDisplay: Int
                                        try {
                                            priceRoundedToDisplay = price.toDouble().roundToInt()
                                        } catch (e: Exception) {
                                            priceRoundedToDisplay = 0
                                        }
                                        if (it == 0 || startingPrice > price.toDouble()) {
                                            startingPrice = price.toDouble()
                                            startingSessionNumber = getString("no_of_sessions")
                                        }

                                        ActivityFlexiSessionModel(
                                            getString("flexiact_price_manage_id"),
                                            getString("no_of_sessions"),
                                            "$ageGroup yrs",
                                            price,
                                            convertMinutesToHours(getInt("duration")),
                                            venueName,
                                            flexiActType,
                                            fullAddress,
                                            lat,
                                            lng,
                                            getString("valid_days"), fromAge, toAge,
                                            priceRoundedToDisplay = priceRoundedToDisplay
                                        ).also {
                                            it.intDuration = getString("duration")
                                            arrFlexiModel.add(it)
                                        }
                                    }
                                }

                                model.flexiStartingPrice = startingPrice.toString()
                                model.flexiStartingSessionNumber = startingSessionNumber

                                if (has("get_timeslot")) {
                                    val jarTimeTable = getJSONArray("get_timeslot")
                                    val arrAllColumnFlexiTimeTable =
                                        ArrayList<ArrayList<FlexiTimeTableModel?>?>()
                                    var arrFlexiTimeTableModel = ArrayList<FlexiTimeTableModel?>()
                                    //  var previousDay = ""
                                    var maxColumnCount = 0
                                    //   var lastDayWas = 0
                                    repeat(jarTimeTable.length())
                                    {
                                        val job = jarTimeTable.getJSONObject(it)
                                        /* if ((lastDayWas + 1) < job.getString("flexiact_timeslot_week_num").toInt()) {
                                             arrFlexiTimeTableModel = ArrayList<FlexiTimeTableModel?>()
                                             arrFlexiTimeTableModel.add(null)
                                         }*/
                                        job.run {
                                            FlexiTimeTableModel(
                                                ChangeDateFormat.convertDate(
                                                    getString("flexiact_timeslot_start_from"),
                                                    "HH:mm:ss",
                                                    "HH:mm"
                                                ),
                                                ChangeDateFormat.convertDate(
                                                    getString("flexiact_timeslot_start_to"),
                                                    "HH:mm:ss",
                                                    "HH:mm"
                                                ),
                                                getString("flexiact_timeslot_week"),
                                                getString("flexiact_timeslot_week_num").toInt()
                                            ).also {
                                                // if (it.dayName == previousDay || previousDay == "")
                                                //       arrFlexiTimeTableModel.add(it)
                                                //   else {
                                                /* if (maxColumnCount < arrFlexiTimeTableModel.size) {
                                                     maxColumnCount = arrFlexiTimeTableModel.size
                                                 }*/
                                                //   arrAllColumnFlexiTimeTable.add(arrFlexiTimeTableModel)

                                                //   arrFlexiTimeTableModel = ArrayList<FlexiTimeTableModel?>()
                                                arrFlexiTimeTableModel.add(it)
                                                //    }
                                                //  previousDay = it.dayName
                                                //    lastDayWas = it.dayNumber
                                            }
                                        }
                                    }

                                    for (i in 1..7) {
                                        try {
                                            //dayName
                                            val list = arrFlexiTimeTableModel.filter {
                                                (StaticValues.arrDays.indexOf(it!!.dayName) + 1) == i
                                            } as ArrayList<FlexiTimeTableModel?>
                                            //  arrFlexiTimeTableModel.filter { it!!.dayNumber == i } as ArrayList<FlexiTimeTableModel?>
                                            if (maxColumnCount < list.size) {
                                                maxColumnCount = list.size
                                            }
                                            arrAllColumnFlexiTimeTable.add(list)
                                        } catch (e: Exception) {
                                            Log.e("noDataFound", e.toString())
                                            arrAllColumnFlexiTimeTable.add(null)
                                        }
                                    }

                                    val arrayTimeTableRowMajor =
                                        ArrayList<ArrayList<FlexiTimeTableModel?>>()
                                    repeat(maxColumnCount)
                                    { row ->
                                        val arrayOfRow = ArrayList<FlexiTimeTableModel?>()
                                        repeat(arrAllColumnFlexiTimeTable.size)
                                        { column ->
                                            val arrayList = arrAllColumnFlexiTimeTable[column]
                                            if (arrayList != null && arrayList.size > row) {
                                                arrayOfRow.add(arrayList[row])
                                            } else {
                                                arrayOfRow.add(null)
                                            }
                                        }
                                        arrayTimeTableRowMajor.add(arrayOfRow)

                                    }
                                    model.arrFlexiTimeTable = arrayTimeTableRowMajor
                                    // model.maxColumnTimeTable=maxColumnCount
                                }
                            }
                        //   }

                        model.arrFlexiSession = arrFlexiModel
                    }

                    val jsonArrReview = jsonObj.getJSONArray("get_review_details");
                    if (jsonArrReview.length() > 0) {
                        val jsonObjReview = jsonArrReview.getJSONObject(0)
                        try {
                            model.activityStarRating =
                                jsonObjReview.optString("avg_rate", "0").toFloat()
                            model.activityStarRatingCount = jsonObjReview.optInt("count", 0)
                        } catch (e: Exception) {
                            Log.e("NumFormatStar", "" + e)
                        }
                    }

                    try {
                        val jsonArrImages = jsonObj.getJSONArray("get_image_gallery")
                        model.arrayImages.clear()
                        model.arrayImages.add(jsonObj.optString("activity_feature_img", ""))
                        if (jsonArrImages.length() > 0) {
                            repeat(jsonArrImages.length()) { imageIndex ->
                                val objImage = jsonArrImages.getJSONObject(imageIndex)
                                model.arrayImages.add(objImage.getString("photo_gallery_image"))
                            }
                        }
                    } catch (e: Exception) {
                        Log.e("ImageGalleryExc", "" + e)
                    }

                    try {
                        val jsonArrVideos = jsonObj.getJSONArray("get_video_gallery")
                        if (jsonArrVideos.length() > 0) {
                            repeat(jsonArrVideos.length()) { imageVideo ->
                                val objVideo = jsonArrVideos.getJSONObject(imageVideo)
                                model.arrayImages.add(
                                    "http://img.youtube.com/vi/" + objVideo.getString(
                                        "video_gallery"
                                    ) + "/hqdefault.jpg"
                                )
                            }
                        }
                    } catch (e: Exception) {
                        // Log.e("Exc_ActivityExc3", "" + e)
                    }

                    // set promotion related data in activity model
                    PromotionOperation.setPromotionDataInActModel(
                        jsonObj,
                        model,
                        model.activityProviderId,
                        arraySlotsAsItIs,
                        model.arrFlexiSession,
                        jsonMainParent
                    )

                }
        } catch (e: Exception) {
            Log.e("Exc_ActivityExc4", "" + e)
            return ActivityModel()
        }

    }

    fun convertMinutesToHours(minutes: Int): String {
        if (minutes < 60) {
            if (minutes == 1)
                return "$minutes Minute"
            else
                return "$minutes Minutes"
        } else {
            val hour = minutes / 60
            val remainingMinutes = minutes % 60

            var hourLabel = "Hr"
            if (hour > 1)
                hourLabel = "Hrs"

            var remainingMInuteLabel = "Min"
            if (remainingMinutes > 1)
                remainingMInuteLabel = "Mins"

            if (remainingMinutes == 0) {
                return "$hour $hourLabel"
            } else {
                return "$hour $hourLabel $remainingMinutes $remainingMInuteLabel"
            }
        }
    }

    private fun getFormattedStartTime(date: String): String {
        var da: Date? = null
        var parsedDate = ""
        var sdf = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
        try {
            da = sdf.parse(date)
            sdf = SimpleDateFormat("HH:mm", Locale.ENGLISH)
            parsedDate = sdf.format(da)
            return parsedDate.toLowerCase(Locale.ENGLISH).trim()

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    private fun addMinutesToTime(strFromTime: String, minutes: Int = 0): String {
        try {
            val sdf = SimpleDateFormat("HH:mm", Locale.ENGLISH)
            val strTime = sdf.parse(strFromTime)
            val cal = Calendar.getInstance()
            cal.time = strTime
            cal.add(Calendar.MINUTE, minutes)
            return sdf.format(cal.time).toLowerCase(Locale.ENGLISH).trim()
        } catch (e: Exception) {
            Log.d("Exc_Time", "" + e)
            return ""
        }
    }

    private fun getFormattedDate(date: String): String {
        var da: Date? = null
        var parsedDate = ""
        var sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        try {
            da = sdf.parse(date)
            sdf = SimpleDateFormat("MMM dd", Locale.ENGLISH)
            parsedDate = sdf.format(da)
            return parsedDate.toUpperCase(Locale.ENGLISH).trim()

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    fun getFormattedDateTimeTable(date: String): String {
        var da: Date? = null
        var parsedDate = ""
        var sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        try {
            da = sdf.parse(date)
            sdf = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
            parsedDate = sdf.format(da)
            return parsedDate.trim()

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    fun getEnquireDateTime(time: String): Date? {
        val sdf = SimpleDateFormat("EEEE MMM yyyy HH:mm:ss", Locale.ENGLISH)
        try {
            val dt = sdf.parse(time)
            //Log.d("Date__Enquire",""+dt)
            return dt
        } catch (e: Exception) {

        }
        return null
    }

    fun getFormattedDateCancellation(date: String): String {
        var da: Date? = null
        var parsedDate = ""
        var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            da = sdf.parse(date)
            sdf = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ENGLISH)
            parsedDate = sdf.format(da)
            return parsedDate.trim()

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    fun getFormattedDateCancellation(date: Date): String {
        var parsedDate = ""
        try {
            val sdf = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ENGLISH)
            parsedDate = sdf.format(date)
            return parsedDate.trim()

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    fun getFormattedDateTimeTableReverse(date: String): String {
        var da: Date? = null
        var parsedDate = ""
        var sdf = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        try {
            da = sdf.parse(date)
            sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            parsedDate = sdf.format(da)
            return parsedDate.trim()

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }


    fun getFormattedServerDateJava(
        strDate: String,
        strSdfFormat: String = "yyyy-MM-dd HH:mm:ss"
    ): Date? {
        val sdf = SimpleDateFormat(strSdfFormat, Locale.ENGLISH)
        try {
            return sdf.parse(strDate)
        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return null
    }


    fun getFormattedServerDateJavaReverse(date: Date?): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            return sdf.format(date)
        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    fun getFormattedDateEnrolmentStartValid(date: String): String {
        var da: Date? = null
        var parsedDate = ""
        var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            da = sdf.parse(date)
            sdf = SimpleDateFormat("EEE, MMM dd", Locale.ENGLISH)
            parsedDate = sdf.format(da)
            val strDay = parsedDate.substring(0, 3).trim().toUpperCase(Locale.ENGLISH)
            val strRest = parsedDate.substring(3, parsedDate.length).trim()
            return strDay + strRest

        } catch (e: ParseException) {
            Log.v("DateParse", "" + e)
        }

        return ""
    }

    private fun filterDateForBookings(date: String, strSdfFormat: String = "yyyy-MM-dd"): String {
        val da: Date
        var parsedMonth = ""
        var parsedDate = ""
        val sdf = SimpleDateFormat(strSdfFormat, Locale.ENGLISH)
        try {
            da = sdf.parse(date)

            val sdfMonth = SimpleDateFormat("MMM, yyyy", Locale.ENGLISH)
            val sdfDate = SimpleDateFormat("d", Locale.ENGLISH)

            parsedMonth = sdfMonth.format(da)
            parsedDate = sdfDate.format(da)

            parsedDate += getDateSuffix(parsedDate.toInt())

            return "$parsedDate $parsedMonth"
        } catch (e: ParseException) {
            Log.d("DateParse", "" + e)
        }

        return date
    }

    private fun getDateSuffix(intDay: Int): String {
        val suffix: String
        val sufixes = arrayOf("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th")
        when (intDay % 100) {
            11, 12, 13 -> suffix = "th"
            else -> suffix = sufixes[intDay % 10]
        }

        return suffix
    }
}
package com.pursueit.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.fcm.MyFirebaseMessagingService
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.model.PlaceModel
import com.pursueit.model.UserModel
import java.util.*
import kotlin.concurrent.schedule


object LoginBasicsUi {
    fun setTransparentStatusBarAndBgImage(act: AppCompatActivity, img: ImageView? = null) {
        act.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            act.window.statusBarColor = Color.TRANSPARENT
        }

        if (img != null)
            Glide.with(act).load(R.drawable.bg_main).into(img)
    }


    //if navigateAlso is false it means we only need to reset data and need not navigate to MainActivity
    fun logOutUser(act:Activity, navigateAlso:Boolean=true){
        FastNetworking.compositeDisposable.clear()
        resetAllUserTokensAndId(act.applicationContext)
        MyPlaceUtils.placeModel= PlaceModel()
        DashboardActivity.ageFrom=""
        DashboardActivity.ageTo=""
        DashboardActivity.selectedSearchModel=null

        //MyFirebaseMessagingService.unsubscribeTopicAll()

        if(navigateAlso) {
            Timer().schedule(100) {
                MyNavigations.goToMainActivity(act)
            }
        }

        //act.finishAffinity()
    }

    fun resetAllUserTokensAndId(context:Context){
        val userModel=UserModel("")
        UserModel.setUserModel(context, userModel)
        StaticValues.AUTH_TOKEN=""
        StaticValues.putHeaders(context,"")
        MyProfileUtils.arrayFamilyModel.clear()
        MyProfileUtils.arrayMyAddressModel.clear()

    }

}
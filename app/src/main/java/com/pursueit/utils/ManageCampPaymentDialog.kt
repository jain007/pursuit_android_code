package com.pursueit.utils

import android.app.Dialog
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.activities.CampDetailActivity
import com.pursueit.adapters.AttendeeAdapter
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CampDiscountModel
import com.pursueit.model.PromotionModel
import kotlinx.android.synthetic.main.activity_camp_detail.*
import kotlinx.android.synthetic.main.dialog_camp_payment.*
import kotlinx.android.synthetic.main.layout_apply_promotion.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.text.DecimalFormat

class ManageCampPaymentDialog(val activity: CampDetailActivity) {
    private var wasFetchMembersHit = false
    private lateinit var mySnackBar: MySnackBar
    var dialogCampPayment: Dialog? = null

    var totalPriceToPay = 0.0
    var siblingDiscountedPrice = 0.0
    var earlyRegDiscountPrice = 0.0
    var totalBeforeTax = 0.0
    var vatPrice = 0.0
    private val arrSelectedFamilyMember = ArrayList<String>()
    var arrPromotionModel=ArrayList<PromotionModel>()

    var totalAfterPromotionApplied:Double=0.0

    fun fastFetchFamilyMembersFirstThenShowDialog() {
        if (ConnectionDetector.isConnectingToInternet(activity.applicationContext)) {
            DialogMsg.showPleaseWait(activity)
            MyProfileUtils.fastFetchFamilyMembers(
                activity.applicationContext,
                { success: Boolean, jsonObject: JSONObject?, anError: ANError? ->
                    DialogMsg.dismissPleaseWait(activity)
                    wasFetchMembersHit = true
                    setup()
                },
                activity.compositeDisposable
            )
        } else {
            mySnackBar.showSnackBarError(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }

    fun setup() {
        arrPromotionModel.clear()
        activity.campModel?.arrPromotionModel?.forEach { it.isApplied=false }
        arrPromotionModel.addAll(activity.campModel?.arrPromotionModel?:ArrayList())

        wasFetchMembersHit = false
        mySnackBar = MySnackBar(activity, activity.coordinatorCampDetail)

        if (dialogCampPayment == null)
            dialogCampPayment = MyDialog(activity).getMyDialog(R.layout.dialog_camp_payment)

        if (arrPromotionModel.size==0)
        {
            dialogCampPayment!!.cnsApplyPromoCode.visibility=View.GONE
        }

        dialogCampPayment!!.setCancelable(false)

        if (activity.campModel != null) {
            dialogCampPayment!!.txtCampTitleDialog.text = activity.campModel!!.campName
            dialogCampPayment!!.txtLocationDialog.text = activity.campModel!!.address
            dialogCampPayment!!.txtCampServiceProviderDialog.text = activity.campModel!!.serviceProviderName
            dialogCampPayment!!.txtAgeRange.text = activity.campModel!!.ageRageData
            dialogCampPayment!!.txtTimeRange.text = activity.campModel!!.timeRangeData

            Glide.with(activity.applicationContext)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_CAMP + activity.campModel!!.campImage)
                .into(dialogCampPayment!!.imgCampDialog)

        } else {
            return
        }

        dialogCampPayment!!.rvAttendees.layoutManager = LinearLayoutManager(activity.applicationContext)

        MyProfileUtils.arrayFamilyModel.forEach {
            it.isSelected = false
        }

        if (arrSelectedFamilyMember.size > 0) {
            MyProfileUtils.arrayFamilyModel.forEach {
                it.isSelected = arrSelectedFamilyMember.contains(it.familyId)
            }
        }

        val adapter = AttendeeAdapter(activity, MyProfileUtils.arrayFamilyModel, { pos, isChecked, adapter ->
            val model = adapter.arrayModel[pos]
            if (!arrSelectedFamilyMember.contains(model.familyId)) {
                arrSelectedFamilyMember.add(model.familyId)
            }

            val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
            calculateTotal(numberOfSelectedPerson)
            /*MyProfileUtils.arrayFamilyModel.forEach {
                it.isSelected = false
            }
            val model = MyProfileUtils.arrayFamilyModel[pos]
            model.isSelected = isChecked
            MyProfileUtils.arrayFamilyModel.set(pos, model)
            adapter.notifyDataSetChanged()*/
        }, {

        })
        dialogCampPayment!!.rvAttendees.adapter = adapter

        if (activity.selectedDayOrWeekPriceModel != null) {
            dialogCampPayment!!.txtSchedule.text = activity.selectedDayOrWeekPriceModel!!.sessionName
            dialogCampPayment!!.txtSelectSchedule.text = activity.selectedDayOrWeekPriceModel!!.sessionName
            dialogCampPayment!!.txtCampLabel.text = activity.selectedDayOrWeekPriceModel!!.sessionName
            dialogCampPayment!!.txtFullDayPriceDialog.text = "AED " + activity.selectedDayOrWeekPriceModel!!.price

            val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
            calculateTotal(numberOfSelectedPerson)
        }

        dialogCampPayment!!.relSelectSchedule.setOnClickListener {
            dialogCampPayment!!.imgClose.performClick()
            activity.showBottomSheetDialogForScheduleSelection()
        }

        /* dialogFlexiActivityPayment!!.imgAddAttendee.setOnClickListener {
             dialogFlexiActivityPayment!!.dismiss()
             MyProfileUtils.showAddMemberDialog(activity, {
                 fastFetchFamilyMembersFirstThenShowDialog()
             })
         }*/

        dialogCampPayment!!.imgClose.setOnClickListener {
            // Except dismissal of dialog in case of add attendee all other control will come here to dismiss dialog
            arrSelectedFamilyMember.clear()  // clearing array here because it means that it is not dismissed in case of add attendee
            if (dialogCampPayment!!.isShowing)
                dialogCampPayment!!.dismiss()
        }

        dialogCampPayment!!.imgAddAttendee.setOnClickListener {
            dialogCampPayment!!.dismiss()
            MyProfileUtils.showAddMemberDialog(activity, {
                fastFetchFamilyMembersFirstThenShowDialog()
            }, onDismissListener =  // if user dismissing the add member dialog
            { isMemberAdded ->
                if (isMemberAdded) {

                } else {               // show payment dialog again if user didn't added member
                    setup()
                }
            })
        }

        dialogCampPayment!!.btnMakePayment.setOnClickListener {

            if (MyProfileUtils.arrayFamilyModel.size < 1) {
                activity.showErrorDialog("Please add at least one attendee to proceed")
                return@setOnClickListener
            }

            val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
            if (numberOfSelectedPerson < 1) {
                activity.showErrorDialog("Please select one attendee to proceed")
                return@setOnClickListener
            }

            dialogCampPayment!!.imgClose.performClick()
            val attendeeIds = StringBuilder()
            val arrSelectedArray = adapter.arrayModel.filter { it.isSelected }
            arrSelectedArray.forEach {
                attendeeIds.append("," + it.familyId)
            }


            val weekId = StringBuilder()
            val arrSelectedWeeks = activity.campModel!!.arrWeekModel.filter { it.isSelected }
            arrSelectedWeeks.forEach {
                weekId.append("," + it.id)
            }

            //New work tried
            /*//first check for user's phone number
            val userModel=UserModel.getUserModel(activity.applicationContext)
            if (userModel.userPhone.trim().isEmpty()) {
                dialogCampPayment!!.dismiss()
                UserProfileUtils.showEnterPhoneDialog(activity){ dialog, phone ->
                    //dialogPhoneNumber=dialog
                    dialog.dismiss()
                    SendPhoneOtp(activity).sendOtp(phone) { otp->
                        MyNavigations.goToEnterOtpForPhone(activity,otp,phone)
                    }
                }
            }else{

            }*/

            fastCampBooking(attendeeIds.replaceFirst(",".toRegex(), ""), weekId.replaceFirst(",".toRegex(), ""))


        }

        dialogCampPayment!!.cnsApplyPromoCode.setOnClickListener {
            PromotionOperation.showApplyPromoCodeDialog(arrPromotionModel,activity,totalPriceToPay.toDouble()
            ) { totalDiscountPrice:Double, discountedPrice:Double->
                val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
                calculateTotal(numberOfSelectedPerson) //,totalDiscountPrice,discountedPrice
            }
        }

        dialogCampPayment!!.show()
        //ApplyPromotionAdapter.clearSelectionArray()
    }




    private fun calculateTotal(noOfAttendees: Int) {
        val originalNumberOfAttendee=noOfAttendees
        val numberOfAttendee =if (noOfAttendees==0) 1 else noOfAttendees
        totalPriceToPay = 0.0
        siblingDiscountedPrice = 0.0
        earlyRegDiscountPrice = 0.0
        totalBeforeTax = 0.0
        vatPrice = 0.0

        var basePrice = activity.selectedDayOrWeekPriceModel!!.price.toDouble()
        dialogCampPayment!!.txtCampBasePrice.text = "AED ${DecimalFormat("##.##").format(basePrice)}"

        if (dialogCampPayment != null) {


            doAsync {

                // calculating early registration discount
                try {
                    if (numberOfAttendee > 0) {
                        if (activity.campModel?.arrDiscountModel!!.size > 0) {
                            val earlyDiscountModel = activity.campModel!!.arrDiscountModel.singleOrNull { it.flag == CampDiscountModel.EARLY_REG_DISCOUNT }
                            if(earlyDiscountModel!=null){

                                // fetch it from api end
                                val currentDate = ChangeDateFormat.getDateObjFromString(activity.campModel!!.currentDate, "dd MMM yyyy")

                                //ChangeDateFormat.getDateObjFromString(ChangeDateFormat.getCurrentDate(), "dd-MM-yyyy")
                                val lastDateToApplyEarlyRegDiscount = ChangeDateFormat.getDateObjFromString(
                                    earlyDiscountModel.lastDateToApply,
                                    "yyyy-MM-dd"
                                )

                                if (currentDate != null && lastDateToApplyEarlyRegDiscount != null) {
                                    if (currentDate.before(lastDateToApplyEarlyRegDiscount) || currentDate.time == lastDateToApplyEarlyRegDiscount.time) {
                                        val earlyRegDiscountPercent = earlyDiscountModel.discountPercent.toDouble()
                                        earlyRegDiscountPrice = (basePrice * (earlyRegDiscountPercent / 100.0))
                                    }
                                }
                            }


                        }
                    } else {
                        earlyRegDiscountPrice = 0.0
                    }
                } catch (e: Exception) {
                    Log.e("earlyRegDiscoubt", e.toString())
                }

                // update base price
                basePrice -= earlyRegDiscountPrice

                val baseForAllAttendee = basePrice * numberOfAttendee
                uiThread {
                    if (originalNumberOfAttendee == 0) {
                        dialogCampPayment!!.txtAttendee.text = Html.fromHtml("<font color=#7d7d7d>Attendee</font>")

                    } else {
                        dialogCampPayment!!.txtAttendee.text = Html.fromHtml("<font color=#7d7d7d>Attendee</font> <font color=#F06537>($originalNumberOfAttendee* AED ${DecimalFormat("##.###").format(basePrice)})</font>")
                    }

                    dialogCampPayment!!.txtAttendeePrice.text = "AED " + DecimalFormat("##.##").format(baseForAllAttendee)
                }

                // calculating sibling discount
                var discountPriceForOneSibling=0.0
                try {
                    if (originalNumberOfAttendee > 1) {
                        if (activity.campModel?.arrDiscountModel!!.size > 0) {
                            val siblingDiscountModel = activity.campModel!!.arrDiscountModel.singleOrNull { it.flag == CampDiscountModel.SIBLING_DISCOUNT }
                            if(siblingDiscountModel!=null) {
                                val siblingDiscountPercent = siblingDiscountModel.discountPercent.toDouble()
                                discountPriceForOneSibling = (basePrice) * (siblingDiscountPercent / 100.0)
                                siblingDiscountedPrice = (discountPriceForOneSibling * (originalNumberOfAttendee - 1))
                            }
                        }
                    } else {
                        siblingDiscountedPrice = 0.0
                    }
                } catch (e: Exception) {
                    Log.e("siblingDiscount", e.toString())
                }

                uiThread {
                    if (originalNumberOfAttendee <=1) {
                        dialogCampPayment!!.txtSiblingDiscountDialog.text =
                            Html.fromHtml("<font color=#7d7d7d>Sibling Discount</font>")
                    } else {
                        dialogCampPayment!!.txtSiblingDiscountDialog.text =
                            Html.fromHtml("<font color=#7d7d7d>Sibling Discount</font> <font color=#F06537>(${originalNumberOfAttendee-1}* AED ${DecimalFormat("##.##").format(discountPriceForOneSibling)})</font>")
                    }
                }


                val discountedValue = baseForAllAttendee - siblingDiscountedPrice

                totalBeforeTax = discountedValue

                // commenting this because, we will get VAT inclusive prices from now
                //vatPrice = Math.round((discountedValue * (MyAppConfig.vatRate / 100.0))).toDouble()
                vatPrice =0.0
                dialogCampPayment!!.linVat.visibility=View.GONE
                    totalPriceToPay = discountedValue + vatPrice //Math.round(discountedValue + vatPrice)


                arrPromotionModel.forEach {
                    if (totalPriceToPay<it.promotionMinimumOrderAmount.toDouble())
                        it.isApplied=false
                }

                var totalDiscountPrice:Double=0.0
                var discountedPrice:Double=0.0
                PromotionOperation.calculateFinalPromotionDiscount(totalPriceToPay.toDouble(), {totalDiscountPriceInner:Double, discountedPriceInner:Double->
                    totalDiscountPrice=totalDiscountPriceInner
                    discountedPrice=discountedPriceInner
                }, arrPromotionModel)


                uiThread {
                    if (siblingDiscountedPrice > 0) {

                        dialogCampPayment!!.txtSiblingDiscountPriceDialog.text =
                            "- AED ${DecimalFormat("##.##").format(siblingDiscountedPrice)}"
                        dialogCampPayment!!.linSiblingDiscount.visibility = View.VISIBLE
                        dialogCampPayment!!.txtSiblingDiscountInfo.text="Sibling discount is applied on ${originalNumberOfAttendee-1} Attendee."
                        dialogCampPayment!!.linSiblingDiscountInfo.visibility=View.VISIBLE
                    } else {
                        dialogCampPayment!!.linSiblingDiscount.visibility = View.GONE
                        dialogCampPayment!!.linSiblingDiscountInfo.visibility=View.GONE
                    }

                    if (earlyRegDiscountPrice > 0) {
                        dialogCampPayment!!.txtEarlyRegDiscountPriceDialog.text =
                            "- AED ${DecimalFormat("##.##").format(earlyRegDiscountPrice)}"
                        dialogCampPayment!!.linEarlyRegDiscount.visibility = View.VISIBLE
                    } else {
                        dialogCampPayment!!.linEarlyRegDiscount.visibility = View.GONE
                    }

                    Log.d("formatted value", String.format("%.2f", vatPrice))
                    Log.d("formatted value1", String.format("%.2f", vatPrice).replace(".0", ""))
                    Log.d("formatted value2", String.format("%.2f", vatPrice).replace(".0", "").replace(".00", ""))
                    dialogCampPayment!!.txtVatValueDialog.text =
                        "AED " + DecimalFormat("##.##").format(vatPrice).toString()

                    //dialogCampPayment!!.txtTotalPriceDialog.text = "AED $totalPriceToPay"

                    dialogCampPayment!!.txtVatTitleDialog.text = "VAT (${MyAppConfig.vatRate}%)"

                    if (totalDiscountPrice==0.0)
                        totalAfterPromotionApplied=totalPriceToPay.toDouble()
                    else
                        totalAfterPromotionApplied=discountedPrice

                    PromotionOperation.setupUiForDiscountAndTotal(activity,totalDiscountPrice,dialogCampPayment!!.linDiscount,
                        dialogCampPayment!!.txtApplyPromoCode, dialogCampPayment!!.imgPromotion,dialogCampPayment!!.txtDiscountValueDialog
                        ,dialogCampPayment!!.txtTotalPriceDialog,discountedPrice,totalPriceToPay.toDouble(),dialogCampPayment!!.btnMakePayment,true, txtDiscountTitle = dialogCampPayment!!.txtDiscount, arrPromotionModel = arrPromotionModel)
                }
            }
        }
    }

    private fun fastCampBooking(familyId: String, weekId: String) {
        if (ConnectionDetector.isConnectingToInternet(activity.applicationContext)) {
            val orderId = "" + System.currentTimeMillis()

            val campModel = activity.campModel!!
            val hashParams = HashMap<String, String>()
            hashParams["providerId"] = campModel.providerId
            hashParams["campId"] = campModel.campId
            hashParams["familyId"] = familyId // comma separated attendee id
            hashParams["device"] = "android"
            hashParams["pricePayable"] = totalAfterPromotionApplied.toString()//totalPriceToPay.toString()
            hashParams["totalBeforeTax"] = totalBeforeTax.toString()
            hashParams["vatAmount"] = vatPrice.toString()
            hashParams["vatRate"] = MyAppConfig.vatRate.toString()
            hashParams["sessionJson"] = "1"
            hashParams["orderId"] = orderId
            hashParams["weekIds"] = weekId
            hashParams["promotionDetails"]=PromotionOperation.getPromotionJsonArray(arrPromotionModel)
            hashParams["bookingForFlag"] = activity.selectedDayOrWeekPriceModel!!.scheduleFlag

            DialogMsg.showPleaseWait(activity)
            FastNetworking.makeRxCallPost(activity, Urls.CAMP_BOOKING, true, hashParams, "CampBooking"
                , object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(activity)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            val strSuccessMsg = json.optString(
                                "message",
                                "Your booking costing AED ${hashParams.get("pricePayable")} is successful"
                            )
                            MyNavigations.goToCcAvenue(
                                activity, json.optString("booking_id", ""),
                                "" + totalAfterPromotionApplied,
                                "" + campModel.providerId,
                                strSuccessMsg,
                                orderId,true)
                        } else {
                            val userStatus = json.optString("customer_status", "1")
                            val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)

                            //show info pop up then log out user if user is deactivated
                            if (userStatus.equals("0", true)) {
                                activity.dialogMsg?.showErrorRetryDialog(
                                    activity,
                                    ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,
                                    strErrMsg,
                                    "OK",
                                    View.OnClickListener {
                                        activity.dialogMsg?.dismissDialog(activity)
                                        LoginBasicsUi.logOutUser(activity)
                                    },
                                    isCancellable = false
                                )
                            } else {
                                activity.showErrorDialog(strErrMsg)
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(activity)
                        activity.showApiError()
                    }
                })
        } else {
            activity.showConnectionError()
        }
    }
}
package com.pursueit.utils

import android.app.Dialog
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.activities.FlexiActivityDetailActivity
import com.pursueit.adapters.ApplyPromotionAdapter
import com.pursueit.adapters.AttendeeAdapter
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PromotionModel
import kotlinx.android.synthetic.main.activity_flexi_detail.*
import kotlinx.android.synthetic.main.dialog_camp_payment.*
import kotlinx.android.synthetic.main.layout_apply_promotion.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.textColor
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat

class ManageFlexiActivityPaymentDialog(val activity: FlexiActivityDetailActivity) {
    private var wasFetchMembersHit = false
    private lateinit var mySnackBar: MySnackBar
    var dialogFlexiActivityPayment: Dialog? = null

    var totalPriceToPay = 0.0
    var totalBeforeTax = 0.0
    var vatPrice = 0.0
    private val arrSelectedFamilyMember = ArrayList<String>()
    var arrPromotionModel=ArrayList<PromotionModel>()
    var totalAfterPromotionApplied:Double=0.0

    fun fastFetchFamilyMembersFirstThenShowDialog() {
        if (ConnectionDetector.isConnectingToInternet(activity.applicationContext)) {
            DialogMsg.showPleaseWait(activity)
            MyProfileUtils.fastFetchFamilyMembers(
                activity.applicationContext,
                { success: Boolean, jsonObject: JSONObject?, anError: ANError? ->
                    DialogMsg.dismissPleaseWait(activity)
                    wasFetchMembersHit = true
                    setup()
                },
                activity.compositeDisposable
            )
        } else {
            mySnackBar.showSnackBarError(ErrorMsgs.ERR_CONNECTION_MSG)
        }
    }

    fun setup() {
        arrPromotionModel.clear()
        arrPromotionModel.addAll(activity.activityModel?.arrPromotionModel?:ArrayList())

        wasFetchMembersHit = false
        mySnackBar = MySnackBar(activity, activity.relParentActivityDetail)

        if (dialogFlexiActivityPayment == null)
            dialogFlexiActivityPayment = MyDialog(activity).getMyDialog(R.layout.dialog_camp_payment)

        dialogFlexiActivityPayment?.linEarlyRegDiscount?.visibility=View.GONE
        dialogFlexiActivityPayment?.linSiblingDiscount?.visibility=View.GONE
        dialogFlexiActivityPayment?.linSiblingDiscountInfo?.visibility=View.GONE

        dialogFlexiActivityPayment!!.setCancelable(false)

        if (activity.activityModel != null) {
            dialogFlexiActivityPayment!!.txtCampTitleDialog.text = activity.activityModel!!.activityName
            dialogFlexiActivityPayment!!.txtLocationDialog.text = activity.activityModel!!.activityLocation
            dialogFlexiActivityPayment!!.txtCampServiceProviderDialog.text = activity.activityModel!!.activityProviderName
            dialogFlexiActivityPayment!!.txtAgeRange.text = activity.activityModel!!.arrFlexiSession[0].ageRange
            dialogFlexiActivityPayment!!.txtTimeRange.text = activity.activityModel!!.arrFlexiSession[0].duration

            Glide.with(activity.applicationContext)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_ACTIVITY + activity.activityModel!!.activityImageUrl)
                .into(dialogFlexiActivityPayment!!.imgCampDialog)

        } else {
            return
        }

        dialogFlexiActivityPayment!!.rvAttendees.layoutManager = LinearLayoutManager(activity.applicationContext)

        MyProfileUtils.arrayFamilyModel.forEach {
            it.isSelected = false
        }

        if (arrSelectedFamilyMember.size > 0) {
            MyProfileUtils.arrayFamilyModel.forEach {
                it.isSelected = arrSelectedFamilyMember.contains(it.familyId)
            }
        }

        val adapter = AttendeeAdapter(activity, MyProfileUtils.arrayFamilyModel, { pos, isChecked, adapter ->
            val model = adapter.arrayModel[pos]
            if (!arrSelectedFamilyMember.contains(model.familyId)) {
                arrSelectedFamilyMember.add(model.familyId)
            }

            val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
            calculateTotal(numberOfSelectedPerson)
            /*MyProfileUtils.arrayFamilyModel.forEach {
                it.isSelected = false
            }
            val model = MyProfileUtils.arrayFamilyModel[pos]
            model.isSelected = isChecked
            MyProfileUtils.arrayFamilyModel.set(pos, model)
            adapter.notifyDataSetChanged()*/
        }, {

        })
        dialogFlexiActivityPayment!!.rvAttendees.adapter = adapter

        if (activity.selectedFlexiSessionModel != null) {
            dialogFlexiActivityPayment!!.txtSchedule.text = ResourceUtil.getSessionsString(activity.applicationContext,activity.selectedFlexiSessionModel!!.numberOfSession.toInt())//activity.resources.getQuantityString(R.plurals.session,activity.selectedFlexiSessionModel!!.numberOfSession.toInt(),activity.selectedFlexiSessionModel!!.numberOfSession.toInt()) //activity.selectedFlexiSessionModel!!.numberOfSession+" Session(s)"
            dialogFlexiActivityPayment!!.txtSelectSchedule.text = ResourceUtil.getSessionsString(activity.applicationContext,activity.selectedFlexiSessionModel!!.numberOfSession.toInt())//activity.resources.getQuantityString(R.plurals.session,activity.selectedFlexiSessionModel!!.numberOfSession.toInt(),activity.selectedFlexiSessionModel!!.numberOfSession.toInt()) //activity.selectedFlexiSessionModel!!.numberOfSession+" Session(s)"
            dialogFlexiActivityPayment!!.txtCampLabel.text = ResourceUtil.getSessionsString(activity.applicationContext,activity.selectedFlexiSessionModel!!.numberOfSession.toInt())//activity.resources.getQuantityString(R.plurals.session,activity.selectedFlexiSessionModel!!.numberOfSession.toInt(),activity.selectedFlexiSessionModel!!.numberOfSession.toInt()) //activity.selectedFlexiSessionModel!!.numberOfSession+" Session(s)"
            dialogFlexiActivityPayment!!.txtFullDayPriceDialog.text = "AED " + activity.selectedFlexiSessionModel!!.price
            dialogFlexiActivityPayment!!.txtTimeRange.text = activity.selectedFlexiSessionModel!!.duration

            val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
            calculateTotal(numberOfSelectedPerson)
        }

        dialogFlexiActivityPayment!!.relSelectSchedule.setOnClickListener {
            dialogFlexiActivityPayment!!.imgClose.performClick()
            activity.showDialogForSessionSelection()
        }

        /* dialogFlexiActivityPayment!!.imgAddAttendee.setOnClickListener {
             dialogFlexiActivityPayment!!.dismiss()
             MyProfileUtils.showAddMemberDialog(activity, {
                 fastFetchFamilyMembersFirstThenShowDialog()
             })
         }*/

        dialogFlexiActivityPayment!!.imgClose.setOnClickListener {
            // Except dismissal of dialog in case of add attendee all other control will come here to dismiss dialog
            arrSelectedFamilyMember.clear()  // clearing array here because it means that it is not dismissed in case of add attendee
            if (dialogFlexiActivityPayment!!.isShowing)
                dialogFlexiActivityPayment!!.dismiss()
        }

        dialogFlexiActivityPayment!!.imgAddAttendee.setOnClickListener {
            dialogFlexiActivityPayment!!.dismiss()
            MyProfileUtils.showAddMemberDialog(activity, {
                fastFetchFamilyMembersFirstThenShowDialog()
            }, onDismissListener =  // if user dismissing the add member dialog
            { isMemberAdded ->
                if (isMemberAdded) {

                } else {               // show payment dialog again if user didn't added member
                    setup()
                }
            })
        }

        dialogFlexiActivityPayment!!.btnMakePayment.setOnClickListener {

            if (MyProfileUtils.arrayFamilyModel.size < 1) {
                activity.showErrorDialog("Please add at least one attendee to proceed")
                return@setOnClickListener
            }

            val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
            if (numberOfSelectedPerson < 1) {
                activity.showErrorDialog("Please select one attendee to proceed")
                return@setOnClickListener
            }

            dialogFlexiActivityPayment!!.imgClose.performClick()
            val attendeeIds = StringBuilder()
            val arrSelectedArray = adapter.arrayModel.filter { it.isSelected }
            arrSelectedArray.forEach {
                attendeeIds.append("," + it.familyId)
            }


           /* val weekId = StringBuilder()
            val arrSelectedWeeks = activity.activityModel!!.arrWeekModel.filter { it.isSelected }
            arrSelectedWeeks.forEach {
                weekId.append("," + it.id)
            }*/



            fastFlexiBooking(attendeeIds.replaceFirst(",".toRegex(), ""))
        }

        if (arrPromotionModel.size==0)
            dialogFlexiActivityPayment!!.cnsApplyPromoCode.visibility=View.GONE

        dialogFlexiActivityPayment!!.cnsApplyPromoCode.setOnClickListener {
            PromotionOperation.showApplyPromoCodeDialog(arrPromotionModel,activity,totalPriceToPay.toDouble()) { totalDiscountPrice:Double, discountedPrice:Double->
                val numberOfSelectedPerson = adapter.arrayModel.count { it.isSelected }
                calculateTotal(numberOfSelectedPerson) //,totalDiscountPrice,discountedPrice
            }
        }

        dialogFlexiActivityPayment!!.show()
        //ApplyPromotionAdapter.clearSelectionArray()
    }

    private fun calculateTotal(numberOfAttendee: Int/*, totalDiscountPrice:Double=0.0, discountedPrice:Double=0.0*/) {

        var originalNumberOfAttendee=numberOfAttendee
        var numberOfAttendee =if (numberOfAttendee==0) 1 else numberOfAttendee
        totalPriceToPay = 0.0
        totalBeforeTax = 0.0
        vatPrice = 0.0

        var basePrice = activity.selectedFlexiSessionModel!!.price.toDouble()
        dialogFlexiActivityPayment!!.txtCampBasePrice.text = "AED ${DecimalFormat("##.##").format(basePrice)}"

        if (dialogFlexiActivityPayment != null) {


            doAsync {

                val baseForAllAttendee = basePrice * numberOfAttendee
                uiThread {
                    if (originalNumberOfAttendee == 0) {
                        dialogFlexiActivityPayment!!.txtAttendee.text =
                            Html.fromHtml("<font color=#7d7d7d>Attendee</font>")

                    } else {
                        dialogFlexiActivityPayment!!.txtAttendee.text =
                            Html.fromHtml("<font color=#7d7d7d>Attendee</font> <font color=#F06537>($originalNumberOfAttendee* AED $basePrice)</font>")
                    }

                    dialogFlexiActivityPayment!!.txtAttendeePrice.text = "AED " + DecimalFormat("##.##").format(baseForAllAttendee)
                }

               /* uiThread {
                    if (originalNumberOfAttendee <=1) {
                        dialogFlexiActivityPayment!!.txtSiblingDiscountDialog.text =
                            Html.fromHtml("<font color=#7d7d7d>Sibling Discount</font>")
                    } else {
                        dialogFlexiActivityPayment!!.txtSiblingDiscountDialog.text =
                            Html.fromHtml("<font color=#7d7d7d>Sibling Discount</font> <font color=#F06537>(${originalNumberOfAttendee-1}* AED ${DecimalFormat("##.##").format(discountPriceForOneSibling)})</font>")
                    }
                }*/


                val discountedValue = baseForAllAttendee

                totalBeforeTax = discountedValue

                // Commenting this because we will get VAT inclusive prices from now
                //vatPrice = Math.round((discountedValue * (MyAppConfig.vatRate / 100.0))).toDouble()
                vatPrice=0.0

                totalPriceToPay = discountedValue + vatPrice//Math.round(discountedValue + vatPrice)

                arrPromotionModel.forEach {
                        if (totalPriceToPay<it.promotionMinimumOrderAmount.toDouble())
                            it.isApplied=false
                }

                var totalDiscountPrice:Double=0.0
                var discountedPrice:Double=0.0
                PromotionOperation.calculateFinalPromotionDiscount(totalPriceToPay.toDouble(), {totalDiscountPriceInner:Double, discountedPriceInner:Double->
                    totalDiscountPrice=totalDiscountPriceInner
                    discountedPrice=discountedPriceInner
                    }, arrPromotionModel)

                //totalPriceToPay-=totalDiscountPrice.toLong()

                uiThread {
                    Log.d("formatted value", String.format("%.2f", vatPrice))
                    Log.d("formatted value1", String.format("%.2f", vatPrice).replace(".0", ""))
                    Log.d("formatted value2", String.format("%.2f", vatPrice).replace(".0", "").replace(".00", ""))
                    dialogFlexiActivityPayment!!.txtVatValueDialog.text =
                        "AED " + DecimalFormat("##.##").format(vatPrice).toString()


                    if (totalDiscountPrice==0.0)
                        totalAfterPromotionApplied=totalPriceToPay.toDouble()
                    else
                        totalAfterPromotionApplied=discountedPrice

                    PromotionOperation.setupUiForDiscountAndTotal(activity,totalDiscountPrice,dialogFlexiActivityPayment!!.linDiscount,
                        dialogFlexiActivityPayment!!.txtApplyPromoCode, dialogFlexiActivityPayment!!.imgPromotion,dialogFlexiActivityPayment!!.txtDiscountValueDialog
                        ,dialogFlexiActivityPayment!!.txtTotalPriceDialog,discountedPrice,totalPriceToPay.toDouble(),dialogFlexiActivityPayment!!.btnMakePayment,txtDiscountTitle = dialogFlexiActivityPayment!!.txtDiscount, arrPromotionModel = arrPromotionModel)
                  /*  if (totalDiscountPrice==0.0)
                    {
                        dialogFlexiActivityPayment!!.linDiscount.visibility=View.GONE
                        dialogFlexiActivityPayment!!.txtApplyPromoCode.textColor=ContextCompat.getColor(activity,R.color.colorAccent)
                        dialogFlexiActivityPayment!!.imgPromotion.setColorFilter(ContextCompat.getColor(activity,R.color.colorAccent),PorterDuff.Mode.SRC_IN)
                        dialogFlexiActivityPayment!!.txtApplyPromoCode.text="Apply Promo Code"
                    }
                    else {
                        dialogFlexiActivityPayment!!.txtApplyPromoCode.textColor=ContextCompat.getColor(activity,R.color.colorSuccess)
                        dialogFlexiActivityPayment!!.imgPromotion.setColorFilter(ContextCompat.getColor(activity,R.color.colorSuccess),PorterDuff.Mode.SRC_IN)
                        dialogFlexiActivityPayment!!.txtApplyPromoCode.text="Promo code applied successfully!!"
                        dialogFlexiActivityPayment!!.linDiscount.visibility=View.VISIBLE
                        dialogFlexiActivityPayment!!.txtDiscountValueDialog.text =
                            "- AED $totalDiscountPrice"
                    }



                    if (discountedPrice==0.0)
                        dialogFlexiActivityPayment!!.txtTotalPriceDialog.text = "AED $totalPriceToPay"
                    else
                        dialogFlexiActivityPayment!!.txtTotalPriceDialog.text = "AED $discountedPrice"*/

                    dialogFlexiActivityPayment!!.txtVatTitleDialog.text = "VAT (${MyAppConfig.vatRate}%)"
                }
            }
        }
    }

    private fun fastFlexiBooking(familyId: String) {
        if (ConnectionDetector.isConnectingToInternet(activity.applicationContext)) {
            val orderId = "" + System.currentTimeMillis()
            val activityModel = activity.activityModel!!

            //[{"flexi_activity_id":"12","flexiact_price_manage_id":"26","valid_days":"45","price":"200","no_of_sessions":"20","duration":"60"}]
            val sessionJson=JSONObject()
            val selectedFlexiSession=activity.selectedFlexiSessionModel!!
            sessionJson.put("flexi_activity_id",activityModel.activityId)
            sessionJson.put("flexiact_price_manage_id",selectedFlexiSession.flexiPriceManageId)
            sessionJson.put("valid_days",selectedFlexiSession.validDays)
            sessionJson.put("price",selectedFlexiSession.price)
            sessionJson.put("no_of_sessions",selectedFlexiSession.numberOfSession)
            sessionJson.put("duration",selectedFlexiSession.intDuration)


            val hashParams = HashMap<String, String>()
            hashParams["providerId"] = activityModel.activityProviderId
            hashParams["activityId"] = activityModel.activityId
            hashParams["familyId"] = familyId // comma separated attendee id
            hashParams["device"] = "android"
            hashParams["pricePayable"] = totalAfterPromotionApplied.toString()//totalPriceToPay.toString()
            hashParams["totalBeforeTax"] = totalBeforeTax.toString()
            hashParams["vatAmount"] = vatPrice.toString()
            hashParams["vatRate"] = MyAppConfig.vatRate.toString()
            hashParams["sessionJson"] =JSONArray().put(0,sessionJson).toString()
            hashParams["orderId"] = orderId
            hashParams["activityFlexiId"] = activityModel.activityFlexiId
            hashParams["flexiact_price_manage_id"] =selectedFlexiSession.flexiPriceManageId
            hashParams["promotionDetails"]=PromotionOperation.getPromotionJsonArray(arrPromotionModel)

            DialogMsg.showPleaseWait(activity)
            FastNetworking.makeRxCallPost(activity, Urls.FLEXI_BOOKING, true, hashParams, "FlexiBooking"
                , object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(activity)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            val strSuccessMsg = json.optString(
                                "message",
                                "Your booking costing AED ${hashParams.get("pricePayable")} is successful")
                            MyNavigations.goToCcAvenue(
                                activity,
                                json.optString("booking_id", ""),
                                "" + totalAfterPromotionApplied,
                                "" + activityModel.activityProviderId,
                                strSuccessMsg,
                                orderId)
                        } else {
                            val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)

                            if(json.has("seat_full") && json.optString("seat_full","0")=="1"){
                                activity.showErrorDialog(strErrMsg,true)
                            }else {
                                val userStatus = json.optString("customer_status", "1")

                                //show info pop up then log out user if user is deactivated
                                if (userStatus.equals("0", true)) {
                                    activity.dialogMsg?.showErrorRetryDialog(
                                        activity,
                                        ErrorMsgs.ERR_AUTH_LOGIN_REGISTER_TITLE,
                                        strErrMsg,
                                        "OK",
                                        View.OnClickListener {
                                            activity.dialogMsg?.dismissDialog(activity)
                                            LoginBasicsUi.logOutUser(activity)
                                        },
                                        isCancellable = false
                                    )
                                } else {
                                    activity.showErrorDialog(strErrMsg)
                                }
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(activity)
                        activity.showApiError()
                    }
                })
        } else {
            activity.showConnectionError()
        }
    }

}
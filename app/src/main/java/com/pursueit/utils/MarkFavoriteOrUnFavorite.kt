package com.pursueit.utils

import android.support.v7.app.AppCompatActivity
import android.view.View
import com.androidnetworking.error.ANError
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import org.json.JSONObject

class MarkFavoriteOrUnFavorite(val act:AppCompatActivity)
{
    companion object{
        const val ITEM_FLAG_CAMP="1"
        const val ITEM_FLAG_ACTIVITY="0"

        const val FLAG_FAVORITE="1"
        const val FLAG_UNFAVORITE="0"

        var LAST_CAMP_ID=""
        var LAST_FAV_ACTIVITY_ID=""
        var LAST_FLAG_FOR_ACTIVITY_OR_CAMP=""

        var favouriteUpdated=false
    }

    val dialogMsg=DialogMsg()
    fun fastMark(activityId:String="", campId:String="",itemFlag:String,favUnFavFlag:String, fromFavouriteListing:Boolean=false)
    {
        if (ConnectionDetector.isConnectingToInternet(act)) {
            val hashMap=HashMap<String,String>()
            hashMap["activityId"] = activityId
            hashMap["campId"] = campId
            hashMap["favoriteFlag"] = itemFlag
            hashMap["addRemoveFlog"] = favUnFavFlag

            LAST_CAMP_ID=campId
            LAST_FAV_ACTIVITY_ID=activityId
            LAST_FLAG_FOR_ACTIVITY_OR_CAMP=favUnFavFlag

            if (!fromFavouriteListing)
            {
                favouriteUpdated=true
            }


            FastNetworking.makeRxCallPost(act.applicationContext,Urls.ADD_REMOVE_FAVORITE,true,hashMap,"Fav-UnFav-API",
                object : FastNetworking.OnApiResult{
                    override fun onApiSuccess(json: JSONObject?) {
                    }

                    override fun onApiError(error: ANError) {

                    }
                })
        }
        else
        {
            dialogMsg?.showErrorConnectionDialog(act,"OK", View.OnClickListener { dialogMsg?.dismissDialog(act) })
        }
    }
}
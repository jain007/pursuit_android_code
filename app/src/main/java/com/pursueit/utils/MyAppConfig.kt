package com.pursueit.utils

import android.app.Activity
import android.content.Context
import android.text.Html
import android.text.Spanned
import android.util.Log
import com.androidnetworking.error.ANError
import com.pursueit.BuildConfig
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.AgeGroupModel
import com.pursueit.model.PlaceModel
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.share
import org.json.JSONObject


object MyAppConfig {
    var strAboutUsTitle = ""
    var strAboutUsDesc: Spanned? = null


    var serverAppVersionCode = ""
    var isServerAppUpdateMandatory = false

    var strLinkFacebook = ""
    var strLinkInstagram = ""
    var strLinkTwitter = ""

    var howItWorksUrl="https://www.youtube.com/watch?v=EJ04QyhdkEs"

    var PLAY_STORE_URL = "https://play.google.com/store/apps/details?id="
    var IOS_APP_URL="\n\nDownload the iOS app here:\nhttps://itunes.apple.com/app/id1458477849"

    var defaultPlaceModel:PlaceModel?=null

    var noOfAttempts=0

    var arrayAgeGroupModel=ArrayList<AgeGroupModel>()

    var vatRate=0.0 //VAT %

    var URL_TERMS=""
    var URL_PRIVACY_POLICY=""
    var URL_FAQ=""

    var startPriceForFilter=""
    var maxPriceForFilter=""

    fun shareApp(act:Activity,strTitle:String=""){
        try{
            val strMsg=if(strTitle.isNotEmpty()) "Hey, check what I have found at ${act.getString(R.string.app_name)}.\n$strTitle" else "Get started with ${act.getString(R.string.app_name)}."
            act.share("$strMsg\n\nDownload the android app now:\n"+MyAppConfig.PLAY_STORE_URL+BuildConfig.APPLICATION_ID+IOS_APP_URL)
        }catch (e:Exception){

        }
    }

    fun fastFetchAppConfigValues(context: Context, showUiLoader: Boolean = false, act: Activity? = null, onSuccess: () -> Unit, onFailure: () -> Unit, compositeDisposable: CompositeDisposable? = null) {
        if (showUiLoader && act != null)
            DialogMsg.showPleaseWait(act)
        FastNetworking.makeRxCallPost(context, Urls.VIEW_CONFIGURATION, false, HashMap<String, String>(), "ViewConfig", object :
            FastNetworking.OnApiResult {
            override fun onApiSuccess(json: JSONObject?) {
                if (showUiLoader && act != null)
                    DialogMsg.dismissPleaseWait(act)

                doAsync {
                    try {
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            noOfAttempts=0

                            val jsonData = json.getJSONObject("data")
                            val jsonAboutUs = jsonData.getJSONObject("about_us")
                            strAboutUsTitle = jsonAboutUs.optString("page_title", "")
                            strAboutUsDesc = Html.fromHtml(jsonAboutUs.optString("page_content", ""))

                            serverAppVersionCode = jsonData.optString("android_app_version", "1")
                            isServerAppUpdateMandatory = jsonData.optString("mandatory_update_android_app", "0").equals("1", true)

                            strLinkFacebook = jsonData.optString("facebook_link", "")
                            strLinkInstagram = jsonData.optString("instagram_link", "")
                            strLinkTwitter = jsonData.optString("twitter_link", "")


                            howItWorksUrl=jsonData.optString("how_it_works_url","")

                            startPriceForFilter=jsonData.optString("price_range_start","")
                            maxPriceForFilter=jsonData.optString("price_range_end","")

                            defaultPlaceModel=PlaceModel(jsonData.optString("place_id",""),jsonData.optString("place_name",""),jsonData.optString("place_address","")).also {placeModel ->
                                placeModel.placeCityId=jsonData.optString("place_city_id","")
                                try {
                                    placeModel.placeLat=jsonData.optString("latitude","0").toDouble()
                                    placeModel.placeLng=jsonData.optString("longitude","0").toDouble()
                                }catch (e:Exception){

                                }
                            }

                            if(jsonData.has("age_groups")){
                                val jsonAgeGroups=json.getJSONObject("data").getJSONArray("age_groups")
                                arrayAgeGroupModel.clear()
                                repeat(jsonAgeGroups.length()){indexAge->
                                    val objAge=jsonAgeGroups.getJSONObject(indexAge)
                                    val ageGroupModel=AgeGroupModel(objAge.optString("group_id","")).also {ageGroupModel ->
                                        with(ageGroupModel){
                                            groupTitle=objAge.optString("group_title","")
                                            groupAgeFrom=objAge.optString("group_age_from","")
                                            groupAgeTo=objAge.optString("group_age_to","")
                                            groupLabel=objAge.optString("group_age_label","")
                                        }
                                    }

                                    arrayAgeGroupModel.add(ageGroupModel)

                                }
                            }

                            try {
                                vatRate = jsonData.getString("vat_rate").toDouble()
                            }catch (e:Exception){
                                vatRate=0.0
                                Log.d("NumberFormatVat",""+e)
                            }

                            URL_TERMS=jsonData.optString("term_&_conditions_link","")
                            URL_PRIVACY_POLICY=jsonData.optString("privacy_policy_link","")
                            URL_FAQ=jsonData.optString("faq","")

                            onSuccess()
                        }
                    } catch (e: Exception) {
                        Log.d("ExcAppConfig", "" + e)
                    }
                }

            }

            override fun onApiError(error: ANError) {
                if (showUiLoader && act != null)
                    DialogMsg.dismissPleaseWait(act)

                if(noOfAttempts<4){
                    noOfAttempts+=1
                    fastFetchAppConfigValues(context,showUiLoader,act,onSuccess,onFailure,compositeDisposable)
                }else{
                    onFailure()
                }
            }

        }, compositeDisposable)
    }



}
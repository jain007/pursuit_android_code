package com.pursueit.utils

import com.bumptech.glide.request.RequestOptions
import com.pursueit.R


object MyGlideOptions{

    fun getSquareRequestOptions(isCenterCrop:Boolean=true):RequestOptions{
        return RequestOptions().also {
            it.placeholder(R.drawable.ic_placeholder_square)
            it.error(R.drawable.ic_placeholder_square)

            if(isCenterCrop)
                it.centerCrop()
            else
                it.fitCenter()
        }
    }

    fun getRectangleRequestOptions(isCenterCrop:Boolean=true):RequestOptions{
        return RequestOptions().also {
            it.placeholder(R.drawable.ic_placeholder_rectangle)
            it.error(R.drawable.ic_placeholder_rectangle)
            if(isCenterCrop)
                it.centerCrop()
            else
                it.fitCenter()
        }
    }

    fun getRectangleRequestOptionsVideo(isCenterCrop:Boolean=true):RequestOptions{
        return RequestOptions().also {
            it.placeholder(R.drawable.ic_placeholder_video)
            it.error(R.drawable.ic_placeholder_video)
            if(isCenterCrop)
                it.centerCrop()
            else
                it.fitCenter()
        }
    }
}
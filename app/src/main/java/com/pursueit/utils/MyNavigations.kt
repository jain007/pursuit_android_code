package com.pursueit.utils

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.pursueit.activities.*
import com.pursueit.model.ActivityModel
import com.pursueit.model.MyBookingsModel
import com.pursueit.model.NotificationModel
import com.pursueit.model.ReviewModel
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import java.lang.Exception
import java.text.DecimalFormat


object MyNavigations{

    val RQ_CODE_CCAVENUE=100
    val RQ_ADD_MONEY=101
    val RQ_SEND_SMS=102

    fun goToActivityDetail(act:Activity,model:ActivityModel,slotIndex:Int, comingFromBooking:Boolean=false,bookingModel:MyBookingsModel?=null, notificationModel: NotificationModel?=null){
       // Log.d("Slot_Selected_Out",""+slotIndex+"\n"+model.arraySlotModel[slotIndex]+"\nEnrolmentId: "+model.arraySlotModel[slotIndex].slotEnrolmentId)
        act.startActivityForResult<ActivityDetailActivity>(200,"ActivityModel" to model
            ,"SlotIndex" to slotIndex,"ComingFromBooking" to comingFromBooking,"BookingModel" to bookingModel
            ,"notificationModel" to notificationModel)
    }

    fun goToFlexiActivityDetail(act:Activity,model:ActivityModel,sessionIndex:Int,comingFromBooking:Boolean=false,bookingModel:MyBookingsModel?=null){
        Log.d("flexiModel3",model.toString())

        act.startActivityForResult<FlexiActivityDetailActivity>(200,"ActivityModel" to model
            ,"ComingFromBooking" to comingFromBooking,"BookingModel" to bookingModel, "sessionIndex" to sessionIndex)
    }

    fun gotoPromotionListingActivity(act:Activity)
    {
        act.startActivity<PromotionListingActivity>()
    }

    fun gotoActivityListing(act: Activity)
    {
        act.startActivity<NotificationActivity>()
    }



    fun goToDashboard(act:Activity,bundle: Bundle?=null,withNewTask:Boolean=false){
        val intent=Intent(act,DashboardActivity::class.java)
        if (bundle!=null)
            intent.putExtras(bundle)
        /*if (withNewTask)
            intent.clearTask().newTask()*/
        act.startActivity(intent)

        if(withNewTask)
            act.finishAffinity()

        //act.startActivity<DashboardActivity>()
    }

    fun goToRegistration(act:Activity){
        act.startActivity<LoginRegisterActivity>(StaticValues.LOGIN_REGISTER_FLAG to 2)
    }

    fun goToNormalLogin(act:Activity){
        act.startActivity<LoginRegisterActivity>(StaticValues.LOGIN_REGISTER_FLAG to 1)
    }

    fun goToMainActivity(act:Activity,bundle:Bundle?=null){
        val intent= Intent(act,MainActivity::class.java)
        if (bundle!=null)
            intent.putExtras(bundle)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        act.startActivity(intent)
        //act.finishAffinity()
    }

    fun goToMyProfile(act: Activity){
        act.startActivity<MyProfileActivity>()
    }

    fun goToSearchPlace(act: Activity){
        act.startActivity<SearchPlaceActivity>()
    }


    fun goToAllReviews(act: Activity,arrayReviews:ArrayList<ReviewModel>,activityId:String, providerId: String=""){
        act.startActivity<ReviewsListingActivity>("Reviews" to arrayReviews,"ActivityId" to activityId, "providerId" to providerId)
    }

    fun goToEnterOtp(act: Activity,strActualOtp:String="",strEmail:String="",isComingFromRegistration:Boolean=false,json:String="{}"){
        act.startActivity<EnterOtpActivity>("ActualOtp" to strActualOtp,"Email" to strEmail,"IsComingFromRegistration" to isComingFromRegistration,"UserJson" to json)
    }

    fun goToEnterOtpForPhone(act: Activity,strActualOtp:String,phoneNumber:String){
        act.startActivityForResult<EnterOtpActivity>(RQ_SEND_SMS,"fromPhoneVerification" to true,"ActualOtp" to strActualOtp,"phoneNumber" to phoneNumber)
    }

    fun goToAboutUs(act: Activity){
        act.startActivity<AboutUsActivity>()
    }

    fun goToCcAvenue(act: Activity,strBookingId:String,strBookingAmt:String,strProviderId:String,strMsg:String
                     ,orderId:String, fromCamp:Boolean=false, fromAddMoney:Boolean=false, fromPassBooking:Boolean=false){

        var bookingAmt:String?=null
        try {
            bookingAmt=DecimalFormat("##.##").format(strBookingAmt.toDouble())
        }catch (e:Exception){
            bookingAmt=strBookingAmt
        }
        act.startActivityForResult<CCAvenueActivity>(RQ_CODE_CCAVENUE,"BookingId" to strBookingId
            ,"BookingAmount" to bookingAmt,"BookingMsg" to strMsg,"ProviderId" to strProviderId,"OrderId" to orderId
            ,"fromCamp" to fromCamp, "fromAddMoney" to fromAddMoney, "fromPassBooking" to fromPassBooking)
    }

    fun goToWebView(act: Activity,strTitle:String,strUrl:String){
        act.startActivity<WebViewActivity>("Title" to strTitle, "URL" to strUrl)
    }

    fun goToPaymentTransactions(act:Activity, fromPassTransaction:Boolean=false){
        act.startActivity<PaymentTransactionsActivity>("fromPassTransaction" to fromPassTransaction)
    }

    fun goToCampDetail(act: Activity,campId:String, isComingFromBookingScreen:Boolean=false, bookingStatus:String="", bookingModel: MyBookingsModel?=null)
    {
        act.startActivityForResult<CampDetailActivity>(100,"campId" to campId
            ,"isComingFromBookingScreen" to isComingFromBookingScreen, "bookingStatus" to bookingStatus
            ,"bookingModel" to bookingModel)
    }

    fun gotoLocationActivity(act: Activity, arrActivity:ArrayList<ActivityModel>)
    {
        act.startActivity<ActivityLocationActivity>("arrActivity" to arrActivity)
    }

    fun gotoAppSupportActivity(act: Activity){
        act.startActivity<AppSupportActivity>()
    }

    fun gotoProviderDetailActivity(act: Activity,providerId:String)
    {
        act.startActivity<ProviderDetailActivity>("providerId" to providerId)
    }

    fun gotoAddMoneyActivity(act: Activity, fromPaymentDialog:Boolean=false){
        act.startActivityForResult<AddMoneyActivity>(RQ_ADD_MONEY,"from" to fromPaymentDialog)
    }

    fun goToAppIntroActivity(act:Activity,bundle:Bundle?=null){
        val intent= Intent(act,AppIntroActivity::class.java)
        if (bundle!=null)
            intent.putExtras(bundle)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        act.startActivity(intent)
        //act.finishAffinity()
    }
}
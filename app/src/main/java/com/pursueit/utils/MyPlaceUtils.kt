package com.pursueit.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Geocoder
import android.net.Uri
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pursueit.BuildConfig
import com.pursueit.MyApp
import com.pursueit.activities.SearchPlaceActivity
import com.pursueit.dialogs.DialogMsg
import com.pursueit.fragments.NearbyActivitiesListingFragment
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.GoogleApis
import com.pursueit.httpCalls.Urls
import com.pursueit.model.PlaceModel
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap


class MyPlaceUtils(val act: Activity, val onCityFetchedListener: OnCityFetchedListener) {

    var setAsCurrent = true

    companion object {

        var apiKey = BuildConfig.PLACES_API_KEY

        var REQUEST_CHECK_SETTINGS = 52

        // Specify the fields to return.
        var placeFields = arrayListOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)

        var placesClient: PlacesClient? = null
        //var mFusedLocationProviderClient:FusedLocationProviderClient?=null

        var placeModel: PlaceModel = PlaceModel()

        fun initPlaceClient(context: Context) {
            if (placesClient == null) {
                Places.initialize(context, apiKey, Locale.ENGLISH)
                placesClient = Places.createClient(context)

                //mFusedLocationProviderClient=LocationServices.getFusedLocationProviderClient(context)
            }
        }

        fun resetAllCategoryLocalResponses(context: Context) {
            //reset response
            NearbyActivitiesListingFragment.resetMakeResponseEmptyAllCategories(context)
        }

        fun replaceUAECountry(str: String): String {
            return str.replace("- United Arab Emirates", "").replace("United Arab Emirates", "").trim()
        }

        private fun openSettings(act: Activity) {
            try {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                intent.data = uri
                act.startActivityForResult(intent, 101)
            } catch (e: Exception) {

            }
        }

        fun getDirections(act: Activity, lat: Double, lng: Double) {
            try {
                val directionsBuilder = Uri.Builder()
                    .scheme("https")
                    .authority("www.google.com")
                    .appendPath("maps")
                    .appendPath("dir")
                    .appendPath("")
                    .appendQueryParameter("api", "1")
                    //.appendQueryParameter("source", "${MyPlaceUtils.placeModel.placeLat},${MyPlaceUtils.placeModel.placeLng}")
                    .appendQueryParameter("destination", "$lat,$lng")

                act.startActivity(Intent(Intent.ACTION_VIEW, directionsBuilder.build()))
            } catch (e: Exception) {
                Log.d("Exc_get_dir", "" + e)
                act.toast("No app found for this action")
            }
        }

        fun updatePlaceAddress(strAddress: String) {
            placeModel.placeAddress = strAddress.trim()
            if (placeModel.placeAddress.startsWith("-"))
                placeModel.placeAddress = placeModel.placeAddress.substring(1)

            Log.d("PlaceAddress__", placeModel.placeAddress)
        }
    }

    fun requestLocationPermission() {
        Dexter.withActivity(act)
            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        if (report.areAllPermissionsGranted()) {
                            createLocationRequest()
                        } else {
                            act.toast("Please allow location permission for more accurate results nearby")
                            try {
                                if (act is SearchPlaceActivity) {
                                    val dialogInformSettings = DialogMsg()
                                    dialogInformSettings.showYesCancelDialog(act, "Please allow location permission from your app settings to fetch your current location", View.OnClickListener {
                                        dialogInformSettings.dismissDialog(act)
                                        openSettings(act)
                                    }, true, "App Settings", "Not Now")
                                }
                            } catch (e: Exception) {

                            }
                            onCityFetchedListener.onCityError()
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                    token?.continuePermissionRequest()
                }

            })
            .check()
    }


    private fun createLocationRequest() {
        val locationRequest = LocationRequest().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val client: SettingsClient = LocationServices.getSettingsClient(act)
        val resultSettingsLocation: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())



        resultSettingsLocation.addOnSuccessListener { locationSettingsResponse ->
            // All location settings are satisfied. The client can initialize
            // location requests here.
            // ...

            initMyCurrentPlace()


        }

        resultSettingsLocation.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(act, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    onCityFetchedListener.onCityError()
                }
            } else {
                onCityFetchedListener.onCityError()
            }
        }


    }

    fun initMyCurrentPlace() {
        // Use the builder to create a FindCurrentPlaceRequest.
        val request = FindCurrentPlaceRequest.builder(placeFields).build()

        if (ContextCompat.checkSelfPermission(act, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            placesClient?.findCurrentPlace(request)?.addOnSuccessListener { placeResponse ->
                if (placeResponse != null) {

                    //var i = 0
                    for (place in placeResponse.placeLikelihoods) {

                        //Log.d("PlaceName", place.place.name)
                        //Log.d("PlaceId", place.place.id)
                        //Log.d("PlaceLatLng", "" + place.place.latLng?.latitude + " - " + place.place.latLng?.longitude)
                        Log.d("PlaceName", "" + place.place.name + " - Likelihood: " + place.likelihood)
                        Log.d("PlaceAddress", "" + place.place.address)
                        //Log.d("PlaceLikelihood", "" + place.place)

                        val strPlaceAddress = place.place.address ?: ""
                        val strPlaceTitle = place.place.name ?: ""

                        if (strPlaceAddress.trim().isNotEmpty() && (isValidAddressGeocoder(strPlaceAddress) || isValidAddressGeocoder(strPlaceTitle))) {
                            placeModel.placeId = place.place.id ?: ""
                            placeModel.placeTitle = place.place.name?.trim() ?: ""
                            placeModel.placeLat = place.place.latLng?.latitude ?: 0.0
                            placeModel.placeLng = place.place.latLng?.longitude ?: 0.0
                            placeModel.placeAddress = place.place.address ?: ""

                            updatePlaceAddress(placeModel.placeAddress)

                            fastSendCurrentLocationToServer(act.applicationContext, placeModel.placeLat.toString(), placeModel.placeLng.toString())

                            initCurrentCityCallGeocodeApi(
                                place.place.latLng?.latitude
                                    ?: 0.0, place.place.latLng?.longitude ?: 0.0
                            )

                            break
                        }

                        //i++


                    }
                } else {
                    Log.d("LocationNull", "" + placeResponse)
                    onCityFetchedListener.onCityError()
                }
            }?.addOnFailureListener { exception ->
                Log.d("LocationException", "" + exception)
                act.toast("Please enable location services")
                onCityFetchedListener.onCityError()
            }
        }
    }

    fun initCurrentCityCallGeocodeApi(lat: Double, lng: Double, callGeocodeRestApi: Boolean = false, overwritePlaceAddress: Boolean = true) {
        if (callGeocodeRestApi) {
            val hashParams = HashMap<String, String>()
            hashParams.put("key", apiKey)
            hashParams.put("sessiontoken", MyApp.ANDROID_ID)
            hashParams.put("language", "en")
            hashParams.put("latlng", "$lat,$lng")
            hashParams.forEach {
                Log.d(it.key, it.value)
            }
            FastNetworking.makeRxCallGet(act.applicationContext, GoogleApis.GEOCODE_API, hashParams, "GeoCodeApi", object :
                FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    try {
                        if (json?.getString("status").equals("OK", true)) {
                            val jsonObj = json?.getJSONObject("plus_code")

                            var strCompoundCode = ""
                            if (jsonObj!!.has("compound_code"))
                                strCompoundCode = jsonObj.getString("compound_code")

                            val strCityFetched = if (strCompoundCode.contains(" ")) strCompoundCode.substring(strCompoundCode.indexOf(" ")).trim() else strCompoundCode
                            var strCountryFetched = ""
                            if (strCityFetched.contains(","))
                                strCountryFetched = strCityFetched.substring(strCityFetched.lastIndexOf(",") + 1).trim()

                            Log.d("CityFetched", strCityFetched)
                            Log.d("CountryFetched", strCountryFetched)

                            fetchPlaceIdFromCity(strCityFetched)

                        } else {
                            act.toast(ErrorMsgs.ERR_API_MSG)
                            onCityFetchedListener.onCityError()
                        }
                    } catch (e: Exception) {
                        Log.d("Exc_GeoCode", "" + e)
                        onCityFetchedListener.onCityError()
                    }
                }

                override fun onApiError(error: ANError) {
                    act.toast(ErrorMsgs.ERR_API_MSG)
                    onCityFetchedListener.onCityError()
                }

            })

        } else {
            //call inner geocoder first, if found null then go ahead with geolocation API
            val strCityName = getCityByLatLng(act.applicationContext, lat, lng, overwritePlaceAddress)
            if (strCityName != null) {
                fetchPlaceIdFromCity(strCityName)
            } else {
                initCurrentCityCallGeocodeApi(lat, lng, true, overwritePlaceAddress = overwritePlaceAddress)
            }
        }

    }

    private fun fetchPlaceIdFromCity(strCity: String) {
        val hashParams = HashMap<String, String>()
        hashParams.put("key", apiKey)
        hashParams.put("language", "en")
        hashParams.put("sessiontoken", MyApp.ANDROID_ID)
        hashParams.put("input", strCity)

        FastNetworking.makeRxCallGet(act.applicationContext, GoogleApis.SEARCH_CITY, hashParams, "SearchCity", object :
            FastNetworking.OnApiResult {
            override fun onApiSuccess(json: JSONObject?) {
                try {
                    if (json != null) {
                        val isOk = json.getString("status").equals("OK", true)
                        if (isOk) {
                            if (json.has("predictions")) {
                                val arrPredictions = json.getJSONArray("predictions")
                                if (arrPredictions.length() > 0) {
                                    val obj = arrPredictions.getJSONObject(0)
                                    val cityPlaceId = obj.getString("place_id")
                                    if (setAsCurrent) {
                                        placeModel.placeCityId = cityPlaceId
                                        Log.d("CityId", "" + placeModel.placeCityId)
                                    }
                                    onCityFetchedListener.onCityFetched(cityPlaceId)
                                }
                            }
                        } else {
                            onCityFetchedListener.onCityError()
                        }
                    }
                } catch (e: Exception) {
                    Log.d("GoogleCitySearchError", "" + e)
                    onCityFetchedListener.onCityError()
                }
            }

            override fun onApiError(error: ANError) {
                //act.toast(ErrorMsgs.ERR_API_MSG)
                onCityFetchedListener.onCityError()
            }

        })
    }

    private fun getCityByLatLng(context: Context, latitude: Double, longitude: Double, overwritePlaceAddress: Boolean = true): String? {
        var cityName: String? = null
        val geoCoder = Geocoder(context, Locale.ENGLISH)

        try {
            Log.d("LocalityLatLng", "$latitude,$longitude")
            val addresses = geoCoder.getFromLocation(latitude, longitude, 9)
            //Log.d("AddressesFound__",""+addresses)

            for (addr in addresses) {

                if(cityName==null) {
                    cityName = addr.locality
                    Log.d("LocalityCity", "" + addr.locality)
                    Log.d("SubLocalityCity", "" + addr.subLocality)
                    Log.d("AdminSubAdminArea", "" + addr.adminArea + " " + addr.subAdminArea)
                    if (cityName == null) {
                        cityName = addresses[0].subLocality
                        if (cityName == null) {
                            cityName = addresses[0].adminArea
                            if (cityName == null) {
                                cityName = addresses[0].subAdminArea
                            }
                        }
                    }
                }

                val strAddress = addr.getAddressLine(0)
                Log.d("LocalityAddress", "" + strAddress)
                if (overwritePlaceAddress && isValidAddressGeocoder(strAddress) && !strAddress.contains("Unnamed Road", true)) {
                    updatePlaceAddress(strAddress)
                    break
                }

            }


            //Log.d("GeoAddress", "" + addresses[0].getAddressLine(0))
        } catch (e: Exception) {
            Log.d("GecoderExc", "Geocoder_Exc $e")

        }
        return cityName
    }


    private fun fastSendCurrentLocationToServer(context: Context, strLat: String, strLong: String) {
        doAsync {
            if (StaticValues.AUTH_TOKEN.trim().length > 7) {
                val hashParams = HashMap<String, String>()
                hashParams["androidLocation"] = "$strLat,$strLong"
                hashParams["iosLocation"] = ""
                FastNetworking.makeRxCallPost(context, Urls.UPDATE_USER_LOCATION, true, hashParams, "SendLocation", object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {

                    }

                    override fun onApiError(error: ANError) {

                    }

                })
            }
        }
    }


    private fun isValidAddressGeocoder(strAddress: String): Boolean {
        for (i in 0 until strAddress.length) {
            val ch = strAddress[i]
            if ((ch in 'A'..'Z') || (ch in 'a'..'z') || (ch in '0'..'9') || ch == '-' || ch == ',' || ch == ' ' || ch=='\'') {

            } else {
                return false
            }
        }

        return true
    }

    private fun giveValidatedAddress(strAddress: String): String {
        var strPlace = ""
        for (i in 0 until strAddress.length) {
            val ch = strAddress.get(i)
            if ((ch in 'A'..'Z') || (ch in 'a'..'z') || (ch in '0'..'9') || ch == '-' || ch == ',' || ch == ' ') {
                //valid characters there
                strPlace += (ch.toString())
            } else {
                //invalid character found
                strPlace = ""
            }
        }
        Log.d("ValidatedAddress", "" + strPlace)
        return strPlace
    }


    interface OnCityFetchedListener {
        fun onCityFetched(strCityPlaceId: String = "")
        fun onCityError()
    }

}
package com.pursueit.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.androidnetworking.error.ANError
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.FamilyModel
import com.pursueit.model.InterestModel
import com.pursueit.model.PlaceModel
import com.pursueit.model.UserModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_add_member.*
import org.jetbrains.anko.*
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


object MyProfileUtils {

    var arrayFamilyModel = ArrayList<FamilyModel>()
    var arrayMyAddressModel = ArrayList<PlaceModel>()

    fun fastAddFamilyMember(context: Context, hashParams: HashMap<String, String>, listener: FastNetworking.OnApiResult) {
        FastNetworking.makeRxCallPost(context, hashParams = hashParams, url = Urls.ADD_USER_FAMILY_MEMBER, includeHeaders = true, tag = "AddEditMember", onApiResult = listener)
    }

    fun fastFetchFamilyMembers(context: Context, isSuccess: (success: Boolean, json: JSONObject?, error: ANError?) -> Unit, compositeDisposable: CompositeDisposable? = null) {
        FastNetworking.makeRxCallPost(context, hashParams = HashMap<String, String>(), url = Urls.VIEW_USER_FAMILY_MEMBERS, includeHeaders = true, tag = "ViewFamilyMembers", onApiResult = object : FastNetworking.OnApiResult {
            override fun onApiSuccess(json: JSONObject?) {
                doAsync {
                    try {
                        arrayFamilyModel.clear()
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            val jsonData = json.getJSONArray("data")
                            repeat(jsonData.length()) {
                                val obj = jsonData.getJSONObject(it)
                                arrayFamilyModel.add(FamilyModel(obj.optString("family_id", "")).also { model ->

                                    val strName = obj.optString("family_name", "")
                                    model.familyMemberName = if (strName == "null") "" else strName

                                    val strDob=obj.optString("family_dob","")
                                    model.familyDob=if(strDob=="null") "" else strDob



                                    val strGender=obj.optString("family_gender", "")
                                    model.familyGender=if(strGender=="null") "" else strGender



                                    val strPrefLocation=obj.optString("family_preferred_activities", "")
                                    model.familyPrefLocation=if(strPrefLocation=="null") "" else strPrefLocation

                                   // val arrInterestModel=ArrayList<InterestModel>()
                                    try {
                                        val jarInterest=obj.getJSONArray("get_categories")

                                            repeat(jarInterest.length()){
                                                jarInterest.getJSONObject(it).run {
                                                    val strInterestId=optString("cat_id", "")
                                                   // model.familyInterestId=

                                                    val strInterests=optString("cat_name", "")
                                                   // model.familyInterests=

                                                    if (it<jarInterest.length()-1) {
                                                        model.familyInterestId += "$strInterestId,"
                                                        model.familyInterests+= "$strInterests, "
                                                    }
                                                    else
                                                    {
                                                        model.familyInterestId += strInterestId
                                                        model.familyInterests+= strInterests
                                                    }
                                                    //arrInterestModel.add(InterestModel(if(strInterestId=="null") "" else strInterestId,if(strInterests=="null") "" else strInterests))
                                                }
                                            }


                                    }catch (e:Exception){
                                        Log.e("get_categories",e.toString())
                                    }

                                  //  model.arrInterestModel=arrInterestModel

                                    try {
                                        obj.getJSONObject("get_schools").run {
                                            val strSchoolId=optString("school_id", "")
                                            model.familySchoolId=if(strSchoolId=="null") "" else strSchoolId

                                            val strSchoolName=optString("school_name", "")
                                            model.familySchoolName=if(strSchoolName=="null") "" else strSchoolName
                                        }
                                    }catch (e:Exception){
                                        Log.e("get_schools",e.toString())
                                    }

                                    model.isSelected = false
                                    model.calculateAndSetAge()
                                })
                            }
                        }
                    } catch (e: Exception) {
                        Log.d("FamilyParseExc", "" + e)
                    }
                    context.runOnUiThread {
                        isSuccess(true, json, null)
                    }
                }

            }

            override fun onApiError(error: ANError) {
                isSuccess(false, null, null)
            }

        }, compositeDisposable = compositeDisposable)
    }



    fun showAddMemberDialog(act: Activity, mySuccessCallback: () -> Unit
                            , familyModel: FamilyModel? = null, onDismissListener:(isMemberAdded:Boolean)->Unit={}, pos:Int=-1,recyclerView:RecyclerView?=null) {

        val dialogMsg=DialogMsg()

        fun fetchSchoolAndInterest()
        {
            if (ConnectionDetector.isConnectingToInternet(act)) {
                val saveSchoolAndInterest = SaveSchoolAndInterest(act)
                if (saveSchoolAndInterest.getInterestArray().size == 0 || saveSchoolAndInterest.getInterestArray().size == 0)
                    saveSchoolAndInterest.fetchAndSaveIntoSharedPref(true)
            }
            else{
                dialogMsg?.showErrorConnectionDialog(act, onRetryListener = View.OnClickListener {
                    fetchSchoolAndInterest()
                    dialogMsg?.dismissDialog(act)
                })
            }
        }

        var isEditMode = false
        var isMemberAdded=false
        val dialog = MyDialog(act).getMyDialog(R.layout.dialog_add_member)
        var selectedGender = ""
        var selectedDob = ""

        val saveSchoolAndInterest=SaveSchoolAndInterest(act)

        dialog.setOnDismissListener {
                onDismissListener(isMemberAdded)
        }

        dialog.imgMale.setOnClickListener {
            selectedGender = "male"
            dialog.imgMale.imageResource = R.drawable.ic_male_selected
            dialog.imgFemale.imageResource = R.drawable.ic_female_unselected
        }
        dialog.imgFemale.setOnClickListener {
            selectedGender = "female"
            dialog.imgMale.imageResource = R.drawable.ic_male_unselected
            dialog.imgFemale.imageResource = R.drawable.ic_female_selected
        }

        dialog.etSchoolName.setOnClickListener {
            val arraySchool=saveSchoolAndInterest.getSchoolArray()

            if (arraySchool.size==0) {
                act.toast("Please wait while fetching schools")
                fetchSchoolAndInterest()
            }
            else
                dialogMsg.showItemPickerDialog(act,dialog.etSchoolName,arraySchool,"Select School",showSearchEditText = true,searchHint = "Search School")
        }

        dialog.etInterests.setOnClickListener {
            val arrayInterest=saveSchoolAndInterest.getInterestArray()
            if (arrayInterest.size==0) {
                act.toast("Please wait while fetching interests")
                fetchSchoolAndInterest()
            }
            else {
                val interestIds=dialog.etInterests?.tag?:""
                try {
                    if (interestIds!="")
                    {
                        val arraySelectedInterestId=dialog.etInterests.tag.toString().split(",")
                        arraySelectedInterestId.forEach {interestId->
                            arrayInterest.single { it.id == interestId }.isSelected=true
                        }
                    }

                }catch (e:Exception){
                    Log.e("filtering_interest",e.toString())
                }

                dialogMsg.showItemPickerDialog(
                    act,
                    dialog.etInterests,
                    arrayInterest,
                    "Select Interest",
                    true/*,
                    showSearchEditText = true,
                    searchHint = "Search Interest"*/
                )
            }
        }

        dialog.imgCloseDialogAdd.setOnClickListener {
            dialog.dismiss()
        }

        if (familyModel != null) {
            try {
                Log.d("FamilyId", familyModel.familyId)
                isEditMode = true
                dialog.txtHeaderAddMember.text = "Edit Member"

                if (familyModel.familyGender == "male")
                    dialog.imgMale.performClick()
                else
                    dialog.imgFemale.performClick()

                dialog.etFullName.setText(familyModel.familyMemberName.trim())
                dialog.etSchoolName.setText(familyModel.familySchoolName.trim())
                dialog.etSchoolName.tag = familyModel.familySchoolId
                dialog.txtDob.text = JsonParse.getFormattedDateTimeTable(familyModel.familyDob)
                dialog.etInterests.setText(familyModel.familyInterests.trim())
                dialog.etInterests.tag=familyModel.familyInterestId
                dialog.etPrefLocation.setText(familyModel.familyPrefLocation.trim())

                setSelectionEnd(dialog.etFullName)
                setSelectionEnd(dialog.etSchoolName)
                setSelectionEnd(dialog.etInterests)
                setSelectionEnd(dialog.etPrefLocation)

            } catch (e: Exception) {

            }
        }

        dialog.txtDob.setOnClickListener {
            val strDob = dialog.txtDob.text.toString().trim()
            Log.d("strDob", strDob)

            val cal = Calendar.getInstance()

            try {
                val arrDate = JsonParse.getFormattedDateTimeTableReverse(strDob).split("-")
                Log.d("ArrDate", arrDate.toString())
                if (arrDate.size > 1) {
                    cal.set(Calendar.YEAR, arrDate[0].toInt())
                    cal.set(Calendar.MONTH, arrDate[1].toInt() - 1)
                    cal.set(Calendar.DAY_OF_MONTH, arrDate[2].toInt())
                }
            } catch (e: Exception) {
                Log.d("CalSetExc", "" + e)
            }

            val datePickerDialog = DatePickerDialog(act, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val strMonth = if ((month + 1) / 9 <= 1.0) "0${month + 1}" else "${month + 1}"
                val strDay = if ((dayOfMonth) / 9 <= 1.0) "0$dayOfMonth" else dayOfMonth.toString()
                selectedDob = "$year-$strMonth-$strDay"
                Log.d("SelectedDate", selectedDob)
                dialog.txtDob.text = JsonParse.getFormattedDateTimeTable(selectedDob)
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))

            datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis

            datePickerDialog.show()
        }

        dialog.btnSave.setOnClickListener {
            val strName = dialog.etFullName.text.toString().trim()
            val strDob = dialog.txtDob.text.toString().trim()
            val strSchoolName = dialog.etSchoolName.text.toString().trim()
            val strInterests = dialog.etInterests.text.toString().trim()
            val strPrefLocation = dialog.etPrefLocation.text.toString().trim()

            val validations = MyValidations(act)
            if (strName.isEmpty()) {

                showErrorDialog(act, "Please enter your full name")
                return@setOnClickListener
            }
            if (!validations.checkName(strName)) {
                showErrorDialog(act, "Please enter a valid full name")
                return@setOnClickListener
            }

            if (strDob.isEmpty()) {
                showErrorDialog(act, "Please enter your date of birth")
                return@setOnClickListener
            }

            if (selectedGender.isEmpty()) {
                showErrorDialog(act, "Please enter your gender")
                return@setOnClickListener
            }

          /*  if (strInterests.isEmpty()) {
                showErrorDialog(act, "Please enter your interests")
                return@setOnClickListener
            }*/

            val hashParams = HashMap<String, String>()
            hashParams["name"] = strName
            hashParams["gender"] = selectedGender
            hashParams["interests"] = strInterests
            hashParams["schoolName"] = strSchoolName
            hashParams["preferredActivities"] = strPrefLocation
            hashParams["dateOfBirth"] = JsonParse.getFormattedDateTimeTableReverse(strDob)
            hashParams["interestId"] = dialog.etInterests?.tag?.toString()?:""
            hashParams["schoolId"] = dialog.etSchoolName?.tag?.toString()?:""

            if (isEditMode)
                hashParams["familyId"] = "" + familyModel?.familyId

            if (ConnectionDetector.isConnectingToInternet(act)) {
                DialogMsg.showPleaseWait(act)
                fastAddFamilyMember(act, hashParams, object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        DialogMsg.dismissPleaseWait(act)
                        try {
                            if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                isMemberAdded=true

                                dialog.dismiss()

                                if (pos!=-1) {
                                    familyModel?.run {
                                        familyMemberName=strName
                                        familyGender=selectedGender
                                        familyDob=hashParams["dateOfBirth"]?:""
                                        familySchoolName=strSchoolName
                                        familyInterests=strInterests
                                        familyInterestId=hashParams["interestId"]?:""
                                        familySchoolId=hashParams["schoolId"]?:""
                                        arrayFamilyModel[pos]=familyModel
                                        recyclerView?.adapter?.notifyItemChanged(pos)
                                    }

                                }
                                val strSuccessMsg=json.optString("message","Your family member has been successfully updated")

                                showSuccessDialogWithListener(act,strSuccessMsg){
                                    mySuccessCallback()
                                }

                            } else {
                                act.toast(ErrorMsgs.ERR_API_MSG)
                            }
                        } catch (e: Exception) {
                            Log.d("ExcAddMember", "" + e)
                            act.toast(ErrorMsgs.ERR_PARSE_TITLE)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        DialogMsg.dismissPleaseWait(act)
                        act.toast(ErrorMsgs.ERR_API_MSG)
                    }

                })
            } else {
                act.toast(ErrorMsgs.ERR_CONNECTION_MSG)
            }


        }
        dialog.show()
    }

    fun fastFetchMyAddresses(act: Activity, onSuccess: () -> Unit, onFailure: () -> Unit, compositeDisposable: CompositeDisposable? = null) {
        try {
            if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {
                FastNetworking.makeRxCallPost(act.applicationContext, Urls.VIEW_ADDRESSES, true, HashMap<String, String>(), "ViewAddresses", object : FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        doAsync {
                            arrayMyAddressModel.clear()
                            try {
                                if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                                    val jsonArr = json.getJSONArray("data")
                                    repeat(jsonArr.length()) { pos ->
                                        val obj = jsonArr.getJSONObject(pos)
                                        val model = PlaceModel(placeId = "", placeTitle = obj.optString("address_book_title", ""), placeAddress = obj.optString("address_book_location", "")).also { placeModel ->
                                            placeModel.placeId = obj.optString("address_book_place_id", "")
                                            placeModel.placeAddressIdDb = obj.optString("address_book_id", "")
                                            placeModel.placeCityId = obj.optString("address_book_city_place_id", "")

                                            try {
                                                placeModel.placeLat = obj.getString("address_book_lat").toDouble()
                                                placeModel.placeLng = obj.getString("address_book_long").toDouble()
                                            } catch (e: Exception) {
                                                Log.d("LatLongNumberExc", "" + e)
                                            }
                                        }
                                        arrayMyAddressModel.add(model)
                                    }
                                }
                            } catch (e: Exception) {
                                Log.d("ParseExc", "" + e)
                                act.runOnUiThread {
                                    act.toast(ErrorMsgs.ERR_API_MSG)
                                }
                            } finally {
                                act.runOnUiThread {
                                    onSuccess()
                                }
                            }


                        }


                    }

                    override fun onApiError(error: ANError) {
                        act.toast(ErrorMsgs.ERR_API_MSG)
                        onFailure()
                    }

                }, compositeDisposable)
            } else {
                act.toast(ErrorMsgs.ERR_CONNECTION_MSG)
            }
        } catch (e: Exception) {
            Log.d("ViewAddressStart", "" + e)
        }
    }

    fun fastAddAddressNow(act: Activity, placeModel: PlaceModel, onSuccess: () -> Unit, compositeDisposable: CompositeDisposable? = null) {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {
            val dialogMsg = DialogMsg()
            //now add address into the address book
            DialogMsg.showPleaseWait(act)
            val hashParams = HashMap<String, String>()
            hashParams.put("title", "" + placeModel.placeTitle)
            hashParams.put("location", "" + placeModel.placeAddress)
            hashParams.put("placeId", placeModel.placeId)
            hashParams.put("cityPlaceId", placeModel.placeCityId)
            hashParams.put("latitude", "" + placeModel.placeLat)
            hashParams.put("longitude", "" + placeModel.placeLng)
            FastNetworking.makeRxCallPost(act.applicationContext, Urls.ADD_ADDRESS, true, hashParams, "Add-Address", object : FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    try {
                        DialogMsg.dismissPleaseWait(act)
                        if (json!!.getBoolean(StaticValues.KEY_STATUS)) {
                            val strSuccessMsg=json.optString("message","Your address has been successfully added to your address book")

                            showSuccessDialogWithListener(act,strSuccessMsg) {
                                onSuccess()
                            }

                        } else {
                            val strErrMsg = json.optString("message", ErrorMsgs.ERR_API_MSG)
                            dialogMsg.showErrorRetryDialog(act, "Oops!", strErrMsg, "OK", View.OnClickListener { dialogMsg.dismissDialog(act) })
                        }
                    } catch (e: Exception) {
                        Log.d("ExcAddAddress", "" + e)
                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(act)
                    dialogMsg.showErrorApiDialog(act, "OK", View.OnClickListener { dialogMsg.dismissDialog(act) })
                }

            }, compositeDisposable)
        }
    }

    fun fastUpdateFcmToken(act: Activity,clearToken:Boolean=false) {
        if (clearToken) {
            act.runOnUiThread {
                DialogMsg.showPleaseWait(act)
            }
        }

        doAsync {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("FcmTokenFail", "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    doAsync {
                        // Get new Instance ID token
                        val token = task.result?.token

                        val hashParams = HashMap<String, String>()

                        if (clearToken)
                        {
                            hashParams.put("fcmToken", "")
                        }
                        else
                            hashParams.put("fcmToken", token ?: "")
                        FastNetworking.makeRxCallPost(act, Urls.UPDATE_FCM_TOKEN, true, hashParams, "UpdateFcmToken", object : FastNetworking.OnApiResult {
                            override fun onApiSuccess(json: JSONObject?) {
                                if (clearToken) {
                                    act.runOnUiThread {
                                        DialogMsg.dismissPleaseWait(act)
                                        LoginBasicsUi.logOutUser(act)
                                    }

                                }
                            }

                            override fun onApiError(error: ANError) {
                                if (clearToken) {
                                    act.runOnUiThread {
                                        DialogMsg.dismissPleaseWait(act)
                                        LoginBasicsUi.logOutUser(act)
                                    }
                                }

                            }

                        })

                    }

                })
        }
    }

    private fun showErrorDialog(act: Activity, strMsg: String) {
        val dialogMsg = DialogMsg()
        dialogMsg.showErrorRetryDialog(act, "Alert", strMsg, "OK", View.OnClickListener {
            dialogMsg.dismissDialog(act)
        })
    }

    fun isNotNull(str: String): Boolean {
        return !str.trim().equals("null", true)
    }

    fun setSelectionEnd(et: EditText) {
        if (et.text.trim().isNotEmpty()) {
            et.setSelection(et.text.toString().trim().length)
        }
    }

    fun fastEditProfile(act: Activity, userModel: UserModel, imageFile:File?=null,compositeDisposable: CompositeDisposable?=null,isPhoneVerified:Boolean=userModel.isPhoneVerified) {
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {

            val dialogMsg=DialogMsg()

            DialogMsg.showPleaseWait(act)

            val hashParams = HashMap<String, String>()
            hashParams["firstName"] = userModel.userFNAme.trim()
            hashParams["lastName"] = userModel.userLName.trim()
            if (isPhoneVerified)
                hashParams["phoneNumber"] = userModel.userPhone.trim()
            else
                hashParams["phoneNumber"] =""
            hashParams["isVerified_mobile"]=if (isPhoneVerified) "1" else "0"



            FastNetworking.makeRxCallPostImageUpload(act.applicationContext,Urls.EDIT_USER_PROFILE,true,"profilePicture",imageFile,hashParams,"MyProfilePhotoUpdate",object : FastNetworking.OnApiResult{
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(act)
                    try{
                        if(json!!.getBoolean(StaticValues.KEY_STATUS)){

                            if(isPhoneVerified)
                                userModel.isPhoneVerified = isPhoneVerified

                            val strMsg=json.optString("message","Your profile has been successfully updated")
                            dialogMsg.showSuccessDialog(act,"Success",strMsg,"OK",View.OnClickListener {
                                dialogMsg.dismissDialog(act)
                            },isCancellable = true)
                        }else{
                            dialogMsg.showErrorApiDialog(act,"OK",View.OnClickListener {
                                DialogMsg.dismissPleaseWait(act)
                            },isCancellable = true)
                        }
                    }catch (e:Exception){
                        Log.d("ProfileEditParseExc",""+e)
                        dialogMsg.showErrorApiDialog(act,"OK",View.OnClickListener {
                            DialogMsg.dismissPleaseWait(act)
                        },isCancellable = true)
                    }
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(act)
                    try{
                        dialogMsg.showErrorApiDialog(act,"OK",View.OnClickListener {
                            DialogMsg.dismissPleaseWait(act)
                        },isCancellable = true)
                    }catch (e:Exception){

                    }
                }

            },compositeDisposable)

        }
    }

    fun fastRefreshProfile(context: Context, onSuccess: (userModel: UserModel?) -> Unit, compositeDisposable: CompositeDisposable? = null) {
        val userModel=UserModel.getUserModel(context)
        if(userModel.userId.trim().isNotEmpty()) {
            if (ConnectionDetector.isConnectingToInternet(context)) {
                FastNetworking.makeRxCallPost(context, Urls.VIEW_USER_PROFILE, true, HashMap<String, String>(), "ViewMyProfile", object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            UserModel.parseUserJson(context, json!!, { userModel ->
                                onSuccess(userModel)
                            }, { ex ->
                                Log.d("ExcUser", "" + ex)
                                onSuccess(null)
                            })
                        } catch (e: Exception) {
                            Log.d("Exc_ViewProfile", "" + e)
                            onSuccess(null)
                        }
                    }

                    override fun onApiError(error: ANError) {
                        onSuccess(null)
                    }

                }, compositeDisposable)
            }
        }
    }


    //flag-> 1 (address delete),  2 (family member delete)
    fun fastDeleteAddressOrFamilyMember(act: Activity, id: String, flag: Int, onSuccess: () -> Unit, onError: () -> Unit) {
        val hashParams = HashMap<String, String>()
        var strUrl = ""
        var strTag = ""
        if (flag == 1) {
            hashParams.put("addressId", id)
            strUrl = Urls.DELETE_ADDRESS
            strTag = "DeleteAddress"
        } else {
            hashParams.put("familyId", id)
            strUrl = Urls.DELETE_USER_FAMILY_MEMBER
            strTag = "DeleteFamilyMember"
        }

        DialogMsg.showPleaseWait(act)
        FastNetworking.makeRxCallPost(act.applicationContext, strUrl, true, hashParams, strTag, object : FastNetworking.OnApiResult {
            override fun onApiSuccess(json: JSONObject?) {
                DialogMsg.dismissPleaseWait(act)

                try {

                    showSuccessDialogWithListener(act,if(flag==1) "Your address has been successfully deleted" else "Your family member has been successfully deleted"){
                        onSuccess()
                    }

                }catch (e:Exception){

                }
            }

            override fun onApiError(error: ANError) {
                DialogMsg.dismissPleaseWait(act)
                onError()
            }

        })
    }

    fun setPasswordToggle(imgPass: ImageView, etPass: EditText, isPasswordShowing: Boolean) {

        if (isPasswordShowing) {
            imgPass.imageResource = R.drawable.ic_password_showing
            etPass.transformationMethod = DoNothingTransformationMethod()
        } else {
            imgPass.imageResource = R.drawable.ic_password_not_showing
            etPass.transformationMethod = PasswordTransformationMethod()
        }

        if (etPass.text.toString().isNotEmpty()) {
            etPass.setSelection(etPass.text.toString().length)
        }
    }

    fun showSuccessDialogWithListener(act: Activity,strSuccessMsg:String,onSuccess: () -> Unit){
        val dialogSuccess=DialogMsg()
        dialogSuccess.showSuccessDialog(act,"Success",strSuccessMsg,"OK",View.OnClickListener {
            dialogSuccess.dismissDialog(act)
            onSuccess()
        },isCancellable =false)
    }

    fun showSignUpLoginPopUp(act: Activity){
        val dialogErr=DialogMsg()
        dialogErr.showYesCancelLoginRegisterDialog(act,"Please login/register to explore this feature",View.OnClickListener {
            dialogErr.dismissDialog(act)
            MyNavigations.goToMainActivity(act)
        },isCancellable = true)

    }

}
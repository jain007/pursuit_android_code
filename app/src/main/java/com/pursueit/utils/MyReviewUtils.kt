package com.pursueit.utils

import android.app.Activity
import android.view.View
import com.androidnetworking.error.ANError
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONObject
import java.util.*


object MyReviewUtils {


    fun fastAddRatingReview(
        act: Activity, hashParams: HashMap<String, String> = HashMap()
        , tag: String = "RxCall_", onSuccess: (json: JSONObject?) -> Unit
        , compositeDisposable: CompositeDisposable? = null, fromCamp: Boolean = false
    ) {
        val dialogMsg = DialogMsg()

      /*  fun showErrorDialog(strMsg: String) {
            dialogMsg?.showErrorRetryDialog(act, "Alert", strMsg, "OK", View.OnClickListener {
                dialogMsg?.dismissDialog(act)
            })
        }
        hashParams.get("reviewDescription")?.let {
            if (it.isNotEmpty() && it.length < 10) {
                showErrorDialog("Please write some review and then proceed")
                return
            }
        }*/

        DialogMsg.showPleaseWait(act)
        val url = if (fromCamp) Urls.ADD_CAMP_REVIEW else Urls.ADD_REVIEW
        FastNetworking.makeRxCallPost(
            act.applicationContext,
            url,
            true,
            hashParams,
            tag,
            object : FastNetworking.OnApiResult {
                override fun onApiSuccess(json: JSONObject?) {
                    DialogMsg.dismissPleaseWait(act)
                    onSuccess(json)
                }

                override fun onApiError(error: ANError) {
                    DialogMsg.dismissPleaseWait(act)
                    dialogMsg.showErrorApiDialog(
                        act,
                        "OK",
                        View.OnClickListener { dialogMsg.dismissDialog(act) },
                        isCancellable = true
                    )
                }

            },
            compositeDisposable
        )
    }
}
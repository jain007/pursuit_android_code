package com.pursueit.utils

import android.graphics.Color
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.pursueit.R
import com.pursueit.custom.CustomFont
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class MySnackBar(var act: AppCompatActivity, var view: View) {

    var snackbar: Snackbar? = null

    fun showSnackBarError(strMsg: String) {
        if (snackbar == null) {
            snackbar = Snackbar.make(view, strMsg, Snackbar.LENGTH_LONG)
            snackbar?.view?.backgroundColor=ContextCompat.getColor(act, R.color.colorErrorRed)
            val textView=snackbar?.view?.findViewById<TextView>(android.support.design.R.id.snackbar_text)
            textView?.typeface=CustomFont.setFontSemiBold(act.assets)
            textView?.textColor=Color.WHITE
            textView?.maxLines=4
        }

        snackbar?.setText(strMsg)
        snackbar?.show()
    }

}
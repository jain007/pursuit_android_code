package com.pursueit.utils

import android.app.Activity
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import java.util.regex.Pattern


class MyValidations(val act: Activity) {


    private val EMAIL_ADDRESS_PATTERN = Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")

    val NAME_PATTERN = Pattern.compile("^[a-zA-Z ]*$")

    private val PASSWORD_PATTERN = Pattern
            .compile("^[a-zA-Z0-9+_.]*\${4,16}")

    private val MobilePattern = Pattern
            .compile("^[0-9+]{9}$")

    private val PIN_CODE = Pattern.compile("^[1-9][0-9]{5}$")



    fun isValidEmail(email: String): Boolean {

        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun requestFocus(view: View) {
        if (view.requestFocus()) {
            act.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }


    fun checkPassword(password: String): Boolean {
        return PASSWORD_PATTERN.matcher(password).matches()
    }

    fun checkMobile(mobile: String): Boolean {
        return MobilePattern.matcher(mobile).matches()
    }

    fun checkName(name: String): Boolean {
        return NAME_PATTERN.matcher(name).matches()
    }

    fun checkEmail(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

    fun checkWebsiteUrl(url:String):Boolean{
        return android.util.Patterns.WEB_URL.matcher(url).matches()
    }
}
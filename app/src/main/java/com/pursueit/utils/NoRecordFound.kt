package com.pursueit.utils

import android.app.Activity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.content_empty_bookings.*
import kotlinx.android.synthetic.main.content_empty_bookings.view.*
import kotlinx.android.synthetic.main.empty_search_or_filter.view.*




class NoRecordFound(val act:Activity,var view:View?=null, val fromNearByOrCampAct:Boolean=false){

    private lateinit var linEmpty_:LinearLayout
    private lateinit var imgEmpty_:ImageView
    private lateinit var txtEmptyTitle_:TextView
    private lateinit var txtEmptySubTitle_:TextView
    private lateinit var btnRetry_:Button



    fun init(){

        if(view!=null){
            linEmpty_=view!!.linEmpty
            imgEmpty_=view!!.imgEmpty
            txtEmptyTitle_=view!!.txtEmptyTitle
            txtEmptySubTitle_=view!!.txtEmptySubTitle
            btnRetry_=view!!.btnRetry
        }else{
            linEmpty_=act.linEmpty
            imgEmpty_=act.imgEmpty
            txtEmptyTitle_=act.txtEmptyTitle
            txtEmptySubTitle_=act.txtEmptySubTitle
            btnRetry_=act.btnRetry
        }
    }

    fun showNoFavorites(){
        linEmpty_.visibility=View.VISIBLE
        Glide.with(act.applicationContext).load(com.pursueit.R.drawable.ic_empty_favorites).into(imgEmpty_)
        txtEmptyTitle_.text="You don't have any favourites"
        txtEmptySubTitle_.text="Check out camps, activity and add them to your favorite list."
    }

    fun showNoActivities(strMsg:String,clickListener:View.OnClickListener,fromNotification:Boolean=false){
        if (fromNearByOrCampAct)
        {
            linEmpty_.visibility = View.GONE
            imgEmpty_.visibility = View.GONE
            view?.cnsEmptySearchAndFilter?.visibility=View.VISIBLE
        }
        else {
            linEmpty_.visibility = View.VISIBLE
            imgEmpty_.visibility = View.GONE
            txtEmptyTitle_.text =
                if (strMsg.equals(act.getString(com.pursueit.R.string.error_message))) act.getString(
                    com.pursueit.R.string.app_name) else "Oops!"
            txtEmptySubTitle_.text = strMsg
            btnRetry_.visibility =
                if (strMsg.equals(act.getString(com.pursueit.R.string.error_message))) View.GONE else View.VISIBLE
            btnRetry_.setOnClickListener(clickListener)
        }
        if (fromNotification)
        {
            imgEmpty_.visibility=View.VISIBLE
            Glide.with(act.applicationContext).load(com.pursueit.R.drawable.no_records_found).into(imgEmpty_)
            txtEmptyTitle_.text =""
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0,0,0,0)
            txtEmptyTitle_.layoutParams = params
        }
    }

    fun showNoBookings(onSearchActivityClick:()->Unit){
        linEmpty_.visibility=View.VISIBLE
        btnRetry_.visibility=View.VISIBLE
        Glide.with(act.applicationContext).load(com.pursueit.R.drawable.ic_empty_bookings).into(imgEmpty_)
        txtEmptyTitle_.text="You don't have any bookings"
        txtEmptySubTitle_.text="You don't have any upcoming classes.\nTime to book next one."

        btnRetry_.text="Search Activity"
        btnRetry_.setOnClickListener {
            onSearchActivityClick()
        }
    }

    fun showNoPaymentTransactions(){
        linEmpty_.visibility=View.VISIBLE
        Glide.with(act.applicationContext).load(com.pursueit.R.drawable.ic_empty_bookings).into(imgEmpty_)
        txtEmptyTitle_.text="No Payments"
        txtEmptySubTitle_.text="You don't have any payment history."
    }

    fun showMsgError(strTitle:String="Oops!",strDesc:String,onClickListener:View.OnClickListener){
        imgEmpty_.visibility=View.GONE
        linEmpty_.visibility=View.VISIBLE
        Glide.with(act.applicationContext).load(com.pursueit.R.drawable.ic_empty_bookings).into(imgEmpty_)
        txtEmptyTitle_.text="$strTitle"
        txtEmptySubTitle_.text="$strDesc"
        btnRetry_.setOnClickListener(onClickListener)
        btnRetry_.visibility=View.VISIBLE
    }

    fun hideEmptyUi(){
        if (fromNearByOrCampAct)
        {
            linEmpty_.visibility = View.GONE
            imgEmpty_.visibility = View.GONE
            view?.cnsEmptySearchAndFilter?.visibility=View.GONE
        }
        linEmpty_.visibility=View.GONE
    }

}
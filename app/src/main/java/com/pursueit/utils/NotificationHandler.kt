package com.pursueit.utils

import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.pursueit.activities.DashboardActivity
import com.pursueit.model.ActivityModel
import com.pursueit.model.MyBookingsModel
import com.pursueit.model.NotificationModel

class NotificationHandler(val activity:AppCompatActivity)
{
    fun navigateNotification(notificationModel: NotificationModel, finishActivityAfterNavigation:Boolean=false)
    {
        notificationModel.bookingsModel?.bookingStatus=getBookingStatusFromNotificationType(notificationModel.notificationType)
        if (notificationModel.bookingsModel?.bookingStatus==StaticValues.BOOKING_STATUS_CANCELLED)
        {
            notificationModel.bookingsModel.bookingCancelledBy =MyBookingsModel.CANCELLED_BY_ADMIN
        }
        when(notificationModel.notificationType)
        {

            NotificationModel.TYPE_REVIEW_ACTIVITY->{
                val activityModel=ActivityModel(notificationModel.moduleId,notificationModel.moduleName,activityImageUrl = notificationModel.moduleImageUrl)
                notificationModel.bookingsModel?.bookingStatus=StaticValues.BOOKING_STATUS_COMPLETED
                MyNavigations.goToActivityDetail(activity,activityModel,0,true,notificationModel.bookingsModel,notificationModel)
            }

            NotificationModel.TYPE_REVIEW_FLEXI_ACTIVITY->{
                val activityModel=ActivityModel(notificationModel.moduleId,notificationModel.moduleName,activityImageUrl = notificationModel.moduleImageUrl)
                notificationModel.bookingsModel?.bookingStatus=StaticValues.BOOKING_STATUS_COMPLETED
                MyNavigations.goToFlexiActivityDetail(activity,activityModel,0,true,notificationModel.bookingsModel)
            }

            NotificationModel.TYPE_REVIEW_CAMP->{
                MyNavigations.goToCampDetail(activity,notificationModel.moduleId,true,StaticValues.BOOKING_STATUS_COMPLETED,notificationModel.bookingsModel)
            }

            NotificationModel.TYPE_ACTIVITY_BOOKING_REMIDER, NotificationModel.TYPE_ACTIVITY_ACCEPTED, NotificationModel.TYPE_ACTIVITY_REJECTED, NotificationModel.TYPE_ACTIVITY_CANCELLED->{
                val activityModel=ActivityModel(notificationModel.moduleId,notificationModel.moduleName,activityImageUrl = notificationModel.moduleImageUrl)
                MyNavigations.goToActivityDetail(activity,activityModel,0,true,notificationModel.bookingsModel,notificationModel)
            }

            NotificationModel.TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER, NotificationModel.TYPE_FLEXI_ACCEPTED, NotificationModel.TYPE_FLEXI_REJECTED, NotificationModel.TYPE_FLEXI_CANCELLED->{
                val activityModel=ActivityModel(notificationModel.moduleId,notificationModel.moduleName,activityImageUrl = notificationModel.moduleImageUrl)
               /* if (NotificationModel.TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER==notificationModel.notificationType)
                {
                    notificationModel.bookingsModel?.bookingStatus=StaticValues.BOOKING_STATUS_ONGOING
                }*/

                Log.d("BookingStatusNoti","id: "+notificationModel.bookingsModel?.bookingId+" -> "+notificationModel.bookingsModel?.bookingStatus)
                MyNavigations.goToFlexiActivityDetail(activity,activityModel,0,true,notificationModel.bookingsModel)
            }

            NotificationModel.TYPE_CAMP_ACCEPTED, NotificationModel.TYPE_CAMP_REJECTED, NotificationModel.TYPE_CAMP_CANCELLED->{
                Log.d("BookingStatus_Camp",""+notificationModel.bookingsModel?.bookingStatus)
                MyNavigations.goToCampDetail(activity,notificationModel.moduleId,true,notificationModel.bookingsModel?.bookingStatus?:"",notificationModel.bookingsModel)
            }
        }
        DashboardActivity.refreshNearbyPlaces=true
        if (finishActivityAfterNavigation)
            activity.finish()
    }

    private fun getBookingStatusFromNotificationType(notificationType:String):String{
        NotificationModel.run {
            when(notificationType){
                TYPE_REVIEW_ACTIVITY, TYPE_REVIEW_FLEXI_ACTIVITY, TYPE_REVIEW_CAMP->{
                    return StaticValues.BOOKING_STATUS_COMPLETED
                }
                TYPE_ACTIVITY_BOOKING_REMIDER, TYPE_FLEXI_ACTIVITY_BOOKING_REMINDER->{
                    return StaticValues.BOOKING_STATUS_ONGOING
                }
                TYPE_ACTIVITY_ACCEPTED,TYPE_CAMP_ACCEPTED,TYPE_FLEXI_ACCEPTED->{
                    return StaticValues.BOOKING_STATUS_UPCOMING
                }
                TYPE_ACTIVITY_REJECTED,TYPE_CAMP_REJECTED,TYPE_FLEXI_REJECTED->{
                    return StaticValues.BOOKING_STATUS_DECLINED
                }
                TYPE_ACTIVITY_CANCELLED,TYPE_FLEXI_CANCELLED,TYPE_CAMP_CANCELLED->{
                    return StaticValues.BOOKING_STATUS_CANCELLED
                }
                else->{
                    return ""
                }
            }
        }

    }
}
package com.pursueit.utils

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class PaddingItemDecoration(
    private val paddingStart: Int = 0,
    private val paddingEnd: Int = 0) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)

        if (position == 0) {
                outRect.left += paddingStart
           // outRect.top+=paddingTop
        }
        if (position == (parent.adapter?.itemCount ?: 0) - 1) {
                outRect.right += paddingEnd
           // outRect.bottom+=paddingBottom
        }
    }
}
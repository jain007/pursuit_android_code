package com.pursueit.utils

import android.app.Activity
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.pursueit.R
import com.pursueit.activities.DashboardActivity
import com.pursueit.adapters.ApplyPromotionAdapter
import com.pursueit.dialogs.MyDialog
import com.pursueit.model.*
import kotlinx.android.synthetic.main.dialog_apply_promocode.*
import kotlinx.android.synthetic.main.layout_promotion_ribbon.view.*
import org.jetbrains.anko.textColor
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat

object PromotionOperation {
    fun setPromotionText(linPromotion: View, maxValuePromotionModel: PromotionModel?) {
        if (maxValuePromotionModel != null) {
            /*if (maxValuePromotionModel.offerType == PromotionModel.OFFER_TYPE_FLAT) {
                linPromotion.txtPromotion.text="AED "+maxValuePromotionModel.offerValue + " Off"
            } else {
                linPromotion.txtPromotion.text=maxValuePromotionModel.offerValue + "% Off"
            }*/
            linPromotion.txtPromotion.text = getPromotionText(maxValuePromotionModel)
            linPromotion.visibility = View.VISIBLE
        } else {
            linPromotion.visibility = View.GONE
        }
    }

    fun getPromotionText(promotionModel: PromotionModel): String {
        if (promotionModel.offerType == PromotionModel.OFFER_TYPE_FLAT) {
            return "AED " + promotionModel.offerValue + " Off"
        } else {
            return promotionModel.offerValue + "% Off"
        }
    }


    fun setPromotionDataInActModel(jsonObj: JSONObject,
                                   model: ActivityModel,
                                   providerId: String,
                                   arraySlots: ArrayList<ActivitySlotModel>?,
                                   arrayFlexiSessions: ArrayList<ActivityFlexiSessionModel>?,
                                   jsonMainParent:JSONObject?) {

        //calculate discount to be displayed in case of normal/single activities
        fun getMaxPromotionModelRevised(actName: String,
                                        arrPromotionModel: ArrayList<PromotionModel>,
                                        arraySlotsInner: ArrayList<ActivitySlotModel>): PromotionModel? {
            try {
                if (arraySlotsInner.size > 0)
                    Log.d("PromoSlotPriceOne", "" + arraySlotsInner[0].slotPricePayable)


                val maxSlotPricePayable = arraySlotsInner.maxBy { it.slotPricePayable.toDoubleOrNull() ?: 0.0 }?.slotPricePayable?.toDoubleOrNull() ?: 0.0

                val percentageOffPromotionModel = arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }.maxBy { it.offerValue.toDoubleOrNull() ?: 0.0 }
                val priceFlatOffPromotionModel = arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_FLAT }.maxBy { it.offerValue.toDoubleOrNull() ?: 0.0 }

                val percentPoint = (percentageOffPromotionModel?.offerValue?.toDoubleOrNull() ?: 0.0) / 100.0
                val maxPromotionPercentage = maxSlotPricePayable * percentPoint
                val maxPromotionFlat = priceFlatOffPromotionModel?.offerValue?.toDoubleOrNull() ?: 0.0

                Log.d("PromotionMaxSlot_", "$actName: MaxSlot: $maxSlotPricePayable")
                Log.d("PromotionMaxPercent", "$actName: Deduction: ${percentPoint*100}%")
                Log.d("PromotionMaxPercent", "$actName: $maxPromotionPercentage")
                Log.d("PromotionMaxFlat", "$actName: $maxPromotionFlat")

                return if (maxPromotionPercentage > maxPromotionFlat) percentageOffPromotionModel else priceFlatOffPromotionModel

            } catch (e: Exception) {
                Log.d("PromotionOffModel", "$actName: $e")
            }

            return getMaxPromotionModel(arrPromotionModel)

        }

        //calculate discount to be displayed in case of flexi activities
        fun getMaxPromotionModelRevisedFlexi(actName: String,
                                             arrPromotionModel: ArrayList<PromotionModel>,
                                             arrayFlexiSessions: ArrayList<ActivityFlexiSessionModel>): PromotionModel? {
            try {

                val maxPrice = arrayFlexiSessions.maxBy { it.price.toDoubleOrNull() ?: 0.0 }?.price?.toDoubleOrNull() ?: 0.0

                val percentageOffPromotionModel = arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }.maxBy { it.offerValue.toDoubleOrNull() ?: 0.0 }
                val priceFlatOffPromotionModel = arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_FLAT }.maxBy { it.offerValue.toDoubleOrNull() ?: 0.0 }

                val percentPoint = (percentageOffPromotionModel?.offerValue?.toDoubleOrNull() ?: 0.0) / 100.0
                val maxPromotionPercentage = maxPrice * percentPoint
                val maxPromotionFlat = priceFlatOffPromotionModel?.offerValue?.toDoubleOrNull() ?: 0.0

                Log.d("PromotionMaxFlexi_", "$actName: MaxSlot: $maxPrice")
                Log.d("PromotionMaxPercent", "$actName: Deduction: ${percentPoint*100}%")
                Log.d("PromotionMaxPercent", "$actName: Deduction: Flat $maxPromotionPercentage")
                Log.d("PromotionMaxFlat", "$actName: $maxPromotionFlat")

                return if (maxPromotionPercentage > maxPromotionFlat) percentageOffPromotionModel else priceFlatOffPromotionModel

            } catch (e: Exception) {
                Log.d("PromotionOffModel", "$actName: $e")
            }

            return getMaxPromotionModel(arrPromotionModel)

        }

        val arrPromotionModel = ArrayList<PromotionModel>()
        if (jsonObj.has("category_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("category_promotion"), providerId))
        }
        //multipleCategoryPromotion
        if (jsonObj.has("multiple_category_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("multiple_category_promotion"), providerId))
        }

        if (jsonObj.has("activity_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("activity_promotion"), providerId))
        }

        //Review Promotion
        try {
            if (jsonMainParent != null) {
                if (jsonMainParent.has("reviews_promotion")) {
                    Log.d("PromoReviewModel",""+jsonMainParent.optJSONArray("reviews_promotion"))
                    arrPromotionModel.addAll(JsonParse.parsePromotion(jsonMainParent.getJSONArray("reviews_promotion"), providerId))
                }
            }
        }catch (e:Exception){
            Log.d("Exc_RevPromo",""+e)
        }

        StaticValues.arrGlobalPromotion.forEach {
            it.isApplied = false
        }

        arrPromotionModel.addAll(StaticValues.arrGlobalPromotion)

        model.arrPromotionModel = arrPromotionModel

        Log.d("PromoArraySize",""+arrPromotionModel.size)

        if ((model.activityType == ActivityModel.NORMAL_ACTIVITY || model.activityType == ActivityModel.SINGLE_ACTIVITY) && arraySlots != null)
            model.maxValuePromotionModel = getMaxPromotionModelRevised(actName = model.activityName, arrPromotionModel = arrPromotionModel, arraySlotsInner = arraySlots)
        else if(model.activityType==ActivityModel.FLEXI_ACTIVITY && arrayFlexiSessions!=null)
            model.maxValuePromotionModel = getMaxPromotionModelRevisedFlexi(actName = model.activityName, arrPromotionModel = arrPromotionModel, arrayFlexiSessions = arrayFlexiSessions)
        else
            model.maxValuePromotionModel = getMaxPromotionModel(arrPromotionModel)
    }

    private fun getMaxPromotionModel(arrPromotionModel: ArrayList<PromotionModel>): PromotionModel? {
        try {
            if (arrPromotionModel.any { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }) {
                return arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }
                    .maxBy { it.offerValue.toFloat() }
            } else {
                return arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_FLAT }
                    .maxBy { it.offerValue.toFloat() }
            }
        } catch (e: Exception) {
            Log.d("PromotionOffModel", "OldModelPicked: $e")
        }
        return null
    }


    fun setPromotionDataInCampModel(jsonObj: JSONObject, campModel: CampModel, providerId: String, maxPrice:String, jsonMainParent: JSONObject?) {

        //calculate discount to be displayed in case of flexi activities
        fun getMaxPromotionModelCamp(campName: String,
                                             arrPromotionModel: ArrayList<PromotionModel>,
                                             maxPrice:Double): PromotionModel? {
            try {


                val percentageOffPromotionModel = arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }.maxBy { it.offerValue.toDoubleOrNull() ?: 0.0 }
                val priceFlatOffPromotionModel = arrPromotionModel.filter { it.offerType == PromotionModel.OFFER_TYPE_FLAT }.maxBy { it.offerValue.toDoubleOrNull() ?: 0.0 }

                val percentPoint = (percentageOffPromotionModel?.offerValue?.toDoubleOrNull() ?: 0.0) / 100.0
                val maxPromotionPercentage = maxPrice * percentPoint
                val maxPromotionFlat = priceFlatOffPromotionModel?.offerValue?.toDoubleOrNull() ?: 0.0

                Log.d("PromotionMaxCamp_", "$campName: MaxPrice: $maxPrice")
                Log.d("PromotionMaxPercent", "$campName: Deduction: ${percentPoint*100}%")
                Log.d("PromotionMaxPercent", "$campName: Deduction: Flat $maxPromotionPercentage")
                Log.d("PromotionMaxFlat", "$campName: $maxPromotionFlat")

                return if (maxPromotionPercentage > maxPromotionFlat) percentageOffPromotionModel else priceFlatOffPromotionModel

            } catch (e: Exception) {
                Log.d("PromotionOffModel", "$campName: $e")
            }

            return getMaxPromotionModel(arrPromotionModel)

        }

        val arrPromotionModel = ArrayList<PromotionModel>()
        if (jsonObj.has("get_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("get_promotion"), providerId))
        }
        if (jsonObj.has("get_multiple_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("get_multiple_promotion"), providerId))
        }

        //Review Promotion
        try {
            if (jsonMainParent != null) {
                if (jsonMainParent.has("reviews_promotion")) {
                    Log.d("PromoReviewModel",""+jsonMainParent.optJSONArray("reviews_promotion"))
                    arrPromotionModel.addAll(JsonParse.parsePromotion(jsonMainParent.getJSONArray("reviews_promotion"), providerId))
                }
            }
        }catch (e:Exception){
            Log.d("Exc_RevPromo",""+e)
        }

        Log.d("PromoArraySize",""+arrPromotionModel.size)

        StaticValues.arrGlobalPromotion.forEach {
            it.isApplied = false
        }

        arrPromotionModel.addAll(StaticValues.arrGlobalPromotion)
        campModel.arrPromotionModel = arrPromotionModel
        campModel.maxValuePromotionModel = getMaxPromotionModelCamp(campModel.campName,arrPromotionModel,maxPrice.toDoubleOrNull() ?: 0.0)
    }


    fun setPromotionDataInPassModel(jsonObj: JSONObject, passModel: PassModel, providerId: String) {

        val arrPromotionModel = ArrayList<PromotionModel>()
        if (jsonObj.has("multiple_category_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("multiple_category_promotion"), providerId))
        }
        if (jsonObj.has("activity_promotion")) {
            arrPromotionModel.addAll(JsonParse.parsePromotion(jsonObj.getJSONArray("activity_promotion"), providerId))
        }

        StaticValues.arrGlobalPromotion.forEach {
            it.isApplied = false
        }

        arrPromotionModel.addAll(StaticValues.arrGlobalPromotion)

        passModel.arrPromotionModel = arrPromotionModel

        passModel.maxValuePromotionModel = getMaxPromotionModel(arrPromotionModel)

    }

    fun showApplyPromoCodeDialog(arrPromotionModel: ArrayList<PromotionModel>,
                                 act: Activity,
                                 totalPrice: Double,
                                 getCalculationResult: (totalDiscountPrice: Double, discountedPrice: Double) -> Unit) {
        if (arrPromotionModel.size == 0)
            return
        val sortedArray = arrPromotionModel.sortedWith(compareBy({ it.offerValue }, { it.offerType }))
        val dialog = MyDialog(act).getMyDialog(R.layout.dialog_apply_promocode)
        dialog.rvApplyPromoCode.layoutManager = LinearLayoutManager(act)
        dialog.rvApplyPromoCode.addItemDecoration(DividerItemDecoration(act, DividerItemDecoration.VERTICAL))

        sortedArray.forEach {
            Log.d("Promotion___", "" + it.promotionMinimumOrderAmount)
        }

        val adapter = ApplyPromotionAdapter(act, ArrayList(sortedArray), totalPrice, getCalculationResult, dialog.txtErrorPromodeCode)
        dialog.rvApplyPromoCode.adapter = adapter
        if (!act.isFinishing)
            dialog.show()

        dialog.imgClose.setOnClickListener {
            if (dialog.isShowing)
                dialog.dismiss()
        }
    }

    fun calculateFinalPromotionDiscount(
        currentOrderAmount: Double,
        getCalculationResult: (totalDiscountPrice: Double, discountedPrice: Double) -> Unit,
        arrPromotionModel: ArrayList<PromotionModel>
    ) {
        var totalDiscountPrice = 0.0
        val arrAppliedPromotion = arrPromotionModel.filter { it.isApplied }.sortedBy { it.offerValue }
        if (arrAppliedPromotion.any { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE } && arrAppliedPromotion.any { it.offerType == PromotionModel.OFFER_TYPE_FLAT }) {
            val percentPromotionModel =
                arrAppliedPromotion.single { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }
            totalDiscountPrice += calculatePromotion(currentOrderAmount, percentPromotionModel)

            val flatPromotionModel = arrAppliedPromotion.single { it.offerType == PromotionModel.OFFER_TYPE_FLAT }
            if (currentOrderAmount > totalDiscountPrice)
                totalDiscountPrice += calculatePromotion(currentOrderAmount - totalDiscountPrice, flatPromotionModel)
        } else if (arrAppliedPromotion.all { it.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE }) {

            /*if (arrAppliedPromotion.size>1) {
                val smallestValueModel = arrAppliedPromotion.minBy { it.offerValue }!!
                totalDiscountPrice += calculatePromotion(currentOrderAmount, smallestValueModel)

                val largestValueModel = arrAppliedPromotion.maxBy { it.offerValue }!!
                totalDiscountPrice += calculatePromotion(currentOrderAmount, largestValueModel)
            }
            else {
                totalDiscountPrice += calculatePromotion(currentOrderAmount,arrAppliedPromotion[0])
            }*/

            arrAppliedPromotion.forEach {
                if (currentOrderAmount > totalDiscountPrice)
                    totalDiscountPrice += calculatePromotion(currentOrderAmount - totalDiscountPrice, it)
            }
        } else if (arrAppliedPromotion.all { it.offerType == PromotionModel.OFFER_TYPE_FLAT }) {
            arrAppliedPromotion.forEach {
                if (currentOrderAmount > totalDiscountPrice)
                    totalDiscountPrice += calculatePromotion(currentOrderAmount - totalDiscountPrice, it)
            }
        }

        val refreshedDiscountPrice: Double
        if (totalDiscountPrice >= currentOrderAmount) {
            // 100% discount
            refreshedDiscountPrice = currentOrderAmount
        } else {
            refreshedDiscountPrice = totalDiscountPrice
        }
        val discountedPrice = currentOrderAmount - refreshedDiscountPrice

        //getCalculationResult(refreshedDiscountPrice.roundToLong().toDouble(), Math.round(discountedPrice).toDouble())

        //getCalculationResult(refreshedDiscountPrice, discountedPrice)
        getCalculationResult(DecimalFormat("##.##").format(refreshedDiscountPrice).toDouble(), discountedPrice)
    }

    private fun calculatePromotion(amount: Double, promotionModel: PromotionModel): Double {
        var discountValue: Double
        if (promotionModel.offerType == PromotionModel.OFFER_TYPE_PERCENTAGE) {
            discountValue = amount * (promotionModel.offerValue.toFloat() / 100.0f)
            if (discountValue >= promotionModel.upToMaxAmount.toDouble()) {
                discountValue = promotionModel.upToMaxAmount.toDouble()
            }
        } else {
            discountValue = promotionModel.offerValue.toDouble()
        }
        Log.d("DiscountValue", discountValue.toString())

        Log.d("DiscountValueAfterUpto", discountValue.toString())
        promotionModel.discountAmount = discountValue
        return discountValue
    }

    fun getPromotionJsonArray(arrPromotionModel: ArrayList<PromotionModel>): String {
        val jarPromotion = JSONArray()
        val appliedPromotionArray = arrPromotionModel.filter { it.isApplied }

        if (appliedPromotionArray.isEmpty())
            return ""

        appliedPromotionArray.forEach {
            JSONObject().run {
                put("promotion_id", it.promotionId)
                put("promotion_type", it.offerType)
                put("promotion_value", it.offerValue)
                put("promotion_code", it.promoCode)
                put("discounted_amount", it.discountAmount)
            }.also {
                jarPromotion.put(it)
            }
        }

        return jarPromotion.toString()
    }

    fun setupUiForDiscountAndTotal(
        activity: Activity,
        totalDiscountPrice: Double,
        linDiscount: LinearLayout,
        txtApplyPromoCode: TextView,
        imgPromotion: ImageView,
        txtDiscountValueDialog: TextView,
        txtTotalPriceDialog: TextView,
        discountedPrice: Double,
        totalBeforePromoDiscount: Double, btnMakePayment: Button, fromCamp: Boolean = false,
        txtDiscountTitle: TextView? = null,
        arrPromotionModel: ArrayList<PromotionModel>? = null
    ) {
        if (totalDiscountPrice == 0.0) {
            linDiscount.visibility = View.GONE
            txtApplyPromoCode.textColor =
                ContextCompat.getColor(activity, R.color.colorAccent)
            imgPromotion.setColorFilter(
                ContextCompat.getColor(activity, R.color.colorAccent),
                PorterDuff.Mode.SRC_IN
            )
            txtApplyPromoCode.text = "Apply Promo Code"
        } else {
            txtApplyPromoCode.textColor =
                ContextCompat.getColor(activity, R.color.colorSuccess)
            imgPromotion.setColorFilter(
                ContextCompat.getColor(activity, R.color.colorSuccess),
                PorterDuff.Mode.SRC_IN
            )
            txtApplyPromoCode.text = "Promo applied successfully!!"
            linDiscount.visibility = View.VISIBLE
            txtDiscountValueDialog.text = "- AED $totalDiscountPrice"
        }

        if (totalDiscountPrice == 0.0)
            txtTotalPriceDialog.text = "AED ${ConvertToValidData.removeZeroAfterDecimal(totalBeforePromoDiscount.toString())}"
        else
            txtTotalPriceDialog.text = "AED ${ConvertToValidData.removeZeroAfterDecimal(discountedPrice.toString())}"


        if (discountedPrice == 0.0) {
            if (fromCamp)
                btnMakePayment.text = "Book Camp"
            else
                btnMakePayment.text = "Book Activity"
        } else {
            btnMakePayment.text = "Make Payment"
        }

        if (txtDiscountTitle != null && arrPromotionModel != null) {
            var strPromotion = ""
            arrPromotionModel.filter { it.isApplied }.forEach { strPromotion += it.promoCode + ", " }

            if (strPromotion.trim().length > 1) {
                strPromotion = strPromotion.trim().substring(0, strPromotion.trim().length - 1)
                txtDiscountTitle.text = "Promo - ($strPromotion)"
            }
        }
    }

    fun setPromotionClickListenerForListing(act: AppCompatActivity, model: PromotionModel) {
        if (model.catId == "null")
            model.catId = ""
        if (model.activityId == "null")
            model.activityId = ""
        if (model.campId == "null")
            model.campId = ""

        var intCampPromotionCount = 0
        try {
            intCampPromotionCount = model.campPromotionCount.toInt()
        } catch (e: Exception) {
        }

        if (/*model.campId != "" && model.campId != "null" && */intCampPromotionCount > 0) {
            // go to camp listing
            if (intCampPromotionCount == 1) {
                MyNavigations.goToCampDetail(act, model.onlyOneCampId)
            } else
                DashboardActivity.filterCampOnPromotion(act, model)

        } else if ((model.catId != "" && model.catId != "null") || (model.activityId != "" && model.activityId != "null")) {
            // go to act listing(explore)
            DashboardActivity.filterActivitiesOnPromotion(act, model)
        } else {
            // goto explore as default
            if (act is DashboardActivity) {
                DashboardActivity.showAllAct(act)
            }
        }

    }
}
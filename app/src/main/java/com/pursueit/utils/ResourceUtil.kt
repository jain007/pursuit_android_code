package com.pursueit.utils

import android.content.Context
import com.pursueit.R

object ResourceUtil
{
    fun getSessionsString(context: Context,numberOfSession:Int):String
    {
        return if (numberOfSession==0)
            "Unlimited"
        else
            context.resources.getQuantityString(R.plurals.session,numberOfSession,numberOfSession)
    }
}
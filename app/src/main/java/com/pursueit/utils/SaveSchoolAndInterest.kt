package com.pursueit.utils

import android.app.Activity
import android.util.Log
import com.androidnetworking.error.ANError
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.InterestModel
import com.pursueit.model.SchoolModel
import com.pursueit.model.SimpleModel
import io.reactivex.disposables.CompositeDisposable
import org.json.JSONObject

class SaveSchoolAndInterest(val act:Activity, val compositeDisposable : CompositeDisposable?=null) {

    private val getSharedPref=GetSetSharedPrefs(act)

    private val KEY_SCHOOL_ARRAY="schoolArray"
    private val KEY_INTEREST_ARRAY="interestArray"

    fun fetchAndSaveIntoSharedPref(showLoader:Boolean=false){
        if (ConnectionDetector.isConnectingToInternet(act)) {
            if (showLoader)
                DialogMsg.showPleaseWait(act)
            FastNetworking.makeRxCallPost(
                act,
                Urls.GET_SCHOOL_AND_INTEREST, true, HashMap(), "SCHOOL_INTEREST", object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        if (showLoader)
                            DialogMsg.dismissPleaseWait(act)
                        if (json != null) {
                            if (json.getBoolean("status")) {
                                val jobData=json.getJSONObject("data")


                                val arrSchoolModel=ArrayList<SimpleModel>()
                                val jarSchool=jobData.getJSONArray("schools")
                                repeat(jarSchool.length()){
                                    val job=jarSchool.getJSONObject(it)
                                    job.run {
                                        SimpleModel(getString("school_id"),getString("school_name"))
                                            .apply{
                                                arrSchoolModel.add(this)
                                            }
                                    }
                                }

                                val arrInterestModel=ArrayList<SimpleModel>()
                                val jarInterest=jobData.getJSONArray("categories")
                                repeat(jarInterest.length()){
                                    val job=jarInterest.getJSONObject(it)
                                    job.run {
                                        SimpleModel(getString("cat_id"),getString("cat_name"))
                                            .also {
                                                arrInterestModel.add(it)
                                            }
                                    }
                                }


                                saveSchoolAndInterestArray(arrSchoolModel,arrInterestModel)
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                        Log.e("fetchAndSave",""+error.errorBody)

                        if (showLoader)
                            DialogMsg.dismissPleaseWait(act)

                    }

                },compositeDisposable = compositeDisposable)
        } else {
            Log.e("fetchAndSave","No internet")
        }
    }

    fun saveSchoolAndInterestArray(arrSchoolModel:ArrayList<SimpleModel>, arrInterestModel:ArrayList<SimpleModel>){
        getSharedPref.setArrayModel(KEY_SCHOOL_ARRAY,arrSchoolModel)
        getSharedPref.setArrayModel(KEY_INTEREST_ARRAY,arrInterestModel)
    }

    fun getSchoolArray():ArrayList<SimpleModel>{
        return getSharedPref.getSimpleArrayModel(KEY_SCHOOL_ARRAY)
    }

    fun getInterestArray():ArrayList<SimpleModel>{
        return getSharedPref.getSimpleArrayModel(KEY_INTEREST_ARRAY)
    }

}
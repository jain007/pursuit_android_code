package com.pursueit.utils

import android.util.Log
import com.pursueit.model.AgeGroupModel
import com.pursueit.model.FilterDayModel
import com.pursueit.model.SortModel

object SearchAndFilterStaticData {
    var selectedDays = ""
    var gender = ""
    var sorting = ""
    var timeFrom = ""
    var timeTo = ""
    var actType = ""
    var priceFrom = ""
    var priceTo = ""
    var date = ""
    var premise = "" //1->Your premise, 2->Company Venue
    var selectedAgeGroupModel: AgeGroupModel? = null

    var arrFilterDaysModel = ArrayList<FilterDayModel>()

    var arrSortModel = ArrayList<SortModel>()

    var isFilterApplied: Boolean = false

    var isAgeFilterApplied: Boolean = false

    fun addFilterData(hashMap: HashMap<String, String>): HashMap<String, String> {
        hashMap["days"] = selectedDays
        hashMap["date"] = date
        hashMap["actType"] = actType
        if (priceFrom != MyAppConfig.startPriceForFilter || priceTo != MyAppConfig.maxPriceForFilter) {
            hashMap["priceFrom"] = priceFrom
            hashMap["priceTo"] = priceTo
        } else {
            hashMap["priceFrom"] = ""
            hashMap["priceTo"] = ""
        }
        if (timeFrom == "24")
            hashMap["timeTo"] = "23:59:59"
        hashMap["timeFrom"] = ChangeDateFormat.convertDate(timeFrom, "HH", "HH:mm:ss")
        if (timeTo == "24")
            hashMap["timeTo"] = "23:59:59"
        else
            hashMap["timeTo"] = ChangeDateFormat.convertDate(timeTo, "HH", "HH:mm:ss")
        hashMap["gender"] = gender
        if (sorting.trim() == "")
            hashMap["sorting"] = "1"
        else
            hashMap["sorting"] = sorting

        // uncomment this for complete filter dialog
        if (selectedAgeGroupModel != null) {
            hashMap["ageFrom"] = selectedAgeGroupModel?.groupAgeFrom ?: ""
            hashMap["ageTo"] = selectedAgeGroupModel?.groupAgeTo ?: ""
        } else {
            hashMap["ageFrom"] = ""
            hashMap["ageTo"] = ""
        }

        hashMap["yourPremise"] = premise

        return hashMap
    }

    fun areAllFiltersEmpty():Boolean{
        /*Log.d("Filters=>", "isAgeFilterApplied => ${!isAgeFilterApplied}")
        Log.d("Filters=>", "SelectedDays => $selectedDays")
        Log.d("Filters=>", "Gender => $gender")
        Log.d("Filters=>", "TimeFrom => $timeFrom")
        Log.d("Filters=>", "TimeTo => $timeTo")
        Log.d("Filters=>", "ActType => $actType")
        Log.d("Filters=>", "PriceFrom => $priceFrom")
        Log.d("Filters=>", "PriceTo => $priceTo")
        Log.d("Filters=>", "Date => $date")
        Log.d("Filters=>", "SelectedAgeModel => $selectedAgeGroupModel")
        Log.d("Filters=>", "FilterDaysModel => $arrFilterDaysModel")*/
        Log.d("SelectedDays",""+ selectedDays)
        return !isAgeFilterApplied && selectedDays.trim().isEmpty() && gender.trim().isEmpty() && premise.trim().isEmpty()
                && (timeFrom.trim().isEmpty() || timeFrom.trim()=="0") && (timeTo.trim().isEmpty() || timeTo.trim()=="24")
                && actType.trim().isEmpty() && (priceFrom.trim().isEmpty() || priceFrom.trim()=="0")
                && (priceTo.trim().isEmpty() || priceTo.trim()==MyAppConfig.maxPriceForFilter) && date.trim().isEmpty()
                && selectedAgeGroupModel==null && arrFilterDaysModel.none { it.status }

    }


}
package com.pursueit.utils

import android.app.Activity
import android.util.Log
import android.view.View
import com.androidnetworking.error.ANError
import com.pursueit.dialogs.DialogMsg
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import org.json.JSONObject

class SendPhoneOtp(val act:Activity) {

    val dialogMsg=DialogMsg()

    fun sendOtp(mobileNumber:String, getOtp:(String)->Unit){
        if (ConnectionDetector.isConnectingToInternet(act.applicationContext)) {
            val hashMap=HashMap<String,String>()
            hashMap.put("mobileNumber", "+971$mobileNumber")

            DialogMsg.showPleaseWait(act)
            FastNetworking.makeRxCallPost(
                act,
                Urls.SEND_OTP, true, hashMap, "SendPhoneOtp", object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                            DialogMsg.dismissPleaseWait(act)
                        if (json != null) {
                            if (json.getBoolean("status")) {
                                val jobData=json.getJSONObject("data")
                                val otp=jobData.getString("otp")
                                getOtp(otp)
                            }
                            else
                            {
                                dialogMsg.showErrorRetryDialog(act,"Alert",json.getString("message"),"Okay",
                                    View.OnClickListener { dialogMsg.dismissDialog(act) },isCancellable = false)
                            }
                        }
                    }

                    override fun onApiError(error: ANError) {
                            DialogMsg.dismissPleaseWait(act)
                        Log.e("fetchAndSave",""+error.errorBody)
                    }

                })
        } else {
            Log.e("sendOtp","No internet")
        }
    }
}
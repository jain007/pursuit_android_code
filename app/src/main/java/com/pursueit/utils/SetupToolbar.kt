package com.pursueit.utils

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.TextView
import com.pursueit.R
import com.pursueit.custom.CustomFont


object SetupToolbar {

    fun setToolbar(activity: AppCompatActivity, title: String, backIcon: Boolean,backIconDrawable:Int=R.drawable.ic_back_arrow) {

        val toolbar = activity.findViewById<Toolbar>(R.id.toolbar) as Toolbar
        setToolbar(activity, title, backIcon, toolbar,backIconDrawable)
    }


    fun setToolbar(activity: AppCompatActivity, title: String, backIcon: Boolean, toolbar: Toolbar,backIconDrawable:Int=R.drawable.ic_back_arrow) {
        activity.setSupportActionBar(toolbar)
        val actionBar = activity.supportActionBar


        if (backIcon) {
            actionBar?.setHomeButtonEnabled(true)
            actionBar?.setDisplayHomeAsUpEnabled(backIcon)
            actionBar?.setHomeAsUpIndicator(backIconDrawable)

           /* if (showCrossIcon) {
                actionBar?.setHomeButtonEnabled(true)
                actionBar?.setHomeAsUpIndicator(R.drawable.ic_close_my_app)
            }*/
            actionBar?.title=title.trim()

        }
        applyFontForToolbarTitle(activity, toolbar)


    }


    private fun applyFontForToolbarTitle(activity: Activity, toolbar: Toolbar) {
        for (i in 0 until toolbar.childCount) {
            val view = toolbar.getChildAt(i)
            if (view is TextView) {
                val titleFont = CustomFont.setFontRegular(activity.assets)
                if (view.text == toolbar.title) {
                    view.typeface = titleFont
                    view.textSize = 18f
                    break
                }
            }
        }
    }


}
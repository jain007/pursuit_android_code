package com.pursueit.utils

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.pursueit.R
import com.pursueit.adapters.AttendeeAdapter
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.Urls
import com.pursueit.model.CampModel
import kotlinx.android.synthetic.main.dialog_camp_payment_detail.*

class ShowCampBookingInfoDialog(val activity: AppCompatActivity, private val campModel: CampModel?)
{
    var dialogCampPayment: Dialog? = null

    fun setup() {

        if (dialogCampPayment == null)
            dialogCampPayment = MyDialog(activity).getMyDialog(R.layout.dialog_camp_payment_detail)

        if (campModel != null) {
            dialogCampPayment!!.txtCampTitleDialog.text = campModel!!.campName
            dialogCampPayment!!.txtLocationDialog.text = campModel!!.address
            dialogCampPayment!!.txtCampServiceProviderDialog.text = campModel!!.serviceProviderName
            dialogCampPayment!!.txtAgeRange.text = campModel!!.ageRageData
            dialogCampPayment!!.txtTimeRange.text = campModel!!.timeRangeData

            Glide.with(activity.applicationContext)
                .applyDefaultRequestOptions(MyGlideOptions.getRectangleRequestOptions())
                .load(Urls.IMAGE_FEATURE_CAMP + campModel!!.campImage)
                .into(dialogCampPayment!!.imgCampDialog)

        } else {
            return
        }

      //  dialogFlexiActivityPayment!!.rvAttendees.layoutManager = LinearLayoutManager(activity.applicationContext)

        val adapter = AttendeeAdapter(activity, MyProfileUtils.arrayFamilyModel, { pos, isChecked, adapter ->
        }, {

        })
     //   dialogFlexiActivityPayment!!.rvAttendees.adapter = adapter

        dialogCampPayment!!.imgClose.setOnClickListener {
            if (dialogCampPayment!!.isShowing)
                dialogCampPayment!!.dismiss()
        }

        dialogCampPayment!!.show()
    }
}
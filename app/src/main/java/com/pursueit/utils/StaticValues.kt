package com.pursueit.utils

import android.content.Context
import android.util.Log
import com.pursueit.model.CampModel
import com.pursueit.model.CategoryModel
import com.pursueit.model.PromotionModel
import com.pursueit.model.UserModel


object StaticValues {
    //My Preferences, intent, local keys
    val LOGIN_REGISTER_FLAG = "login_register_flag"

    val KEY_STATUS = "status"

    val KEY_AUTH = "Authorization"
    val KEY_ACCEPT = "Accept"

    //constants
    var AUTH_TOKEN = ""
    var Accept = ""

    var isExploreAlreadyClickedFirstTime =
        false //if false show the filter dialog of age first time automatically, if true don't show

    val BOOKING_STATUS_COMPLETED = "completed"
    val BOOKING_STATUS_CANCELLED = "cancelled"
    val BOOKING_STATUS_ONGOING = "on going"
    val BOOKING_STATUS_UPCOMING = "upcoming"

    val BOOKING_STATUS_DECLINED = "declined"
    val BOOKING_STATUS_AWAITING = "awaiting"

    val BOOKING_UNDER_PROCESS = "Under Process"

    //Just managing at app end, they are not using it at server end
    val CAMP_BOOKED = "Camp Booked"

    val arrayDashboardCategories = ArrayList<CategoryModel>()
    val arrayCatSubCatModel = ArrayList<CategoryModel>()

    //Key App Intro kept in local shared prefs
    val KEY_APP_INTRO_SHOWN = "AppIntroShown"


    //Heading: "Pass Bal" earlier, now "Wallet Balance"
    val PASS_WALLET_BALANCE = "Wallet Balance"
    fun getSuffixValidTill(userModel: UserModel): String {
        try {
            val userWalletBalance = userModel.currentPassWallet.toDouble()
            return if (userWalletBalance > 0.00) " (Valid till ${userModel.passExpiryDate})" else ""
        } catch (e: Exception) {
            return ""
        }
    }

    var PLAY_STORE_BASE_URL = "https://play.google.com/store/apps/details?id="

    fun reviveHeaders(context: Context) {
        if (AUTH_TOKEN.trim().isEmpty()) {
            val prefs = GetSetSharedPrefs(context)
            if (prefs.getData(KEY_AUTH).trim().isNotEmpty())
                AUTH_TOKEN = prefs.getData(KEY_AUTH).trim()
        }
    }

    fun putHeaders(context: Context, strAuthToken: String) {
        AUTH_TOKEN = strAuthToken
        val prefs = GetSetSharedPrefs(context)
        prefs.putData(KEY_AUTH, strAuthToken)
        Log.d("AuthTokenReceived", strAuthToken)
    }

    val arrDays =
        arrayOf("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday")

    var arrGlobalPromotion = ArrayList<PromotionModel>()
}
package com.pursueit.utils

import android.app.Dialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.androidnetworking.error.ANError
import com.pursueit.R
import com.pursueit.dialogs.DialogMsg
import com.pursueit.dialogs.MyDialog
import com.pursueit.httpCalls.FastNetworking
import com.pursueit.httpCalls.Urls
import com.pursueit.model.UserModel
import kotlinx.android.synthetic.main.dialog_enter_name.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject

object UserProfileUtils {

    fun showEnterPhoneDialog(act:AppCompatActivity, onSubmit:(dialog:Dialog, strPhone:String)-> Unit) {
        val dialog = MyDialog(act).getMyDialog(R.layout.dialog_enter_name)
        dialog.setCancelable(false)
        dialog.btnSaveName.setOnClickListener {
            val strPhone = dialog.etPhoneNumber.text.toString().trim()
            // if (MyValidations(this).checkMobile(strPhone)) {
            //dialog.dismiss()

            onSubmit(dialog,strPhone)



            //   } else {
            //       showError("Please enter a valid phone number to proceed")
            //   }
        }

        DialogMsg.animateViewDialog(dialog.linParentEnterName)

        dialog.show()
    }

    fun fastUpdatePhoneProfileUser(context:Context, isPhoneVerified:Boolean=false) {
        doAsync {
            val userModel = UserModel.getUserModel(context)
            val hashParams = HashMap<String, String>()
            hashParams["firstName"] = userModel.userFNAme.trim()
            hashParams["lastName"] = userModel.userLName.trim()
            hashParams["phoneNumber"] = userModel.userPhone.trim()

            hashParams["isVerified_mobile"]=if (isPhoneVerified) "1" else "0"

            FastNetworking.makeRxCallPostImageUpload(
                context,
                Urls.EDIT_USER_PROFILE,
                true,
                "profilePicture",
                null,
                hashParams,
                "MyProfileNameUpdate",
                object :
                    FastNetworking.OnApiResult {
                    override fun onApiSuccess(json: JSONObject?) {
                        try {
                            UserModel.setUserModel(context, userModel)
                        } catch (e: Exception) {

                        }
                    }

                    override fun onApiError(error: ANError) {

                    }
                })
        }
    }
}